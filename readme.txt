Aanmaken van de tabellen voor de CentralInstance


Installatie PostgreSQL op Linux:
=================================================================
sudo apt-get update
sudo apt-get install postgresql postgresql-contrib
sudo -i -u postgres
createdb sched20

Aanmaken Database (uitvoeren als Linux user postgres):
=================================================================
psql -d sched20
CREATE USER sched20 WITH PASSWORD 'Welkom01'; 
CREATE DATABASE CENTRALINSTANCE; 
GRANT ALL PRIVILEGES ON DATABASE CENTRALINSTANCE to sched20 ;

Aanmaken tabellen voor de CentralInstance Repository
=================================================================
log in als postgres (sudo su - postgres)
Ga naar de server/src/repository-directory van de uitgecheckte directory
psql -d sched20 -f catalog.sql



UITVOEREN CENTRALINSTANCE
=================================================================
In homedirectory: . ./setEnv
cd server/src/centralinstance
go run centralinstance.go


AANMAKEN AGENT
=================================================================
In homedirectory: . ./setEnv
cd server/src/worker
go run worker2.go -create -name=WRK_001 -url=localhost

OPVRAGEN STATUS AGENTS (Wanneer CentralInstance draait)
=================================================================
https://localhost:8080/worker/status
