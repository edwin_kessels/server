package validatie

import "strconv"
import "strings"
import "utils"
import "logging"
import "github.com/asaskevich/govalidator"
import "melding"
import "regexp"
import "repository"
import "naming"
import "constanten"

var prefix = "validatie: "

// Constanten om aan te geven om wat voor een soort object het gaat
var EmailAddress = 1
var FirstName = 2
var LastName = 3
var ObjectName = 4
var Description = 5
var NotNullObject = 6

var maxLenLastName = 64
var maxLenFirstName = 32
var maxLenObjectName = 64
var maxLenObjectDescription = 128

// Valide character die in een objectnaam gebruikt kunnen worden. Dit is dus bijvoorbeeld
// de naam van een Worker of Partition
var validCharaterSetObjectName = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz_1234567890"

var validCharaterSetNaam = "!@#$%^&*()_+=[]{}\\|\":;?/<>,0123456789"

var defaultMsg = "All checks were succesful"

var reservedObjectNames = "[SYSTEM][INTERNAL][SYS][GLOBAL]"

// **********************************************************************
// * Controlen van een naam (achternaam en voornaam) of deze
// * ongeldige karaketers (invalid_characters_naam) bevat
// **********************************************************************
func containsInvalidCharacters(stringToBeChecked string) bool {
	outVal := false

	for _, r := range stringToBeChecked {
		c := string(r)
		if strings.Contains(validCharaterSetNaam, c) == true {
			outVal = true
		}
	}

	return outVal
}

// **********************************************************************
// * Controleren van de achternaam
// * Hierbij worden de volgende controles uitgevoerd
// *  - Naam moet liggen tussen de 1 en 64 karakters
// *  - Karakters mogen niet voorkomen in de invalid_characters_naam
// **********************************************************************
func checkLastName(inLastName string) (int, string) {

	// Verwijderen van spaties
	lastName := utils.Trim(inLastName)

	logging.DevMSG(prefix + "Checking Last Name '" + lastName + "'")

	// Controleren of de naam uberhaupt gevuld is. Dit is een extra controle
	// om een zinvollere foutmelding te geven (achternaam is leeg in plaatst van te lang
	// of te kort)
	if lastName == "" {
		return 14, melding.Melding_usr_014
	}

	// Controleren op de maximale lengte van 64 karakter
	if len(lastName) > maxLenLastName {

		return 2, strings.Replace(melding.Melding_usr_002, "$1", strconv.Itoa(maxLenLastName), 1)
	}

	// Controleren of er geen ongeldige tekens in de naam zitten
	if containsInvalidCharacters(lastName) == true {
		return 3, melding.Melding_usr_003
	}

	return 0, defaultMsg
}

// **********************************************************************
// * Controleren van de voornaam
// * Hierbij worden de volgende controles uitgevoerd
// *  - Naam moet liggen tussen de 1 en 64 karakters
// *  - Karakters mogen niet voorkomen in de invalid_characters_naam
// **********************************************************************
func checkFirstName(inLastName string) (int, string) {

	// Verwijderen van spaties
	lastName := utils.Trim(inLastName)

	logging.DevMSG(prefix + "Checking First Name '" + lastName + "'")

	// Controleren of de naam uberhaupt gevuld is. Dit is een extra controle
	// om een zinvollere foutmelding te geven (achternaam is leeg in plaatst van te lang
	// of te kort)
	if lastName == "" {
		return 1, melding.Melding_usr_011
	}

	// Controleren op de maximale lengte van 64 karakter
	if len(lastName) > maxLenFirstName {
		return 12, strings.Replace(melding.Melding_usr_012, "$1", strconv.Itoa(maxLenFirstName), 1)
	}

	// Controleren of er geen ongeldige tekens in de naam zitten
	if containsInvalidCharacters(lastName) == true {
		return 3, melding.Melding_usr_013
	}

	return 0, defaultMsg
}

// **********************************************************************
// * Controleren van een object naam
// * Hierbij worden de volgende controles uitgevoerd
// *  - Naam moet liggen tussen de 1 en 64 karakters
// *  - Karakters moeten voorkomen in validCharaterSetObjectName
// **********************************************************************
func checkObjectName(inObjectName string) (int, string) {

	objectName := utils.Trim(inObjectName)

	logging.DevMSG(prefix + "Checking Object Name '" + objectName + "'")

	// Controleren of de Objectnaam niet leeg is
	if objectName == "" {
		return 1, melding.Melding_usr_001
	}

	// Controleren of er alleen maar geldige tekens zijn gebruikt
	if ValidObjectName(inObjectName) == false {
		return 2, melding.Melding_obj_002
	}

	// Controleren of de Object Naam niet te lang is
	if len(objectName) > maxLenObjectName {
		return 5, strings.Replace(melding.Melding_obj_005, "$1", strconv.Itoa(maxLenObjectName), 1)
	}

	// Controleren of er geen reserved words zijn gebruikt. Dit wordt
	// gecontroleerd aan de hand van de variable reservedObjectNames
	if strings.Contains(reservedObjectNames, objectName) == true {
		return 3, melding.Melding_obj_003
	}

	// Controleren of een Objectnaam niet met een cijfer begint
	tmp := inObjectName[0]
	if govalidator.IsNumeric(string(tmp)) == true {
		return 4, melding.Melding_obj_004
	}

	return 0, defaultMsg
}

// **********************************************************************
// * Controleren van een EmailAdres
// **********************************************************************
func checkEmailAddress(inEmailAddress string) (int, string) {

	emailAdres := utils.Trim(inEmailAddress)

	logging.DevMSG(prefix + "Checking EmailAddress '" + emailAdres + "'")

	if govalidator.IsEmail(emailAdres) == false {
		return 4, melding.Melding_usr_004 + " ('" + emailAdres + "')"
	}

	// Uitvoeren van een tweede controle
	Re := regexp.MustCompile(`^[a-z0-9._%+\-]+@[a-z0-9.\-]+\.[a-z]{2,4}$`)
	if Re.MatchString(emailAdres) == false {
		return 19, melding.Melding_usr_019
	}

	return 0, defaultMsg
}

// **********************************************************************
// * Controle of een naam niet leeg is (Not Null Check
// * Het description argument wordt meegegeven om een zinvolle
// * foutmelding te kunnen gevenn
// **********************************************************************
func CheckNotNull(inValue string, inDescription string) (int, string) {

	value := utils.Trim(inValue)

	logging.DevMSG(prefix + "Checking value for being Not Null '" + value + "'")

	if govalidator.IsNull(value) == true {
		return 7, strings.Replace(melding.Melding_obj_007, "$1", inDescription, 1)
	}

	return 0, defaultMsg
}

// *************************************************************************************
// Procedure waarbij een controle wordt uitgevoerd of een ObjectNaam (bijvoorbeeld de naam
// van een partitie), louter bestaat uit bepaalde geldige karakters
// *************************************************************************************
func ValidObjectName(objectname string) bool {

	outVal := true

	for _, r := range objectname {
		c := string(r)
		if strings.Contains(validCharaterSetObjectName, c) == false {
			outVal = false
		}
	}

	return outVal
}

// *************************************************************************************
// * Procedure waarbij de Description wordt gecontroleerd
// *************************************************************************************
func checkDescription(description string) (int, string) {

	// Controleren op de maximale lengte van 128 karakter
	if len(description) > maxLenObjectDescription {
		return 6, strings.Replace(melding.Melding_obj_006, "$1", strconv.Itoa(maxLenObjectDescription), 1)
	}

	return 0, "Description check succesful "
}

// **********************************************************************
// Methode waarbij de verplichte Partition en Application worden
// gecontroleerd. Beide worden als Id doorgegeven en er wordt ondermeer
// gecontroleerd of de zogenaamde containers niet verwijderd zijn en dat
// ze ook user-defined-objecten mogen bevatten
// **********************************************************************
func CheckContainers(partitionId int, applicationId int) (int, string) {

	partitionFound := false

	// Ophalen van de gegevens van de Partition.
	sqlCmd := repository.SqlCmd_par_008
	logging.InfoMSG(prefix + "getting " + naming.PartitionObjNameEV + " information by id (" + strconv.Itoa(partitionId) + ")")
	// Variabelen declareren die gelezen worden uit de tabel
	partition_name := ""
	partition_description := ""
	partition_default := ""
	partition_internal := ""
	partition_fqname := ""
	partition_public := ""
	rows, err := repository.DBConnection.Query(sqlCmd, partitionId, constanten.No)
	if err != nil {
		logging.ErrorMSG(prefix + "Failed to connect to repository: " + err.Error())
	}
	for rows.Next() {
		err := rows.Scan(&partition_name, &partition_description, &partition_default, &partition_internal, &partition_fqname, &partition_name, &partition_public)
		if err != nil {
			logging.DebugMSG(err.Error())
		} else {
			partitionFound = true

		}
	}

	if partitionFound == true {

		// Controleren of het niet de SYSTEM-partition is
		if partition_name == naming.SystemPartitionName {
			return 7, strings.Replace(melding.Melding_prt_007, "$1", partition_name, 1)
		}

		// Controleren of het geen Internal partition is
		if partition_internal == constanten.Yes {
			return 8, strings.Replace(melding.Melding_prt_008, "$1", partition_name, 1)
		}

	}

	// Controleren of de Partition is gevonden
	if partitionFound == false {
		return 6, melding.Melding_prt_006
	}

	// Controleren van de Application
	applicationFound := false

	sqlCmd = repository.SqlCmd_app_004
	logging.InfoMSG(prefix + "getting " + naming.ApplicationObjNameEV + " information by id (" + strconv.Itoa(applicationId) + ")")
	// Variabelen declareren die gelezen worden uit de tabel
	application_internal := ""
	application_name := ""

	rows, err = repository.DBConnection.Query(sqlCmd, applicationId, constanten.No)
	if err != nil {
		logging.ErrorMSG(prefix + "Failed to connect to repository: " + err.Error())
	}
	for rows.Next() {
		err := rows.Scan(&application_name, &application_internal)
		if err != nil {
			logging.DebugMSG(err.Error())
		} else {
			applicationFound = true
		}
	}

	// Controleren of het geen Internal Application is
	if applicationFound && application_internal == constanten.Yes {
		return 3, strings.Replace(melding.Melding_app_003, "$1", application_name, 1)
	}

	// Controleren of de applicatie is gevonden
	if applicationFound == false {
		return 2, melding.Melding_app_002
	}

	return 0, "All Container checks succesful"
}

// **********************************************************************
// Main functie voor het uitvoeren van de controles
// Vanuit deze functie worden de onderliggende controles aangeroep
// * Het resultaat wordt weer teruggeven aan de aanroepende functie
// **********************************************************************
func Check(varValue string, checkType int) (int, string) {

	if checkType == LastName {
		exitCode, exitMsg := checkLastName(varValue)
		return exitCode, exitMsg
	}

	if checkType == FirstName {
		exitCode, exitMsg := checkFirstName(varValue)
		return exitCode, exitMsg
	}

	if checkType == ObjectName {
		exitCode, exitMsg := checkObjectName(varValue)
		return exitCode, exitMsg
	}

	if checkType == EmailAddress {
		exitCode, exitMsg := checkEmailAddress(varValue)
		return exitCode, exitMsg
	}

	if checkType == Description {
		exitCode, exitMsg := checkDescription(varValue)
		return exitCode, exitMsg
	}

	return 0, defaultMsg
}
