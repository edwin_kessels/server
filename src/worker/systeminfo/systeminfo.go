package systeminfo

import "github.com/shirou/gopsutil/host"
import "github.com/shirou/gopsutil/load"
import "fmt"
import "strconv"
import "utils"
import "time"
import "constanten"
import "esb"
import "encoding/xml"
import "berichten"


var hostinfo  host.InfoStat
var loadinfo  load.AvgStat 



func LoadMonitor(workerId int) {
    for {
 
 
        loadinfo, _ := load.Avg()  
        
        var wrkMetrics berichten.WorkerMetrics
        wrkMetrics.WorkerId = workerId 
        wrkMetrics.Load1 = strconv.FormatFloat(loadinfo.Load1, 'g', 1, 64) 
        wrkMetrics.Load5 = strconv.FormatFloat(loadinfo.Load5, 'g', 1, 64) 
        wrkMetrics.Load15 = strconv.FormatFloat(loadinfo.Load15, 'g', 1, 64) 
        wrkMetrics.TimeStamp = utils.CurrentTimeStamp(constanten.DateFormatLong)
        xmlBericht, _ := xml.MarshalIndent(wrkMetrics, "", constanten.XMLIndent)
    	esb.SendMessageESBWithoutWait(string(xmlBericht), "WorkerMetrics")
    		
        time.Sleep(10 * time.Second)
    }
    
}


// *********************************************************************
// * GetLoadMetrics
// * Methode voor het opvragen van de actuele Load-metrics van de server
// * waarop de Worker draait. 
// *********************************************************************
func GetLoadMetrics() (string, string, string) {
    
     loadinfo, _ := load.Avg()  
     load1 := strconv.FormatFloat(loadinfo.Load1, 'g', 1, 64) 
     load5 := strconv.FormatFloat(loadinfo.Load5, 'g', 1, 64) 
     load15 := strconv.FormatFloat(loadinfo.Load15, 'g', 1, 64) 
     return load1, load5, load15
}


func GetHostProperties() berichten.WorkerConfiguration {
    
    hostinfo, _ := host.Info()
    
    var tmpCfg berichten.WorkerConfiguration 
    tmpCfg.HostName            		= hostinfo.Hostname
    tmpCfg.Platform            		= hostinfo.Platform
    tmpCfg.PlatformFamily      		= hostinfo.PlatformFamily
    tmpCfg.PlatformVersion     		= hostinfo.PlatformVersion
    tmpCfg.KernelVersion       		= hostinfo.KernelVersion 
    tmpCfg.VirtualizationSystem		= hostinfo.VirtualizationSystem 
    tmpCfg.VirtualizationRole  		= hostinfo.VirtualizationRole
    tmpCfg.HostID              		= hostinfo.HostID
    
    return tmpCfg
    
}

func main() {
    
   hostPlatform,hostFamily, hostVersion, _ := host.PlatformInformation()  
   fmt.Println("Platform=" + hostPlatform)
   fmt.Println("Family=" + hostFamily)
   fmt.Println("Version=" + hostVersion)
   
   st1, st2, _ := host.Virtualization() 
   fmt.Println("st1=" + st1)
   fmt.Println("st2=" + st2)
    
    hostinfo, _ := host.Info()
    fmt.Println("Hostname=" + hostinfo.Hostname)
    fmt.Println("OS=" + hostinfo.OS)
    fmt.Println("VirtualizationSystem=" + hostinfo.VirtualizationSystem )
    
    LoadMonitor(23) 
    
}

