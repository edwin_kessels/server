package main

import "esb"
import "logging"
import "versieinfo"
import "platform"
import "utils"
import "constanten"
import "time"
import "berichten"
import "encoding/xml"
import "settings"
import "flag"
import "os"
import "os/exec"
import "bytes"
import "strconv"
import "github.com/streadway/amqp"
import "io/ioutil"
import "strings"
import "worker/systeminfo"
import "centralinstance/jobtype"
import "statussen/jobstatus"

// Struct voor het opslaan van informatie over de Worker
// Deze struct vervangt in principe alle losse variabelen
type worker struct {
	Id              int64
	Name            string
	SoftwareVersion string
	SoftwareBuild   string
	SoftwareDate    string
	Platform        string
	StartTime       string
	HostName        string
	JobDirectory    string
	CfgDirectory    string
	Queue           string
}

var wrk worker

// *************************************************************************
// * checkWorkerQueue
// * Methode om een dummy bericht in de queue van de Worker te zetten om
// * op die manier eventueel de queue automatisch te laten aanmaken wanneer
// * deze verwijderd is
// *************************************************************************
func checkWorkerQueue() {

	logging.DebugMSG("Checking queue '" + wrk.Queue + "'")
	var workerSelfTest berichten.WorkerSelfTest
	workerSelfTest.TimeStamp = utils.CurrentTimeStamp(constanten.DateFormatLong)
	xmlBericht, _ := xml.MarshalIndent(workerSelfTest, "", "\t")
	esb.SendMessageESBWithoutWait(string(xmlBericht), wrk.Queue)
}

// *************************************************************************
// * listener
// * Methode die luistert naar de sepcifieke queue voor deze Worker. Via deze
// * queue kunnen allerlei soorten requests binnenkomen:
// *   - WorkerConfigurationRequest (verstuurd door CentralInstance)
// *   - JobRequest
// * Voor elk verzoek wordt de desbetreffende Handler aangeroepen
// *************************************************************************
func listener() {

	logging.InfoMSG("Listening on queue '" + wrk.Queue + "'")
	conn, err := amqp.Dial(esb.ConnectString)
	utils.FailOnError(err, "Failed to connect to RabbitMQ")
	defer conn.Close()

	ch, err := conn.Channel()
	utils.FailOnError(err, "Failed to open a channel")
	defer ch.Close()

	//	q, err := ch.QueueDeclare(wrk.Queue, false, false, false, false, nil)
	//	utils.FailOnError(err, "Failed to declare a queue")

	msgs, err := ch.Consume(wrk.Queue, "", true, false, false, false, nil)
	utils.FailOnError(err, "Failed to register a consumer")

	forever := make(chan bool)

	go func() {
		for d := range msgs {

			action := "UNKNOWN"

			if strings.Contains(string(d.Body), "WorkerConfiguration") == true {
				action = "WORKERCONFIGURATIONREQUEST"
			}
			if strings.Contains(string(d.Body), "JobRequest") == true {
				action = "JOBREQUEST"
			}

			logging.DebugMSG("Execute action for " + action)

			// Versturen van de actuele Configuration van de Worker
			if action == "WORKERCONFIGURATIONREQUEST" {
				sendWorkerConfiguration()
				action = "UNKNOWN"
			}

			// Afhandelen van een JobRequest
			if action == "JOBREQUEST" {
				var tmpXML berichten.JobRequest
				xml.Unmarshal(d.Body, &tmpXML)
				preProcesJobRequest(tmpXML)
				action = "UNKNOWN"
			}

		}
	}()

	<-forever

}

// *************************************************************************
// * preProcesJobRequest
// * Methode om het uitvoeren van eeen Job voor te bereiden. Dit bestaat
// * uit de volgende stappen:
// *
// * - Aanmaken van de JobDirectory
// * - Wegschrijven van de SourceCode
// *************************************************************************
func preProcesJobRequest(jobRequest berichten.JobRequest) {

	// Aanmaken van de JobDirectory
	jobRequest.JobDirectory = wrk.JobDirectory + string(os.PathSeparator) + jobRequest.JobId
	logging.DebugMSG("Create Job Directory " + jobRequest.JobDirectory)
	err := os.MkdirAll(jobRequest.JobDirectory, 0755)
	if err != nil {
		logging.DebugMSG("An error occurred during the creation of directory '" + jobRequest.JobDirectory + "'")
		logging.DebugMSG("Cannot create the JobDirectory")
	}

	filenameRunFile := ""
	if jobRequest.JobType == jobtype.Linux_sh {
		filenameRunFile = wrk.JobDirectory + string(os.PathSeparator) + jobRequest.JobId + string(os.PathSeparator) + "run.sh"
	}

	// Wegschrijven van de SourceCode naar de RunFile
	logging.DebugMSG("create Runfile " + filenameRunFile + " for job " + jobRequest.JobId)
	err = ioutil.WriteFile(filenameRunFile, []byte(string(jobRequest.SourceCode)), 0600)
	if err != nil {
		logging.ErrorMSG("Cannot write Run File")
		os.Exit(1)
	}

	// Starten van de RunFile
	if jobRequest.JobType == jobtype.Linux_sh {
		go procesJobRequestLinuxSH(jobRequest)
	}

}

// *************************************************************************
// * procesJobRequestLinuxSH
// * Uitvoeren van een Linux SH Job
// *************************************************************************
func procesJobRequestLinuxSH(jobRequest berichten.JobRequest) {

	logging.DebugMSG("Execute Linux SH Job with JobId " + jobRequest.JobId)
	logging.DebugMSG("Change to Working Directory to " + jobRequest.JobDirectory)
	os.Chdir(jobRequest.JobDirectory)

	// Uitvoeren van de run.sh
	cmd := exec.Command("sh", "run.sh")

	// Use a bytes.Buffer to get the output and error
	var bufOut bytes.Buffer
	cmd.Stdout = &bufOut
	var bufErr bytes.Buffer
	cmd.Stderr = &bufErr

	// Vastleggen van de StartTijd van de Job
	runStart := time.Now()

	jobLog := "Job started at " + runStart.Format("2006-01-02 15:04:05") + " (local time on Worker server)\n"

	// Versturen van een JobStartMessage dat de job is gestart
	var tmpXML berichten.JobStatusChange
	tmpXML.WorkerId = wrk.Id
	tmpXML.JobId = jobRequest.JobId
	tmpXML.Status = jobstatus.Running
	xmlBericht, _ := xml.MarshalIndent(tmpXML, "", "\t")
	go esb.SendMessageESBWithoutWait(string(xmlBericht), settings.ESB_QUEUE_JobStatus)

	type Status interface {
		ExitStatus() int
	}

	cmd.Start()
	cmd.Wait()

	status1 := cmd.ProcessState.Sys().(Status).ExitStatus()
	//fmt.Printf("Cmd %v exited with status %v\n", cmd.Args, status1)

	//fmt.Println("OUT:", bufOut.String())
	//fmt.Println("ERR:", bufErr.String())

	// Registratie van de Job verwijderen
	//delete (JobsRunning, newJob.JobId)
	//delete (JobsKilling, newJob.JobId)

	elapsed := time.Since(runStart)

	runEnd := time.Now()

	jobLog = jobLog + "Job ended at " + runEnd.Format("2006-01-02 15:04:05") + " (local time on Worker server)\n"
	jobLog = jobLog + "Elapsed time " + strconv.FormatFloat(elapsed.Seconds(), 'g', -1, 64) + " seconds\n"
	jobLog = jobLog + "Exit Code " + strconv.Itoa(status1) + "\n"
	jobLog = jobLog + "Size stdOut.log " + strconv.Itoa(len(bufOut.String())) + "\n"
	jobLog = jobLog + "Size stdErr.log " + strconv.Itoa(len(bufErr.String())) + "\n"

	// Wegschrijven van de stdout.log
	filenameStdOut := jobRequest.JobDirectory + string(os.PathSeparator) + "stdout.log"
	logging.DebugMSG("Writing stdout to " + filenameStdOut)
	err := ioutil.WriteFile(filenameStdOut, []byte(bufOut.String()), 0600)
	if err != nil {
		logging.ErrorMSG("Cannot create stdout: " + filenameStdOut)
	}

	// Wegschrijven van de stderr.log
	filenameStdErr := jobRequest.JobDirectory + string(os.PathSeparator) + "stderr.log"
	logging.DebugMSG("Writing stderr to " + filenameStdErr)
	err = ioutil.WriteFile(filenameStdErr, []byte(bufErr.String()), 0600)
	if err != nil {
		logging.ErrorMSG("Cannot create stderr: " + filenameStdErr)
	}

	tmpStatus := jobstatus.Completed
	if status1 != 0 {
		tmpStatus = jobstatus.Error
	}

	// Bericht samenstellen met informatie over de afgeronde Job
	var jobStatusCompletion berichten.JobStatusCompletion
	jobStatusCompletion.JobId = jobRequest.JobId
	jobStatusCompletion.WorkerId = wrk.Id
	jobStatusCompletion.ExitCode = strconv.Itoa(status1)
	jobStatusCompletion.ExecutionLog = jobLog
	jobStatusCompletion.Status = tmpStatus
	jobStatusCompletion.ElapsedTime = strconv.FormatFloat(elapsed.Seconds(), 'g', -1, 64)
	jobStatusCompletion.OutFileSize = strconv.Itoa(len(bufOut.String()))
	jobStatusCompletion.OutFileName = filenameStdOut
	jobStatusCompletion.ErrFileSize = strconv.Itoa(len(bufErr.String()))
	jobStatusCompletion.ErrFileName = filenameStdErr
	xmlBericht, _ = xml.MarshalIndent(jobStatusCompletion, "", "\t")
	esb.SendMessageESBWithoutWait(string(xmlBericht), settings.ESB_QUEUE_JobStatus)

	// Versturen van de stdout.log wanneer deze gevuld is
	if len(bufOut.String()) > 0 {
		var tmpJobFileLog berichten.JobFileTransfer
		tmpJobFileLog.JobId = jobRequest.JobId
		tmpJobFileLog.JobFileType = constanten.JobFile_StdOut
		tmpJobFileLog.JobFileName = "stdout.log"
		tmpJobFileLog.JobFileContent = bufOut.String()
		xmlBericht, _ = xml.MarshalIndent(tmpJobFileLog, "", "\t")
		esb.SendMessageESBWithoutWait(string(xmlBericht), settings.ESB_QUEUE_JobStatus)
	}

	// Versturen van de stderr.log wanneer deze gevuld is
	if len(bufErr.String()) > 0 {
		var tmpJobFileErr berichten.JobFileTransfer
		tmpJobFileErr.JobId = jobRequest.JobId
		tmpJobFileErr.JobFileType = constanten.JobFile_StdErr
		tmpJobFileErr.JobFileName = "stderr.log"
		tmpJobFileErr.JobFileContent = bufErr.String()
		xmlBericht, _ = xml.MarshalIndent(tmpJobFileErr, "", "\t")
		esb.SendMessageESBWithoutWait(string(xmlBericht), settings.ESB_QUEUE_JobStatus)
	}

}

// *************************************************************************
// * sendLocalWorkerStartUp
// * Versturen van het LocalWorkerStartUp bericht naar de CentralInstance
// *************************************************************************
//func sendLocalWorkerStartUp() {
//
//    // Samenstellen van het bericht
//    var tmpXML berichten.LocalWorkerStartUp
//    tmpXML.WorkerId	        = wrk.Id
//	tmpXML.SoftwareVersion	= wrk.SoftwareVersion
//	tmpXML.SoftwareBuild	= wrk.SoftwareBuild
//	tmpXML.SoftwareDate     = wrk.SoftwareDate
//	tmpXML.Platform         = wrk.Platform
//	tmpXML.StartTime        = utils.CurrentTimeStamp(constanten.DateFormatLong)
//	tmpXML.TimeStamp        = utils.CurrentTimeStamp(constanten.DateFormatShort)
//	tmpXML.CheckSum         = berichten.GetCheckSumLocalWorkerStartUp(tmpXML)
//	tmpXML.HostName         = wrk.HostName
//	xmlBericht, _ := xml.MarshalIndent(tmpXML, "", constanten.XMLIndent)
//	esb.SendMessageESBWithoutWait(string(xmlBericht), settings.ESB_OUT_Queue_WorkerStartup)   //TODO in een variabele zetten
//}

// *************************************************************************
// * executeTestJobLinux
// * Methode voor het uitvoeren van een TestJob in Linux
// *************************************************************************
func executeTestJobLinux() {

	jobSrcFileName := "ls"
	jobLog := ""

	cmd := exec.Command(jobSrcFileName)

	// Use a bytes.Buffer to get the output and error
	var bufOut bytes.Buffer
	cmd.Stdout = &bufOut
	var bufErr bytes.Buffer
	cmd.Stderr = &bufErr

	type Status interface {
		ExitStatus() int
	}

	cmd.Start()
	cmd.Wait()
	endStatus := cmd.ProcessState.Sys().(Status).ExitStatus()

	jobLog = jobLog + "Exit Code " + strconv.Itoa(endStatus) + "\n"
	jobLog = jobLog + "******* stdOut.log **********" + "\n"
	jobLog = jobLog + bufOut.String() + "\n"
	jobLog = jobLog + "********** end *** **********" + "\n"
	jobLog = jobLog + "\n"
	jobLog = jobLog + "******* stdErr.log **********" + "\n"
	jobLog = jobLog + bufErr.String() + "\n"
	jobLog = jobLog + "********** end *** **********" + "\n"
	logging.ScreenMSG(jobLog)
}

// *************************************************************************
// * heartbeatAgent
// * Methode die om ingestelde intervallen WorkerHeartBeat-berichten verstuurd
// * naar de CentralInstance. Omdat dit een frequent bericht is, wordt ook
// * loadPerformance data van de server waarop de Worker draait, meegestuurd
// *************************************************************************
func heartbeatAgent() {

	var heartbeatXML berichten.WorkerHeartBeat

	logging.InfoMSG("Starting background process to send Heartbeat to HUB")
	for {
		load1 := "-1"
		load5 := "-1"
		load15 := "-1"

		// Opvragen van de Load-metrics van de server waarop de Worker graait
		if settings.CollectWorkerMetrics == constanten.Yes {
			load1, load5, load15 = systeminfo.GetLoadMetrics()
		}

		// Samenstellen van het Heartbeat-bericht
		// Dit moet eerdere keer opnieuw worden opgebouwd omdat er een TimeStamp
		// in zit verwerkt
		heartbeatXML.WorkerId = wrk.Id
		heartbeatXML.TimeStamp = utils.CurrentTimeStamp(constanten.DateFormatShort)
		heartbeatXML.CheckSum = berichten.GetCheckSumWorkerHeartBeat(heartbeatXML)
		heartbeatXML.Load1 = load1
		heartbeatXML.Load5 = load5
		heartbeatXML.Load15 = load15
		xmlBericht, _ := xml.MarshalIndent(heartbeatXML, "", constanten.XMLIndent)
		esb.SendMessageESBWithoutWait(string(xmlBericht), settings.ESB_INNER_QUEUE_HeartBeat)

		// Nadat het bericht is verstuurd, kan er 20 seconden worden gewacht
		// voordat er een nieuw bericht wordt verstuurd
		time.Sleep(20 * time.Second)
	}

}

// *************************************************************************
// * generateWorkerId
// * Methode om een unieke WorkerId te genereren die als sleutel wordt gebruikt
// * De Workerid bestaat uit een md5-hash van de volgende informatie
// *     - Actueel Timestamp
// *     - Hostname van de server waarop de Worker draait
// *************************************************************************
func generateWorkerId() string {
	return utils.GetMD5Hash(utils.CurrentTimeStamp(constanten.DateFormatShort) + wrk.HostName)
}

// *************************************************************************
// * sendWorkerConfiguration
// * Bericht om de actuele Worker Configuration te versturen naar de
// * CentralInstance
// *************************************************************************
func sendWorkerConfiguration() {

	var workerCfg berichten.WorkerConfiguration

	// Ophalen van de eigenschappen van de Worker
	var tmpCfg berichten.WorkerConfiguration
	tmpCfg = systeminfo.GetHostProperties()
	workerCfg.HostName = tmpCfg.HostName
	workerCfg.Platform = tmpCfg.Platform
	workerCfg.PlatformFamily = tmpCfg.PlatformFamily
	workerCfg.PlatformVersion = tmpCfg.PlatformVersion
	workerCfg.KernelVersion = tmpCfg.KernelVersion
	workerCfg.VirtualizationSystem = tmpCfg.VirtualizationSystem
	workerCfg.VirtualizationRole = tmpCfg.VirtualizationRole
	workerCfg.HostID = tmpCfg.HostID
	workerCfg.WorkerId = wrk.Id
	workerCfg.StartupTimestamp = wrk.StartTime
	workerCfg.SoftwareVersion = wrk.SoftwareVersion
	workerCfg.SoftwareDate = wrk.SoftwareDate
	workerCfg.JobDirectory = wrk.JobDirectory
	workerCfg.TimeStamp = utils.CurrentTimeStamp(constanten.DateFormatShort)
	workerCfg.CheckSum = berichten.GetCheckSumWorkerConfiguration(workerCfg)
	xmlBericht, _ := xml.MarshalIndent(workerCfg, "", constanten.XMLIndent)
	esb.SendMessageESBWithoutWait(string(xmlBericht), settings.ESB_OUT_Queue_WorkerConfiguration)
}

// *************************************************************************
// * waitForWorkerRegistration
// * Methode die wacht op een reply van de CentralInstance op een createNewWorker
// * aanvraag. Wanneer het Reply-bericht ontvangen is, kan het configuratiebestand
// * voor de Worker worden aangemaakt en de Worker actief worden gemaakt
func waitForWorkerRegistration(newWorkerCreation berichten.NewWorkerCreation) {
	logging.ScreenMSG("Waiting for reply from CentralInstance")

	queueName := newWorkerCreation.RegistrationId

	conn, err := amqp.Dial(esb.ConnectString)
	utils.FailOnError(err, "Failed to connect to RabbitMQ")
	defer conn.Close()

	ch, err := conn.Channel()
	utils.FailOnError(err, "Failed to open a channel")
	defer ch.Close()

	q, err := ch.QueueDeclare(queueName, false, false, false, false, nil)
	utils.FailOnError(err, "Failed to declare a queue")

	msgs, err := ch.Consume(q.Name, "", true, false, false, false, nil)
	utils.FailOnError(err, "Failed to register a consumer")

	//	forever := make(chan bool)

	//go func() {
	for d := range msgs {

		// Converteren van het bericht naar een Object
		var tmpXML berichten.NewWorkerCreationReply
		xml.Unmarshal(d.Body, &tmpXML)
		logging.DebugMSG(string(d.Body))
		logging.DebugMSG("Registration Complete")

		// Wanneer de ExitCode niet gelijk is aan 0, dan moet de foutmelding worden
		// getoond en ook de Worker worden afgesloten
		if tmpXML.ExitCode != 0 {
			// Er is een fout opgetreden tijdens het aanmaken van de Worker
			logging.ErrorMSG("Error during the creation of the worker")
			logging.ErrorMSG(strconv.Itoa(tmpXML.ExitCode) + ": " + tmpXML.ExitMsg)
			os.Exit(1)
		}

		wrk.Id = tmpXML.WorkerId
		wrk.Name = newWorkerCreation.Name
		wrk.Queue = tmpXML.WorkerQueue
		logging.DebugMSG("Worker was created succesfully")
		return

	}
	//	}()

	//	<-forever

}

// *************************************************************************
// * createNewWorker
// * Methode waarmee op de CommandLine een nieuwe Worker kan worden aangemaakt
// * Op basis van de ingevoerde gegevens wordt een bericht naar de
// * CentralInstance gestuurd. Op de Worker wacht op een reponse van de
// * CentralInstance
// *************************************************************************
func createNewWorker(workerName string, workerDescription string, workerURL string, workerJobDirectory string) {

	// Controleren of de verplichte parameters workerName en workerURL
	if utils.Trim(workerName) == "" {
		logging.ErrorMSG("Worker is mandatory and must be specified with -name flag")
		os.Exit(1)
	}

	// Controleren of de verplichte parameters workerName en workerURL
	if utils.Trim(workerURL) == "" {
		logging.ErrorMSG("The URL of the Enterprise Service Bus is mandatory and must be specified with -url flag")
		os.Exit(1)
	}

	// Er moet nu een bericht wordt gemaakt waarin de details van de nieuwe Worker staan. Deze moeten naar de
	// CentralInstance wordt gestuurd.
	var newWorkerCreation berichten.NewWorkerCreation
	newWorkerCreation.RegistrationId = generateWorkerId()
	newWorkerCreation.Name = workerName
	newWorkerCreation.URL = workerURL
	newWorkerCreation.Description = workerDescription
	newWorkerCreation.HostName = utils.GetHostName()
	newWorkerCreation.Platform = wrk.Platform
	xmlBericht, _ := xml.MarshalIndent(newWorkerCreation, "", constanten.XMLIndent)
	esb.SendMessageESBWithoutWait(string(xmlBericht), settings.ESB_IN_Queue_NewWorkerRegistration)

	waitForWorkerRegistration(newWorkerCreation)

	// Worker is geregistreerd en de Configuratie file kan worden weggeschreven
	// Er moet eerst gecontroleerd worden of de directory wel bestaat en schrijfbaar is,
	//want anders moet deze worden aangemaakt
	cfgFileName := wrk.CfgDirectory + string(os.PathSeparator) + "worker.cfg"
	if utils.IsWritableDirectory(wrk.CfgDirectory) == false {
		// Directory kan niet worden aangemaakt of is niet schrijfbaar
		logging.ErrorMSG("Cannot write configuration file to " + cfgFileName)
		os.Exit(1)
	}

	confBericht, _ := xml.MarshalIndent(wrk, "", constanten.XMLIndent)
	err := ioutil.WriteFile(cfgFileName, confBericht, 0644)
	if err != nil {
		logging.ErrorMSG("Cannot write configuration file")
		os.Exit(1)
	}

	logging.DebugMSG("MoveOn")
}

// **************************************************************************************
// * getHomeDirectory
// * Methode om de Home directory te bepalen op basis van de bin-directory waarin de
// * Worker draait. De Home-directory is de basis voor de cfg en job directory
// **************************************************************************************
func getHomeDirectory() string {
	workingDirectory, _ := os.Getwd()
	subDirs := strings.Split(workingDirectory, string(os.PathSeparator))
	homeDirectory := subDirs[0]
	for i := 1; i < len(subDirs)-1; i++ {
		homeDirectory = homeDirectory + string(os.PathSeparator) + subDirs[i]
	}
	return homeDirectory
}

func main() {

	logging.InfoMSG("Starting Worker")

	// Initialiseren van variabelen
	wrk.SoftwareVersion = versieinfo.WorkerVersion
	wrk.SoftwareBuild = versieinfo.WorkerBuild
	wrk.SoftwareDate = versieinfo.WorkerDatum
	wrk.Platform = platform.GetPlatform()
	wrk.HostName = utils.GetHostName()
	wrk.StartTime = utils.CurrentTimeStamp(constanten.DateFormatLong)

	homeDirectory := getHomeDirectory()
	wrk.JobDirectory = homeDirectory + string(os.PathSeparator) + "job"
	wrk.CfgDirectory = homeDirectory + string(os.PathSeparator) + "cfg"

	logging.DebugMSG("HomeDirectory=" + homeDirectory)
	logging.DebugMSG("JobDirectory=" + wrk.JobDirectory)
	logging.DebugMSG("CfgDirectory=" + wrk.CfgDirectory)

	// Bepalen of er Commandline-argumenten zijn meegegeven bij het opstarten van de Worker
	cmdArgInTestJob := flag.Bool("testjob", false, "Execute TestJob")
	cmdArgInCreateWorker := flag.Bool("create", false, "create Worker")
	cmdArgInWorkerName := flag.String("name", "", "Name of the Worker")
	cmdArgInWorkerDesc := flag.String("desc", "", "Description of the Worker")
	cmdArgInURL := flag.String("url", "", "URL of the Enterprise Service Bus")
	cmdArgInJobDirectory := flag.String("jobdir", "", "Directory where temporary Job information is stored")
	flag.Parse()
	if *cmdArgInTestJob == true {
		logging.ScreenMSG("Execute Test Job")
		if wrk.Platform == platform.Linux {
			executeTestJobLinux()
		}

		// Afsluiten van de Worker
		os.Exit(0)
	}

	if *cmdArgInCreateWorker == true {
		// Aanmaken van een nieuwe Worker op de commandline
		createNewWorker(*cmdArgInWorkerName, *cmdArgInWorkerDesc, *cmdArgInURL, *cmdArgInJobDirectory)

		// Nadat het bericht is verstuurd naar de CentralInstance, moet er gewacht worden op een bericht van de
		// CentralInstance. Hiervoor wordt nieuwe queue aangemaakt, met dezelfde naam als de registrationId
	}

	// Inlezen van de Configuratie File
	logging.DebugMSG("Reading Worker's configuration")
	cfgFileName := wrk.CfgDirectory + string(os.PathSeparator) + "worker.cfg"

	// Controleren of de CFG-Directory bestaat en schrijfbaar is
	if utils.IsWritableDirectory(wrk.CfgDirectory) == false {
		logging.ErrorMSG("Configuration directory (" + wrk.CfgDirectory + ") cannot be created or is not writable")
		os.Exit(1)
	}

	// Controleren of de Job-Directory bestaat en schrijfbaar is
	if utils.IsWritableDirectory(wrk.JobDirectory) == false {
		logging.ErrorMSG("Job directory (" + wrk.JobDirectory + ") cannot be created or is not writable")
		os.Exit(1)
	}

	logging.DebugMSG("Reading Worker Configuration from file " + cfgFileName)
	tmp, err := ioutil.ReadFile(cfgFileName)
	if err != nil {
		logging.ErrorMSG("Worker's Configuration File (" + cfgFileName + ") is missing")
		logging.ErrorMSG("If this Worker is already configured, you can download the Configuration file from the CentralInstance")
		logging.ErrorMSG("and save this file (worker.cfg) in the directory " + wrk.CfgDirectory)
		os.Exit(2)
	}
	xml.Unmarshal(tmp, &wrk)
	logging.ScreenMSG("Starting Worker " + wrk.Name)

	// Starten van de Heartbeat-agent
	go heartbeatAgent()

	// Controleren en eventueel opnieuw aanmaken van de Worker
	// Deze actie wordt synchroon uitgevoerd (er wordt gewacht tot dat deze is afgerond)
	checkWorkerQueue()

	// Starten van de listener die luistert naar berichten die specifiek voor de
	// Worker zijn bedoeld. De Queuenaam is opgeslagen in het Configuratiebestand.
	// De queuename is vastgelegd door de CentralInstance bij het aanmaken van de Worker
	go listener()

	// Versturen van LocalWorkerStartUp
	//sendLocalWorkerStartUp()

	// Versturen van de actuele configuratie
	sendWorkerConfiguration()

	time.Sleep(3600 * time.Second)

}
