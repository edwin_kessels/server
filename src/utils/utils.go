package utils

import "os"
import "net"
import "strings"
import "logging"
import "crypto/md5"
import "encoding/hex"
import "time"
import "html"
import "crypto/rand"
import "net/http"
import "encoding/base64"
import "settings"
import "fmt"
import "constanten"
import "io/ioutil"

//TODO: Central Instance Connectstring is hardcoded opgenomen
var ESBConnectString = "amqp://jbukjugz:9_FCD3K_7J0mCa-6mOGdInrTQTVzfnAD@owl.rmq.cloudamqp.com/jbukjugz"

// In de berichten wordt ook een CheckSum gebruikt voor controle van de 'echtheid' van
// het bericht. Wanneer een bericht wordt aangemaakt, moet hier tijdelijk een placeholder
// voor worden opgenomen. Nadat de bericht compleet is, wordt de Checksum bepaald en wordt
// de placeholder vervnagen door deze berekende waarde
var XMLPlaceHolderChecksum = "[[%%CheckSum%%]]"
var xmlChecksumStartTag = "<CheckSum>"
var xmlChecksumEindeTag = "</CheckSum>"

// Variabele die als zout fungeert bij het bepalen van de Checksum van een bericht
// Hiervoor wordt de variabele maxLenghtMessage gebruikt, om het moeilijker te maken
// om via reserve eniginering acter de code te komen.
var maxLenghtMessage = 33354232

// Voor diverse doeleinden wordt een scheidingsteken gebruikt. Met behulp van de variabele scheidingsteken kan dit teken
// eenmalig worden gebruikt. Dit wordt ondermeer gebruikt om Workers en Hubs te scheiden in geheugen lijsten welke wel
// en welke niet actief zijn
var Scheidingsteken = "|"

// Generieke function om te bepalen of een bestand / directory bestaat
func FileExists(name string) bool {
	if _, err := os.Stat(name); err != nil {
		if os.IsNotExist(err) {
			return false
		}
	}
	return true
}

// Procedure die een hostname:portnumber string scheidt en als twee afzonderlijke
// parameters worden geretourneerd
func SplitHostnamePortnumber(address string) (string, string) {
	outHostname := ""
	outPortnumber := ""
	outHostname, outPortnumber, _ = net.SplitHostPort(address)
	return outHostname, outPortnumber
}


// *************************************************************************************
// * FormatDate()
// * Methode om eventueel de datum aan te passen wanneer MagicDate TRUE is
// *************************************************************************************
func FormatDate(inDate string) string {

  var outDate string = inDate

	if settings.MagicDates == true {
		// Bepalen van de datum van vandaag
		dateToday := time.Now()
		var dateTodayFormatted string  = dateToday.Format(constanten.DateFormatDate)
		outDate = strings.Replace(outDate, dateTodayFormatted, "", -1)

  		dateYesterday := time.Now().AddDate(0, 0, -1)
		var dateYesterdayFormatted string  = dateYesterday.Format(constanten.DateFormatDate)
		outDate = strings.Replace(outDate, dateYesterdayFormatted, "Yesterday ", -1)
	}
	
  return outDate
}



// *************************************************************************************
// Procedure waarbij een controle wordt uitgevoerd of een ObjectNaam (bijvoorbeeld de naam
// van een partitie), louter bestaat uit bepaalde geldige karakters
// *************************************************************************************
//func ValidObjectName(objectname string) bool {
//
//	outVal := true
//
//	for _, r := range objectname {
//        c := string(r)
//        if strings.Contains(constanten.ValidCharaterSetObjectName, c) == false {
//          outVal = false
//        }
//    }
//
//	return outVal
//}

// *************************************************************************************
// validCheckSumMessage
// *************************************************************************************
// Functie om te controleren of de Checksum die in het bericht is opgenomen,
// valide is en in overeenstemming met de inhoud van het bericht
// Om dit te bepalen wordt de ingesloten Checksum veliggesteld en weer
// vervangen door de PlaceHolder van het bericht en de checksum van het bericht
// opnieuw te bepalen
func ValidCheckSumMessage(inMessage string, validate bool) bool {

	if validate == false {
		return true
	}

	outVal := false

	startPos := strings.Index(inMessage, xmlChecksumStartTag)
	endPos := strings.Index(inMessage, xmlChecksumEindeTag)
	logging.DebugMSG("inMessage=" + inMessage)
	logging.DebugMSG("startPos=" + string(startPos))
	logging.DebugMSG("endPos=" + string(endPos))
	tmp := inMessage[startPos:endPos]
	checksum := strings.Replace(tmp, xmlChecksumStartTag, "", 1)

	tmp1 := xmlChecksumStartTag + checksum + xmlChecksumEindeTag
	tmp2 := xmlChecksumStartTag + XMLPlaceHolderChecksum + xmlChecksumEindeTag

	tmpMsg := strings.Replace(inMessage, tmp1, tmp2, 1)
	tmpMsg = SecureMessageWithCheckSum(tmpMsg)

	// Controleren of de twee berichten gelijk zijn aan elkaar
	if inMessage == tmpMsg {
		outVal = true
	}
	return (outVal)
}

// *************************************************************************************
// * secureMessageWithCheckSum
// *************************************************************************************
// Functie voor het bepalen van een Checksum van een bericht dat verstuurd / ontvangen
// wordt. Deze CheckSum wordt automatisch in het bericht geplaatst
// Als zout wordt de variabele maxLenghtMessage gebruikt. Deze wordt achter aan het
// bericht geplaatst. Met de parameter inject kan worden aangegeven of de Checksum
// ook daadwerkelijk in het bericht moet worden opgenomen
func SecureMessageWithCheckSum(msg string) string {

	outMsg := string(maxLenghtMessage) + msg
	//logging.DebugMSG(outMsg)
	signedMessage := GetMD5Hash(reverseString(outMsg))
	//logging.DebugMSG("signedMessage=" + signedMessage)
	tmp1 := xmlChecksumStartTag + XMLPlaceHolderChecksum + xmlChecksumEindeTag
	tmp2 := xmlChecksumStartTag + signedMessage + xmlChecksumEindeTag
	//logging.DebugMSG("tmp1=" + tmp1)
	//logging.DebugMSG("tmp2=" + tmp2)
	outMsg = strings.Replace(msg, tmp1, tmp2, 1)
	//logging.DebugMSG(outMsg)
	return (outMsg)
}

func GetMD5Hash(text string) string {
	hasher := md5.New()
	hasher.Write([]byte(text))
	return hex.EncodeToString(hasher.Sum(nil))
}



func reverseString(value string) string {
	data := []rune(value)
	result := []rune{}
	for i := len(data) - 1; i >= 0; i-- {
		result = append(result, data[i])
	}
	return string(result)
}

// Retourneren van het huidige Datum/tijd
// Hierbij de is TimeZone nu nog hardcoded opgenomen
func Now() string {
	t := time.Now()
	utc, _ := time.LoadLocation("Europe/Amsterdam")
	outValues := t.In(utc).Format(time.RFC822)
	return strings.ToUpper(outValues)
}

// Retourneren van het huidige Datum/tijd
// Hierbij de is TimeZone nu nog hardcoded opgenomen
func CurrentTimeStamp(timestampFormat string) string {
	t := time.Now()
	return t.Format(timestampFormat)
}


// Retourneren van een timestamp die x-seconden in de toekomst ligt
// Deze functionaliteit wordt gebruikt voor het bepalen van de
// DeadManSwitchTimeStamp (uiterlijk Timestamp dat de Worker 
// weer een HeartBeat-bericht verstuurd moet hebben)
func DeadManSwitchTimeStamp(inSec int32 ) string {
	t := time.Now()
	t = t.Add(time.Duration(inSec) * time.Second)
	return t.Format(constanten.DateFormatShort)
}


// Procedure voor het omsluiten van een XML-tag met CData zodat
// de inhoud van dit veld niet letterlijke geinterpreteerd wordt
func CData(xmlText string) string {
	outVal := html.UnescapeString("&lt;![CDATA[") + html.UnescapeString(xmlText) + html.UnescapeString("]]&gt;")
	return outVal
}

// Procedure voor het verwijderen van spaties aan het begin en einde
// van een  string
func Trim(trimString string) string {
	tmp := strings.TrimLeft(trimString, " ")
	tmp = strings.TrimRight(tmp, " ")
	return tmp
}

// **********************************************************************
// * Genereren van een unieke Hash. Deze hash wordt gebruikt om te controleren
// * of een object wanneer deze wordt gemuteerd, niet al door een andere users
// * opgeslagen is. Wanneer een object wordt opgeslagen, wordt een nieuwe hash
// * bepaald. Wanneer de hash toen het object wordt gelezen, niet meer gelijk
// * is aan de hash die wordt gecontroleerd bij het opslaan, is het object
// * tussentijds gewijzigd
// **********************************************************************

func GenerateHash(strSize int, randType string) string {

	var dictionary string

	if randType == "alphanum" {
		dictionary = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
	}

	if randType == "alpha" {
		dictionary = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
	}

	if randType == "number" {
		dictionary = "0123456789"
	}

	var bytes = make([]byte, strSize)
	rand.Read(bytes)
	for k, v := range bytes {
		bytes[k] = dictionary[v%byte(len(dictionary))]
	}
	return string(bytes)
}


// Function waarbij een controle wordt uitgevoerd om het http-request wel 
// voorzien is van de juiste credentials. Deze credentials zijn niet bekend
// bij de eindgebruiker maar zijn hard gecodeerd.
func CheckCredentialsHTTPRequest(r *http.Request) bool {

  if settings.ProtectURLWithCredentials == false {
    return true 
  }

  outValue := false 
  s := strings.SplitN(r.Header.Get("Authorization"), " ", 2)
	if len(s) != 2 { return false }

	b, err := base64.StdEncoding.DecodeString(s[1])
	if err != nil { return false }

	pair := strings.SplitN(string(b), ":", 2)	
	if len(pair) != 2 { return false }
	
	logging.DevMSG("Check Credentials: User=" + pair[0] + " Passwd=" + pair[1])

  //TODO Credential in een variabele zetten
  tmp := pair[0] + pair[1]
  if tmp == settings.Pass1 + settings.Pass2  {
  	outValue = true 
  }
	   
  return outValue
}



func FailOnError(err error, msg string) {
	if err != nil {
		panic(fmt.Sprintf("%s: %s", msg, err))
	}
}


// Methode die op basis van het JobType bepaald welke Route URL daarbij hoort
func GetURLRouteForJobType(jobType string) string {
	
	urlRoute := ""
	
	if jobType == "LINUXSH" {
		urlRoute = "/createjoblinuxsh"
	}
	
	return urlRoute 
}


// Procedure om een Id (int) te converteren naar een HEX-decimale waarde
// en aan te vullen met voorloopnullen, totdat de lengte 8 karakter is
func ConvertIdToHexValue(id int) string {
	
	tmp := "00000000"
	hexValue := fmt.Sprintf("%x", id)
	hexValue = tmp + hexValue 
	newId  := hexValue[len(hexValue)-8:]
	return newId
}


// Procedure om de Hostname van de machine op te vragen waar
// de Worker gestart is
func GetHostName() string {
	
	hostname, err := os.Hostname()
	if err != nil {
		hostname = "N/A"
	}
	return hostname
}

// Procedure om te controleren of de configuratie-directory bestaat. Wanneer de directory niet bestaat
// zal er worden geprobeerd om de directory aan te maken. Met een dummy bestand wat later
// weer verwijderd wordt, zal worden bepaald of de directory 'schrijfbaar' is
// Het resultaat wordt in de vorm van een bool teruggegeven. TRUE houdt in dat de directory bestaat en
// ook schrijfbaar is. Alles anders dan dat retourneert de status FALSE
func IsWritableDirectory(directoryName string) bool {

	retValue := false
	logging.DebugMSG("Checking existance of directory '" + directoryName + "'")

	// Eerst controleren of de opgegeven directory bestaat
	if FileExists(directoryName) == false {
		// Directory bestaat niet; deze proberen aan te maken
		logging.DebugMSG("Directory '" + directoryName + "' does not exist. Try to create this directory")
		err := os.MkdirAll(directoryName, 0755)
		if err != nil {
			logging.DebugMSG("An error occurred during the creation of directory '" + directoryName + "'")
			logging.DebugMSG("Program is exiting")
			os.Exit(1)
		}
	} else {
		logging.DebugMSG("Directory exists. Continue...")
	}

	// Controleren of het pad wel echt een directory is en niet toevallig een bestand
	// wat dezelfde naam heeft. Dit wordt gecontroleerd door een cd te doen
	// Eerst de huidige directory veiligstellen zodat we ook weer terug kunnen switchen
	currentWorkingDirectory, _ := os.Getwd()
	err := os.Chdir(directoryName)
	if err != nil {
		// Er is een probleem om naar de directory te switchen
		logging.DebugMSG("Cannot change directory to '" + directoryName + "'")
		logging.DebugMSG("Program is exiting")
		os.Exit(1)
	}
	// Directory terug zetten naar de oorspronkelijke WorkingDirectory
	os.Chdir(currentWorkingDirectory)

	// Controleren of de directory schrijfbaar is. Dit wordt vastgesteld door een tijdelijk
	// bestand 'touch.tmp' aan te maken
	directoryConcat := []string{directoryName, "temp.tmp"}
	touchFile := strings.Join(directoryConcat, string(os.PathSeparator))
	logging.DebugMSG("Creating file 'temp.tmp' in directory '" + directoryName + "'")
	ioutil.WriteFile(touchFile, []byte("test...."), 0644)
	// Controleren of het touch-bestand bestaat
	if FileExists(touchFile) == false {
		// Het touch bestand is niet aangemaakt
		logging.DebugMSG("Directory '" + directoryName + "' is not writable")
		logging.DebugMSG("Program is exiting")
		os.Exit(1)
	} else {
		// Touch-bestand is aangemaakt en kan verwijderd worden
		os.Remove(touchFile)
		logging.DebugMSG("Removing '" + touchFile + "'")
		retValue = true
	}

	return (retValue)
}