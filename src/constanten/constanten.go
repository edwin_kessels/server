package constanten

var Yes = "YES"
var No = "NO"

var DeleteTag = "*DELETED*"

// JobType
var JobType_Internal = 0
var Jobtype_Linux_SH = 1


// JobFile Types
var JobFile_StdOut = 1 
var JobFile_StdErr = 2
var JobFile_Custom = 99

// DateFormat
var DateFormatLong = "2006-01-02 15:04:05"
var DateFormatShort = "20060102150405"
var DateFormatDate = "2006-01-02"

var XMLIndent = "\t"

// JobStatussen
var JobStatus_created = 0

// Severity van Jobs
// Dit kan worden gebruikt indien er geescaleerd kan worden
var JobSeverity_normal = 1
var JobSeverity_severe = 2
var JobSeverity_critical = 3

// Statussen van UserAccounts
var UserAccount_Created = 0
var UserAccount_Open = 1
var UserAccount_Disabled = 2

// Audit acties voor JobSource
var AuditActie_jobsource_promote = 1
var AuditActie_jobsource_setCurrentVersion = 2

// ID voor de verschillende objecten. Deze IDs worden ondermeer gebruikt voor
// het auditen van de verschillende objecten
var Object_JobSource = 1
var Object_Job = 2
var Object_Worker = 3

// Delete-tag die wordt gebruikt om een FQNAME van een verwijderd object
// te vervangen zodat deze niet meer op naam kan worden gevonden

// In de berichten wordt ook een CheckSum gebruikt voor controle van de 'echtheid' van
// het bericht. Wanneer een bericht wordt aangemaakt, moet hier tijdelijk een placeholder
// voor worden opgenomen. Nadat de bericht compleet is, wordt de Checksum bepaald en wordt
// de placeholder vervnagen door deze berekende waarde
var XMLPlaceHolderChecksum = "[[%%CheckSum%%]]"
var XMLChecksumStartTag = "<CheckSum>"
var XMLChecksumEindeTag = "</CheckSum>"

// Gegevens met betrekking tot de repository
var RepositoryType = "postgres"
var RepositoryConnectString = "user=sched20 password=Welkom01 dbname=sched20 sslmode=disable"

// Functie om de naam van het JobType te retourneren op
// basis van JobType
func GetJobTypeDescription(jobtype int) string {

	outValue := "Unknown"

	switch jobtype {
	case 0:
		return "Internal Job"
	case 1:
		return "Linux Shell"
	}

	return outValue
}
