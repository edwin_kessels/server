package main


import "logging"
import "fmt"
import "encoding/xml"
import "settings"
import "github.com/streadway/amqp"
import "berichten"
import "statussen/jobstatus"

var cfgESBConnectString = "amqp://jbukjugz:9_FCD3K_7J0mCa-6mOGdInrTQTVzfnAD@owl.rmq.cloudamqp.com/jbukjugz"
var showContentMessage = true

var tmpXML berichten.JobCompletion



func sendMessageESB(msg string, queue string) {
	conn, err := amqp.Dial(cfgESBConnectString)
	defer conn.Close()
	if err != nil {
		fmt.Println("Cannot connect to ESB:" + err.Error())
	}

	// Afhankelijk van de parameter showContentMessage moet de inhoud van de message getoond worden
	if showContentMessage == true {
		logging.DebugMSG("-- Start outgoing ESB message --")
		logging.DebugMSG(msg)
		logging.DebugMSG("-- End outgoing ESB message --")
	}

	ch, err := conn.Channel()
	if err != nil {
		fmt.Println("Cannot allocate Channel")
	}
	defer ch.Close()

	q, err := ch.QueueDeclare(queue, false, false, false, false, nil)
	err = ch.Publish("", q.Name, false, false, amqp.Publishing{ContentType: "text/plain", Body: []byte(msg)})
	
}





func main() {
    
    	tmpXML.JobId            = "1"
		tmpXML.WorkerId                = 5 
		tmpXML.WorkerAddress           = "localhost"
		tmpXML.PortNumber              = "9000"
		tmpXML.ExitCode                = "1"
		tmpXML.ExecutionLog            = ""
		tmpXML.Status                  = jobstatus.Error     
		tmpXML.ElapsedTime             = "0.02 sec"      
		tmpXML.OutFileSize	            = "100"
		tmpXML.OutFileName			    = "/tmp/job/1/stdout.log"
		tmpXML.ErrFileSize			    = "255"
		tmpXML.ErrFileName			= "/tmp/job/1/stderr.log"
    
    
	
	xmlBericht, _ := xml.MarshalIndent(tmpXML, "", "\t")
	sendMessageESB(string(xmlBericht), settings.ESB_IN_Queue_JobCompletion)
    
}
