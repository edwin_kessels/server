package main


import "logging"
import "fmt"
import "encoding/xml"
import "settings"
import "github.com/streadway/amqp"
import "berichten"
import "statussen/workerstatus"

var cfgESBConnectString = "amqp://jbukjugz:9_FCD3K_7J0mCa-6mOGdInrTQTVzfnAD@owl.rmq.cloudamqp.com/jbukjugz"
var showContentMessage = true

var tmpXML berichten.WorkerStatus



func sendMessageESB(msg string, queue string) {
	conn, err := amqp.Dial(cfgESBConnectString)
	defer conn.Close()
	if err != nil {
		fmt.Println("Cannot connect to ESB:" + err.Error())
	}

	// Afhankelijk van de parameter showContentMessage moet de inhoud van de message getoond worden
	if showContentMessage == true {
		logging.DebugMSG("-- Start outgoing ESB message --")
		logging.DebugMSG(msg)
		logging.DebugMSG("-- End outgoing ESB message --")
	}

	ch, err := conn.Channel()
	if err != nil {
		fmt.Println("Cannot allocate Channel")
	}
	defer ch.Close()

	q, err := ch.QueueDeclare(queue, false, false, false, false, nil)
	err = ch.Publish("", q.Name, false, false, amqp.Publishing{ContentType: "text/plain", Body: []byte(msg)})
	
}





func main() {
    
    
    tmpXML.WorkerId               = 5
	tmpXML.ActualStatus        = workerstatus.Down
	tmpXML.RequestedStatus      = workerstatus.Connecting
	tmpXML.WorkerAddress        = "localhost"
	tmpXML.PortNumber           = "9000"
	tmpXML.Comment              = "Worker is not responding on Ping-request from HUB"
	
	xmlBericht, _ := xml.MarshalIndent(tmpXML, "", "\t")
	sendMessageESB(string(xmlBericht), settings.ESB_IN_Queue_WorkerStatus)
    
}
