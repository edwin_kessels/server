package main

import "net/http"
import "logging"
import "fmt"
import "bytes"
import "io/ioutil"
import "encoding/xml"
import "settings"
import "github.com/streadway/amqp"
import "berichten"

var cfgESBConnectString = "amqp://jbukjugz:9_FCD3K_7J0mCa-6mOGdInrTQTVzfnAD@owl.rmq.cloudamqp.com/jbukjugz"
var showContentMessage = true

type JobProperties struct {
		JobId				string
		JobType				string
		JobName 			string
		WorkerAddress		string
		JobDirectory		string
		MetaDirectory		string
		JobEnv				string
		JobSrc				string
		MaxRunTime			int
}

var jobEnvFileName berichten.DataWithSpecialCharacters = "env.sh"
var jobSrcFileName = "src.sh"

// Test Programma om een Ping te simuleren vanuit een HUB naar een Worker

func old() {
	
	jobEnvironment := "export ORACLE_HOME=/opt/oracle \n"
	jobEnvironment = jobEnvironment + "export ORACLE_SID=ORCL \n"
	
	jobSource := "#!/bin/sh" + "\n" 
	jobSource = jobSource + "set -e" + "\n" 
	//jobSource = jobSource + ". ./" + jobEnvFileName + "\n" 
	jobSource = jobSource + "ping -c 5 8.8.8.8 \n"
	jobSource = jobSource + "cd $ORACLE_HOME \n"
	jobSource = jobSource + "echo oracle_sid=$ORACLE_SID \n"
	jobSource = jobSource + "pwd \n"
	jobSource = jobSource + "ls -ltr \n" 
	
	//url := "http://localhost:8082/ping"
	url := "https://sched20-edwin-kessels.c9users.io:8080/createjoblinuxsh"

	logging.InfoMSG("URL=" + url)

	// Samenstellen van de XML
	var tmpXML berichten.JobRequest
	tmpXML.JobId				= "1001"
	tmpXML.JobType				= "LINUXSH"
	tmpXML.JobName  			= "AZR.JD_QUERY_001"
	tmpXML.WorkerAddress		= "https://sched20-edwin-kessels.c9users.io:8080"
	tmpXML.JobDirectory		= "/tmp/job/1001"
	tmpXML.MetaDirectory		= "/tmp/meta"
//	tmpXML.JobEnv				= jobEnvironment
//	tmpXML.JobSrc				= jobSource
	tmpXML.MaxRunTime			= 0 
	
	bericht, _ := xml.MarshalIndent(tmpXML, "", "\t")
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(bericht))
	req.Header.Set("X-Custom-Header", "myvalue")
	req.Header.Set("Content-Type", "application/xml")

	client := &http.Client{}
	
	// Instellen van de credentials
	req.SetBasicAuth("user", "pass")
	
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	fmt.Println("response Status:", resp.Status)
	fmt.Println("response Headers:", resp.Header)
	body, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("response Body:", string(body))
}



func sendMessageESB(msg string, queue string) {
	conn, err := amqp.Dial(cfgESBConnectString)
	defer conn.Close()
	if err != nil {
		fmt.Println("Cannot connect to ESB:" + err.Error())
	}

	// Afhankelijk van de parameter showContentMessage moet de inhoud van de message getoond worden
	if showContentMessage == true {
		logging.DebugMSG("-- Start outgoing ESB message --")
		logging.DebugMSG(msg)
		logging.DebugMSG("-- End outgoing ESB message --")
	}

	ch, err := conn.Channel()
	if err != nil {
		fmt.Println("Cannot allocate Channel")
	}
	defer ch.Close()

	q, err := ch.QueueDeclare(queue, false, false, false, false, nil)
	err = ch.Publish("", q.Name, false, false, amqp.Publishing{ContentType: "text/plain", Body: []byte(msg)})
	
}


func verstuurJobNaarESB() {
	
	var jobEnvironment berichten.DataWithSpecialCharacters
	var jobSource berichten.DataWithSpecialCharacters
	
	jobEnvironment = "export ORACLE_HOME=/opt/oracle \n"
	jobEnvironment = jobEnvironment + "export ORACLE_SID=ORCL \n"
	
	jobSource = "#!/bin/sh" + "\n" 
	jobSource = jobSource + "set -e" + "\n" 
	jobSource = jobSource + ". ./" + jobEnvFileName + "\n" 
	jobSource = jobSource + "ping -c 5 8.8.8.8 \n"
	jobSource = jobSource + "cd $ORACLE_HOME \n"
	jobSource = jobSource + "echo oracle_sid=$ORACLE_SID \n"
	jobSource = jobSource + "pwd \n"
	jobSource = jobSource + "ls -ltr \n" 
	
	//url := "http://localhost:8082/ping"
	//url := "https://sched20-edwin-kessels.c9users.io:8080/createjoblinuxsh"

	//logging.InfoMSG("URL=" + url)

	// Samenstellen van de XML
	var tmpXML berichten.JobRequest
	tmpXML.JobId				= "1001"
	tmpXML.JobType				= "LINUXSH"
	tmpXML.JobName  			= "AZR.JD_QUERY_001"
	tmpXML.WorkerAddress		= "https://sched20-edwin-kessels.c9users.io:8080"
	tmpXML.JobDirectory		= "/tmp/job/1001"
	tmpXML.MetaDirectory		= "/tmp/meta"
	tmpXML.JobEnv				= jobEnvironment
	tmpXML.JobSrc				= jobSource
	tmpXML.MaxRunTime			= 0 
	
	bericht, _ := xml.MarshalIndent(tmpXML, "", "\t")
	sendMessageESB( string(bericht), settings.ESB_OUT_Queue_JobRequest )
	
}


func main() {
	verstuurJobNaarESB() 
}
