package main

import "centralinstance/application"
import "repository"
import "database/sql"
import _ "github.com/lib/pq"
import "constanten"
import "fmt"

func main() {
    
    var err error
	repository.DBConnection, err = sql.Open(constanten.RepositoryType, constanten.RepositoryConnectString)
    if err != nil {
        fmt.Println("Gaat fout")
    }
    
    app := application.Application{}
    app.SetName("Test")
    app.Commit() 
}
