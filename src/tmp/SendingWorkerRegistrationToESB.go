package main


import "logging"
import "fmt"
import "encoding/xml"
import "settings"
import "github.com/streadway/amqp"
import "berichten"
import "utils"
import "versieinfo"

var cfgESBConnectString = "amqp://jbukjugz:9_FCD3K_7J0mCa-6mOGdInrTQTVzfnAD@owl.rmq.cloudamqp.com/jbukjugz"
var showContentMessage = true

var tmpXML berichten.WorkerRegistration



func sendMessageESB(msg string, queue string) {
	conn, err := amqp.Dial(cfgESBConnectString)
	defer conn.Close()
	if err != nil {
		fmt.Println("Cannot connect to ESB:" + err.Error())
	}

	// Afhankelijk van de parameter showContentMessage moet de inhoud van de message getoond worden
	if showContentMessage == true {
		logging.DebugMSG("-- Start outgoing ESB message --")
		logging.DebugMSG(msg)
		logging.DebugMSG("-- End outgoing ESB message --")
	}

	ch, err := conn.Channel()
	if err != nil {
		fmt.Println("Cannot allocate Channel")
	}
	defer ch.Close()

	q, err := ch.QueueDeclare(queue, false, false, false, false, nil)
	err = ch.Publish("", q.Name, false, false, amqp.Publishing{ContentType: "text/plain", Body: []byte(msg)})
	
}





func main() {
    
    
    tmpXML.Id               = 1
	tmpXML.WorkerAddress    = "localhost"      
	tmpXML.PortNumber       = "8082"
	tmpXML.StartTime        = utils.CurrentTimeStamp("2006-01-02 15:04:05")
	tmpXML.SoftwareVersion  = versieinfo.WorkerVersion
	tmpXML.SoftwareDate     = versieinfo.WorkerDatum
	tmpXML.Platform         = "Linux"
	tmpXML.CfgDirectory     = "/tmp/cfg"
	tmpXML.TmpDirectory     = "/tmp/tmp"
	tmpXML.MetaDirectory    = "/tmp/meta"
	tmpXML.JobDirectory     = "/tmp/job"
	tmpXML.WorkingDirectory = "/tmp/bin"
	tmpXML.HomeDirectory    = "/tmp/bin"
	tmpXML.LastJobId        = "1001"
	tmpXML.LastVisitedTime  = "N/A"
	tmpXML.LastVisitedHub   = "N/A"
	xmlBericht, _ := xml.MarshalIndent(tmpXML, "", "\t")
	sendMessageESB(string(xmlBericht), settings.ESB_IN_Queue_WorkerConfiguration)
    
}
