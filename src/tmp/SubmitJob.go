package main

import "centralinstance/jobsource"
import "centralinstance/job"
import "logging"
import "constanten"
import "repository"
import "database/sql"
import _ "github.com/lib/pq"

func main() {


	var err error
	repository.DBConnection, err = sql.Open(constanten.RepositoryType, constanten.RepositoryConnectString)
	if err != nil {
		logging.ErrorMSG("Probleem met connectie met de Repository:" + err.Error())
	}

    myJobSource := jobsource.JobSource{}
    myJobSource.GetJobSourceById(10)
    
    logging.ScreenMSG("Name=" + myJobSource.GetName()  ) 
    
    myJob := job.Job{}
    myJob.Prepare(myJobSource)
    myJob.Start()
    
}