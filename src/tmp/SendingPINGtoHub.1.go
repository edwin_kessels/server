package main

import "net/http"
import "logging"
import "fmt"
import "bytes"
import "io/ioutil"

// Test Programma om een Ping te simuleren vanuit een HUB naar een Worker

func main() {
	//url := "http://localhost:8082/ping"
	url := "https://worker-edwin-kessels.c9users.io:8080/ping"

	logging.InfoMSG("URL=" + url)

	var jsonStr = []byte(`{"Test":"8765"}`)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	req.Header.Set("X-Custom-Header", "myvalue")
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	
	// Instellen van de credentials
	req.SetBasicAuth("user", "pass")
	
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	fmt.Println("response Status:", resp.Status)
	fmt.Println("response Headers:", resp.Header)
	body, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("response Body:", string(body))
}
