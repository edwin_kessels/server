package main

import "logging"
import "net/http"
import "github.com/streadway/amqp"
import "strconv"
import "encoding/xml"
import "utils"
import "berichten"
import "settings"
import "esb"
import "strings"
import "constanten"
import "time"
import "statussen/workerstatus"

// Het kan voorkomen dat het Address van de HUB die de Worker via de https-request
// niet juist is. Hierdoor kan de behoefte bestaan om in het Ping-bericht van de
// HUB het reply-address en reply-portnumber op te nemen
var hubReplyAddress = ""
var hubReplyPortnumber = ""


//TODO
// Bericht voor het versturen van een PING-bericht tussen de HUB en Worker
// Hierbij kan optioneel ook het adres worden opgegeven.
type WorkerStatusChange struct {
	WorkerAddress		string
	Status					string
}

type test_struct struct {
	WorkerAddress		string
	HUBAddress			string
	HUBPortNumber		string
}
var cfgESBConnectString = "amqp://jbukjugz:9_FCD3K_7J0mCa-6mOGdInrTQTVzfnAD@owl.rmq.cloudamqp.com/jbukjugz"
var showContentMessage = true 
var MaxPingAttempts = 20

// Declaraties van de Map voor het opnemen van de Workers.
// Een Worker met waarde 0 is een actieve Worker die gewoon reageert
// Een Worker met een hogere waarde heeft tenminste 1x niet gereageerd
// en staat te boek als inactief
var workers map[int]string


// *****************************************************************************
// * monitorHeartBeatWorkers
// * Methode om te controleren of Workers de heartbeat bijwerken. Wanneer de 
// * laatste heartbeat in het verleden ligt, dan wordt aangenomen dat de Worker
// * niet meer actief is. Deze status wordt teruggekoppeld naar de CentralInstance
// *****************************************************************************
func monitorHeartBeatWorkers() {
	
	for {
	
		for k, v := range workers {
		
			currentTimeStamp := utils.CurrentTimeStamp(constanten.DateFormatShort)
		
			// Controleren van de opgeslagen TimeStamp met de CurrentTimeStamp
			if currentTimeStamp > v {
				// Laatste timestamp ligt in het verleden
				// Worker is down
				logging.DevMSG("Worker with ID " + strconv.Itoa(k) + " did not update the TimeStamp" )
				logging.DevMSG("Worker is removed from Monitoring")
				delete (workers, k)
				
				// Versturen van bericht naar de CentralInstance dat de Worker Down is
				var tmpXML berichten.WorkerUpdateStatus
				tmpXML.WorkerId = k
				tmpXML.ActualStatus	= workerstatus.Down
				tmpXML.Comment = "Worker is not responding"
				tmpXML.TimeStamp = utils.CurrentTimeStamp(constanten.DateFormatShort)
				tmpXML.CheckSum = berichten.GetCheckSumWorkerUpdateStatus(tmpXML)	
				xmlBericht, _ := xml.MarshalIndent(tmpXML, "", constanten.XMLIndent)
				esb.SendMessageESBWithoutWait(string(xmlBericht), settings.ESB_IN_Queue_WorkerStatus )  
			}
		
		}
		time.Sleep(5 * time.Second)
	}
}



// *****************************************************************************
// * receiveMonitorWorker
// * Methode om MonitorWorker en WorkerHeartBeat berichten te ontvangen en te verwerken
// * Op basis van de ontvangen Body worden bepaald of het een MonitorWorker of 
// * WorkerHeartBeat bericht is. Afhankelijk van het type bericht, wordt de bijbehorende
// * actie uitgevoerd. 
// *****************************************************************************
func receiveMonitorWorker() {
	
	queueName := settings.ESB_INNER_QUEUE_HeartBeat
	logging.InfoMSG("Listening on queue '" + queueName + "'")
	conn, err := amqp.Dial(esb.ConnectString)
	utils.FailOnError(err, "Failed to connect to RabbitMQ")
	defer conn.Close()

	ch, err := conn.Channel()
	utils.FailOnError(err, "Failed to open a channel")
	defer ch.Close()

	q, err := ch.QueueDeclare(queueName, false, false, false, false, nil)
	utils.FailOnError(err, "Failed to declare a queue")

	msgs, err := ch.Consume(q.Name, "", true, false, false, false, nil)
	utils.FailOnError(err, "Failed to register a consumer")

	forever := make(chan bool)

	go func() {
		for d := range msgs {
			
			// Er zijn 2 verschillende acties mogelijk
			action := "HEARTBEATWORKER"
			action = "MONITORWORKER"
			
			if strings.Contains(string(d.Body), "WorkerHeartBeat")  == true {
				action = "HEARTBEATWORKER"
			}
			
			// Afhandelen van een MonitorWorker bericht
			if action == "MONITORWORKER" {
				// Converteren van het bericht naar een Object
				var tmpXML berichten.MonitorWorker
				xml.Unmarshal(d.Body, &tmpXML)
			
				// Door de CentralInstance is er aangegeven dat er een nieuwe Worker
				// moet worden gemonitored. Dit wordt geadministreerd in de MemoryMap workers
				// Er moeten echter 2 gegevens worden bijgehouden, dus er wordt gebruik gemaakt
				// van een csv-lijst (ExpireTimeInSeconds;CurrentTimeStamp;DeadManSwitchTimeStamp)
				tmp := utils.DeadManSwitchTimeStamp(30)
				workers[tmpXML.WorkerId] = tmp
				logging.DevMSG("Worker with ID " + strconv.Itoa(tmpXML.WorkerId)  + " has been added to monitored Workers " + tmp)
			}
			
			// Afhandelen van een HeartBeatWorker bericht
			if action == "HEARTBEATWORKER" {
				var tmpXML berichten.WorkerHeartBeat
				xml.Unmarshal(d.Body, &tmpXML)
				tmp := utils.DeadManSwitchTimeStamp(30)
				workers[tmpXML.WorkerId] = tmp
				logging.DevMSG("Updating Heartbeat of Worker with ID " + strconv.Itoa(tmpXML.WorkerId)  + " to " + tmp)
				
			}
			
			
		}
	}()

	<-forever
	
}



// ************************************************************
// * registerHUB
// * Registeren van een HUB. Dit houdt in dat de volgende 
// * zaken worden uitgevoerd
// *  - Aanmaken van de nodige Exchanges
// *  - Genereren van een tijdelijk naam voor de HUB
// *  - Aanmaken van een Private Queue voor de HUB
// *  - Subscribe van de Queue op de Fan Exchange Worker_Heartbeat
func registerHUB() {

//	esb.CreateFanOutExchange("exch_test")
	esb.AddQueueToExchange("test123", "exch_test")
	
}





func main() {
	logging.DebugMSG("Starting HUB")
	
	// Initialiseren van de Map voor de Workers
	logging.DebugMSG("MemoryMap for registering Workers has been initialized")
	workers = make(map[int]string) 
	
	// Opstarten en registeren van de HUB
    registerHUB()
	
	// Background proces starten voor het afhandelen van MonitorWorker berichten
	//go receiveMonitorWorker() 
	//go monitorHeartBeatWorkers() 
	
	//getActiveConfigurationWorker("https://sched20-edwin-kessels.c9users.io:8080")	
	
	// Starten van de Agent die de WorkerStartUp berichten afhandelt
	//go WorkerStartUpAgent()
	
	// Starten van het Ping-proces naar actieve en niet-actieve Workers
	//go pingWorkers()
	
	//addMonitoringWorker("https://sched20-edwin-kessels.c9users.io:8080")
	//addMonitoringWorker("https://worker-edwin-kessels.c9users.io:8080")
		
	// Starten van de Queue-listeners voor de Enterprise Service Bus
	// Listener om te bepalen of er een Job moet worden aangemaakt en gestart worden
	//go startEnterpriseServerBusListener(settings.ESB_OUT_Queue_JobRequest)
	
		// Route voor het doorsturen van een StartJob bericht via de HUB naar de CentralInstance
		//http.HandleFunc("/relaystartjob", relayStartJobMessage) 

//workerConfig.PortNumber =8080 


http.ListenAndServe(":8082", nil)

}
