package importobject

import "objecttype"
import "logging"
import "strconv"
import "strings"
import "encoding/xml"

//import "berichten"
import "centralinstance/jobsource"
import "centralinstance/partition"
import "centralinstance/application"

// ******************************************************
// * getObjectType
// * Methode die bepaald om welk Object het gaat dat geimporteerd
// * moet worden
// ******************************************************
func getObjectType(objectDefinition string) int {

	var objectType int = objecttype.Unknown

	// Controleren of het gaat om een JobSource
	if strings.Contains(objectDefinition, "<JobSourceDefinition>") == true {
		objectType = objecttype.JobSource
	}
	return objectType
}

// ***********************************************************************
// * getPartitionId
// * Methode die op basis van de PartitionNaam zoals die in de XML is opgenomen
// * het Id van de Partition ophaalt.
// * Wanneer de Partition niet is gevonden, wordt 0 teruggegeven
// ***********************************************************************
func getPartitionId(inPartition string) int64 {

	part := partition.Partition{}
	part.GetPartitionByName(inPartition)

	return part.Id
}

// ***********************************************************************
// * getApplicationId
// * Methode die op basis van de ApplicationNaam zoals die in de XML is opgenomen
// * het Id van de Application ophaalt.
// * Wanneer de Application niet is gevonden, wordt 0 teruggegeven
// ***********************************************************************
func getApplicationId(inApplication string) int64 {

	app := application.Application{}
	app.GetApplicationByFQName(inApplication)

	return app.Id
}

// ************************************************************************
// * importJobSource
// * Importeren van een JobSource
// ************************************************************************
func importJobSource(objectDefinition string) string {

	var importWithErrors bool = false

	var url string = ""

	var impJobSourceDefinition jobsource.JobSourceDefinition
	xml.Unmarshal([]byte(objectDefinition), &impJobSourceDefinition)

	// Bepalen of de JobSource al bestaat
	// Eerst moet de FQName worden samengesteld omdat de FQName geen deel uitmaakt van de
	// Export-XML
	var fqName string = strings.ToUpper(impJobSourceDefinition.Partition) + "." + strings.ToUpper(impJobSourceDefinition.Name)
	jobSource := jobsource.JobSource{}
	jobSource.GetJobSourceByFQName(fqName)

	// Bepalen van de Id van de Partition die in de XML is opgenomen
	var partitionId int64 = getPartitionId(impJobSourceDefinition.Partition)
	if partitionId == 0 {
		logging.ErrorMSG("Unknown Partition: " + impJobSourceDefinition.Partition)
		importWithErrors = true
	}

	// Bepalen van de Id van de Application die in de XML is opgenomen
	var applicationId int64 = getApplicationId(impJobSourceDefinition.Application)
	if applicationId == 0 {
		logging.ErrorMSG("Unknown Application: " + impJobSourceDefinition.Application)
		importWithErrors = true
	}

	// Verwijderen van eventueel aanwezige parameters
	jobSource.DeleteCurrentJobSourceParameters()

	// Verwerken van eventuele JobSource-parameters
	for i := 0; i < len(impJobSourceDefinition.JobSourceParameter); i++ {
		par := jobsource.JobSourceParameter{}
		par.Name = impJobSourceDefinition.JobSourceParameter[i].Name
		par.Description = impJobSourceDefinition.JobSourceParameter[i].Description
		par.DefaultValue = impJobSourceDefinition.JobSourceParameter[i].DefaultValue
		par.Visible = impJobSourceDefinition.JobSourceParameter[i].Visible
		par.Scope = impJobSourceDefinition.JobSourceParameter[i].Scope
		par.Format = impJobSourceDefinition.JobSourceParameter[i].Format
		par.Order = impJobSourceDefinition.JobSourceParameter[i].Order
		par.Expression = impJobSourceDefinition.JobSourceParameter[i].Expression
		jobSource.AddJobSourceParameter(par)
	}

	if importWithErrors == false {
		jobSource.Name = impJobSourceDefinition.Name
		jobSource.PartitionId = partitionId
		jobSource.ApplicationId = applicationId
		jobSource.FQName = fqName
		jobSource.Description = impJobSourceDefinition.Description
		jobSource.JobSourceType = impJobSourceDefinition.JobSourceType
		jobSource.SourceCode = string(impJobSourceDefinition.SourceCode)
		jobSource.DumpObject()

		// Opslaan van de JobSource
		exitCode, exitMsg := jobSource.Commit()

		if exitCode != 0 {
			logging.ErrorMSG("Error during import: " + exitMsg)
		} else {
			logging.DebugMSG("JobSource " + fqName + " has been imported")
			// Samenstellen van de URL zodat er een redirect naar deze pagina kan worden
			// gedaan om meteen de geimporteerde JobSource te tonen
			url = "/jobsource/details/" + strconv.FormatInt(jobSource.Id, 10)
		}
	} else {
		// Er zijn fouten opgetreden
		logging.ErrorMSG("One or more errors occured during import of JobSource")
	}

	return url
}

// ******************************************************
// * Import
// * Methode die de import van een object aanstuurt en controleert
// * Wanneer een Object zonder fouten wordt aangemaakt, wordt
// * er een URL gegenereerd zodat meteen de nieuwe definitie
// * bekeken kan worden
// ******************************************************
func Import(objectDefinition string) string {

	var url string = ""

	// Bepalen om welke ObjectType het gaat
	var objectType int = getObjectType(objectDefinition)

	logging.DebugMSG("ObjectType=" + strconv.Itoa(objectType))

	if objectType == objecttype.JobSource {
		url = importJobSource(objectDefinition)
	}

	return url
}
