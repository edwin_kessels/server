package logging

import "os"
import "fmt"
import "settings"
import "log"

var maxVerboseMode = true
var envMAXVERBOSE = "MAXVERBOSE"

// Verschillende loglevels
// Debug =>
// Parameter
// Info
// Error

var DevLogging = settings.DevelopmentLogging
var ParameterLogging = true
var InfoLogging = true
var DumpObjectLogging = true

func DebugMSG(msg string) {
	if maxVerboseMode == true {
		//t := time.Now()
		//utc, _ := time.LoadLocation("Europe/Amsterdam")
		//timestamp := t.In(utc).Format(time.StampMilli )
		//fmt.Println("DBG: " + msg)
		log.SetFlags(log.LstdFlags | log.Lshortfile)
		log.Println("DBG: " + msg)
	}
}

func ParameterMSG(msg string) {
	if ParameterLogging {
		log.SetFlags(log.LstdFlags | log.Lshortfile)
		log.Println("PAR: " + msg)
	}
}

func UnitTestMSG(msg string) {
	fmt.Println("UNIT TEST: " + msg)
}

func ScreenMSG(msg string) {
	fmt.Println(msg)
}

func EmptyLine() {
	fmt.Println(" ")
}

func InfoMSG(msg string) {
	if InfoLogging == true {
		log.SetFlags(log.LstdFlags | log.Lshortfile)
		log.Println("INF: " + msg)
	}
}

func ErrorMSG(msg string) {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	log.Println("ERR: " + msg)
}

func DevMSG(msg string) {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	log.Println("DEV: " + msg)
}

func ObjectDumpMSG(msg string) {
	if DumpObjectLogging == true {
		fmt.Println("Object Dump: " + msg)
	}
}

// Procedure voor het instellen van de VerboseMode. Dit is het niveau / detail
// waarin de logging wordt aangemaakt. Standaard staat deze op minimaal, maar
// met behulp van de Environment Variabele MAXVERBOSE kan het niveau worden
// verhoogd
func SetVerboseMode() {

	// Eerst bepalen of de Environment variabele is gezet
	// Inlezen of er maximaal gelogd moet worden
	if os.Getenv(envMAXVERBOSE) != "" {
		maxVerboseMode = true
	}
}
