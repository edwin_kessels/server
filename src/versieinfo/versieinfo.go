package versieinfo

// Dit package bevat informatie over de versie van de verschillende componenten

var WorkerBuild = "001"
var WorkerVersion = "0.0.1"
var WorkerDatum = "13-JUL-2016"

var HubBuild = "001"
var HubVersion = "Hub 0.0.1 build " + WorkerBuild
var HubDatum = "13-JUL-2016"

var CentralInstanceBuild = "001"
var CentralInstanceVersion = "Central Instance 0.0.1 build " + WorkerBuild
var CentralInstanceDatum = "13-JUL-2016"

// **************************************************************
// * Versie informatie over de verschillende agents die
// * vanuit de CentralInstance worden gestart
// **************************************************************
var WrkcfgBuild = "001"
var WrkcfgVersion = "wrkcfg 0.0.1 build " + WrkcfgBuild
var WrkcfgDatum = "02-NOV-2016"

var WrkstatBuild = "001"
var WrkstatVersion = "wrkstat 0.0.1 build " + WrkstatBuild
var WrkstatDatum = "19-OCT-2016"

var JobCompBuild = "001"
var JobCompVersion = "jobcomp 0.0.1 build " + WrkstatBuild
var JobCompDatum = "20-OCT-2016"

var WrkstrtBuild = "001"
var WrkstrtVersion = "wrkstrt 0.0.1 build " + WrkstatBuild
var WrkstrtDatum = "26-OCT-2016"

var WrkregBuild = "001"
var WrkregVersion = "wrkreg 0.0.1 build " + WrkregBuild
var WrkregDatum = "07-NOV-2016"

var WrkmetricBuild = "001"
var WrkmetricVersion = "wrkmetric 0.0.1 build " + WrkmetricBuild
var WrkmetricDatum = "09-NOV-2016"

var WrkhrtbeatBuild = "001"
var WrkhrtbeatVersion = "wrkmetric 0.0.1 build " + WrkhrtbeatBuild
var WrkhrtbeatDatum = "09-NOV-2016"

var JobStatBuild = "001"
var JobStatVersion = "jobstat 0.0.1 build " + WrkhrtbeatBuild
var JobStatDatum = "17-NOV-2016"