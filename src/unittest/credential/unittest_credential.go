package main

import "logging"
import "centralinstance/credential"
import "strconv"

import "constanten"
import "repository"
import "database/sql"
import _ "github.com/lib/pq"
import "flag"

var component = ""

func verwerkResultaat(check string, exitCode int, exitMsg string, verwachtResultaat int) {

	output := ""

	// Juiste situatie
	if exitCode == verwachtResultaat {
		output = "[Ok    ] Check: '" + check + "' Resultaat als verwacht (" + exitMsg + ")"
	} else {
		output = "[Not Ok] Check: '" + check + "' NOT OK: " + exitMsg + " (exitcode=" + strconv.Itoa(exitCode) + ", expected=" + strconv.Itoa(verwachtResultaat) + ")"

	}
	logging.UnitTestMSG("[" + component + "]" + output)

}

func CheckCredential() {

	var err error
	repository.DBConnection, err = sql.Open(constanten.RepositoryType, constanten.RepositoryConnectString)
	if err != nil {
		logging.ErrorMSG("Probleem met connectie met de Repository:" + err.Error())
	}

	ex := 0
	em := ""
	chk := ""

	component = "CRE"

	logging.UnitTestMSG("Start Unit Test Credential Package")
	logging.UnitTestMSG("======================================================")

	chk = "Credential aanmaken waarbij geen Application is gespecificeerd"
	cred := credential.Credential{}
	cred.Name = "TestCredential"
	cred.Deleted = constanten.No
	cred.PartitionId = 1
	cred.Username = "test"
	cred.Password = "Welkom01"
	cred.DumpObject()
	ex, em = cred.Commit()
	verwerkResultaat(chk, ex, em, 1)
	logging.ObjectDumpMSG("======================================================")

	chk = "Credential aanmaken waarbij geen Partition is gespecificeerd"
	cred = credential.Credential{}
	cred.Name = "TestCredential"
	cred.Deleted = constanten.No
	cred.ApplicationId = 1
	cred.Username = "test"
	cred.Password = "Welkom01"
	cred.DumpObject()
	ex, em = cred.Commit()
	verwerkResultaat(chk, ex, em, 2)
	logging.ObjectDumpMSG("======================================================")

	chk = "Credential met een te lange naam"
	cred = credential.Credential{}
	cred.Name = "TestCredentialdsdasdsadasdsadasdasdasdasdsadasdskhdkjashdksahdkhasdhaksj"
	cred.Deleted = constanten.No
	cred.PartitionId = 1
	cred.ApplicationId = 1
	cred.Username = "test"
	cred.Password = "Welkom01"
	cred.DumpObject()
	ex, em = cred.Commit()
	verwerkResultaat(chk, ex, em, 5)
	logging.ObjectDumpMSG("======================================================")

	chk = "Credential zonder naam"
	cred = credential.Credential{}
	cred.Name = ""
	cred.Username = "test"
	cred.Password = "Welkom01"
	cred.DumpObject()
	ex, em = cred.Commit()
	verwerkResultaat(chk, ex, em, 1)
	logging.ObjectDumpMSG("======================================================")

	chk = "Credential met te lange omschrijving"
	cred = credential.Credential{}
	cred.Name = "Test"
	cred.Username = "test"
	cred.Password = "Welkom01"
	cred.Description = "TdadasdasdasdjkldlhdakhflhfowiefhwehfwehflwehflswwswswswswswsqwdsqwdqewwqdwqdwqedwewiwehflwehflewhflwehflihewflwehflwehflwehflwehflwehflweihflwehflwehflwehflwegfweestTdadasdasdasdjkldlhdakhflhfowiefhwehfwehflwehflswwswswswswswsqwdsqwdqewwqdwqdwqedwewiwehflwehflewhflwehflihewflwehflwehflwehflwehflwehflweihflwehflwehflwehflwegfweest"
	cred.PartitionId = 1
	cred.ApplicationId = 1
	cred.DumpObject()
	ex, em = cred.Commit()
	verwerkResultaat(chk, ex, em, 6)
	logging.ObjectDumpMSG("======================================================")

	chk = "Credential zonder Username"
	cred = credential.Credential{}
	cred.Name = "Test"
	cred.Username = ""
	cred.Password = "Welkom01"
	cred.Description = "Tdadahewflwehflwehflwehflwehflwehflweihflwehflwehflwehflwegfweest"
	cred.PartitionId = 1
	cred.ApplicationId = 1
	cred.DumpObject()
	ex, em = cred.Commit()
	verwerkResultaat(chk, ex, em, 7)
	logging.ObjectDumpMSG("======================================================")

	chk = "Credential zonder Password"
	cred = credential.Credential{}
	cred.Name = "Test"
	cred.Username = "ed"
	cred.Password = ""
	cred.Description = "Tdadahewflwehflwehflwehflwehflwehflweihflwehflwehflwehflwegfweest"
	cred.PartitionId = 1
	cred.ApplicationId = 1
	cred.DumpObject()
	ex, em = cred.Commit()
	verwerkResultaat(chk, ex, em, 7)
	logging.ObjectDumpMSG("======================================================")

	chk = "Credential met onbekende Partition aanmaken"
	cred = credential.Credential{}
	cred.Name = "Credential"
	cred.Deleted = constanten.No
	cred.PartitionId = 10000000
	cred.ApplicationId = 1
	cred.Username = "scott"
	cred.Password = "tiger"
	cred.DumpObject()
	ex, em = cred.Commit()
	verwerkResultaat(chk, ex, em, 6)
	logging.ObjectDumpMSG("======================================================")

	chk = "Credential in System Partition aanmaken"
	cred = credential.Credential{}
	cred.Name = "Credential"
	cred.Deleted = constanten.No
	cred.PartitionId = 1
	cred.ApplicationId = 1
	cred.Username = "scott"
	cred.Password = "tiger"
	cred.DumpObject()
	ex, em = cred.Commit()
	verwerkResultaat(chk, ex, em, 7)
	logging.ObjectDumpMSG("======================================================")

	chk = "Credential in Internal Partition aanmaken"
	cred = credential.Credential{}
	cred.Name = "Credential"
	cred.Deleted = constanten.No
	cred.PartitionId = 5
	cred.ApplicationId = 1
	cred.Username = "scott"
	cred.Password = "tiger"
	cred.DumpObject()
	ex, em = cred.Commit()
	verwerkResultaat(chk, ex, em, 8)
	logging.ObjectDumpMSG("======================================================")

	chk = "Credential aanmaken met onbekende Application"
	cred = credential.Credential{}
	cred.Name = "TestCredential"
	cred.Deleted = constanten.No
	cred.PartitionId = 2
	cred.ApplicationId = 100000000
	cred.Username = "scott"
	cred.Password = "tiger"
	cred.DumpObject()
	ex, em = cred.Commit()
	verwerkResultaat(chk, ex, em, 2)
	logging.ObjectDumpMSG("======================================================")

	chk = "Credential aanmaken met een Internal Application"
	cred = credential.Credential{}
	cred.Name = "TestCredential"
	cred.Deleted = constanten.No
	cred.PartitionId = 2
	cred.ApplicationId = 6
	cred.Username = "scott"
	cred.Password = "tiger"
	cred.DumpObject()
	ex, em = cred.Commit()
	verwerkResultaat(chk, ex, em, 3)
	logging.ObjectDumpMSG("======================================================")

	chk = "Correcte testCredential aanmaken"
	cred = credential.Credential{}
	cred.Name = "TestCredential"
	cred.Deleted = constanten.No
	cred.PartitionId = 2
	cred.ApplicationId = 8
	cred.Username = "scott"
	cred.Password = "tiger"
	cred.DumpObject()
	ex, em = cred.Commit()
	verwerkResultaat(chk, ex, em, 0)
	logging.ObjectDumpMSG("======================================================")

}

func main() {

	verboseOutput := false

	parVerbose1 := flag.Bool("v", false, "Maximize verbose")
	parVerbose2 := flag.Bool("verbose", false, "Maximize verbose")
	flag.Parse()

	logging.DevLogging = false
	logging.DumpObjectLogging = false
	logging.InfoLogging = false

	if *parVerbose1 == true {
		verboseOutput = true
	}
	if *parVerbose2 == true {
		verboseOutput = true
	}

	if verboseOutput == true {
		logging.DevLogging = true
		logging.DumpObjectLogging = true
		logging.InfoLogging = true
	}

	CheckCredential()
}
