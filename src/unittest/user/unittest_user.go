package main

import "logging"
import "centralinstance/user"
import "strconv"
import "constanten"
import "repository"
import "database/sql"
import _ "github.com/lib/pq"
import "flag"

var component = ""

func verwerkResultaat(check string, exitCode int, exitMsg string, verwachtResultaat int) {

	output := ""

	// Juiste situatie
	if exitCode == verwachtResultaat {
		output = "[Ok    ] Check: '" + check + "' Resultaat als verwacht (" + exitMsg + ")"
	} else {
		output = "[Not Ok] Check: '" + check + "' NOT OK: " + exitMsg + " (exitcode=" + strconv.Itoa(exitCode) + ", expected=" + strconv.Itoa(verwachtResultaat) + ")"

	}
	logging.UnitTestMSG("[" + component + "]" + output)

}

func CheckUser() {

	var err error
	repository.DBConnection, err = sql.Open(constanten.RepositoryType, constanten.RepositoryConnectString)
	if err != nil {
		logging.ErrorMSG("Probleem met connectie met de Repository:" + err.Error())
	}

	ex := 0
	em := ""
	chk := ""

	component = "USR"

	logging.UnitTestMSG("Start Unit Test User Package")
	logging.UnitTestMSG("======================================================")

	// Test Case 001: Opvoeren lege gebruikersnaam
	chk = "Opvoeren lege gebruikersnaam"
	//usr01 := user.User{}
	usr01 := new(user.User)
	usr01.AccountNameHash = "null"
	usr01.AccountName = "null"
	usr01.PasswordHash = "null"
	usr01.Firstname = "null"
	usr01.Lastname = "null"
	usr01.EmailAddress = "test@test.com"
	usr01.TwoFactorAuthentication = constanten.No
	usr01.Token = "null"
	usr01.TokenExpire = "null"
	usr01.Status = 0
	usr01.DefaultPartition = 0
	usr01.DefaultApplication = 0
	usr01.ProfileId = 0
	usr01.LastLogin = ""
	usr01.Deleted = ""

	//usr01.New()
	ex, em = usr01.CheckObject()
	//logging.UnitTestMSG("Start situation")
	//usr01.DumpObject()
	//verwerkResultaat(chk, ex,em,1)

	chk = "Opvoeren van gebruikersnaam met alleen spaties"
	usr01.SetAccountName("    ")
	usr01.DumpObject()
	ex, em = usr01.Commit()
	verwerkResultaat(chk, ex, em, 1)
	logging.ObjectDumpMSG("======================================================")

	chk = "Opvoeren van gebruikersnaam die met een cijfer begint"
	usr01.SetAccountName("123TEST")
	ex, em = usr01.Commit()
	usr01.DumpObject()
	verwerkResultaat(chk, ex, em, 4)
	logging.ObjectDumpMSG("======================================================")

	chk = "Gebruikersnaam met ongeldig karakter"
	usr01.SetAccountName("test$")
	ex, em = usr01.Commit()
	usr01.DumpObject()
	verwerkResultaat(chk, ex, em, 2)
	logging.ObjectDumpMSG("======================================================")

	chk = "Te lange Gebruikersnaam"
	usr01.SetAccountName("test")
	usr01.SetAccountName("ajhfsdjfhsdhfsdkfhsdkjhfsdfhuiwerfsskdhfksdhfusdhfjksdhfksdhfksdjhfksjdhfskdhfsdk")
	ex, em = usr01.Commit()
	usr01.DumpObject()
	verwerkResultaat(chk, ex, em, 5)
	logging.ObjectDumpMSG("======================================================")

	chk = "Gebruikersnaam SYSTEM (reserved Word)"
	usr01.SetAccountName("SYSTEM")
	ex, em = usr01.Commit()
	usr01.DumpObject()
	verwerkResultaat(chk, ex, em, 3)
	logging.ObjectDumpMSG("======================================================")

	chk = "Geen voornaam gespecificeerd"
	usr01.SetAccountName("TEST")
	usr01.SetFirstName("")
	ex, em = usr01.Commit()
	usr01.DumpObject()
	verwerkResultaat(chk, ex, em, 1)
	logging.ObjectDumpMSG("======================================================")

	chk = "Te lange voornaam gespecificeerd"
	usr01.SetAccountName("TEST")
	usr01.SetFirstName("ajhfsdjfhsdhfsdkfhsdkjhfsdfhuiwerfsskdhfksdhfusdhfjksdhfksdhfksdjhfksjdhfskdhfsdk")
	ex, em = usr01.Commit()
	usr01.DumpObject()
	verwerkResultaat(chk, ex, em, 12)
	logging.ObjectDumpMSG("======================================================")

	chk = "Geen achternaam gespecificeerd"
	usr01.SetFirstName("NULL")
	usr01.SetLastName("")
	ex, em = usr01.Commit()
	usr01.DumpObject()
	verwerkResultaat(chk, ex, em, 14)
	logging.ObjectDumpMSG("======================================================")

	chk = "Te lange achternaam gespecificeerd"
	usr01.SetLastName("ajhfsdjfhsdhfsdkfhsdkjhfsdfhuiwerfsskdhfksdhfusdhfjksdhfksdhfksdjwerfsskdhfksdhfusdhfjksdhfksdhfksdjhfksjdhfskdhfsdk")
	ex, em = usr01.Commit()
	usr01.DumpObject()
	verwerkResultaat(chk, ex, em, 2)
	logging.ObjectDumpMSG("======================================================")

	chk = "Ongeldig EmailAdres test 1"
	usr01.SetLastName("NULL")
	usr01.SetEmailAddress("user.domain.nl")
	ex, em = usr01.Commit()
	usr01.DumpObject()
	verwerkResultaat(chk, ex, em, 4)
	logging.ObjectDumpMSG("======================================================")

	chk = "Ongeldig EmailAdres test 2"
	usr01.SetLastName("NULL")
	usr01.SetEmailAddress("use#r@domain.nl")
	ex, em = usr01.Commit()
	usr01.DumpObject()
	verwerkResultaat(chk, ex, em, 19)
	logging.ObjectDumpMSG("======================================================")

	chk = "Echte gebruiker aanmaken"
	usr01.AccountNameHash = "null"
	usr01.SetAccountName("test01")
	usr01.PasswordHash = "null"
	usr01.Firstname = "test"
	usr01.Lastname = "gebruiker01"
	usr01.EmailAddress = "test@test.com"
	usr01.TwoFactorAuthentication = constanten.No
	usr01.Token = "null"
	usr01.TokenExpire = "null"
	usr01.Status = 0
	usr01.DefaultPartition = 0
	usr01.DefaultApplication = 0
	usr01.ProfileId = 0
	usr01.LastLogin = ""
	usr01.Deleted = ""
	usr01.DumpObject()
	ex, em = usr01.Commit()
	verwerkResultaat(chk, ex, em, 0)
	logging.ObjectDumpMSG("======================================================")

	chk = "Vorige gebruiker nogmaals aanmaken"
	usr01.DumpObject()
	ex, em = usr01.Commit()
	verwerkResultaat(chk, ex, em, 16)
	logging.ObjectDumpMSG("======================================================")

	chk = "Verwijderen van een gebruiker"
	usr02 := user.User{}
	usr02.GetUserByAccountName("test01")
	//usr02.GetUserById(2)
	usr02.Delete()
	usr02.DumpObject()
	ex, em = usr02.Commit()
	verwerkResultaat(chk, ex, em, 0)
	logging.ObjectDumpMSG("======================================================")

	chk = "Verwijderde gebruiker nogmaals aanmaken"
	usr01.DumpObject()
	ex, em = usr01.Commit()
	verwerkResultaat(chk, ex, em, 0)
	logging.ObjectDumpMSG("======================================================")

	// logging.UnitTestMSG("Test Case 006: Te lange voornaam gespecificeerd")

}

func main() {

	verboseOutput := false

	parVerbose1 := flag.Bool("v", false, "Maximize verbose")
	parVerbose2 := flag.Bool("verbose", false, "Maximize verbose")
	flag.Parse()

	logging.DevLogging = false
	logging.DumpObjectLogging = false
	logging.InfoLogging = false

	if *parVerbose1 == true {
		verboseOutput = true
	}
	if *parVerbose2 == true {
		verboseOutput = true
	}

	if verboseOutput == true {
		logging.DevLogging = true
		logging.DumpObjectLogging = true
		logging.InfoLogging = true
	}

	CheckUser()
}
