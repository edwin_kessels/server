package main

import "logging"
import "centralinstance/partition"
import "centralinstance/privilege"
import "strconv"

import "constanten"
import "repository"
import "database/sql"
import _ "github.com/lib/pq"
import "flag"

var component = ""

func verwerkResultaat(check string, exitCode int, exitMsg string, verwachtResultaat int) {

	output := ""

	// Juiste situatie
	if exitCode == verwachtResultaat {
		output = "[Ok    ] Check: '" + check + "' Resultaat als verwacht (" + exitMsg + ")"
	} else {
		output = "[Not Ok] Check: '" + check + "' NOT OK: " + exitMsg + " (exitcode=" + strconv.Itoa(exitCode) + ", expected=" + strconv.Itoa(verwachtResultaat) + ")"

	}
	logging.UnitTestMSG("[" + component + "]" + output)

}

func CheckPartition() {

	var err error
	repository.DBConnection, err = sql.Open("postgres", "user=sched20 password=Welkom01 dbname=sched20 sslmode=disable")
	if err != nil {
		logging.ErrorMSG("Probleem met connectie met de Repository:" + err.Error())
	}

	ex := 0
	em := ""
	chk := ""

	component = "PRT"

	logging.UnitTestMSG("Start Unit Test Partition Package")
	logging.UnitTestMSG("======================================================")

	// Uitvoeren van de StartupCheck. Dit maakt geen deel uit van de UnitTest, maar omdat er
	// met een lege database wordt gestart, is het wel essentieel dat de system-objecten
	// bestaan
	partition.StartupCheck()

	chk = "Partition aanmaken zonder geldig UserID"
	partition.UserId = 0
	part := partition.Partition{}
	part.SetName("TestPartition")
	part.SetDescription("Partition created by Unit Test")
	part.Internal = constanten.No
	part.DumpObject()
	ex, em = part.Commit()
	verwerkResultaat(chk, ex, em, 9)
	logging.ObjectDumpMSG("======================================================")

	chk = "Private Partition aanmaken"
	part = partition.Partition{}
	partition.UserId = 1
	//part.SetUserId()
	part.SetName("TestPartition")
	part.SetDescription("Partition created by Unit Test")
	part.Internal = constanten.No
	part.DumpObject()
	ex, em = part.Commit()
	verwerkResultaat(chk, ex, em, 0)
	logging.ObjectDumpMSG("======================================================")

	chk = "Nogmaals dezelfde Partition aanmaken"
	part.Id = 0
	part.SetName("TestPartition")
	part.SetDescription("Partition created by Unit Test")
	part.Internal = constanten.No
	part.DumpObject()
	ex, em = part.Commit()
	verwerkResultaat(chk, ex, em, 1)
	logging.ObjectDumpMSG("======================================================")

	chk = "Publieke Partition aanmaken"
	part.SetName("PublicPartition")
	part.SetDescription("Partition created by Unit Test")
	part.Internal = constanten.No
	part.SetPublic()
	part.DumpObject()
	ex, em = part.Commit()
	verwerkResultaat(chk, ex, em, 0)
	logging.ObjectDumpMSG("======================================================")

	chk = "Aanmaken Partition met ongeldig karakter in de naam"
	part.SetName("TestPartition$")
	part.Id = 0
	part.DumpObject()
	ex, em = part.Commit()
	verwerkResultaat(chk, ex, em, 2)
	logging.ObjectDumpMSG("======================================================")

	chk = "Aanmaken Partition met een te lange naam"
	part.SetName("TestPartitiondashdfsjkdfsdhfsdjhfsdfsdfffffffffffffffffffffffffffffffffffffffffffffffffffsdfsd")
	part.Id = 0
	part.DumpObject()
	ex, em = part.Commit()
	verwerkResultaat(chk, ex, em, 5)
	logging.ObjectDumpMSG("======================================================")

	chk = "Aanmaken Partition zonder naam"
	part.SetName("")
	part.Id = 0
	part.DumpObject()
	ex, em = part.Commit()
	verwerkResultaat(chk, ex, em, 1)
	logging.ObjectDumpMSG("======================================================")

	chk = "Partition met een te lange omschrijving"
	part.GetPartitionByName("TestPartition")
	part.Description = "TdadasdasdasdjkldlhdakhflhfowiefhwehfwehflwehflswwswswswswswsqwdsqwdqewwqdwqdwqedwewiwehflwehflewhflwehflihewflwehflwehflwehflwehflwehflweihflwehflwehflwehflwegfweestTdadasdasdasdjkldlhdakhflhfowiefhwehfwehflwehflswwswswswswswsqwdsqwdqewwqdwqdwqedwewiwehflwehflewhflwehflihewflwehflwehflwehflwehflwehflweihflwehflwehflwehflwegfweest"
	part.DumpObject()
	ex, em = part.Commit()
	verwerkResultaat(chk, ex, em, 6)
	logging.ObjectDumpMSG("======================================================")

	chk = "Verwijderen van de TestPartition"
	part.GetPartitionByName("TestPartition")
	part.Delete()
	part.DumpObject()
	ex, em = part.Commit()
	verwerkResultaat(chk, ex, em, 0)
	logging.ObjectDumpMSG("======================================================")

	chk = "Verwijderen van de System Partition"
	part.GetPartitionByName("system")
	part.Delete()
	part.DumpObject()
	ex, em = part.Commit()
	verwerkResultaat(chk, ex, em, 5)
	logging.ObjectDumpMSG("======================================================")

	chk = "Private Partition aanmaken"
	part1 := partition.Partition{}
	//part1.SetUserId()
	part1.SetName("InternalPartition")
	part1.SetDescription("Partition created by Unit Test")
	part1.Internal = constanten.Yes
	part1.Deleted = constanten.No
	part1.SetPublic()
	part1.DumpObject()
	ex, em = part1.Commit()
	verwerkResultaat(chk, ex, em, 0)
	logging.ObjectDumpMSG("======================================================")

	chk = "Aanpassen van de omschrijving van InternalPartition"
	part2 := partition.Partition{}
	//part2.SetUserId()
	part2.GetPartitionByName("InternalPartition")
	part2.SetDescription("[renamed] Partition created by Unit Test")
	part2.DumpObject()
	ex, em = part2.Commit()
	verwerkResultaat(chk, ex, em, 0)
	logging.ObjectDumpMSG("======================================================")

	chk = "Privileges toekennen op Publieke Partition"
	part3 := partition.Partition{}
	part3.GetPartitionByName("PublicPartition")
	ex, em = part3.SetPartitionModeForUser(999, privilege.PartitionReadWriteMode)
	verwerkResultaat(chk, ex, em, 0)
	logging.ObjectDumpMSG("======================================================")

	chk = "Privileges intrekken op Publieke Partition"
	ex, em = part3.SetPartitionModeForUser(999, privilege.PartitionNoAccessMode)
	verwerkResultaat(chk, ex, em, 0)
	logging.ObjectDumpMSG("======================================================")

	//usr01.New()
	//ex, em = usr01.CheckObject()
	//logging.UnitTestMSG("Start situation")
	//usr01.DumpObject()
	//verwerkResultaat(chk, ex,em,1)

}

func main() {

	verboseOutput := false

	parVerbose1 := flag.Bool("v", false, "Maximize verbose")
	parVerbose2 := flag.Bool("verbose", false, "Maximize verbose")
	flag.Parse()

	logging.DevLogging = false
	logging.DumpObjectLogging = false
	logging.InfoLogging = false

	if *parVerbose1 == true {
		verboseOutput = true
	}
	if *parVerbose2 == true {
		verboseOutput = true
	}

	if verboseOutput == true {
		logging.DevLogging = true
		logging.DumpObjectLogging = true
		logging.InfoLogging = true
	}

	CheckPartition()
}
