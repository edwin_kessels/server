package main

import "logging"
import "centralinstance/application"
import "strconv"

import "constanten"
import "repository"
import "database/sql"
import _ "github.com/lib/pq"
import "flag"

var component = ""

func verwerkResultaat(check string, exitCode int, exitMsg string, verwachtResultaat int) {

	output := ""

	// Juiste situatie
	if exitCode == verwachtResultaat {
		output = "[Ok    ] Check: '" + check + "' Resultaat als verwacht (" + exitMsg + ")"
	} else {
		output = "[Not Ok] Check: '" + check + "' NOT OK: " + exitMsg + " (exitcode=" + strconv.Itoa(exitCode) + ", expected=" + strconv.Itoa(verwachtResultaat) + ")"

	}
	logging.UnitTestMSG("[" + component + "]" + output)

}

func CheckApplication() {

	var err error
	repository.DBConnection, err = sql.Open(constanten.RepositoryType, constanten.RepositoryConnectString)
	if err != nil {
		logging.ErrorMSG("Probleem met connectie met de Repository:" + err.Error())
	}

	ex := 0
	em := ""
	chk := ""

	component = "APP"

	logging.UnitTestMSG("Start Unit Test Application Package")
	logging.UnitTestMSG("======================================================")

	// Test : Applicatie met lege Application Name
	chk = "Opvoeren Applicatie met lege Application Name"
	app := application.Application{}
	app.SetName("")
	app.DumpObject()
	ex, em = app.Commit()
	verwerkResultaat(chk, ex, em, 1)
	logging.ObjectDumpMSG("======================================================")

	// Test : Applicatie met ongeldig teken in de Application Name
	chk = "Applicatie met ongeldig teken in de Application Name"
	app.SetName("@pplication")
	app.DumpObject()
	ex, em = app.Commit()
	verwerkResultaat(chk, ex, em, 2)
	logging.ObjectDumpMSG("======================================================")

	// Test : Applicatie met een te lange Application Name
	chk = "Applicatie met ongeldig teken in de Application Name"
	app.SetName("pplicationpplicationpplicationpplicationpplicationpplicationpplicationpplicationpplicationpplicationpplicationpplicationpplicationpplication")
	app.DumpObject()
	ex, em = app.Commit()
	verwerkResultaat(chk, ex, em, 5)
	logging.ObjectDumpMSG("======================================================")

	// Test : Aanmaken correct internal Application
	chk = "Aanmaken correct internal Application"
	app.SetName("Buildin")
	app.SetDescription("Application for BuiltIn objects")
	app.Internal = constanten.Yes
	app.DumpObject()
	ex, em = app.Commit()
	verwerkResultaat(chk, ex, em, 0)
	logging.ObjectDumpMSG("======================================================")

	// Test : Nogaamls aanmaken correct internal Application
	chk = "Nogmaals aanmaken correct internal Application"
	app2 := application.Application{}
	app2.SetName("Buildin")
	app2.SetDescription("Application for BuiltIn objects")
	app2.Internal = constanten.Yes
	app2.DumpObject()
	ex, em = app2.Commit()
	verwerkResultaat(chk, ex, em, 4)
	logging.ObjectDumpMSG("======================================================")

	// Test : Verwijderen van een internal Application
	chk = "Verwijderen van een internal Application"
	app2.Delete()
	app2.DumpObject()
	ex, em = app2.Commit()
	verwerkResultaat(chk, ex, em, 6)
	logging.ObjectDumpMSG("======================================================")

	// Test : Aanmaken van een Application die weer wordt verwijderd
	chk = "Aanmaken correct internal Application"
	app1 := application.Application{}
	app1.SetName("Test123")
	app1.SetDescription("Deze Application kan verwijderd worden")
	app1.SetDescription("Application for BuiltIn objects")
	app1.Internal = constanten.Yes
	app1.DumpObject()
	ex, em = app1.Commit()
	verwerkResultaat(chk, ex, em, 0)
	logging.ObjectDumpMSG("======================================================")

	// Test : Aanmaken van een publieke Application
	chk = "Aanmaken van een publieke Application"
	app3 := application.Application{}
	app3.SetName("PublicApplication")
	app3.SetDescription("Dit is een publieke Application")
	app3.Internal = constanten.No
	app3.DumpObject()
	ex, em = app3.Commit()
	verwerkResultaat(chk, ex, em, 0)
	logging.ObjectDumpMSG("======================================================")

	// Test : Aanmaken van een publieke Application
	chk = "Aanmaken van een publieke Application"
	app4 := application.Application{}
	app4.SetName("PublicApplicationDeletion")
	app4.SetDescription("Dit is een publieke Application")
	app4.Internal = constanten.No
	app4.DumpObject()
	ex, em = app4.Commit()
	verwerkResultaat(chk, ex, em, 0)
	logging.ObjectDumpMSG("======================================================")

	// Test : Aanmaken van een publieke Application die verwijderd wordt
	chk = "Aanmaken van een publieke Application die verwijderd wordt"
	app5 := application.Application{}
	app5.GetApplicationByName("PublicApplicationDeletion")
	app5.Delete()
	app5.DumpObject()
	ex, em = app5.Commit()
	verwerkResultaat(chk, ex, em, 0)
	logging.ObjectDumpMSG("======================================================")
	
	// Test : Aanmaken van een publieke Application
	chk = "Aanmaken van een publieke Application"
	app6 := application.Application{}
	app6.SetName("UserApp")
	app6.SetDescription("Dit is een publieke Application")
	app6.Internal = constanten.No
	app6.DumpObject()
	ex, em = app6.Commit()
	verwerkResultaat(chk, ex, em, 0)
	logging.ObjectDumpMSG("======================================================")

}

func main() {

	verboseOutput := false

	parVerbose1 := flag.Bool("v", false, "Maximize verbose")
	parVerbose2 := flag.Bool("verbose", false, "Maximize verbose")
	flag.Parse()

	logging.DevLogging = false
	logging.DumpObjectLogging = false
	logging.InfoLogging = false

	if *parVerbose1 == true {
		verboseOutput = true
	}
	if *parVerbose2 == true {
		verboseOutput = true
	}

	if verboseOutput == true {
		logging.DevLogging = true
		logging.DumpObjectLogging = true
		logging.InfoLogging = true
	}

	CheckApplication()
}
