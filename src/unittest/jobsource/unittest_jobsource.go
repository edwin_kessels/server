package main

import "logging"
import "centralinstance/jobsource"
import "centralinstance/job"
import "strconv"
import "berichten"

import "constanten"
import "repository"
import "database/sql"
import _ "github.com/lib/pq"
import "flag"

var component = ""

func verwerkResultaat(check string, exitCode int, exitMsg string, verwachtResultaat int) {

	output := ""

	// Juiste situatie
	if exitCode == verwachtResultaat {
		output = "[Ok    ] Check: '" + check + "' Resultaat als verwacht (" + exitMsg + ")"
	} else {
		output = "[Not Ok] Check: '" + check + "' NOT OK: " + exitMsg + " (exitcode=" + strconv.Itoa(exitCode) + ", expected=" + strconv.Itoa(verwachtResultaat) + ")"

	}
	logging.UnitTestMSG("[" + component + "]" + output)

}

func CheckJobSource() {
    
    var tmpSourceCode berichten.DataWithSpecialCharacters
    tmpSourceCode = "cd /home" + "\n"
    tmpSourceCode = tmpSourceCode + "ls -ltr" + "\n"

	var err error
	repository.DBConnection, err = sql.Open(constanten.RepositoryType, constanten.RepositoryConnectString)
	if err != nil {
		logging.ErrorMSG("Probleem met connectie met de Repository:" + err.Error())
	}

	ex := 0
	em := ""
	chk := ""

	component = "CRE"

	logging.UnitTestMSG("Start Unit Test JobSource Package")
	logging.UnitTestMSG("======================================================")

    chk = "JobSource aanmaken met een lege Name"
	js := jobsource.JobSource{}
	js.SetName("")
	js.SetDescription("TestJob - sleep 30 seconden")
	js.SetSourceCode(tmpSourceCode)
	ex, em = js.Commit()
	verwerkResultaat(chk, ex, em, 7)
	logging.ObjectDumpMSG("======================================================")

	chk = "JobSource aanmaken"
	//js := jobsource.JobSource{}
	js.SetName("TestJob$")
	js.SetDescription("TestJob - sleep 30 seconden")
	js.SetSourceCode(tmpSourceCode)
	ex, em = js.Commit()
	verwerkResultaat(chk, ex, em, 2)
	logging.ObjectDumpMSG("======================================================")
	
	chk = "JobSource aanmaken zonder SourceCode"
	//js := jobsource.JobSource{}
	js.SetName("TestJob")
	js.SetDescription("TestJob - sleep 30 seconden")
	js.SetSourceCode("")
	ex, em = js.Commit()
	verwerkResultaat(chk, ex, em, 7)
	logging.ObjectDumpMSG("======================================================")
	
	chk = "Correcte JobSource aanmaken"
	js1 := jobsource.JobSource{}
	js1.SetName("TestJob")
	js1.SetDescription("TestJob - sleep 30 seconden")
	js1.SetSourceCode(tmpSourceCode)
	js1.SetApplication("test")
	js1.SetPartition("GLOBAL")
	js1.SetJobSourceType(1)
	ex, em = js1.Commit()
	verwerkResultaat(chk, ex, em, 0)
	logging.ObjectDumpMSG("======================================================")
	
	chk = "Ophalen van een JobSource op basis van het ID (=2)"
	logging.DevLogging =  true 
	js2 := jobsource.JobSource{}
	js2.GetJobSourceByFQName("GLOBAL.TESTJOB")
	js2.DumpObject()
	
	// Preparen van de job
	job := job.Job{}
	job.Prepare(js2)
	
	
	

}

func main() {

	verboseOutput := false

	parVerbose1 := flag.Bool("v", false, "Maximize verbose")
	parVerbose2 := flag.Bool("verbose", false, "Maximize verbose")
	flag.Parse()

	logging.DevLogging = false
	logging.DumpObjectLogging = false
	logging.InfoLogging = false

	if *parVerbose1 == true {
		verboseOutput = true
	}
	if *parVerbose2 == true {
		verboseOutput = true
	}

	if verboseOutput == true {
		logging.DevLogging = true
		logging.DumpObjectLogging = true
		logging.InfoLogging = true
	}

	CheckJobSource()
}
