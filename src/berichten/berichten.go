package berichten

import "encoding/xml"
import "utils"
import "strconv"

///////////////////////////////////////////////////////////////////////////////////////////////////////////
// Berichtenverkeer vanuit Worker gezien
///////////////////////////////////////////////////////////////////////////////////////////////////////////

// *************************************************************************************
// * WorkerSelfTest
// * Bericht dat wordt gebruikt om te controleren of de Worker Queue bestaat
// * Met dit bericht zelf wordt verder niets gedaan
// *************************************************************************************
type WorkerSelfTest struct {
	TimeStamp string // TimeStamp aanmaak bericht
}

// *************************************************************************************
// * JobStatusChange
// * Bericht om een wijziging in de status van een Job door te geven aan de
// * CentralInstance. Dit kan bijvoorbeeld bestaan uit de melding dat een Job is
// * gestart door de Worker
// *************************************************************************************
type JobStatusChange struct {
	WorkerId int64
	JobId    string
	Status   string
}

// *************************************************************************************
// * JobStatusCompletion
// * Bericht met informatie over een job met een eindstatus
// *************************************************************************************
type JobStatusCompletion struct {
	JobId        string
	WorkerId     int64
	ExitCode     string
	ExecutionLog string
	Status       string
	ElapsedTime  string
	OutFileSize  string
	OutFileName  string
	ErrFileSize  string
	ErrFileName  string
}

// *************************************************************************************
// * JobFile
// * Bericht voor het versturen van JobFiles waaronder de stdout en stderr
// *************************************************************************************
type JobFileTransfer struct {
	JobId          string
	JobFileType    int
	JobFileName    string
	JobFileContent string
}

// *************************************************************************************
// * LocalWorkerStartup
// * Bericht dat verstuurd wordt door de Worker wanneer deze geconfigureerd is en
// * wordt gestart. Dit bericht wordt via de InterConnect naar de Central Instance
// * gestuurd
// *************************************************************************************

type LocalWorkerStartUpObsolete struct {
	WorkerId        int
	SoftwareVersion string // SoftwareVersie van de WorkerSoftware.
	SoftwareBuild   string
	SoftwareDate    string
	Platform        string
	StartTime       string
	HostName        string
	TimeStamp       string // TimeStamp aanmaak bericht
	CheckSum        string
}

// Functie voor het berekenen van de Checksum van het bericht
//func GetCheckSumLocalWorkerStartUpObslotete(localWorkerStartUp LocalWorkerStartUp) string {
//	tmp := "ffsdsd2234&"
//	tmp = tmp +  strconv.Itoa(localWorkerStartUp.WorkerId) + "ddsdsds"
//	tmp = tmp +  localWorkerStartUp.SoftwareVersion + "944ms,"
//	tmp = tmp +  localWorkerStartUp.SoftwareDate + "w034s"
//	tmp = tmp +  localWorkerStartUp.Platform + "3732n dwd"
//	tmp = tmp +  localWorkerStartUp.TimeStamp + "ghdsa"
//	tmp = tmp +  localWorkerStartUp.StartTime +"2[323"
//	tmp = tmp +  localWorkerStartUp.HostName +"QwWW"
//	return utils.GetMD5Hash(tmp)
//
//}

// *************************************************************************************
// * NewWorkerCreation
// * Bericht waarmee vanaf de commandline een Worker kan worden aangemaakt
// * De CentralInstance reageert op dit bericht met een NewWorkerCreationReply
// *************************************************************************************
type NewWorkerCreation struct {
	WorkerId       int64
	RegistrationId string
	Name           string
	HostName       string
	Platform       string
	Description    string
	JobDirectory   string
	URL            string
}

type NewWorkerCreationReply struct {
	WorkerId    int64
	WorkerQueue string
	ExitCode    int
	ExitMsg     string
}

// *************************************************************************************
// * WorkerHeartBeat
// * Bericht waarmee Worker aangeeft dat hij actief is. Dit bericht wordt met een vaste
// * tussenpoos verstuurd naar de HUB
// *************************************************************************************
type WorkerHeartBeat struct {
	WorkerId  int64
	Load1     string
	Load5     string
	Load15    string
	TimeStamp string // TimeStamp aanmaak bericht
	CheckSum  string
}

// Functie voor het berekenen van de Checksum van het bericht
func GetCheckSumWorkerHeartBeat(workerHeartBeat WorkerHeartBeat) string {
	tmp := "ffsdsd2234&"
	tmp = tmp + strconv.FormatInt(workerHeartBeat.WorkerId, 10) + "ddsdsd2122s"
	tmp = tmp + workerHeartBeat.TimeStamp + "944ms,"
	return utils.GetMD5Hash(tmp)

}

// *************************************************************************************
// * WorkerConfiguration
// * Bericht waarmee de actuele configuratie van de Worker wordt doorgegeven aan de
// * CentralInstance.
// *************************************************************************************
type WorkerConfiguration struct {
	WorkerId             int64
	HostName             string
	Platform             string
	PlatformFamily       string
	PlatformVersion      string
	KernelVersion        string
	VirtualizationSystem string
	VirtualizationRole   string
	HostID               string
	StartupTimestamp     string
	JobDirectory         string
	SoftwareVersion      string
	SoftwareDate         string
	TimeStamp            string // TimeStamp aanmaak bericht
	CheckSum             string
}

// Functie voor het berekenen van de Checksum van het bericht
func GetCheckSumWorkerConfiguration(workerConfiguration WorkerConfiguration) string {
	tmp := "ffsdsde3234&"
	tmp = tmp + strconv.FormatInt(workerConfiguration.WorkerId, 10) + "ddgdfcvd2122s"
	tmp = tmp + workerConfiguration.TimeStamp + "944ms,"
	tmp = tmp + workerConfiguration.StartupTimestamp + "4rwerwerewrvb,"
	tmp = tmp + workerConfiguration.JobDirectory + "543lsdfl;;'["
	return utils.GetMD5Hash(tmp)

}

// *************************************************************************************
// * WorkerMetrics
// * bericht waarn de load statistieken van de laatste 1, 5 en 15 minuten worden gedeeld
// * Dit is alleen van toepassing op Workers op Linux / Unix platformen
// *************************************************************************************
type WorkerMetrics struct {
	WorkerId  int
	Load1     string
	Load5     string
	Load15    string
	TimeStamp string // TimeStamp aanmaak bericht
}

// Bericht waarmee vanaf de commandline van een Worker, de Worker kan worden aangemaakt in de
// CentralInstance. Dit bericht wordt ook gebruikt om de configuratie van de Worker op te slaan
type WorkerRegistration struct {
	WorkerId     string
	Name         string
	BusAddress   string
	SharedSecret string
	JobDirectory string
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////
// Berichtenverkeer vanuit CentralInstance gezien
///////////////////////////////////////////////////////////////////////////////////////////////////////////

// bericht om aan de HUB kenbaar te maken dat er een Worker moet worden toegevoegd
// aan de lijst met Workers die gemonitored worden.
type MonitorWorker struct {
	WorkerId         int64
	HeartBeatTimeOut int    // Tijd (in seconden) dat de laatste heartbeat oud mag zijn
	TimeStamp        string // TimeStamp aanmaak bericht
	CheckSum         string
}

// Functie voor het berekenen van de Checksum van het bericht
func GetCheckSumMonitorWorker(monitorWorker MonitorWorker) string {
	tmp := "ffsdsd2234&"
	tmp = tmp + strconv.FormatInt(monitorWorker.WorkerId, 10) + "ddsdsxxx"
	tmp = tmp + strconv.Itoa(monitorWorker.HeartBeatTimeOut) + "dasdcazx"
	tmp = tmp + monitorWorker.TimeStamp + "ghddsadas"
	return utils.GetMD5Hash(tmp)
}

// Bericht waarmee om een (versnelde) heartbeat wordt gevraagd. Dit bericht
// wordt verstuurd wanneer de Worker in de CentralInstance wordt gestart en
// dus zonde is om eventueel op het volgende heartbeat bericht te wachten
type RequestHeartBeatWorker123 struct {
	WorkerId int64
}

// Bericht om een JobAanvraag te versturen naar de Worker
type JobRequest struct {
	JobId        string
	JobSourceId  int64
	Worker       string
	JobType      int
	Name         string
	Description  string
	JobDirectory string
	SourceCode   DataWithSpecialCharacters
	TimeStamp    string // TimeStamp aanmaak bericht
	CheckSum     string
}

// Functie voor het berekenen van de Checksum van het bericht
func GetCheckSumJobRequest(jobRequest JobRequest) string {
	tmp := "dsa3e&"
	tmp = tmp + jobRequest.JobId + "223dfs/|"
	tmp = tmp + strconv.FormatInt(jobRequest.JobSourceId, 10) + "dasdadafefw"
	tmp = tmp + jobRequest.Name + "655dasJJKKK"
	tmp = tmp + jobRequest.Description + "32sAAs13s"
	tmp = tmp + jobRequest.JobDirectory + "1DSADASDSDA"
	//	tmp = tmp +  jobRequest.SourceCode + "oidsds0432"
	tmp = tmp + jobRequest.TimeStamp + "ghddsaasadas"
	return utils.GetMD5Hash(tmp)
}

// Bericht om de actuele configuratie van een Worker op te vragen
type RequestWorkerConfiguration struct {
	WorkerId  int64
	TimeStamp string // TimeStamp aanmaak bericht
	CheckSum  string
}

// Functie voor het berekenen van de Checksum van het bericht
func GetCheckSumRequestWorkerConfiguration(requestWorkerConfiguration RequestWorkerConfiguration) string {
	tmp := "dswfwfa3fewf&"
	tmp = tmp + strconv.FormatInt(requestWorkerConfiguration.WorkerId, 10) + "fsdfds80-[p';ds|"
	return utils.GetMD5Hash(tmp)
}


// Berichten voor het doorgeven van de Central Instance Configuratie vanuit de Enterprise Manager
// Dit bericht wordt opgevangen door de wrkcfg-agent en wanneer een nieuwe configuratie wordt ontvangen
// wordt dit direct actief gemaakt. Het configuratie bestand wordt opgeslagen in de cfg-directory
type CentralInstanceConfiguration struct {
	Name				string
	JobDirectory		string
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////
// Berichtenverkeer vanuit HUB gezien
///////////////////////////////////////////////////////////////////////////////////////////////////////////

// Bericht om de CentralInstance te informeren dat de status van een Worker is gewijzigd
type WorkerUpdateStatus struct {
	WorkerId     int
	ActualStatus string
	Comment      string
	TimeStamp    string // TimeStamp aanmaak bericht
	CheckSum     string
}

// Functie voor het berekenen van de Checksum van het bericht
func GetCheckSumWorkerUpdateStatus(workerUpdateStatus WorkerUpdateStatus) string {
	tmp := "ffsdsd2234&"
	tmp = tmp + strconv.Itoa(workerUpdateStatus.WorkerId) + "ddsddddsxxx"
	tmp = tmp + workerUpdateStatus.ActualStatus + "dasdcsasaazx"
	tmp = tmp + workerUpdateStatus.Comment + "3213s"
	tmp = tmp + workerUpdateStatus.TimeStamp + "ghddsaasadas"
	return utils.GetMD5Hash(tmp)
}

// ***********************************************************************************************
// * Hieronder is nog de oude code opgenomen die wanneer de Outer en Inner Loop functionaliteit
// * geheel is doorgevoerd, verwijderd kan worden. Hierboven staat dus de nieuwe code
// ***********************************************************************************************

// Parameter die aangeeft of de inhoud van een bericht getoond wordt
// wanneer het bericht wordt aangemaakt
var ShowContentMessage = true

// Tekst van het bericht die wordt gebruikt voor het pingen (bepalen of de Central Instance
// beschikbaar is). Dit soort berichten kunnen direct door de Central Instance worden verwijderd
var PingNaarCentralInstance = "<PingCentralInstance>"

// *************************************************************************************
// * Definities van de berichten die verstuurd worden tussen de HUB en Worker          *
// * en vice versa                                                                     *
// *************************************************************************************

// Bericht met details over hoe een job moet worden uitgevoerd. Dit bericht wordt aangemaakt
// op de CentralInstance en gaat via een HUB naar de aangewezen Worker. De HUB registeert
// alleen de job maar geef hem verder alleen maar door.
// De Worker wordt gespecificeerd in het veld Worker. Hierin staat het IP-adres en portNumber
var JobRunnerMetaDataVersion = "1.0"

type JobRunnerMetaData struct {
	JobID      string
	Worker     string
	MaxRunTime int
	JobType    int
	SourceCode DataWithSpecialCharacters
	TimeStamp  string
	CheckSum   string
	Version    string
}

// Message waarmee vanuit de Central Instance wordt aangegeven dat een Worker
// gestart of gestop kan worden. Per Worker wordt er 1 message gestuurd. Bij het starten
// van de Central Instance kan het dus voorkomen dat er meerdere berichten
// verstuurd worden. Vanuit de Central Instance wordt ook de WorkerID meegestuurd zodat
// wanneer deze ID niet bekend is bij de Worker, deze meteen opgeslagen kan worden
var ChangeWorkerStatusVersion = "1.0"

type ChangeWorkerStatus struct {
	WorkerID        string
	Worker          string
	RequestedStatus string
	TimeStamp       string
	CheckSum        string
	Version         string
}

// Bericht voor het doorgeven van een StatusWijziging in de Worker
// Dit bericht wordt vanaf een Worker via de HUB naar de CentralInstance
// gestuurd
type WorkerStatus struct {
	WorkerId        string
	ActualStatus    string
	RequestedStatus string
	WorkerAddress   string
	PortNumber      string
	Comment         string
}

// Message die wordt gebruikt voor het controleren of een Worker bereikbaar is gezien vanuit een HUB
// met het veld Sender wordt de hostname:portnumber van de HUB doorgegeven. Dit is van belang omdat
// de HUB geregisteerd wordt, wanneer deze nog niet bekend is
// Het veld Recipient is de hostname:portnumber van de Worker waar de Ping naar toe wordt gestuurd.
// Uit dit veld moet de Hostnaam worden geisoleerd. Het is namelijk zeer foutgevoelig als op de Worker
// zelf de hostnaam wordt bepaald.
var PingBerichtVersion = "1.0"

type PingBericht struct {
	WorkerId      string
	WorkerAddress string
	HUBAddress    string
	HUBPortNumber string
}

// Bericht waarmee wordt aangegeven (vanuit de CentralInstance) dat een Worker
// gestart moet worden. Dit houdt in dat de HUB de Worker gaat pingen. Er wordt
// ook het Id van de Worker meegegeven omdat dit de sleutel is om gegevens
// aan te passen wanneer er vanuit de Worker terugkoppeling wordt gegeven (bijvoorbeeld
// wanneer de configuration wordt opgestuurd)
type WorkerStartUpObsolete struct {
	Id            int
	WorkerAddress string
}

// Het WorkerConfiguration bericht wordt gebruikt om de configuratie van de Worker
// te delen met de Central Instance zodat daar inzichtelijk is wat de configuratie is
var WorkerConfigurationVersion = "1.0"

type WorkerRegistrationObsolete struct {
	Id               int
	WorkerAddress    string
	PortNumber       string
	StartTime        string
	SoftwareVersion  string
	SoftwareDate     string
	Platform         string
	CfgDirectory     string
	TmpDirectory     string
	MetaDirectory    string
	JobDirectory     string
	WorkingDirectory string
	HomeDirectory    string
	LastJobId        string
	LastVisitedTime  string
	LastVisitedHub   string
}

//type WorkerConfiguration struct {
//	WorkerID        string
//	Hostname        string
//	PortNumber      string
//	SoftwareVersion string
//	Platform        string
//	HomeDir         string
//	MetaDir         string
//	JobDir          string
//	CfgDir          string
//	TmpDir          string
//	StartTime       string
//	TimeStamp       string
//	CheckSum        string
//	Version         string
//}

// XML-structuur om de status van een Worker door te geven. Dit bericht wordt
// vanuit de HUB naar de Central Instance gestuurd op basis van de PINGs naar
// de verschillende Workers. Dit bericht wordt ook gebruikt om vanuit de Central Instance
// aan te geven welke Workers een andere status krijgen (een Worker wordt bijvoorbeeld
// vanuit de Central Instance gestopt). Daarnaast wordt dit bericht gebruikt om in de
//situatie dat een HUB zich registreert, dat er word doorgegeven wat de actieve Workers zijn.
// Dit bericht wordt dan in een tijdelijke dedicated queue (HubID) doorgegeven
var WorkerStatusVersion = "1.0"

//type WorkerStatus struct {
//	Hostname   string
//	PortNumber string
//	Status     string
//	TimeStamp  string
//	Version    string
//}

// XML-Structuur voor de registratie van een HUB bij de Central Instance
// Dit bericht moet in een dedicated (tijdelijke) queue worden verstuurd
// Wanneer dit bericht namelijk door de CentralInstance wordt ontvangen, worden
// er een aantal berichten (bijvoorbeeld welke Workers er zijn) gestuurd naar
// deze specifieke HUB
var HubRegistrationVersion = "1.0"

type HubRegistration struct {
	HubID     string
	TimeStamp string
	CheckSum  string
	Version   string
}

// Structuur om een JobDefinition in op te slaan. Vanuit de WebServer wordt een
// JobSourceDefinition op deze manier weergegeven. Dit bericht kan in een later
// stadium ook worden gebruikt om Promoties mee uit te voeren
var JobSourceDefinitionVersion = "1.0"

type JobSourceDefinition struct {
	Partition    string
	Application  string
	Name         string
	Description  string
	JobType      int
	DefaultQueue string
	SourceCode   string
	CheckSum     string
	Version      string
}

// ***************************************************************************
// *                                                                         *
// * BerichtDefinities voor het afhandelen van GUI verzoeken                 *
// *                                                                         *
// ***************************************************************************

// Bericht voor het aanmaken van een nieuwe Worker
var CreateWorkerVersion = "1.0"

type CreateWorker struct {
	Name        string
	Hostname    string
	PortNumber  string
	AutoStart   string
	CheckSum    string
	Version     string
	TimeStamp   string
	Partition   int
	Application int
	UserID      int
}

// Bericht voor het verwijderen van een Worker
var DeleteWorkerVersion = "1.0"

type DeleteWorker struct {
	Hostname   string
	PortNumber string
	CheckSum   string
	Version    string
	TimeStamp  string
	UserID     int
}

// Bericht voor het stoppen van een Worker
var StopWorkerVersion = "1.0"

type StopWorker struct {
	Hostname   string
	PortNumber string
	CheckSum   string
	Version    string
	TimeStamp  string
	UserID     int
}

// Bericht voor het starten van een Worker
var StartWorkerVersion = "1.0"

type StartWorker struct {
	Hostname   string
	PortNumber string
	CheckSum   string
	Version    string
	TimeStamp  string
	UserID     int
}

// Definitie voor het doorgeven

type GuiRequestJobSource struct {
	JobSource []GuiRequestJobSourceDetail
}

type GuiRequestJobSourceDetail struct {
	Id          string
	Partition   string
	Application string
	Name        string
	Description string
	ScriptType  string
}

// ****************************************************************************
// *
// * Berichten voor Jobs
// *
// ****************************************************************************

// Bericht van het aanmaken van een Job
type JobRequestObsolete struct {
	JobId         string
	JobType       string
	JobName       string
	WorkerAddress string
	JobDirectory  string
	MetaDirectory string
	JobEnv        DataWithSpecialCharacters
	JobSrc        DataWithSpecialCharacters
	MaxRunTime    int
}

// bericht dat vanuit de Worker via de HUB naar de CentralInstance wordt verstuurd
// om te informeren dat de aangevraagde job is gestart
type JobStart struct {
	JobId         string
	WorkerAddress string
}

// Bericht dat wordt verstuurd door de Worker via een HUB naar de CentralInstance
// om te melden dat de Job een eindstatus heeft bereikt
type JobCompletion struct {
	JobId         string
	WorkerId      int
	WorkerAddress string
	PortNumber    string
	ExitCode      string
	ExecutionLog  string
	Status        string
	ElapsedTime   string
	OutFileSize   string
	OutFileName   string
	ErrFileSize   string
	ErrFileName   string
}

// ****************************************************************************
// *
// * Berichten voor Partitions
// *
// ****************************************************************************

// PartitionCreateMessage
var PartitionCreateVersion = "1.0"

type PartitionCreate struct {
	Name              string
	Description       string
	DefaultPartition  string
	InternalPartition string
	CheckSum          string
	Version           string
	TimeStamp         string
	UserID            int
}

// PartitionDeleteMessage
var PartitionDeleteVersion = "1.0"

type PartitionDelete struct {
	Name      string
	CheckSum  string
	Version   string
	TimeStamp string
	UserID    int
}

// PartitionModifyMessage
var PartitionModifyVersion = "1.0"

type PartitionModify struct {
	Name              string
	Description       string
	DefaultPartition  string
	InternalPartition string
	CheckSum          string
	Version           string
	TimeStamp         string
	UserID            int
}

// XML-Structuur om een Partition te exporteren
type PartitionXML struct {
	FQName      string
	Name        string
	Description string
	Internal    string
	Default     string
	//ExportDate  string
}

// ****************************************************************************
// *
// * Berichten voor Users
// *
// ****************************************************************************

// CreateUserMessage
// De default Partition en Applicatie worden in de vorm van een string doorgegeven
// (naam van het Object). Bij het opslaan van de Gebruiker wordt bepaald welk Id
// hierbij hoort
var CreateUserVersion = "1.0"

type CreateUser struct {
	AccountName        string
	PasswordHash       string
	Firstname          string
	Lastname           string
	Emailaddress       string
	DefaultPartition   string
	DefaultApplication string
}

type DataWithSpecialCharacters string

func (n DataWithSpecialCharacters) MarshalXML(e *xml.Encoder, start xml.StartElement) error {
	return e.EncodeElement(struct {
		S string `xml:",innerxml"`
	}{
		S: "<![CDATA[" + string(n) + "]]>",
	}, start)
}
