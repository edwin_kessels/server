drop schema centralinstance cascade;
create schema centralinstance ;
 
--DROP TABLE centralinstance.environment CASCADE; 
--CREATE TABLE centralinstance.environment (
--  environment_id varchar(32) 
--) ;
 
--insert into centralinstance.environment values ('T-ENV') ;

--DROP TABLE centralinstance.worker CASCADE; 
CREATE TABLE centralinstance.worker_old (
    worker_id bigint primary key,
    worker_protocol varchar(32) NOT NULL,
    worker_name varchar(64) NOT NULL,
    worker_description varchar(128) NOT NULL,
    worker_hostname varchar(128) NOT NULL,
    worker_queue varchar(80) NOT NULL,
    worker_fqname varchar(135) ,
    worker_status_requested varchar(32) default 'nil',
    worker_status_actual varchar(32) default 'nil',
    worker_autostart varchar(3) default 'NO',
    worker_maxjobs integer default 1000, 
    worker_partition bigint NOT NULL,
    worker_application bigint NOT NULL,
    worker_deleted varchar(3) default 'NO'
);


CREATE TABLE centralinstance.worker (
    worker_id               bigint primary key,
    worker_name             varchar(64) NOT NULL,
    worker_fqname           varchar(135),
    worker_description      varchar(128),
    worker_hostname         varchar(128),
    worker_esb_queue        varchar(80),
    worker_status_requested varchar(32) default 'nil',
    worker_status_actual    varchar(32) default 'nil',
    worker_autostart        varchar(3) default 'NO',
    worker_maxjobs          integer default 1000, 
    worker_partition        bigint NOT NULL,
    worker_application      bigint NOT NULL,
    worker_deleted          varchar(3) default 'NO', 
    worker_platform         varchar(32) default 'Unknown', 
    worker_starttime        varchar(32)
);


-- Tabel voor het opnemen van een opmerking over de Worker
CREATE TABLE centralinstance.workercomment (
    worker_id               bigint,
    worker_comment_actual   varchar(3) default 'YES',
    comment_timestamp       varchar(32),
    worker_comment          varchar(255)
);
 

CREATE TABLE centralinstance.audit (
    audit_id                bigserial primary key,
    audit_user_id           bigint NOT NULL,
    audit_object_type       bigint NOT NULL,
    audit_object_name       varchar(128),  
    audit_object_id         bigint NOT NULL,
    audit_object_fqname     varchar(135) ,
    audit_object_action     varchar(10) NOT NULL,
    audit_timestamp         varchar(32)
); 

CREATE TABLE centralinstance.audit_diff (
    audit_id                bigint primary key,
    audit_before_image      varchar,
    audit_after_image       varchar
);
 
 
CREATE TABLE centralinstance.workerenvironment (
  workerenv_id           bigserial primary key,
  workerenv_worker_id    bigint,
  workerenv_parameter    varchar(128),
  workerenv_value        varchar(256) 
);


CREATE TABLE centralinstance.workermetric (
    worker_id           bigint primary key,
    metric_load01       int,
    metric_load05       int,
    metric_load15       int,
    metric_timestamp    varchar(32)
);


CREATE TABLE centralinstance.workerconfiguration (
  workercfg_id                      bigint primary key, 
  workercfg_platform                varchar(255),                  
  workercfg_platform_family         varchar(255),
  workercfg_platform_version        varchar(128),
  workercfg_kernel_version          varchar(128),
  workercfg_hostname                varchar(255),
  workercfg_host_id                 varchar(255),
  workercfg_virtualization_system   varchar(255),
  workercfg_virtualization_role     varchar(255),
  workercfg_softwareversion         varchar(64),
  workercfg_date                    varchar(20),
  workercfg_jobdir                  varchar(255)
);


insert into centralinstance.workerenvironment (workerenv_worker_id, workerenv_parameter, workerenv_value) values (1,'TMPDIR','/tmp') ; 

--DROP TABLE centralinstance.application CASCADE; 
CREATE TABLE centralinstance.application (
    application_id        bigserial primary key,
    application_name      varchar(64) NOT NULL,
    application_fqname         varchar(128) NOT NULL,
    application_description    varchar(128),
    application_default   varchar(3) NOT NULL default 'NO',
    application_internal  varchar(3) NOT NULL default 'NO',
    application_deleted   varchar(3) default 'NO'
) ;

--insert into centralinstance.application (application_name, application_fqname, application_default) values ('System', 'SYSTEM', 'YES') ; 


CREATE TABLE centralinstance.partition (
    partition_id             bigint primary key,
    partition_fqname         varchar(128) NOT NULL,
    partition_name           varchar(64) NOT NULL,
    partition_description    varchar(128),
    partition_default        varchar(3) NOT NULL default 'NO',
    partition_public         varchar(3) NOT NULL default 'NO',
    partition_internal       varchar(3) NOT NULL default 'NO',
    partition_deleted        varchar(3) default 'NO'
) ; 

-- Tabel voor het opslaan van de toegangsrechten tot de partition
CREATE TABLE centralinstance.partition_priv (
    partition_id             bigint,
    user_id                  bigint,
    access_mode              integer
) ; 



CREATE TABLE centralinstance.user (
    user_id	                    bigserial primary key,
    user_hash	                varchar(64)	NOT NULL,
    user_account	            varchar(64) NOT NULL,
    user_password	            varchar(64)	NOT NULL,
    user_firstname              varchar(32)	NOT NULL,
    user_lastname               varchar(64)	NOT NULL,
    user_email	                varchar(128) NOT NULL,
    user_token	                varchar(15),
    user_token_expire           varchar(20), 
    TwoFactorAuthentication     varchar(3) NOT NULL default 'YES',
    user_status	    	        integer NOT NULL default 0,
    user_default_partition	    bigint default 0, 
    user_default_application	bigint default 0, 
    user_profile_id	            bigint default 0, 
    user_last_login	            varchar(20) default 'NEVER', 
    user_system_user            varchar(3) NOT NULL default 'NO',
    user_deleted	            varchar(3) NOT NULL default 'NO'
) ; 

-- System Gebruiker aanmaken. Deze wordt aangemaakt met het wachtwoord NULL
-- Bij het opstarten van de CentralInstance moet deze gebruiker worden gecontroleerd
insert into centralinstance.User (user_hash, user_account, user_password, user_firstname, user_lastname, user_email, user_default_partition, user_default_application, TwoFactorAuthentication,user_system_user) values ('NULL', 'SYSTEM', 'NULL', 'NULL', 'NULL', 'NULL', 0, 0, 'NO','YES'); 


CREATE TABLE centralinstance.jobsource (
    jobsrc_id               bigint primary key,
    jobsrc_partition_id     bigint NOT NULL,
    jobsrc_application_id   bigint NOT NULL,
	jobsrc_name             varchar(64),
	jobsrc_fqname           varchar(128),
	jobsrc_description      varchar(128),
	jobsrc_type             int, 
	jobsrc_majorversion     varchar(3) default '1',
	jobsrc_minorversion      varchar(3) default '0',
--	jobsrc_revision         varchar(3),
--	jobsrc_sourcecode_id    bigint,
    jobsrc_sourcecode       text, 
	jobsrc_queue_id         bigint default 0,
	jobsrc_timewindow_id    bigint default 0,
	jobsrc_priority         int default 50,
	jobsrc_maxruntime       int default 0,
	jobsrc_severity         int default 0, 
	jobsrc_currentversion   varchar(3) default 'YES',
	jobsrc_audit            varchar(3) default 'YES',
	jobsrc_deleted          varchar(3) default 'NO'
);

CREATE TABLE centralinstance.jobsourceparameter (
    jobsrcpar_id               bigserial primary key,
    jobsrcpar_jobsrc_id        bigint NOT NULL,
    jobsrcpar_name             varchar(64), 
    jobsrcpar_description      varchar(128), 
    jobsrcpar_format           varchar(128), 
    jobsrcpar_order            int, 
    jobsrcpar_default_value    varchar(1024), 
    jobsrcpar_expression       varchar(3) default 'NO',
    jobsrcpar_visible          varchar(3) default 'YES',
    jobsrcpar_scope            varchar(3) default 'IN',
    jobsrcpar_deleted          varchar(3) default 'NO'
) ; 

--insert into centralinstance.jobsource (jobsrc_partition,jobsrc_application,jobsrc_name,jobsrc_description,jobsrc_majorversion,jobsrc_minorversion,jobsrc_revision,jobsrc_sourcecode,jobsrc_type) 
--values (1,1,'SystemWait','Wait for 10 seconds','1','0','001','sleep 10',1) ;
--insert into centralinstance.jobsource (jobsrc_partition,jobsrc_application,jobsrc_name,jobsrc_description,jobsrc_majorversion,jobsrc_minorversion,jobsrc_revision,jobsrc_sourcecode,jobsrc_type) 
--values (1,1,'DirList','Echo Dirlisting to file','1','0','001','ls -ltr > dir.txt',1) ;
	

--DROP TABLE centralinstance.job CASCADE; 
CREATE TABLE centralinstance.job (
    job_id             bigserial primary key,
    job_jobsrc_id      bigint NOT NULL,
	job_partition      bigint default 0,
    job_application    bigint default 0,
    job_type           int, 
	job_name           varchar(128),
	job_description    varchar(128),
	job_queue          bigint,
	job_worker         bigint,
	job_timewindow     bigint,
	job_status         varchar(32) default 'Created', 
	job_exitcode       varchar(32), 
	job_priority       int default 0,
	job_maxruntime     int default 0, 
	job_runstart       varchar(32),
	job_runend         varchar(32), 
	job_elapsedtime    varchar(32), 
	job_xml            text
	) ;
	
CREATE TABLE centralinstance.jobparameter (
    jobpar_id          bigserial primary key,
    jobpar_jobid       bigint, 
    jobpar_name        varchar(64), 
    jobpar_value       varchar(1024), 
    jobpar_visible     varchar(3)
) ; 

--DROP TABLE centralinstance.joblog CASCADE; 
CREATE TABLE centralinstance.joblog (
    job_id             bigint primary key,
    job_log            text
);

--DROP TABLE centralinstance.jobfile CASCADE; 
CREATE TABLE centralinstance.jobfile (
    jobfile_id         bigserial primary key,    
    jobfile_job_id     bigint,
    jobfile_type       int, 
    jobfile_size       varchar(12) , 
    jobfile_filename   varchar(1024) 
); 
	
	
	
--DROP TABLE centralinstance.metaobject CASCADE; 
CREATE TABLE centralinstance.metaobject (
    object_id                   bigserial primary key,
    object_type                 int NOT NULL,
    object_partition            bigint NOT NULL,
    object_application          bigint NOT NULL,
    object_hash                 varchar(32), 
    object_readonly             varchar(3), 
    object_date_created         varchar(20) default '', 
    object_user_created         bigint default 0,
    object_date_modified        varchar(20) default '',
    object_user_modified        bigint default 0,
    object_checkout_time        varchar(20) default '',
    object_checkout_user_id     bigint default 0 
	) ;
	
CREATE TABLE centralinstance.jobsource_type (
    jobsource_type_id      bigint primary key,
    jobsource_name         varchar(32), 
    jobsource_description  varchar(64), 
    jobsource_worker_type  varchar(64) 
) ;	

insert into centralinstance.jobsource_type (jobsource_type_id , jobsource_name, jobsource_description, jobsource_worker_type) values (1000, 'Linux Shell script', 'Linux Shell script (sh)','Linux')	;


CREATE TABLE centralinstance.agent (
    agent_id                varchar(12) primary key,
    agent_software_version  varchar(64), 
    agent_timestamp         varchar(20),
    agent_last_activity     varchar(20),
    agent_stop              varchar(3) default 'NO',
    agent_num_restart       int default 0,
    agent_active            varchar(3) default 'YES'
) ; 

insert into  centralinstance.agent (agent_id) values ('wrkcfg') ; 
insert into  centralinstance.agent (agent_id) values ('wrkstat') ; 
insert into  centralinstance.agent (agent_id) values ('wrkstrt') ; 
insert into  centralinstance.agent (agent_id) values ('wrkreg') ; 
insert into  centralinstance.agent (agent_id) values ('wrkmetric') ; 
insert into  centralinstance.agent (agent_id) values ('wrkhrtbeat') ; 
insert into  centralinstance.agent (agent_id) values ('jobstat') ; 
--insert into  centralinstance.agent (agent_id) values ('hubreg') ; 
--insert into  centralinstance.agent (agent_id) values ('jobcomp') ; 
--insert into  centralinstance.agent (agent_id) values ('smon') ; 

--create view centralinstance.v_jobsources as
--select a.jobsrc_id,
--       b.partition_name,
--       c.application_name,
--	     a.jobsrc_name,
--	     a.jobsrc_description,
--	     a.jobsrc_type, 
--	     a.jobsrc_majorversion||'.'||a.jobsrc_minorversion||'.'||a.jobsrc_revision as jobsrc_version
--	     a.jobsrc_sourcecode 
--	from centralinstance.jobsource a,
--	     centralinstance.partition b,
--	     centralinstance.application c
--	where a.jobsrc_partition = b.partition_id     
--	and   a.jobsrc_application = c.application_id 
--	and   a.	jobsrc_currentversion='YES' 
--	order by partition_name, application_name, jobsrc_name; 

ALTER SEQUENCE centralinstance.metaobject_object_id_seq START 1000000000000000000 ;
ALTER SEQUENCE centralinstance.metaobject_object_id_seq RESTART ;
 
GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA centralinstance TO sched20;
GRANT ALL PRIVILEGES ON SCHEMA centralinstance TO sched20;
--GRANT USAGE, SELECT ON SEQUENCE centralinstance.worker_worker_id_seq TO sched20;
GRANT USAGE, SELECT ON SEQUENCE centralinstance.job_job_id_seq TO sched20;
--GRANT USAGE, SELECT ON SEQUENCE centralinstance.jobsource_jobsrc_id_seq TO sched20;
GRANT USAGE, SELECT ON SEQUENCE centralinstance.application_application_id_seq TO sched20;
--GRANT USAGE, SELECT ON SEQUENCE centralinstance.partition_partition_id_seq TO sched20;
GRANT USAGE, SELECT ON SEQUENCE centralinstance.workerenvironment_workerenv_id_seq TO sched20;
GRANT USAGE, SELECT ON SEQUENCE centralinstance.metaobject_object_id_seq TO sched20;
GRANT USAGE, SELECT ON SEQUENCE centralinstance.user_user_id_seq TO sched20;
GRANT USAGE, SELECT ON SEQUENCE centralinstance.audit_audit_id_seq TO sched20;
GRANT USAGE, SELECT ON SEQUENCE centralinstance.jobfile_jobfile_id_seq TO sched20;
GRANT USAGE, SELECT ON SEQUENCE centralinstance.jobsourceparameter_jobsrcpar_id_seq TO sched20;
GRANT USAGE, SELECT ON SEQUENCE centralinstance.jobparameter_jobpar_id_seq TO sched20;

