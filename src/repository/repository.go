package repository

import "database/sql"

var (
	// DBCon is the connection handle for the database
	DBConnection *sql.DB
)

// ********************************************************************************
// *                                                                              *
// *  Stucturen voor de onderliggende tabellen in de repository                   *                                                                            *
// *                                                                              *
// ********************************************************************************

// Record voor het opslaan van een Job. Deze structuur bestaat deels uit normale
// velden die op een relationele manier worden gevuld. Daarnaast wordt alle informatie
// noodzakelijk is voor het uitvoeren van de job door een Worker, 'klaargezet' in een XML.
// Wanneer er iets wijzigt in de 'metadata' van de job, moet de XML opnieuw opgebouwd worden
// en worden opgeslagen in de Repository.
type RecordJob struct {
	JobID         int
	Partition     int
	Application   int
	Name          string
	Queue         int
	TimeWindow    int
	Description   string
	Status        int
	RunStart      string
	RunEnd        string
	VersionSource string
	XML           string
}

var RepositorySchema = "centralinstance."

// SQL-Commando's
// sqlCmd_wrk_001 : SQL Commando om te bepalen of een Worker er al bestaat (op basis van hostname / portnumber)
// sqlCmd_wrk_002 : SQL Commando om een Worker toe te voegen aan de tabel worker
// sqlCmd_wrk_003 : SQL Commando om de configuratie van een Worker bij te werken
// sqlCmd_wrk_004 : SQL Commando om de status van running Workers op stopped te zetten
// sqlCmd_wrk_005 : SQL Commando om de workers te selecteren waarvoor autostart is ingesteld
// sqlCmd_wrk_006 : SQL Commando om de Requested en Actual status van een Worker aan te passen
// sqlCmd_wrk_007 : SQL Commando om de Actual status van een Worker aan te passen
// sqlCmd_wrk_008 : SQL Commando om te bepalen of een Worker er al bestaat (op basis van naam van de worker)
// sqlCmd_wrk_009 : SQL Commando om zowel de actual als requested status te zetten voor alle niet-verwijderde Workers
// sqlCmd_wrk_010 : SQL Commando om een Worker te verwijderen (eigenlijk op inactiief te zetten)

// sqlCmd_app_001 : SQL Commando om de default applicatie op te halen
// sqlCmd_app_002 : SQL Commando om de Naam van de application op te halen op basis van de ID

// sqlCmd_job_001 : SQL Commando om vanuit de JobSource een Job aan te maken
// sqlCmd_job_002 : SQL Commando om gegevens van een Job op te halen om de XML te kunnen genereren
// sqlCmd_job_003 : SQL Commando om de JobXML (bericht om job te starten) op te slaan in het Job-record

// sqlCmd_par_001: SQL Commando om een nieuwe Partition toe te voegen
// sqlCmd_par_002: SQL Commando om de default-indicator van partities op NO te zetten
// sqlCmd_par_003: SQL Commando om de gegevens van de Partition op te halen op basis van de naam
// sqlCmd_par_004: SQL Commando om een bestaande Partition bij te werken
// sqlCmd_par_005: SQL Commando om op basis van de PartitionID, de naam van de Partition op te halen
// sqlCmd_par_006: SQL Commando om te bepalen hoeveel objecten er zijn toegewezen aan de Partition
// sqlCmd_par_007: SQL Commando om een Partition op indicatie deleted te zetten
// sqlCmd_par_008: SQL Commando om de gegevens van de Partition op te halen op basis van de Id

// SQL Commando's die betrekking hebben op MetaObject
// ================================================================================================
// sqlcmd_met_001 : SQL Commando om een MetaObject toe te voegen
// sqlcmd_met_002 : SQL Commando om een MetaObject in te lezen op basis van de ID
// sqlcmd_met_003 : SQL Commando om een gewijzigd MetaObject op te slaan
// sqlcmd_met_004 : SQL Commando om een  MetaObject te verwijderen

// SQL Commando's die betrekking hebben op Users
// ================================================================================================
// sqlcmd_usr_001 : SQL Commando om een User toe te voegen
// sqlcmd_usr_002 : SQL Commando om de gegevens ven een User op te halen op basis van UserId
// sqlcmd_usr_003 : SQL Commando om op basis van de AccountName de Id op te halen
// sqlcmd_usr_004 : SQL Commando om een UserAccount (logisch) te verwijderen

var SqlCmd_wrk_001 = "select count(*) as cnt from " + RepositorySchema + "worker where worker_hostname=$1 and worker_portnumber=$2 and worker_deleted=$3"
var SqlCmd_wrk_002 = "insert into " + RepositorySchema + "worker (worker_name, worker_hostname, worker_portnumber, worker_date_created, worker_user_created, worker_partition, worker_application,worker_autostart, worker_fqname)  values ($1, $2, $3, $4, $5, $6, $7, $8, $9)"
var SqlCmd_wrk_003 = "update " + RepositorySchema + "worker set worker_platform=$1, worker_status_actual=$2, worker_cfg_dir=$3, worker_job_dir=$4, worker_tmp_dir=$5, worker_meta_dir=$6, worker_software_version=$7 where worker_hostname=$8 and worker_portnumber=$9"
var SqlCmd_wrk_004 = "update " + RepositorySchema + "worker set worker_status_actual=$1 where worker_status_actual=$2"
var SqlCmd_wrk_005 = "select worker_hostname, worker_portnumber from " + RepositorySchema + "worker where worker_autostart=$1 and worker_deleted=$2"
var SqlCmd_wrk_006 = "update " + RepositorySchema + "worker set worker_status_actual=$1, worker_status_requested=$2 where worker_hostname=$3 and worker_portnumber=$4"
var SqlCmd_wrk_007 = "update " + RepositorySchema + "worker set worker_status_actual=$1 where worker_hostname=$2 and worker_portnumber=$3"
var SqlCmd_wrk_008 = "select count(*) as cnt from " + RepositorySchema + "worker where worker_name=$1 and worker_deleted=$2"
var SqlCmd_wrk_009 = "update " + RepositorySchema + "worker set worker_status_actual=$1, worker_status_requested=$1  where worker_deleted=$2"
var SqlCmd_wrk_010 = "update " + RepositorySchema + "worker set worker_deleted=$1  where worker_hostname=$2 and worker_portnumber=$3"

// Opslaan van een nieuwe worker
var SqlCmd_wrk_011 = "insert into " + RepositorySchema + "worker (worker_id, worker_name, worker_description, worker_hostname,  worker_partition, worker_application, worker_autostart, worker_fqname, worker_esb_queue, worker_platform)  values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)"

// Ophalen van de actieve Workers om in een Combobox te tonen
var SqlCmd_wrk_012 = "select worker_id, worker_name, worker_description, worker_fqname from " + RepositorySchema + "worker where worker_deleted=$1 order by worker_fqname"

// Ophalen van een Worker op basis van de Id van de Worker
var SqlCmd_wrk_013 = "select worker_name,worker_description,worker_hostname,worker_fqname, worker_status_requested,worker_status_actual,worker_autostart, worker_maxjobs, worker_partition,worker_application,worker_deleted, worker_esb_queue,  COALESCE(worker_starttime, '') as worker_starttime from " + RepositorySchema + "worker where worker_id=$1"

// SQL om de Actual Status van een Worker te wijzigen
var SqlCmd_wrk_014 = "update " + RepositorySchema + "worker set worker_status_actual=$1 where worker_id=$2"

// SQL om de Requested Status van een Worker te wijzigen
var SqlCmd_wrk_015 = "update " + RepositorySchema + "worker set worker_status_requested=$1 where worker_id=$2"

// SQL om alle Workers op shutdown te zetten
var SqlCmd_wrk_016 = "update " + RepositorySchema + "worker set worker_status_actual=$1, worker_status_requested=$2"

// SQL om de ACTUAL=No te zetten voor alle comments van de Worker
var SqlCmd_wrk_017 = "update " + RepositorySchema + "workercomment set worker_comment_actual=$1, worker_id=$2"

// SQL om een nieuwe Comment toe te voegen
var SqlCmd_wrk_018 = "insert into " + RepositorySchema + "workercomment (worker_id, comment_timestamp, worker_comment) values ($1, $2, $3)"

// SQL om de Workers te selecteren waarvoor AutoStart is ingesteld
var SqlCmd_wrk_019 = "select worker_id from " + RepositorySchema + "worker where worker_autostart=$1 and worker_deleted=$2"

// SQL om te controleren of een bepaalde Worker (op basis van FQName) al voorkomt
var SqlCmd_wrk_020 = "select worker_id from " + RepositorySchema + "worker where worker_fqname=$1 and worker_deleted=$2"

// SQL om de Hostname van de Worker te actualiseren
var SqlCmd_wrk_021 = "update " + RepositorySchema + "worker set worker_hostname=$1  where worker_id=$2"

// SQL om de Status van de Workers in HTML te tonen
var SqlCmd_wrk_022 = "select worker_id, worker_name, worker_fqname, worker_status_actual, worker_status_requested, worker_hostname, worker_platform from " + RepositorySchema + "worker where worker_deleted=$1 order by worker_name"

// SQL om alle Workers op te halen die een bepaalde status hebben
var SqlCmd_wrk_024 = "select worker_id, worker_name, worker_fqname from " + RepositorySchema + "worker where worker_status_actual=$1 and worker_deleted=$2 order by worker_name"

// SQL om de Id van een Worker te bepalen op basis van de FQName
var SqlCmd_wrk_025 = "select worker_id from " + RepositorySchema + "worker where worker_fqname=$1"

// SQL om de Starttijd van de Worker in te stellen
var SqlCmd_wrk_026 = "update " + RepositorySchema + "worker set worker_starttime=$1  where worker_id=$2"

// SQL Commando's die betrekking hebben op Worker Metrics
// SQL om de huidige Metrics van een Worker te verwijderen
var SqlCmd_wrkmet_001 = "delete from " + RepositorySchema + "workermetric where worker_id=$1"

// SQL om Metrics op te slaan voor een specifieke Worker
var SqlCmd_wrkmet_002 = "insert into " + RepositorySchema + "workermetric values ($1, $2, $3, $4, $5)"

// SQL om de Metrics van een Worker op te halen
var SqlCmd_wrkmet_003 = "select metric_load01, metric_load05, metric_load15 from " + RepositorySchema + "workermetric where worker_id=$1"

// SQL Commando's die betrekking hebben op Worker Configuration
// SQL om een bestaande Worker Configuration te verwijderen voordat er een nieuwe wordt toegevoegd
var SqlCmd_wrkcfg_001 = "delete from " + RepositorySchema + "workerconfiguration where workercfg_id=$1"

// SQL om een nieuwe Worker Configuration toe te voegen
var SqlCmd_wrkcfg_002 = "insert into " + RepositorySchema + "workerconfiguration (workercfg_id, workercfg_platform, workercfg_platform_family, workercfg_platform_version, workercfg_kernel_version, workercfg_hostname, workercfg_host_id, workercfg_virtualization_system,workercfg_virtualization_role, workercfg_softwareversion, workercfg_date, workercfg_jobdir) values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12)"

// SQL om De WorkerCOnfiguration uit te lezen om te tonen in de details van een Worker
var SqlCmd_wrkcfg_003 = "select workercfg_platform, workercfg_platform_family, workercfg_platform_version, workercfg_kernel_version, workercfg_host_id, workercfg_virtualization_system, workercfg_virtualization_role, workercfg_softwareversion, workercfg_date, workercfg_jobdir from " + RepositorySchema + "workerconfiguration where workercfg_id=$1"

var SqlCmd_app_001 = "select application_id, application_name from " + RepositorySchema + "application where application_default=$1"

var SqlCmd_app_002 = "select application_name from " + RepositorySchema + "application where application_id=$1"

var SqlCmd_job_001 = "insert into centralinstance.job (job_jobsrc_id, job_partition, job_application, job_name, job_description, job_type) select jobsrc_id, jobsrc_partition, jobsrc_application, jobsrc_name, jobsrc_description, jobsrc_type from centralinstance.jobsource where jobsrc_id=$1 RETURNING job_id"

var SqlCmd_job_002 = "select jobsrc_type, jobsrc_sourcecode from " + RepositorySchema + "jobsource where jobsrc_id=$1"

var SqlCmd_job_003 = "update " + RepositorySchema + "job set job_XML=$1 where job_id=$2"

var SqlCmd_par_001 = "insert into " + RepositorySchema + "partition values ($1, $2, $3, $4, $5, $6, $7, $8)"
var SqlCmd_par_002 = "update " + RepositorySchema + "partition set partition_default=$1"
var SqlCmd_par_003 = "select partition_id, partition_description, partition_default, partition_internal, partition_fqname, partition_name, partition_public from " + RepositorySchema + "partition where partition_fqname=$1 and partition_deleted=$2"
var SqlCmd_par_004 = "update " + RepositorySchema + "partition set partition_description=$1, partition_default=$2, partition_internal=$3,partition_deleted=$4, partition_fqname=$6, partition_public=$7 where partition_id=$5"
var SqlCmd_par_005 = "select partition_name from " + RepositorySchema + "partition where partition_id=$1"
var SqlCmd_par_006 = "select count(*) as cnt from " + RepositorySchema + "metaobject where object_partition=$1"
var SqlCmd_par_007 = "update " + RepositorySchema + "partition set partition_deleted=$1, partition_fqname=$3  where partition_id=$2"
var SqlCmd_par_008 = "select partition_name, partition_description, partition_default, partition_internal, partition_fqname, partition_name, partition_public from " + RepositorySchema + "partition where partition_id=$1 and partition_deleted=$2"
var SqlCmd_par_009 = "select partition_name from " + RepositorySchema + "partition where partition_id=$1 and partition_deleted=$2"

// SQL om bestaande privileges van de gebruiker op de Partition te verwijderen
var SqlCmd_par_010 = "delete from " + RepositorySchema + "partition_priv where partition_id=$1 and user_id=$2"

// SQL om een grant toe te kennen op een Partition
var SqlCmd_par_011 = "insert into " + RepositorySchema + "partition_priv values ($1, $2, $3)"

// Query voor het ophalen van de Partitions voor het opbouwen van een ListBox tbv de WebServer
var SqlCmd_par_012 = "select partition_id, partition_name, partition_description from " + RepositorySchema + "partition where partition_internal=$1 and partition_deleted=$2 order by partition_name"

// SQL Commando's die betrekking hebben op MetaObject
// ================================================================================================
var SqlCmd_met_001 = "insert into " + RepositorySchema + "metaobject (object_type, object_partition, object_application, object_hash, object_date_created, object_user_created, object_readonly) values ($1, $2, $3, $4, $5, $6, $7) RETURNING object_id"
var SqlCmd_met_002 = "select object_type, object_partition, object_application, object_hash, object_readonly, object_date_created, object_user_created, object_date_modified, object_user_modified from " + RepositorySchema + "metaobject where object_id=$1"
var SqlCmd_met_003 = "update " + RepositorySchema + "metaobject set object_partition=$1, object_application=$2, object_hash=$3, object_readonly=$4, object_date_modified=$5, object_user_modified=$6 where object_id=$7"
var SqlCmd_met_004 = "delete from " + RepositorySchema + "metaobject where object_id=$1"

// Controleren of een Partition Objecten heeft
var SqlCmd_met_005 = "select count(*) as cnt from " + RepositorySchema + "metaobject where object_partition=$1"

// SQL om de MetaData aan te passen wanneer een gewijzigd object (bijvoorbeeld JobSource) wordt opgeslagen
var SqlCmd_met_006 = "update " + RepositorySchema + "metaobject set object_hash=$1, object_date_modified=$2 where object_id=$3"

// SQL Commando's die betrekking hebben op Users
// ================================================================================================
var SqlCmd_usr_001 = "insert into " + RepositorySchema + "User (user_hash, user_account, user_password, user_firstname, user_lastname, user_email, user_default_partition, user_default_application, TwoFactorAuthentication) values ($1, $2, $3, $4, $5, $6, $7, $8, $9)"
var SqlCmd_usr_002 = "select user_hash, user_account, user_password, user_firstname, user_lastname, user_email, user_default_partition, user_default_application, TwoFactorAuthentication, user_last_login, user_deleted from " + RepositorySchema + "User where user_id=$1"
var SqlCmd_usr_003 = "select user_id from " + RepositorySchema + "user where user_account=$1 and user_deleted=$2"
var SqlCmd_usr_004 = "update " + RepositorySchema + "user set user_deleted=$1 where user_id=$2"

// SQL Commando's die betrekking hebben op Applications
// ================================================================================================
// SqlCmd_app_003 : Toevoegen van een nieuwe Application
// SqlCmd_app_004 : Application inlezen ten behoeve van validatie Container check. Hier is alleen application_name en application_internal van belang
// SqlCmd_app_005 : Ophalen van alle eigenschappen van een Application op basis van de ID
// SqlCmd_app_006 : Ophalen van de ApplicationId op basis van de Applicatie FQName
// SqlCmd_app_007 : Logisch verwijderen van een Application
var SqlCmd_app_003 = "insert into " + RepositorySchema + "application values ($1, $2, $3, $4, $5, $6, $7)"
var SqlCmd_app_004 = "select application_name, application_internal from " + RepositorySchema + "application where application_id=$1 and application_deleted=$2"
var SqlCmd_app_005 = "select application_name, application_fqname, application_description, application_default, application_internal, application_deleted from " + RepositorySchema + "application where application_id=$1 and application_deleted=$2"
var SqlCmd_app_006 = "select application_id from " + RepositorySchema + "application where application_fqname=$1 and application_deleted=$2"
var SqlCmd_app_007 = "update " + RepositorySchema + "application set application_deleted=$1 where application_id=$2"

// Query voor het ophalen van de Applications voor het opbouwen van een ListBox tbv de WebServer
var SqlCmd_app_008 = "select application_id, application_name, application_description from " + RepositorySchema + "application where application_internal=$1 and application_deleted=$2 order by application_name"

// SQL Commando's die betrekking hebben op Auditing
// ================================================================================================
// SqlCmd_aud_001 : Toevoegen van Audit-informatie aan de tabel
// SqlCmd_aud_002 : Toevoegen van de Before and After Image van het object aan de audit-diff tabel
var SqlCmd_aud_001 = "insert into " + RepositorySchema + "audit (audit_user_id, audit_object_type, audit_object_id, audit_object_action, audit_timestamp, audit_object_fqname, audit_object_name ) values ($1, $2, $3, $4, $5, $6, $7) RETURNING audit_id"
var SqlCmd_aud_002 = "insert into " + RepositorySchema + "audit_diff (audit_id, audit_before_image, audit_after_image) values ($1, $2, $3) RETURNING audit_id"

// SQL Commando's die betrekking hebben op JobSource
var SqlCmd_jsc_001 = "insert into " + RepositorySchema + "jobsource (jobsrc_id, jobsrc_name, jobsrc_description, jobsrc_type, jobsrc_sourcecode, jobsrc_fqname, jobsrc_partition_id, jobsrc_application_id) values ($1, $2, $3, $4, $5, $6, $7, $8) "

//var SqlCmd_jsc_002 = "select jobsrc_name, jobsrc_fqname, jobsrc_description, jobsrc_type, jobsrc_majorversion, jobsrc_minorversion, jobsrc_sourcecode, jobsrc_queue_id, jobsrc_timewindow_id, jobsrc_priority, jobsrc_maxruntime, jobsrc_severity, jobsrc_currentversion, jobsrc_audit from " + RepositorySchema + "jobsource where jobsrc_id=$1 "
var SqlCmd_jsc_002 = "select jobsrc_name, jobsrc_fqname, jobsrc_description, jobsrc_type, jobsrc_sourcecode, jobsrc_partition_id, jobsrc_application_id from " + RepositorySchema + "jobsource where jobsrc_id=$1 "
var SqlCmd_jsc_003 = "select jobsrc_id from " + RepositorySchema + "jobsource where jobsrc_fqname=$1 and jobsrc_currentversion=$2"

// SQL om de JobSource bij te werken
var SqlCmd_jsc_004 = "update " + RepositorySchema + "jobsource set jobsrc_name=$1, jobsrc_description=$2, jobsrc_type=$3, jobsrc_sourcecode=$4, jobsrc_fqname=$5, jobsrc_partition_id=$6, jobsrc_application_id=$7 where jobsrc_id=$8"

// SQL om met een filter de FQName, Description en JobType op te halen
var SqlCmd_jsc_005 = "select jobsrc_id, jobsrc_fqname, jobsrc_description, jobsrc_type from " + RepositorySchema + "jobsource where jobsrc_fqname like $1 and jobsrc_deleted=$2"

// SQL Commando's die betrekking hebben op Job
// Toevoegen van een nieuwe Job
var SqlCmd_job_004 = "insert into " + RepositorySchema + "job (job_partition, job_application, job_type, job_name, job_description, job_queue, job_timewindow, job_priority, job_maxruntime, job_runstart, job_jobsrc_id, job_status,job_worker) values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13) RETURNING job_id"

// Toevoegen van de JobRequestXML aan de nieuwe Job
var SqlCmd_job_005 = "update " + RepositorySchema + "job set job_xml=$1, job_status=1 where job_id=$2 RETURNING job_id"

// Lezen van scheduled jobs die voor een bepaalde timestamp liggen
var SqlCmd_job_006 = "select job_id, job_runstart from " + RepositorySchema + "job where job_status=$1 and job_runStart < $2 order by job_runstart"

// Wijzigen van de status van een Job
var SqlCmd_job_007 = "update " + RepositorySchema + "job set job_status=$1 where job_id=$2"

// SQL voor het updaten van een Job nadat een JobCompletion bericht is ontvangen
var SqlCmd_job_008 = "update " + RepositorySchema + "job set job_exitcode=$1, job_status=$2, job_elapsedtime=$3 where job_id=$4"

// SQL om de status van een Job aan te passen
var SqlCmd_job_009 = "update " + RepositorySchema + "job set job_status=$1 where job_id=$2"

// SQL om gegevens van een Job bij te werken die een eindstatus heeft
var SqlCmd_job_010 = "update " + RepositorySchema + "job set job_status=$1, job_exitcode=$2, job_runend=$3, job_elapsedtime=$4 where job_id=$5"

// SQL om gegevens van een Job op te halen ten behoeve van GetJobByJobId
var SqlCmd_job_011 = "select job_jobsrc_id, job_partition, job_application, job_type, job_name, COALESCE(job_description, '') as job_description, COALESCE(job_status, '') as job_status, COALESCE(job_exitcode, '') as job_exitcode, COALESCE(job_runstart, '') as job_runstart, COALESCE(job_runend, '') as job_runend, COALESCE(job_elapsedtime, '') as job_elapsedtime, job_worker from " + RepositorySchema + "job where job_id=$1"

// SQL om de huidige status van een job op te halen
var SqlCmd_job_012 = "select job_status from " + RepositorySchema + "job where job_id=$1"

// SQL om Releated Jobs van een JobSource op te halen
var SqlCmd_job_013 = "select j.job_id, j.job_name, j.job_description, j.job_status, COALESCE(j.job_exitcode, '') as job_exitcode, j.job_runstart, COALESCE(j.job_runend, '') as job_runend,  COALESCE(j.job_elapsedtime, '') as job_elapsedtime, w.worker_name from " + RepositorySchema + "job j, " + RepositorySchema + " worker w where w.worker_id = j.job_worker and j.job_jobsrc_id=$1 order by j.job_runstart desc"

// Commando's die betrekking hebben op JobSourceParameters
//var SqlCmd_jsp_001 = "select jobsrcpar_id, jobsrcpar_name, jobsrcpar_description, jobsrcpar_default_value, jobsrcpar_format, jobsrcpar_order, jobsrcpar_expression, jobsrcpar_scope from " + RepositorySchema + "jobsourceparameter where jobsrcpar_jobsrc_id=$1 and jobsrcpar_visible=$2 and jobsrcpar_deleted=$3"
var SqlCmd_jsp_001 = "select jobsrcpar_id, jobsrcpar_name, jobsrcpar_description, jobsrcpar_default_value, jobsrcpar_visible from " + RepositorySchema + "jobsourceparameter where jobsrcpar_jobsrc_id=$1 and jobsrcpar_visible=$2 and jobsrcpar_deleted=$3"

// SQL om Parameters toe te voegen aan een JobSource
var SqlCmd_jsp_002 = "insert into " + RepositorySchema + "jobsourceparameter (jobsrcpar_jobsrc_id, jobsrcpar_name, jobsrcpar_description, jobsrcpar_default_value) values ($1, $2, $3, $4)"

// SQL om bestaande JobSourceParameters op Deleted te zetten
var SqlCmd_jsp_003 = "update " + RepositorySchema + "jobsourceparameter set jobsrcpar_deleted=$1 where jobsrcpar_jobsrc_id=$2"

// Commando's die betrekking hebben op JobParameters
// SQL om de Parameters zoals in de JobSource zijn gedefinieerd over te zetten naar de JobParameter tabel
var SqlCmd_jp_001 = "insert into " + RepositorySchema + "jobparameter (jobpar_jobid, jobpar_name, jobpar_value, jobpar_visible) values ($1, $2, $3, $4)"

// SQL om de waarde van een JobParameter aan te passen
var SqlCmd_jp_002 = "update " + RepositorySchema + "jobparameter set jobpar_value=$1 where jobpar_jobid=$2 and jobpar_name=$3"

// SQL om de parameters en waarden in te lezen
var SqlCmd_jp_003 = "select jobpar_name, jobpar_value, jobpar_visible from " + RepositorySchema + "jobparameter where jobpar_jobid=$1"

// SQL Commando's die betrekking hebben op JobLog / ExecutionLog
// SQL om een nieuwe JobLog op te slaan
var SqlCmd_joblog_001 = "insert into " + RepositorySchema + "joblog (job_id, job_log) values ($1, $2)"

// SQL om een JobLog op te halen
var SqlCmd_joblog_002 = "select job_log from " + RepositorySchema + "joblog where job_id=$1"

// SQL Commando's die betrekking hebben op JobFiles
// SQL om een JobFile toe te voegen aan de database
var SqlCmd_jobfile_001 = "insert into " + RepositorySchema + "jobfile (jobfile_job_id, jobfile_type, jobfile_size, jobfile_filename) values ($1, $2, $3, $4)"

// SQL om de JobFile-info op te halen op basis van de JobId
var SqlCmd_jobfile_002 = "select jobfile_id, jobfile_filename, jobfile_size from " + RepositorySchema + "jobfile where jobfile_job_id=$1"

// SQL om de filename van een JobFile op te halen op basis van een Id
var SqlCmd_jobfile_003 = "select jobfile_filename from " + RepositorySchema + "jobfile where jobfile_id=$1"

// SQL Commando's die betrekking hebben op de Agents
// SQL om de Agents te initialiseren bij de start van de CentralInstance
var SqlCmd_agt_001 = "update " + RepositorySchema + "agent set agent_stop=$1, agent_num_restart=0, agent_timestamp='2000-01-01-00:00:00'"

// SQL om de timestamp en versie van de agent bij te werken
var SqlCmd_agt_002 = "update " + RepositorySchema + "agent set agent_timestamp=$1, agent_last_activity=$2, agent_software_version=$3 where agent_id=$4"

// SQL om te bepalen wat de Stop-indicator voor de Agent is
var SqlCmd_agt_003 = "select agent_stop from " + RepositorySchema + "agent where agent_id=$1"

// SQL om te bepalen wat de instelling voor Active is voor de Agent
var SqlCmd_agt_004 = "select agent_active from " + RepositorySchema + "agent where agent_id=$1"

// SQL om de Active/InActive-instelling van een Agent aan te passen
var SqlCmd_agt_005 = "update " + RepositorySchema + "agent set agent_active=$1 where agent_id=$2"

// SQL om de Agent tabel te initieren bij het starten van de agents
var SqlCmd_agt_006 = "update " + RepositorySchema + "agent set agent_stop=$1, agent_timestamp=$2, agent_last_activity=$2"
