package credential

import "naming"
import "constanten"
import "logging"
import "strconv"
import "melding"
import "validatie"
import "centralinstance/metaobject"

var prefix = naming.CredentialObjNameEV + ": "

type Credential struct {
	Id            int
	Name          string
	FQName        string
	Description   string
	Username      string
	Password      string
	Host          string
	PartitionId   int
	ApplicationId int
	Deleted       string
}

// **********************************************************************
// * Opslaan van het Credential
// * Hierbij wordt eerst gecontroleerd of alles wel correct is ingevuld
// **********************************************************************
func (c *Credential) Commit() (int, string) {

	// Wanneer bepaalde velden niet zijn ingevuld, moeten hiervoor
	// standaard waarden worden opgenomen
	if c.Deleted == "" {
		c.Deleted = constanten.No
	}

	// Uitvoeren van de Controles voor het Object Credential
	exitCode, exitMsg := c.CheckObject()
	if exitCode > 0 {
		return exitCode, exitMsg
	}

	// Controles die betrekking hebben op de container (combinatie Partition / Application
	exitCode, exitMsg = validatie.CheckContainers(c.PartitionId, c.ApplicationId)
	if exitCode > 0 {
		return exitCode, exitMsg
	}

	// Wanneer het een nieuw Credential betreft, moet er eerst een MetaObject
	// voor het credential worden aangemaakt. Hierbij is het van belang dat zowel
	// de Partition als Application zijn benoemd. Dit is in de vorige stap gecontroleerd
	if c.Id == 0 {
		logging.InfoMSG("Creating MetaObject for new Credenitial")
		metaObjectPartition := metaobject.MetaObject{}
		metaObjectPartition.SetPartition(c.PartitionId)
		metaObjectPartition.SetApplication(c.ApplicationId)
		metaObjectPartition.SetUser(0)
		metaObjectPartition.SetObjectType(metaobject.ObjectTypeCredential)
		metaObjectPartition.Commit()

		// Na de commit is het MetaObject.Id beschikbaar
	}

	return exitCode, exitMsg
}

// **********************************************************************
// * Procedure voor het dumpen van het Object. Dit is bedoeld om tijdens
// * development van de applicatie makkelijker problemen te kunnen
// * analyseren
// **********************************************************************
func (c *Credential) DumpObject() {
	logging.DevMSG("Id							= " + strconv.Itoa(c.Id))
	logging.DevMSG("FQName						= " + c.FQName)
	logging.DevMSG("Name							= " + c.Name)
	logging.DevMSG("Description					= " + c.Description)
	logging.DevMSG("Partition					= " + strconv.Itoa(c.PartitionId))
	logging.DevMSG("Application					= " + strconv.Itoa(c.ApplicationId))
	logging.DevMSG("Username						= " + c.Username)
	logging.DevMSG("Password						= " + c.Password)
	logging.DevMSG("Host							= " + c.Host)
	logging.DevMSG("Deleted						= " + c.Deleted)
}

// **********************************************************************
// * Controleren van het Credential voordat het wordt opgeslagen. Voor een
// * Credential geldt dat er zowel een Partition als een Application moet worden
// * toegekend aan de Credential
// **********************************************************************
func (c *Credential) CheckObject() (int, string) {
	exitCode := 0
	exitMsg := ""

	if c.Deleted == constanten.No {

		// Controleren van de Object Name
		exitCode, exitMsg = validatie.Check(c.Name, validatie.ObjectName)
		if exitCode > 0 {
			return exitCode, exitMsg
		}

		// Controleren van de Omschrijving
		exitCode, exitMsg = validatie.Check(c.Description, validatie.Description)
		if exitCode > 0 {
			return exitCode, exitMsg
		}

		// Controleren of er een waarde is ingevoerd voor Username
		exitCode, exitMsg = validatie.CheckNotNull(c.Username, "UserName")
		if exitCode > 0 {
			return exitCode, exitMsg
		}

		// Controleren of er een waarde is ingevoerd voor Password
		exitCode, exitMsg = validatie.CheckNotNull(c.Password, "Password")
		if exitCode > 0 {
			return exitCode, exitMsg
		}

		// Controleren of er een Partition is toegekend aan het Credential
		if c.PartitionId == 0 {
			return 2, melding.Melding_cre_002
		}

		// Controleren of er een Application is toegekend aan het Credential
		if c.ApplicationId == 0 {
			return 1, melding.Melding_cre_001
		}
	}

	return exitCode, exitMsg
}
