package metaobject

import "logging"
import "strconv"
import "repository"
import "utils"
import "constanten"
import "naming"


var prefix = "MetaObject: "

type MetaObject struct {
	Id            int64
	ObjectType    int
	PartitionId   int64
	ApplicationId int64
	Hash          string
	ReadOnly      string
	DateCreated   string
	UserCreated   int64
	DateModified  string
	UserModified  int64
	UserId        int64
}

// Constanten voor Objecttypes in de MetaObject tabel
var ObjectTypePartition = 1000
var ObjectTypeCredential = 1001
var ObjectTypeApplication = 1002
var ObjectTypeJobSource = 1003
var ObjectTypeWorker = 1004

// **********************************************************************
// * Procedure om de UserId in te stellen van de gebruiker die een
// * wijziging heeft aangebracht in het object
// **********************************************************************
func (m *MetaObject) SetUserLastModification(userID int64) {
	m.UserModified = userID
	logging.InfoMSG(prefix + "Set LastModifiedUser to '" + strconv.FormatInt(m.UserModified,10) + "'")
}

// **********************************************************************
// * Procedure om het MetaData-object op te halen op basis van de Id
// **********************************************************************
func (m *MetaObject) GetMetaObjectById(metaobjectID int64) {

	sqlCmd := repository.SqlCmd_met_002
	m.Id = metaobjectID
	logging.InfoMSG(prefix + "getting MetaObject information by ID (" + strconv.FormatInt(m.Id,10) + ")")

	// Variabelen declareren die gelezen worden uit de tabel
	object_type := 0
	var object_partition int64 = 0
	var object_application int64 = 0
	object_hash := ""
	object_readonly := ""
	object_date_created := ""
	var object_user_created int64 = 0
	object_date_modified := ""
	var object_user_modified int64= 0

	rows, err := repository.DBConnection.Query(sqlCmd, m.Id)
	if err != nil {
		logging.ErrorMSG(prefix + "Failed to connect to repository: " + err.Error())
	}
	for rows.Next() {
		err := rows.Scan(&object_type, &object_partition, &object_application, &object_hash, &object_readonly, &object_date_created, &object_user_created, &object_date_modified, &object_user_modified)
		if err != nil {
			logging.DebugMSG(err.Error())
		} else {
			// Overzetten van de gegevens
			m.ObjectType = object_type
			m.PartitionId = object_partition
			m.ApplicationId = object_application
			m.Hash = object_hash
			m.ReadOnly = object_readonly
			m.DateCreated = object_date_created
			m.UserCreated = object_user_created
			m.DateModified = object_date_modified
			m.UserModified = object_user_modified
		}
	}
}

// **********************************************************************
// * Verwijderen van het MetaObject als het bijbehorende Scheduling
// * object (bijvoorbeeld Partition) verwijderd wordt
// **********************************************************************
func (m *MetaObject) Delete() {

	logging.InfoMSG("Deleting MetaObject")
	err := repository.DBConnection.QueryRow(repository.SqlCmd_met_004, m.Id)
	if err != nil {
		logging.ErrorMSG(prefix + "Failed to execute SQL command: ")
	} else {
		logging.InfoMSG("MetaObject has been deleted")
	}

}

// **********************************************************************
// * Procedure om het object als readonly in te stellen
// **********************************************************************
func (m *MetaObject) SetReadOnly() {
	m.ReadOnly = constanten.Yes
}

// **********************************************************************
// * Procedure om de Partitie in te stellen
// **********************************************************************
func (m *MetaObject) SetPartition(partitionID int64) {
	m.PartitionId = partitionID
	logging.InfoMSG(prefix + "Set " + naming.PartitionObjNameEV + " to '" + strconv.FormatInt(m.PartitionId,10) + "'")
}

// **********************************************************************
// * Procedure om de Partitie op te vragen
// **********************************************************************
func (m *MetaObject) getPartition() int64 {
	return m.PartitionId
}

// **********************************************************************
// * Procedure om de Applicatie in te stellen
// **********************************************************************
func (m *MetaObject) SetApplication(applicatieID int64) {
	m.ApplicationId = applicatieID
	logging.InfoMSG(prefix + "Set Application to '" + strconv.FormatInt(m.ApplicationId,10) + "'")
}

// **********************************************************************
// * Procedure om de Application op te vragen
// **********************************************************************
func (m *MetaObject) GetApplication() int64 {
	return m.ApplicationId
}

// **********************************************************************
// * Procedure om de User in te stellen. Deze info wordt gebruikt voor
// * registratie van last_modified en created info
// **********************************************************************
func (m *MetaObject) SetUser(userId int64) {
	m.UserId = userId
	logging.InfoMSG(prefix + "Set User to '" + strconv.FormatInt(m.UserId,10) + "'")
}

// **********************************************************************
// * Procedure om de UserID op te vragen van degene die het object
// * heeft aangemaakt
// **********************************************************************
func (m *MetaObject) GetUserCreation() int64 {
	return m.UserCreated
}

// **********************************************************************
// * Procedure om de UserID op te vragen van degene die het object
// * het laatste heeft gewijzigd
// **********************************************************************
func (m *MetaObject) GetUserLastModification() int64 {
	return m.UserModified
}

// **********************************************************************
// * Procedure om de datum/tijd op te vragen dat het object is aangemaakt
// **********************************************************************
func (m *MetaObject) GetCreationDate() string {
	return m.DateCreated
}

// **********************************************************************
// * Procedure om de datum/tijd op te vragen dat het object dat het
// * object voor het laatst is gewijzigd
// **********************************************************************
func (m *MetaObject) GetLastModificationDate() string {
	return m.DateModified
}

// **********************************************************************
// * Procedure om het ObjectType (bijv Partition) in te stellen voor
// * het ObjectType
// **********************************************************************
func (m *MetaObject) SetObjectType(objecttypeID int) {
	m.ObjectType = objecttypeID
	logging.InfoMSG(prefix + "Set ObjectType to '" + strconv.Itoa(m.ObjectType) + "'")
}

// **********************************************************************
// * Procedure om een hash te genereren en in te
// * stellen in het metaobject
// **********************************************************************
func (m *MetaObject) SetHash() {
	m.Hash = utils.GenerateHash(32, "alphanum")
}

// **********************************************************************
// * Procedure om de hash van het object op te vragen
// **********************************************************************
func (m *MetaObject) GetHash() string {
	return m.Hash
}

// **********************************************************************
// * Procedure om de wijzigingen in het MetaObject object weg te schrijven
// * naar de repository
// **********************************************************************
func (m *MetaObject) Commit() (int, string) {

	exitCode := 0
	exitMsg := "MetaObject has been created"

	logging.InfoMSG(prefix + "Start Committing of MetaObject")

	// Nieuwe Hash voor het Object bepalen
	m.SetHash()
	logging.InfoMSG(prefix + "Set Hash to '" + m.GetHash())

	// Er zijn twee mogelijke situaties.
	// Er wordt een nieuwe MetaObject aangemaakt (in het geval van m.Id = 0)
	// Er wordt een bestaand MetaObject geactualiseerd (in het geval van m.Id > 0)

	timeStamp := utils.CurrentTimeStamp(constanten.DateFormatLong)

	// Afhandelen van de Readonly-indicator. Deze is nl als een string geimplementeerd (YES, NO)
	// Dit houdt dus in dat wanneer het MetaObject wordt geinitieerd, deze indicator leeg is
	// Een lege waarde houdt in dat deze op NO moet worden gezet
	if m.ReadOnly == "" {
		m.ReadOnly = constanten.No
	}

	if m.Id == 0 {
		logging.InfoMSG(prefix + "Storing a new MetaObject")

		var object_id int64 = 0
		err := repository.DBConnection.QueryRow(repository.SqlCmd_met_001, m.ObjectType, m.PartitionId, m.ApplicationId, m.Hash, timeStamp, m.UserId, m.ReadOnly).Scan(&object_id)
		if err != nil {
			logging.ErrorMSG(prefix + "Failed to execute SQL command: " + err.Error())
		} else {
			logging.ParameterMSG(prefix + "New ObjectID=" + strconv.FormatInt(object_id,10))
			m.Id = object_id
		}

	} else {
		// Opslaan van een bestaande MetaObject
		logging.InfoMSG(prefix + "Storing an existing MetaObject")

		logging.InfoMSG(prefix + "Starting SQL Transaction")
		tx, err := repository.DBConnection.Begin()
		if err != nil {
			logging.ErrorMSG(prefix + "Failed to start a SQL transaction: " + err.Error())
		}

		sqlCmd := repository.SqlCmd_met_003
		stmt, err := tx.Prepare(sqlCmd)

		_, err = stmt.Exec(m.PartitionId, m.ApplicationId, m.Hash, m.ReadOnly, timeStamp, m.UserModified, m.Id)
		if err != nil {
			logging.ErrorMSG(prefix + "Failed to execute SQL command: " + err.Error())
			logging.InfoMSG(prefix + "Rollback SQL Transaction")
			tx.Rollback()
		}

		// Afsluiten (commit) van de transactie
		logging.InfoMSG(prefix + "Commit SQL Transaction")
		tx.Commit()

	}

	return exitCode, exitMsg
}
