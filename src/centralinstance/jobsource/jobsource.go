package jobsource

import "repository"
import "logging"
import "validatie"
import "naming"
import "strings"
import "centralinstance/metaobject"
import "centralinstance/partition"
import "centralinstance/application"
import "centralinstance/jobtype"
import "statussen/jobstatus"
import "strconv"
import "melding"
import "utils"
import "constanten"
import "berichten"
import "os"
import "encoding/xml"
import "io/ioutil"

// http://stackoverflow.com/questions/18042439/go-append-to-slice-in-struct
// Toevoegen van een Array van JobSourceParameters aan de JobSource Struct

var prefix = naming.JobSourceObjNameEV + ": "

type JobSourceParameter struct {
	Id           int64
	Name         string
	Description  string
	DefaultValue string
	Visible      string
	Scope        string
	Format       string
	Order        int
	Expression   string
}

type JobSource struct {
	Id                 int64
	PartitionId        int64
	Partition          string
	ApplicationId      int64
	Application        string
	JobSourceParameter []JobSourceParameter
	//    jobsrc_application_id   int
	//   jobsrc_application_name string
	Name              string
	FQName            string
	Description       string
	JobSourceType     int
	JobSourceTypeName string
	MajorVersion      string
	MinorVersion      string
	SourceCode        string
	// Metadata
	DateCreated  string
	UserCreated  int64
	DateModified string
	UserModified int64
	UserId       int64
	//SourceCode       berichten.DataWithSpecialCharacters
	//	jobsrc_environment      berichten.DataWithSpecialCharacters
	//	jobsrc_currentversion   string
	//	jobsrc_audit            string
	//	TimeWindowId    		int
	//	jobsrc_timewindow_name  string
	//	QueueId 		        int
	//	Priority				int
	//	MaxRunTime				int
	//	CurrentVersion          string
	//	Audit					string
	// jobsrc_queue_name       string
}

// Object om een JobSource te kunnen promoten
type JobSourceDefinition struct {
	Partition   string
	Application string
	Name        string
	//FQName                  string
	Description        string
	JobSourceType      int
	JobSourceParameter []JobSourceParameter
	SourceCode         berichten.DataWithSpecialCharacters
}

// ************************************************************************
// * GetRelatedJobs
// * Methode om een html-uitlijsting te maken van de jobs die behoren
// * tot de geselecteerde JobSource
// ************************************************************************
func (j *JobSource) GetRelatedJobs() string {

	var htmlCode string = ""
	var relatedjobsFound bool = false

	sqlCmd := repository.SqlCmd_job_013
	logging.DebugMSG("Preparing SQLCommand repository.SqlCmd_job_013")
	logging.DevMSG(prefix + "getting Related Jobs from JobSource " + j.FQName)

	rows, err := repository.DBConnection.Query(sqlCmd, j.Id)
	if err != nil {
		logging.ErrorMSG(prefix + "Failed to connect to repository: " + err.Error())
	}

	// Initialiseren van de variabelen om de query uit te voeren
	var job_id int64 = 0
	var job_name string = ""
	var job_description string = ""
	var job_status string = ""
	var job_exitcode string = ""
	var job_runstart string = ""
	var job_runend string = ""
	var job_elapsedtime string = ""
	var worker_name string = ""

	for rows.Next() {
		err := rows.Scan(&job_id, &job_name, &job_description, &job_status, &job_exitcode, &job_runstart, &job_runend, &job_elapsedtime, &worker_name)
		if err != nil {
			logging.DebugMSG(err.Error())
		} else {
			relatedjobsFound = true
			htmlCode = htmlCode + "<tr>"

			// Link aanmaken naar de Jobdetails
			var link string = "<a href=/job/details/" + strconv.FormatInt(job_id, 10) + ">" + strconv.FormatInt(job_id, 10) + "</a>"
			htmlCode = htmlCode + "<td>" + link + "</td>"
			htmlCode = htmlCode + "<td>" + job_name + "</td>"
			htmlCode = htmlCode + "<td>" + strings.Replace(job_description, " ", "&nbsp;", -1) + "</td>"
			htmlCode = htmlCode + "<td>" + jobstatus.GetStatusWithCSS(job_status) + "</td>"
			htmlCode = htmlCode + "<td>" + worker_name + "</td>"
			htmlCode = htmlCode + "<td>" + job_exitcode + "</td>"
			htmlCode = htmlCode + "<td>" + utils.FormatDate(job_runstart) + "</td>"
			htmlCode = htmlCode + "<td>" + utils.FormatDate(job_runend) + "</td>"
			htmlCode = htmlCode + "<td>" + job_elapsedtime + "</td>"
			htmlCode = htmlCode + "</tr>"
		}
	}

	// Tabel definitie toevoegen
	if relatedjobsFound == true {
		var tableHeader string = "<br><table>"
		tableHeader = tableHeader + "<th>JobID</th>"
		tableHeader = tableHeader + "<th>Naam</th>"
		tableHeader = tableHeader + "<th>Description</th>"
		tableHeader = tableHeader + "<th>Status</th>"
		tableHeader = tableHeader + "<th>Worker</th>"
		tableHeader = tableHeader + "<th>ExitCode</th>"
		tableHeader = tableHeader + "<th>Run Start</th>"
		tableHeader = tableHeader + "<th>Run End</th>"
		tableHeader = tableHeader + "<th>Elapsed Time</th>"
		tableHeader = tableHeader + "</tr>"
		htmlCode = tableHeader + htmlCode + "</table>"

	} else {
		logging.DebugMSG("No Related Jobs found for JobSource " + j.FQName)
	}

	return htmlCode
}

// ************************************************************************
// * Export
// * Exporteren van de JobSource. Hierbij wordt een apart Type gebruikt omdat
// * bijvoorbeeld de applicatie en partitie als int64 worden opgeslagen. Omdat
// * de IDs verschillen per omgeving, moet in de Export de FQ namen van de
// * afhankelijke objecten worden opgenomen.
// * De methode retourneert de fully qualified filename van het exportbestand
// ************************************************************************
func (j *JobSource) Export(inExportDirectory string) string {

	var exp JobSourceDefinition

	var partitionName string = ""
	var applicationName string = ""

	// Ophalen van de Partition van dit Object
	part := partition.Partition{}
	part.GetPartitionById(j.PartitionId)
	partitionName = part.GetFQName()

	// Ophalen van de Application van dit Object
	app := application.Application{}
	app.GetApplicationByID(j.ApplicationId)
	applicationName = app.GetFQName()

	// Vullen van het exp-object
	exp.Partition = partitionName
	exp.Application = applicationName
	exp.Name = j.Name
	//exp.FQName = j.FQName
	exp.Description = j.Description
	exp.JobSourceType = j.JobSourceType
	exp.SourceCode = berichten.DataWithSpecialCharacters(j.SourceCode)

	exp.JobSourceParameter = j.JobSourceParameter

	// Bepalen of de Export-Directory bestaat en schrijfbaar is
	if utils.IsWritableDirectory(inExportDirectory) == false {
		logging.ErrorMSG("Cannot create directory " + inExportDirectory + " or the directory is not writable")
	}

	// Samenstellen van de filename voor deze export
	var filename string = inExportDirectory + string(os.PathSeparator) + j.FQName + ".xml"

	objectDefinition, _ := xml.MarshalIndent(exp, "", constanten.XMLIndent)
	err := ioutil.WriteFile(filename, objectDefinition, 0644)
	if err != nil {
		logging.ErrorMSG("Cannot write to export file " + filename)
		os.Exit(1)
	}

	return filename
}

// ************************************************************************
// * SetName / GetName
// ************************************************************************
func (j *JobSource) SetName(inName string) {
	j.Name = inName
	logging.DevMSG("JobSource Name set to '" + j.Name + "'")
}
func (j *JobSource) GetName() string {
	return j.Name
}

// ************************************************************************
// * SetFQName / GetFQName
// ************************************************************************
func (j *JobSource) SetFQName() {
	// Ophalen van de naam van de Partition
	part := partition.Partition{}
	part.GetPartitionById(j.PartitionId)

	j.FQName = strings.ToUpper(part.Name) + "." + strings.ToUpper(j.Name)
	logging.DevMSG("JobSource FQName set to '" + j.FQName + "'")
}
func (j *JobSource) GetFQName() string {
	return j.FQName
}

// ************************************************************************
// * SetDescription / GetDescription
// ************************************************************************
func (j *JobSource) SetDescription(inDescription string) {
	j.Description = inDescription
	logging.DevMSG("JobSource Description set to '" + j.Description + "'")
}
func (j *JobSource) GetDescription() string {
	return j.Description
}

// ************************************************************************
// * SetPartition / GetPartition
// ************************************************************************
func (j *JobSource) SetPartitionId(inPartition int64) {
	j.PartitionId = inPartition
	logging.DevMSG("JobSource Partition set to '" + strconv.FormatInt(j.PartitionId, 10) + "'")
}
func (j *JobSource) GetPartition() int64 {
	return j.PartitionId
}

// ************************************************************************
// * SetApplication / GetApplication
// ************************************************************************
func (j *JobSource) SetApplication(inApplication int64) {
	j.ApplicationId = inApplication
	logging.DevMSG("JobSource Application set to '" + strconv.FormatInt(j.ApplicationId, 10) + "'")
}
func (j *JobSource) GetApplicationId() int64 {
	return j.ApplicationId
}

// ************************************************************************
// * SetSourceCode / GetSourceCode
// ************************************************************************
func (j *JobSource) SetSourceCode(inSourceCode string) {
	j.SourceCode = inSourceCode
	logging.DevMSG("JobSource SourceCode set to " + j.SourceCode)
}
func (j *JobSource) GetSourceCode() string {
	return j.SourceCode
}

// ************************************************************************
// * SetJobSourceType / GetJobSourceType
// ************************************************************************
func (j *JobSource) SetJobSourceType(inType int) {
	j.JobSourceType = inType
	logging.DevMSG("JobSource Type set")
}
func (j *JobSource) GetJobSourceType() int {
	return j.JobSourceType
}

// ************************************************************************
// *
// * Ophalen van de gegevens van een JobSource op basis van de Naam
// * Er wordt eerst bepaald dat de bijbehorende ID is van de naam. Vervolgens
// * wordt op basis van dit ID het object opgehaald
// *
// ************************************************************************
func (j *JobSource) GetJobSourceByFQName(InJobSourceFQName string) {

	jobSourceFQName := strings.ToUpper(utils.Trim(InJobSourceFQName))
	jobSourceFound := false

	var jobsource_id int64 = 0

	sqlCmd := repository.SqlCmd_jsc_003
	logging.DebugMSG("Preparing SQLCommand repository.SqlCmd_jsc_003")
	logging.DevMSG(prefix + "getting " + naming.JobSourceObjNameEV + " information by name (" + jobSourceFQName + ")")

	rows, err := repository.DBConnection.Query(sqlCmd, jobSourceFQName, constanten.Yes)
	if err != nil {
		logging.ErrorMSG(prefix + "Failed to connect to repository: " + err.Error())
	}

	for rows.Next() {
		err := rows.Scan(&jobsource_id)
		if err != nil {
			logging.DebugMSG(err.Error())
		} else {
			jobSourceFound = true
			logging.InfoMSG(naming.JobSourceObjNameEV + " '" + jobSourceFQName + "' found")
			j.GetJobSourceById(jobsource_id)
		}
	}

	if jobSourceFound == false {
		logging.InfoMSG(naming.JobSourceObjNameEV + " '" + jobSourceFQName + "' not found")
	}

}

// ************************************************************************
// *
// * Ophalen van de gegevens van een JobSource op basis van de ID
// *
// ************************************************************************
func (j *JobSource) GetJobSourceById(jobSourceId int64) {

	logging.DevMSG("Retrieve JobSource by id (" + strconv.FormatInt(jobSourceId, 10) + ")")

	jobsourceFound := false

	// Initialiseren van de variabelen
	jobsrc_name := ""
	jobsrc_fqname := ""
	jobsrc_description := ""
	jobsrc_type := 0
	var jobsrc_applicationid int64 = 0
	var jobsrc_partitionid int64 = 0
	//jobsrc_majorversion := ""
	//jobsrc_minorversion := ""
	//var jobsrc_sourcecode berichten.DataWithSpecialCharacters = ""
	jobsrc_sourcecode := ""
	//jobsrc_queue_id := 0
	//jobsrc_timewindow_id := 0
	//jobsrc_priority := 0
	//jobsrc_maxruntime := 0
	//jobsrc_severity := 0
	//jobsrc_currentversion := ""
	//jobsrc_audit := ""

	logging.DebugMSG("Preparing SQLCommand repository.SqlCmd_jsc_002")
	sqlCmd := repository.SqlCmd_jsc_002

	rows, err := repository.DBConnection.Query(sqlCmd, jobSourceId)
	if err != nil {
		logging.ErrorMSG(prefix + "Failed to connect to repository: " + err.Error())
	}

	for rows.Next() {
		err := rows.Scan(&jobsrc_name, &jobsrc_fqname, &jobsrc_description, &jobsrc_type, &jobsrc_sourcecode, &jobsrc_partitionid, &jobsrc_applicationid)
		if err != nil {
			logging.DebugMSG(err.Error())
		} else {
			jobsourceFound = true
			logging.InfoMSG(naming.JobSourceObjNameEV + " found")
			// Ingelezen waarden overzetten naar het Object
			j.Id = jobSourceId
			j.Name = jobsrc_name
			j.FQName = jobsrc_fqname
			j.Description = jobsrc_description
			j.JobSourceType = jobsrc_type
			j.JobSourceTypeName = jobtype.GetJobTypeName(j.JobSourceType)
			j.ApplicationId = jobsrc_applicationid
			j.PartitionId = jobsrc_partitionid
			//	j.JobSourceType = jobsrc_type
			//	j.MajorVersion = jobsrc_majorversion
			//	j.MinorVersion = jobsrc_minorversion
			//cdata_SourceCode = cdata_SourceCode + jobsrc_sourcecode
			j.SourceCode = jobsrc_sourcecode
			//	j.TimeWindowId = jobsrc_timewindow_id
			//		j.QueueId = jobsrc_queue_id
			//		j.Priority = jobsrc_priority
			//		j.MaxRunTime = jobsrc_maxruntime
			//		j.CurrentVersion = jobsrc_currentversion
			//		j.Audit = jobsrc_audit

			// Ophalen van de Partition
			part := partition.Partition{}
			part.GetPartitionById(j.PartitionId)
			j.Partition = part.GetName()

			// Ophalen van de Applicaton
			app := application.Application{}
			app.GetApplicationByID(j.ApplicationId)
			j.Application = app.GetName()

		}
	}

	// Ophalen van de MetaData van de JobSource
	metaObject := metaobject.MetaObject{}
	metaObject.GetMetaObjectById(j.Id)
	j.DateCreated = metaObject.DateCreated
	j.UserCreated = metaObject.UserCreated
	j.DateModified = metaObject.DateModified
	j.UserModified = metaObject.UserModified
	j.UserId = metaObject.UserId

	// Wanneer de JobSource is gevonden, kunnen ook de eventuele JobSourceParameters
	// worden ingelezen en aan de struct worden toegevoegd
	if jobsourceFound == true {
		logging.DebugMSG("Preparing SQLCommand repository.SqlCmd_jsp_001")
		sqlCmd := repository.SqlCmd_jsp_001
		rows, err := repository.DBConnection.Query(sqlCmd, jobSourceId, constanten.Yes, constanten.No)
		if err != nil {
			logging.ErrorMSG(prefix + "Failed to connect to repository: " + err.Error())
		}

		// Initialiseren van de variabelen
		var id int64 = 0
		var name string = ""
		var description string = ""
		var default_value = ""
		var visible string = ""
		//var format string = ""
		//var order int = 0
		//var expression string = ""
		//var scope string = ""

		for rows.Next() {
			err := rows.Scan(&id, &name, &description, &default_value, &visible)
			if err != nil {
				logging.DebugMSG(err.Error())
			} else {
				par := JobSourceParameter{}
				par.Id = id
				par.Name = name
				par.Description = description
				par.DefaultValue = default_value
				par.Visible = visible
				// par.Format = format
				// par.Order = order
				// par.Expression = expression
				// par.Scope = scope
				j.AddJobSourceParameter(par)
				logging.DebugMSG("Add JobSourceParameter with values " + name + ", " + description + ", " + default_value + ", visible=" + visible)
			}

		}

	}

	if jobsourceFound == false {
		logging.InfoMSG(naming.JobSourceObjNameEV + " not found")
	}
}

// ************************************************************************
// *                                                                      *
// * DumpObject()                                                        *
// *                                                                      *
// ************************************************************************
func (j *JobSource) DumpObject() {
	logging.DevMSG("Id                 = " + strconv.FormatInt(j.Id, 10))
	logging.DevMSG("FQName             = " + j.FQName)
	logging.DevMSG("FQName             = " + j.Name)
	logging.DevMSG("Description        = " + j.Description)
	logging.DevMSG("Job Type           = " + strconv.Itoa(j.JobSourceType))
	logging.DevMSG("Major Version      = " + j.MajorVersion)
	logging.DevMSG("Minor Version      = " + j.MinorVersion)
	logging.DevMSG("SourceCode         = " + strconv.Itoa(len(j.SourceCode)) + " bytes")
	//	logging.DevMSG("TimeWindowId       = " + strconv.Itoa(j.TimeWindowId))
	//	logging.DevMSG("QueueId            = " + strconv.Itoa(j.QueueId))
	//	logging.DevMSG("Priority           = " + strconv.Itoa(j.Priority))
	//	logging.DevMSG("MaxRunTime         = " + strconv.Itoa(j.MaxRunTime))
	//	logging.DevMSG("CurrentVersion     = " + j.CurrentVersion)
	//	logging.DevMSG("Audit              = " + j.Audit)
}

// ************************************************************************
// *                                                                      *
// * CheckObject()                                                        *
// *                                                                      *
// ************************************************************************
// Controleren van het JobSource-object voordat het kan worden opgeslagen
func (j *JobSource) CheckObject() (int, string) {

	exitCode := 0
	exitMsg := "Ok"

	// Wanneer het een nieuw object betreft, moet de FQName worden gezet
	if j.Id == 0 {
		j.SetFQName()
	}

	// Controleren of er wel een Objectnaam is ingevuld
	exitCode, exitMsg = validatie.CheckNotNull(j.Name, "Name")
	if exitCode > 0 {
		return exitCode, exitMsg
	}

	// Controlen of de naam van de JobSource wel een geldige Objectnaam is
	exitCode, exitMsg = validatie.Check(j.Name, validatie.ObjectName)
	// Objectnaam vervangen door echte naam (JobSource)
	if exitCode > 0 {
		exitMsg = strings.Replace(exitMsg, "$OBJECTTYPE", naming.JobSourceObjNameEV, 1)
		return exitCode, exitMsg
	}

	// Controleren of er wel een JobSource is ingevuld
	logging.DevMSG("SourceCode=" + j.SourceCode)
	exitCode, exitMsg = validatie.CheckNotNull(j.SourceCode, "SourceCode")
	if exitCode > 0 {
		return exitCode, exitMsg
	}

	// Controleren of er wel een Partition is ingesteld voor de JobSource
	// Hiervoor moet eerst de naam worden opgehaald
	part := partition.Partition{}
	part.GetPartitionById(j.PartitionId)
	exitCode, exitMsg = validatie.CheckNotNull(part.Name, "Partition")
	if exitCode > 0 {
		return exitCode, exitMsg
	}

	// Controleren of er wel een Application is ingesteld voor de JobSource
	// Hiervoor moet eerst de naam worden opgehaald
	app := application.Application{}
	app.GetApplicationByID(j.ApplicationId)
	exitCode, exitMsg = validatie.CheckNotNull(app.Name, "Application")
	if exitCode > 0 {
		return exitCode, exitMsg
	}

	// controleren of er wel een JobSourceType is ingesteld. Dit is het soort
	// JobType (bijvoorbeeld Linux Shell)
	if j.JobSourceType == 0 {
		return 1, melding.Melding_jsc_001
	}

	// Alle controles waren succesvol. De JobSource kan worden opgeslagen

	return exitCode, exitMsg
}

// ************************************************************************
// * GetApplicationName
// * Methode voor het ophalen van de Application Name. Dit is een extra
// * stap omdat alleen de ApplicatieID wordt opgeslagen
// ************************************************************************
func (j *JobSource) GetApplicationName() string {

	logging.DevMSG("Get Application by Id " + strconv.FormatInt(j.ApplicationId, 10))
	app := application.Application{}
	app.GetApplicationByID(j.ApplicationId)
	return app.GetName()
}

// ************************************************************************
// AddJobSourceParameter
// Methode om een nieuwe Parameter aan de JobSource toe te voegen
// ************************************************************************
func (j *JobSource) AddJobSourceParameter(par JobSourceParameter) []JobSourceParameter {
	j.JobSourceParameter = append(j.JobSourceParameter, par)
	return j.JobSourceParameter
}

// ************************************************************************
// *                                                                      *
// * Commit()                                                  *
// *                                                                      *
// ************************************************************************
func (j *JobSource) Commit() (int, string) {

	exitCode := 0
	exitMsg := "Ok"

	var isNewJobSource bool = true

	// Uitvoeren van de Object Controles
	exitCode, exitMsg = j.CheckObject()
	if exitCode > 0 {
		return exitCode, exitMsg
	}

	// Starten van een transactie
	logging.InfoMSG(prefix + "Starting SQL Transaction")
	tx, err := repository.DBConnection.Begin()
	if err != nil {
		logging.ErrorMSG(prefix + "Failed to start a SQL transaction: " + err.Error())
	}

	// Opslaan van een bestaande JobSource
	if j.Id > 0 {

		logging.DevMSG("Saving an existing JobSource (" + j.FQName + ")")

		isNewJobSource = false

		//Aanpassen van de MetaObject data
		logging.DebugMSG("Preparing SQLCommand repository.SqlCmd_met_006")
		sqlCmd := repository.SqlCmd_met_006
		stmt, err := tx.Prepare(sqlCmd)

		logging.DebugMSG("Updating MetaObject: hash and last_modified_date")
		if err != nil {
			logging.ErrorMSG(prefix + "Failed to parse SQL command: " + err.Error())
		}
		_, err = stmt.Exec(utils.GenerateHash(32, "alphanum"), utils.CurrentTimeStamp(constanten.DateFormatLong), j.Id)
		if err != nil {
			logging.ErrorMSG(prefix + "Failed to execute SQL command: " + err.Error())
			logging.InfoMSG(prefix + "Rollback SQL Transaction")
			tx.Rollback()
		}

		logging.DebugMSG("Preparing SQLCommand repository.SqlCmd_jsc_004")
		sqlCmd = repository.SqlCmd_jsc_004
		stmt, err = tx.Prepare(sqlCmd)
		if err != nil {
			logging.ErrorMSG(prefix + "Failed to parse SQL command: " + err.Error())
		}
		_, err = stmt.Exec(j.Name, j.Description, j.JobSourceType, string(j.SourceCode), j.FQName, j.PartitionId, j.ApplicationId, j.Id)
		if err != nil {
			logging.ErrorMSG(prefix + "Failed to execute SQL command: " + err.Error())
			logging.InfoMSG(prefix + "Rollback SQL Transaction")
			tx.Rollback()
		} else {
			logging.InfoMSG(prefix + naming.JobSourceObjNameEV + " was stored succesfully")
		}

	}

	// Opslaan van het Application Object.
	// Voor een Application wordt een bijbehorend MetaObject aangemaakt
	if j.Id == 0 {
		logging.InfoMSG(prefix + "Storing a new " + naming.JobSourceObjNameEV + " in the repository")

		// Instellen van de Fully Qualified Name voor het object
		j.SetFQName()

		// binden en uitvoeren SQL-commando om de partitie in het geval van een nieuwe Partition
		if j.Id == 0 {

			// Omdat er een nieuwe Application wordt aangemaakt, moet er eerst een MetaObject worden
			// aangemaakt om ondermeer de ID voor de Application te verkrijgen
			metaObject := metaobject.MetaObject{}
			metaObject.SetPartition(j.PartitionId)
			metaObject.SetApplication(j.ApplicationId)
			//TODO metaObjectPartition.SetUser(p.UserId)
			metaObject.SetObjectType(metaobject.ObjectTypeJobSource)

			metaObject.Commit()
			logging.InfoMSG("Created new MetaObject with id " + strconv.FormatInt(metaObject.Id, 10))
			j.Id = metaObject.Id

			// Na de commit is het MetaObject.Id beschikbaar
			logging.DebugMSG("Preparing SQLCommand repository.SqlCmd_jsc_001")
			sqlCmd := repository.SqlCmd_jsc_001
			stmt, err := tx.Prepare(sqlCmd)
			if err != nil {
				logging.ErrorMSG(prefix + "Failed to parse SQL command: " + err.Error())
			}
			_, err = stmt.Exec(metaObject.Id, j.Name, j.Description, j.JobSourceType, string(j.SourceCode), j.FQName, j.PartitionId, j.ApplicationId)
			if err != nil {
				logging.ErrorMSG(prefix + "Failed to execute SQL command: " + err.Error())
				logging.InfoMSG(prefix + "Rollback SQL Transaction")
				tx.Rollback()
			} else {
				logging.InfoMSG(prefix + naming.JobSourceObjNameEV + " was stored succesfully")
			}

		}
	}

	// Opslaan van eventuele aanwezige JobSourceParameters
	logging.DebugMSG("Number of JobSource Parameters=" + strconv.Itoa(len(j.JobSourceParameter)))
	if len(j.JobSourceParameter) > 0 {

		// Verwijderen van eventueel bestaande parameters. Dit alleen doen wanneer de JobSource
		// een bestaande JobSource is
		if isNewJobSource == false {
			j.DeleteCurrentJobSourceParameters()
		}

		// Opslaan van eventuele parameters die zijn gekoppeld aan de JobSource
		var rollbackTx bool = false

		for i := 0; i < len(j.JobSourceParameter); i++ {

			logging.DebugMSG("Saving JobSource Parameter (" + strconv.Itoa(i+1) + " of " + strconv.Itoa(len(j.JobSourceParameter)) + ") named " + j.JobSourceParameter[i].Name)

			// Alleen doorgaan wanneer er een naam voor de Parameter is ingevoerd
			if utils.Trim(j.JobSourceParameter[i].Name) != "" {
				logging.DebugMSG("Preparing SQLCommand repository.SqlCmd_jsp_002")
				sqlCmd := repository.SqlCmd_jsp_002
				stmt, err := tx.Prepare(sqlCmd)
				if err != nil {
					logging.ErrorMSG(prefix + "Failed to parse SQL command: " + err.Error())
				}
				_, err = stmt.Exec(j.Id, j.JobSourceParameter[i].Name, j.JobSourceParameter[i].Description, j.JobSourceParameter[i].DefaultValue)
				if err != nil {
					logging.ErrorMSG(prefix + "Failed to execute SQL command: " + err.Error())
					logging.InfoMSG(prefix + "Rollback SQL Transaction")
					rollbackTx = true
				}
			}
		}

		if rollbackTx {
			tx.Rollback()
		}
	}

	// Afsluiten (commit) van de transactie
	logging.InfoMSG(prefix + "Commit SQL Transaction")
	tx.Commit()

	return exitCode, exitMsg
}

// ************************************************************************
// *                                                                      *
// * SetCurrentVersion()                                                  *
// *                                                                      *
// ************************************************************************
// Functie om een bepaalde versie van een JobSource te bestempelen als de
// current versie. Dit houdt in dat wanneer er bij de submit van een Job
// geen specifieke versie wordt gespecificeerd, deze versie wordt gebruikt

func SetCurrentVersion(jobsourceId int) {

	// Voor alle overige versies van deze JobSource, moet de CurrentVersion
	// indicator op NO worden gezet

}

// ************************************************************************
// * GetJobSourcesByFilter
// * Methode om de FQ-naam + JobSourceType op te halen op basis van een
// * filter. Dit filter wordt toepgepast op de FQName
// * Het resultaat is een HTML-lijst met daarin een link naar de details
// * van de JobSource
// ************************************************************************
func GetJobSourcesByFilter(inFilter string) string {

	// Wanneer de Filter leeg is, wordt dit Filter vervangen door een .
	// Dit zit namelijk in alle FQnames omdat de punt als scheidingsteken
	// wordt gebruikt tussen Partition en Naam
	if utils.Trim(inFilter) == "" {
		inFilter = "."
	}

	var jobSourceFound bool = false

	var js_id int64 = 0
	var js_fqname string = ""
	var js_description string = ""
	var js_type int = 0

	// Filter converteren naar hoofdletters omdat de FQName ook Uppercase is
	// en tevens WildCards toevoegen
	inFilter = "%" + strings.ToUpper(inFilter) + "%"

	var htmlOvz string = ""

	logging.DebugMSG("Preparing SQLCommand repository.SqlCmd_jsc_005")
	sqlCmd := repository.SqlCmd_jsc_005

	rows, err := repository.DBConnection.Query(sqlCmd, inFilter, constanten.No)
	if err != nil {
		logging.ErrorMSG(prefix + "Failed to connect to repository: " + err.Error())
	}

	for rows.Next() {
		err := rows.Scan(&js_id, &js_fqname, &js_description, &js_type)
		if err != nil {
			logging.DebugMSG(err.Error())
		} else {
			jobSourceFound = true
			htmlOvz = htmlOvz + "<tr>"
			htmlOvz = htmlOvz + "<td><a href=/jobsource/details/" + strconv.FormatInt(js_id, 10) + ">" + strconv.FormatInt(js_id, 10) + "</a></td>"
			htmlOvz = htmlOvz + "<td>" + js_fqname + "</td>"
			htmlOvz = htmlOvz + "<td>" + js_description + "</td>"
			htmlOvz = htmlOvz + "<td>" + jobtype.GetJobTypeName(js_type) + "</td>"
			htmlOvz = htmlOvz + "</tr>"
		}
	}

	if jobSourceFound == true {
		var tableHeader string = "<br><table>"
		tableHeader = tableHeader + "<th>SourceID</th>"
		tableHeader = tableHeader + "<th>Name</th>"
		tableHeader = tableHeader + "<th>Description</th>"
		tableHeader = tableHeader + "<th>Type</th>"
		tableHeader = tableHeader + "</tr>"
		htmlOvz = tableHeader + htmlOvz + "</table>"
	} else {
		logging.DebugMSG("No JobSources found matching filter " + inFilter)
	}

	return htmlOvz
}

// ************************************************************************
// * DeleteCurrentJobSourceParameters
// * Methode om de status van de bestaande JobSourceParameters op
// * deleted = YES te zetten
// ************************************************************************
func (j *JobSource) DeleteCurrentJobSourceParameters() {

	logging.DevMSG("Deleting the current Parameters for JobSource " + j.Name)

	tx, err := repository.DBConnection.Begin()
	if err != nil {
		logging.ErrorMSG(prefix + "Failed to start a SQL transaction: " + err.Error())
	}

	logging.DebugMSG("Preparing SQLCommand repository.SqlCmd_jsp_003")
	sqlCmd := repository.SqlCmd_jsp_003
	stmt, err := tx.Prepare(sqlCmd)
	if err != nil {
		logging.ErrorMSG(prefix + "Failed to parse SQL command: " + err.Error())
	}
	_, err = stmt.Exec(constanten.Yes, j.Id)
	if err != nil {
		logging.ErrorMSG(prefix + "Failed to execute SQL command: " + err.Error())
		logging.InfoMSG(prefix + "Rollback SQL Transaction")
		tx.Rollback()
	}

	// Afsluiten (commit) van de transactie
	logging.InfoMSG(prefix + "Commit SQL Transaction")
	tx.Commit()

	// De huidige JobSourceParameters moet worden verwijderd uit het JobSource object
	// Dit gebeurt door de naam van de parameter leeg te maken. Hierdoor wordt
	// deze niet opgeslagen
	//for i := 0; i < len(j.JobSourceParameter); i++ {
	//	j.JobSourceParameter[i].Name = ""
	//}
}

// ************************************************************************
// * GetJobsourceParametersDetailsHTML
// Methode om de JobSourceParameters uit te lijsten (readonly)
// ************************************************************************
func (j *JobSource) GetJobsourceParametersDetailsHTML() string {

	var htmlCode string = ""

	var jobSourceHasParameters = false

	for i := 0; i < len(j.JobSourceParameter); i++ {
		jobSourceHasParameters = true
		htmlCode = htmlCode + "<tr>"
		htmlCode = htmlCode + "<td>" + j.JobSourceParameter[i].Name + "</td>"
		htmlCode = htmlCode + "<td>" + j.JobSourceParameter[i].Description + "</td>"
		htmlCode = htmlCode + "<td>" + j.JobSourceParameter[i].DefaultValue + "</td>"
		htmlCode = htmlCode + "<td>" + j.JobSourceParameter[i].Visible + "</td>"
		htmlCode = htmlCode + "<td>" + j.JobSourceParameter[i].Scope + "</td>"
		htmlCode = htmlCode + "<td>" + j.JobSourceParameter[i].Format + "</td>"
		htmlCode = htmlCode + "<td>" + strconv.Itoa(j.JobSourceParameter[i].Order) + "</td>"
		htmlCode = htmlCode + "<td>" + j.JobSourceParameter[i].Expression + "</td>"
		htmlCode = htmlCode + "</tr>"
	}

	if jobSourceHasParameters == true {
		// Table-definitie toevoegen
		var tableHeader string = ""
		tableHeader = tableHeader + "<table>"
		tableHeader = tableHeader + "<tr>"
		tableHeader = tableHeader + "<th>Name</th>"
		tableHeader = tableHeader + "<th>Description</th>"
		tableHeader = tableHeader + "<th>Default&nbsp;Value</th>"
		tableHeader = tableHeader + "<th>Visible</th>"
		tableHeader = tableHeader + "<th>Scope</th>"
		tableHeader = tableHeader + "<th>Format</th>"
		tableHeader = tableHeader + "<th>Order</th>"
		tableHeader = tableHeader + "<th>Expression</th>"
		tableHeader = tableHeader + "</tr>"
		htmlCode = tableHeader + htmlCode + "</table>"
	}

	return htmlCode
}
