package privilege

var offsetAnyPrivilege = 5000

var Create_Credential = 1000
var Modify_Credential = 1001
var Delete_Credential = 1002
var Select_Credential = 1003
var Create_Any_Credential = Create_Credential + offsetAnyPrivilege
var Modify_Any_Credential = Modify_Credential + offsetAnyPrivilege
var Delete_Any_Credential = Delete_Credential + offsetAnyPrivilege
var Select_Any_Credential = Select_Credential + offsetAnyPrivilege

var PartitionReadMode = 1000
var PartitionReadWriteMode = 1001
var PartitionNoAccessMode = 1002

// **********************************************************************
// Methode om een privilege toe te kennen aan een Group. Om geen
// onderscheid te hoeven maken tussen Users en Groepen, wordt er voor een
// Gebruiker ook altijd een Groep aangemaakt. Wanneer privileges aan de
// gebruiker worden toegekend, gebeurt dit onder water aan de groep die
// specifiek voor de gebruiker is aangemaakt
// **********************************************************************
func Grant(groupId int, priv int) {

}
