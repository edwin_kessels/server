package main

import "fmt"
import "os"
import "strings"
import "path"
import "time"
import "berichten"
import "logging"
import "versieinfo"
import "repository"
import "database/sql"
import _ "github.com/lib/pq"
import "startup"
import "webserver"
import "utils"

//import "centralinstance/agent/wrkstat"
import "centralinstance/agent/wrkcfg"
import "centralinstance/agent/wrkreg"

//import "centralinstance/agent/wrkmetric"
import "centralinstance/agent/wrkhrtbeat"
import "centralinstance/agent/jobstat"
import "centralinstance/agent/scheduler"
import "settings"
import "bytes"
import "os/exec"

// Structuur voor het opslaan van de instellingen van de CentralInstance
type CentralInstanceSettingsType struct {
	StartTime       string
	SoftwareVersion string
	SoftwareDate    string
}

var CentralInstanceSettings CentralInstanceSettingsType

// Command Line parameter
//TODo in een later stadium implementeren met Flags
// Parameter waarmee wordt aangegeven of de inhoud van een bericht moet worden getoond wanneer deze verzonden / ontvangen
// wordt.
var showPingAttemptInactiveWorkers = false
var validateCheckSumInMessage = false

// Global variabelen / constanten specifiek bedoeld voor de Central Instance
var repositorySchema = "centralinstance" + "."

var cfgTimeOutSendMessageInSeconds = 5

// Maximaal aantal pogingen dat wordt ondernomen bij het controleren van de actieve
// workers om te kijken of een Worker actief is. Wanneer dit aantal is overschreden, wordt
// de worker verwijderd uit de lijst en wordt er een bericht naar de Central Instance
// gestuurd
var cfgConnectToServerMaxAttempts = 2

// Wachttijd tussen het versturen van een Ping naar een Worker
var cfgWaitTimeBetweenPingInSec = 2

var stopAgent = false

// Voor diverse doeleinden wordt een scheidingsteken gebruikt. Met behulp van de variabele scheidingsteken kan dit teken
// eenmalig worden gebruikt. Dit wordt ondermeer gebruikt om Workers en Hubs te scheiden in geheugen lijsten welke wel
// en welke niet actief zijn
var scheidingsteken = "|"

var logType = 0
var logSockets = 1
var logMessages = 2

//var cfgID = ""
//var cfgHostname = ""
//var cfgPortNumber = ""
//var cfgEnvironmentID = ""
//var cfgESBHost = ""
//var cfgESBUser = ""
//var cfgESBPassword = ""

//var cfgStartime = ""

// Variabele die als zout fungeert bij het bepalen van de Checksum van een bericht
// Hiervoor wordt de variabele maxLenghtMessage gebruikt, om het moeilijker te maken
// om via reserve eniginering acter de code te komen.
var maxLenghtMessage = 33354232

// Variabelen met betrekking tot de queues die worden gebruikt in de Enterprise Service Bus
// Per onderdeel (HUB en CI) moeten deze natuurlijk verschillend zijn
var queueInbox = "Inbox"
var queueOutbox = "Outbox"

// Tekst van het bericht die wordt gebruikt voor het pingen (bepalen of de Central Instance
// beschikbaar is). Dit soort berichten kunnen direct door de Central Instance worden verwijderd
var pingNaarCentralInstance = "<PingCentralInstance>"

// Directories die worden gebruikt
var homeDirectory = ""
var metaDirectory = ""
var cwdDirectory = ""
var cfgDirectory = ""
var jobDirectory = ""
var tmpDirectory = ""
var naamCfgDirectory = "cfg"
var naamJobDirectory = "job"
var naamMetaDirectory = "meta"
var naamTmpDirectory = "tmp"

// Variabelen die gebruikt wordt voor het eenmalig zetten van de bestandsnaam
// voor het bestand waarin inkomende en uitgaande berichten worden gelogd. De definitieve
// naam wordt gezet waneer bepaald is wat de MetaDirectory is.
var filenameOutbox = ""
var filenameInbox = ""

// Enviroment variabelen die gebruikt worden
var envCFGDIR = "CFGDIR"
var envJOBDIR = "JOBDIR"
var envMETADIR = "METADIR"
var envTMPDIR = "TMPDIR"

// Alle mogelijke jobs die kunnen worden gedefinieerd / uitgevoerd
var jobtype_Linux_sh = 1

// Definitie van een structuur die wordt gebruikt om een bericht te versturen
// Door gebruik te maken van een structuur, is het niet nodig om voor de Worker
// en de HUB een apart sendMessage-procedure te maken. Met behulp van de parameter
// targetServer kan 1 server (ipaddr:portnr) worden gespecificeerd (in het geval dat
// de HUB een bericht naar een specifieke Worker stuurt), of een lijst (wanneer de Worker
// een bericht stuurt waarbij het niet uitmaakt welke HUB dit bericht oppakt)

type pararameterSetSendMessage struct {
	targetServer string
	messageType  string
	message      string
}

// Methode om een wachttijd in te stellen tussen het starten van twee agents
// Hiermee wordt bereikt dat de update van de timestamp voor alle agents
// niet op precies hetzelffde moment wordt uitgevoerd
func sleep() {
	if settings.DevelopmentMode == false {
		time.Sleep(2 * time.Second)
	}
}

// Procedure om uit een Message het juiste type te bepalen. Elke bericht begint
// met de struct-name die is opgenomen als tag (bijvoorbeeld <WorkerConfiguration>)
// Om situaties te voorkomen dat er bijvoorbeeld in een job dezelfde tags worden opgenomen, wordt
// er in onderstaande procedure gezocht wat het eerste voorkomen van > (afsluitende tag is). Alles
// links van deze positie aangevuld met een ">" wordt geretourneerd
func getMessageTypeFromMessage(msg string) string {
	i := strings.Index(msg, ">")
	outvalue := msg[:i] + ">"

	// Het kan voorkomen dat er in het XML-bericht gebruik wordt gemaakt van Indent
	// In dit geval kunnen er spaties staan voor het bericht
	return (strings.Trim(outvalue, " "))
}

func main() {

	// Instellen van de VerboseMode
	logging.SetVerboseMode()

	// Controleren van de PostgreSQL Service
	checkPostgresqlService()

	// vastleggen van de starttime van de CentralInstance
	CentralInstanceSettings.StartTime = time.Now().Format(time.RFC822)
	CentralInstanceSettings.SoftwareVersion = versieinfo.CentralInstanceVersion
	CentralInstanceSettings.SoftwareDate = versieinfo.CentralInstanceDatum

	logging.DevMSG(CentralInstanceSettings.SoftwareVersion + " (Release date " + CentralInstanceSettings.SoftwareDate + ")")

	mainCentralInstance()
	logging.DebugMSG("Central Instance shutdown")
	os.Exit(0)

	// Bepalen van de huidige directory waarin de executable wordt uitgevoerd
	// Dit is namelijk de zogenaamde $HOME/bin directory. Op basis van deze directory
	// worden de overige directories bepaald
	cwdDirectory, _ := os.Getwd()
	logging.DebugMSG("Current Working Directory: " + cwdDirectory)
	homeDirectory = path.Dir(cwdDirectory)

	// Er is een probleem gecontateerd op Windows dat de path.Dir functie niet goed werkt
	// Het resultaat van de aanroep van deze functie geeft namelijk altijd . terug. De
	// consequentie hiervan is dat de job, meta en cfg directory de bin-directoty als
	// parent krijgen terwijl deze op hetzelfde niveau als de bin-directory moet worden aangemaakt
	if strings.Contains(cwdDirectory, homeDirectory) == false {
		logging.DebugMSG("Unable to determine the parent directory. Switching to an alternative methode")
		subDirs := strings.Split(cwdDirectory, string(os.PathSeparator))
		windowsPath := subDirs[0]
		for i := 1; i < len(subDirs)-1; i++ {
			windowsPath = windowsPath + string(os.PathSeparator) + subDirs[i]
		}
		homeDirectory = windowsPath
		logging.DebugMSG("Alternative determined home directory: '" + homeDirectory + "'")

	}

	logging.DebugMSG("Home Directory: " + homeDirectory)
	homeDirectoryConcat := []string{homeDirectory, "bin"}
	sollHomeDirectory := strings.Join(homeDirectoryConcat, string(os.PathSeparator))
	if strings.Trim(homeDirectory, " ") != strings.Trim(sollHomeDirectory, " ") {
		logging.DebugMSG("Executable is not located in the bin-directory")
	}

	// Inlezen van Environment variabelen
	// Hiermee kunnen  de standaard locaties van de CFG, JOB en META directories kunnen worden overruled
	if os.Getenv(envCFGDIR) != "" {
		cfgDirectory = os.Getenv(envCFGDIR)
		logging.DebugMSG("Configuration directory set to '" + cfgDirectory + "' by Environment Variable " + envCFGDIR)
	} else {
		logging.DebugMSG("Environment Variable " + envCFGDIR + " not set")
	}
	if os.Getenv(envJOBDIR) != "" {
		jobDirectory = os.Getenv(envJOBDIR)
		logging.DebugMSG("Job directory set to '" + jobDirectory + "' by Environment Variable " + envJOBDIR)
	} else {
		logging.DebugMSG("Environment Variable " + envJOBDIR + " not set")
	}
	if os.Getenv(envMETADIR) != "" {
		metaDirectory = os.Getenv(envMETADIR)
		logging.DebugMSG("Meta directory set to '" + metaDirectory + "' by Environment Variable " + envMETADIR)
	} else {
		logging.DebugMSG("Environment Variable " + envMETADIR + " not set")
	}
	if os.Getenv(envTMPDIR) != "" {
		tmpDirectory = os.Getenv(envTMPDIR)
		logging.DebugMSG("Tmp directory set to '" + tmpDirectory + "' by Environment Variable " + envTMPDIR)
	} else {
		logging.DebugMSG("Environment Variable " + envTMPDIR + " not set")
	}

	// Wanneer er geen waarde door de Environment variables is gedefinieerd voor de CFG en JOB directory
	// dan worden deze als volgt gedefinieerd:
	// CFG = homeDirectory/cfg
	// JOB = homeDirectory/job
	// META = homeDirectory/meta

	if cfgDirectory == "" {
		cfgDirectoryConcat := []string{homeDirectory, naamCfgDirectory}
		cfgDirectory = strings.Join(cfgDirectoryConcat, string(os.PathSeparator))
		logging.DebugMSG("Configuration directory set to '" + cfgDirectory + "'")
	}

	// De Configuratie directory is nu ingesteld (of door de Environment of door een relatieve directory)
	// Nu controleren of deze bestaat en zo niet of deze kan worden aangemaakt en of deze schrijfbaar is
	if utils.IsWritableDirectory(cfgDirectory) == false {
		// Probleem met de Configuratie Directory
		os.Exit(1)
	}

	if jobDirectory == "" {
		jobDirectoryConcat := []string{homeDirectory, naamJobDirectory}
		jobDirectory = strings.Join(jobDirectoryConcat, string(os.PathSeparator))
		logging.DebugMSG("Job directory set to '" + jobDirectory + "'")
	}

	// De Job directory is nu ingesteld (of door de Environment of door een relatieve directory)
	// Nu controleren of deze bestaat en zo niet of deze kan worden aangemaakt en of deze schrijfbaar is
	if utils.IsWritableDirectory(jobDirectory) == false {
		// Probleem met de Job Directory
		os.Exit(1)
	}

	if metaDirectory == "" {
		metaDirectoryConcat := []string{homeDirectory, naamMetaDirectory}
		metaDirectory = strings.Join(metaDirectoryConcat, string(os.PathSeparator))
		logging.DebugMSG("Meta directory set to '" + metaDirectory + "'")
	}

	// De Meta directory is nu ingesteld (of door de Environment of door een relatieve directory)
	// Nu controleren of deze bestaat en zo niet of deze kan worden aangemaakt en of deze schrijfbaar is
	if utils.IsWritableDirectory(metaDirectory) == false {
		// Probleem met de Meta Directory
		os.Exit(1)
	}

	// In de MetaDirectory wordt voor elk uitgaand bestand een regel gelogd over dit
	// bericht: tijd, recipient, messagetype
	// De bestandsnaam wordt hier eenmalig vastgelegd
	filenameOutbox = metaDirectory + string(os.PathSeparator) + ".outbox"
	filenameInbox = metaDirectory + string(os.PathSeparator) + ".inbox"
	logging.DebugMSG("Log for outgoing messages: " + filenameOutbox)
	logging.DebugMSG("Log for incoming messages: " + filenameInbox)

	if tmpDirectory == "" {
		tmpDirectoryConcat := []string{homeDirectory, naamTmpDirectory}
		tmpDirectory = strings.Join(tmpDirectoryConcat, string(os.PathSeparator))
		logging.DebugMSG("Tmp directory set to '" + tmpDirectory + "'")
	}
	if utils.IsWritableDirectory(tmpDirectory) == false {
		// Probleem met de Tmp Directory
		os.Exit(1)
	}

}

// Procedure om een bericht te versturen naar de Enterprise Service Bus. Deze
// procedure krijgt drie parameters mee:
//  1) bericht
//  2) Queue
//  3) logging

// Wanneer er een tekst is opgenomen in de parameter logging, wordt deze logging als een apart bericht verstuurd naar
// de queue queueLogging. Deze wordt verder verwerkt in de Central Instance.

// Central Instance functionaliteit
func mainCentralInstance() {

	// Initieren van de Database connectie
	//var err error
	var err error
	repository.DBConnection, err = sql.Open("postgres", "user=sched20 password=Welkom01 dbname=sched20 sslmode=disable")
	if err != nil {
		fmt.Println("Probleem met connectie met de Repository:" + err.Error())
	}

	// Uitvoeren van de Startup-Procedure
	// Hierin worden controles uitgevoerd en eventueel verplichten objecten aangemaakt
	// wanneer deze nog niet bestaan
	startup.ExecuteChecks()

	// Starten van de Scheduler. Dit component is verantwoordelijk voor het bepalen of
	// scheduled jobs moeten worden uitgevoerd
	//logging.DevMSG("Starting Scheduler")
	//scheduler.Start()

	// Starten Agent voor bijwerken van Status van Worker
	//go wrkstat.Start()

	// 2 seconden wachten voordat de volgende agent wordt gestart. Dit alleen doen
	// wanneer de Agent neit in de DevelopmentMode draait
	sleep()

	// Starten van de Agent die de WorkerCOnfiguratie-berichten afhandelt
	go wrkcfg.Start()

	// 2 seconden wachten voordat de volgende agent wordt gestart
	sleep()

	// Starten van de Agent om Workers te registeren die vanaf de commandline worden aangemaakt
	go wrkreg.Start()

	// 2 seconden wachten voordat de volgende agent wordt gestart
	sleep()

	// 2 seconden wachten voordat de volgende agent wordt gestart
	sleep()

	// Starten Agent voor verwerking van wijzigingen in JobStatussen
	go jobstat.Start()

	// 2 seconden wachten voordat de volgende agent wordt gestart
	sleep()

	// Agent voor het controleren van de heartbeat van de Worker
	go wrkhrtbeat.Start()

	// 2 seconden wachten voordat de volgende agent wordt gestart
	sleep()

	// Starten van de Scheduler
	go scheduler.Start()

	// 2 seconden wachten voordat de volgende agent wordt gestart
	sleep()

	// Starten van de WebServer voor het hosten van de UI
	logging.ScreenMSG("Starting WebServer")
	webserver.Start()

	os.Exit(0)

}

// **************************************************************************
// * checkPostgresqlService
// * Methode om te controleren of de Postgresql Service op Linux draait
// ***************************************************************************
func checkPostgresqlService() {

	cmd := exec.Command("service", "postgresql", "status")

	// Use a bytes.Buffer to get the output and error
	var bufOut bytes.Buffer
	cmd.Stdout = &bufOut
	var bufErr bytes.Buffer
	cmd.Stderr = &bufErr

	cmd.Start()
	cmd.Wait()

	serviceRepositoryOk := false

	// De log moet 'online' bevatten om er zeker van te zijn dat de PostgreSQL
	// database actief is. Zo niet dan wordt de CentralInstance niet gestart
	if strings.Contains(bufOut.String(), "online") == true {
		serviceRepositoryOk = true
	}

	// De log moet 'online' bevatten om er zeker van te zijn dat de PostgreSQL
	// database actief is. Zo niet dan wordt de CentralInstance niet gestart
	if strings.Contains(bufOut.String(), "Active: active") == true {
		serviceRepositoryOk = true
	}

	if serviceRepositoryOk == false {
		// PostgreSQL database is niet actief
		logging.ErrorMSG("PostgreSQL Repository database seems to be down")
		os.Exit(1)
	}

}

// Procedure om een door de HUB ontvangen ESB-bericht af te handelen
func handlerCentralInstanceIncomingESBMessage(msg string) {

	if berichten.ShowContentMessage == true {
		logging.DebugMSG("-- Start incoming ESB message which will be handled by the Central Instance")
		logging.DebugMSG(msg)
		logging.DebugMSG("-- End incoming ESB message --")
	}

	// Afhandelen van de Ping berichten die worden verstuurd naar de Central Instance
	messageType := pingNaarCentralInstance
	if getMessageTypeFromMessage(msg) == messageType {
		// Dit is een Pingbericht van de HUB
		// Hier hoeft niets mee gedaan te worden
	}

}

// Procedure waarmee logregels kunnen worden toegevoegd aan een extern textbestand.
// De bestandsnaam wordt doorgegeven bij de aanroep van de procedure
func writeLogToFile(filename string, logMessage string) {
	//logging.DebugMSG("writeLogToFile" + logMessage)
	f, _ := os.OpenFile(filenameOutbox, os.O_APPEND, 0666)
	defer f.Close()
	f.WriteString(logMessage)
}
