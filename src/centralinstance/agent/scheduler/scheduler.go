package scheduler

import "logging"
import "time"
import "repository"
import "strconv"
import "constanten"

//import "fmt"
import "centralinstance/job"
import "statussen/jobstatus"
import "settings"

// MemoryMap voor het registeren van Scheduled Jobs
var scheduledJobs map[int64]string // Maps met JobIds die momenteel worden uitgevoerd

var prefix = "SCH"

// ***********************************************************
// GetScheduledJobsCache()
// Methode om te bepalen welke Jobs in de Cache zijn opgenomen om
// gestart te gaan worden door de Scheduler
// ***********************************************************
func GetScheduledJobsCache() string {

	var htmlCode string = ""
	var cacheEmpty bool = true

	for key, value := range scheduledJobs {
		cacheEmpty = false
		htmlCode = htmlCode + "<tr>"
		htmlCode = htmlCode + "<td>" + strconv.FormatInt(key, 10) + "</td>"
		htmlCode = htmlCode + "<td>" + value + "</td>"
		htmlCode = htmlCode + "</tr>"
	}

	if cacheEmpty == false {
		var tableHeader string = ""
		tableHeader = tableHeader + "<h3>Overview Cache Scheduled Jobs</h3>"
		tableHeader = tableHeader + "<table>"
		tableHeader = tableHeader + "<tr>"
		tableHeader = tableHeader + "<th>JobId</th><td>Start</td>"
		tableHeader = tableHeader + "</tr>"
		htmlCode = tableHeader + htmlCode + "</table>"
	}

	return htmlCode
}

func reBuildMemoryMapScheduledJobs() {

	// Controleren of de MemoryMap al geinitialiseerd is. Zo niet
	// dan wordt dat alnog gedaan
	if scheduledJobs == nil {
		logging.DevMSG("Initialising MemoryMap for Scheduled Jobs")
		scheduledJobs = make(map[int64]string)
	}

	logging.DebugMSG("Rebuilding cache for Scheduled jobs")

	// Leegmaken van de bestaande items
	for key, _ := range scheduledJobs {
		logging.DevMSG("Purging Scheduled Jobs")
		delete(scheduledJobs, key)
	}

	// Bepalen van het tijdstip tot waar de scheduled jobs worden gecached
	t := time.Now()
	t = t.Add(180 * time.Minute)
	logging.DevMSG("Caching Scheduled jobs until " + t.Format("2006-01-02 15:04:05"))

	// Variabele om de ingelezen velden te koppelen
	var jobId int64 = 0
	jobRunStart := ""

	// Uitvoeren van de Query om de Scheduled Jobs op te halen
	sqlCmd := repository.SqlCmd_job_006
	rows, err := repository.DBConnection.Query(sqlCmd, jobstatus.Scheduled, t.Format("2006-01-02 15:04:05"))
	if err != nil {
		logging.ErrorMSG(prefix + "Failed to connect to repository: " + err.Error())
	}

	for rows.Next() {
		err := rows.Scan(&jobId, &jobRunStart)
		if err != nil {
			logging.DebugMSG(err.Error())
		} else {
			// Toevoegen van de Job aan de MemoryMap
			logging.DevMSG("Adding job " + strconv.FormatInt(jobId, 10) + " with runstart " + jobRunStart + " to MemoryMap Scheduled Jobs")
			scheduledJobs[jobId] = jobRunStart
		}
	}
}

// Methode om de Scheduler te starten
func Start() {
	logging.InfoMSG("Starting Scheduler")

	reBuildMemoryMapScheduledJobs()

	// Teller mee laten lopen om op bepaalde tijden de Array opnieuw te laten opbouwen
	var cycleCnt int = 0

	// Starten van een loop
	for {

		// Aflopen van de MemoryMap en bepalen of de Job gestart moet worden
		for key, value := range scheduledJobs {
			t := time.Now().Format(constanten.DateFormatLong)
			if value < t {
				// Job kan gestart worden
				logging.DevMSG("Starting job " + strconv.FormatInt(key, 10))
				newJob := job.Job{}
				newJob.GetJobByJobId(key)
				newJob.Start()
				delete(scheduledJobs, key)
			}
		}

		cycleCnt = cycleCnt + 1
		if cycleCnt > 1000 {
			settings.RebuildScheduledJobs = true
			cycleCnt = 0
		}

		// Bepalen of de MemoryMap opnieuw moet worden opgebouwd
		if settings.RebuildScheduledJobs == true {
			reBuildMemoryMapScheduledJobs()
			settings.RebuildScheduledJobs = false
		}

		// 500 Miliseconden wachten
		time.Sleep(500 * time.Millisecond)
		//fmt.Print(".")
	}

}

//func main() {
//	var err error
//	repository.DBConnection, err = sql.Open(constanten.RepositoryType, constanten.RepositoryConnectString)
//	if err != nil {
//		logging.ErrorMSG("Cannot connect to the Repository")
//	}

//	Start()
//}
