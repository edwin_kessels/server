package wrkhrtbeat

import "logging"
import "repository"
import "settings"
import "utils"
import "database/sql"
import _ "github.com/lib/pq"
import "time"
import "os"
import "constanten"
import "github.com/streadway/amqp"
import "esb"
import "encoding/xml"
import "berichten"
import "versieinfo"
import "centralinstance/worker"
import "strconv"
import "statussen/workerstatus"
import "centralinstance/agent/wrkcfg"
import "strings"

var workers map[int64]string


// Instellen van de Agentname. Is ondermeer van belang voor het registeren
// van de agent
var agentName = "wrkhrtbeat"
var agentVersion = versieinfo.WrkhrtbeatVersion
var agentStop = false ; 
var agentLastActivity = "N/A"

var queueName = settings.ESB_INNER_QUEUE_HeartBeat 


// *****************************************************************************
// * monitorHeartBeatWorkers
// * Methode om te controleren of Workers de heartbeat bijwerken. Wanneer de 
// * laatste heartbeat in het verleden ligt, dan wordt aangenomen dat de Worker
// * niet meer actief is. Deze status wordt teruggekoppeld naar de CentralInstance
// *****************************************************************************
func monitorHeartBeatWorkers() {
	
	for {
	
		for k, v := range workers {
		
			currentTimeStamp := utils.CurrentTimeStamp(constanten.DateFormatShort)
		
			// Controleren van de opgeslagen TimeStamp met de CurrentTimeStamp
			if currentTimeStamp > v {
				// Laatste timestamp ligt in het verleden
				// Worker is down
				logging.DevMSG("Worker with ID " + strconv.FormatInt(k,10) + " did not update the TimeStamp" )
				logging.DevMSG("Worker is removed from Monitoring")
				delete (workers, k)
				
				// Updaten van de status in de Repository
				// De actual status moet worden gewijzigd in Connecting
				// De actual status moet worden gewijzigd in NotAvailable
				worker.ChangeActualStatus(k, workerstatus.Connecting)
				worker.ChangeRequestedStatus(k, workerstatus.NotAvailable)
				
				
				//var tmpXML berichten.WorkerUpdateStatus
				//tmpXML.WorkerId = k
				//tmpXML.ActualStatus	= workerstatus.Down
				//tmpXML.Comment = "Worker is not responding"
				//tmpXML.TimeStamp = utils.CurrentTimeStamp(constanten.DateFormatShort)
				//tmpXML.CheckSum = berichten.GetCheckSumWorkerUpdateStatus(tmpXML)	
				//xmlBericht, _ := xml.MarshalIndent(tmpXML, "", constanten.XMLIndent)
				//esb.SendMessageESBWithoutWait(string(xmlBericht), settings.ESB_IN_Queue_WorkerStatus )  
			}
		
		}
		time.Sleep(5 * time.Second)
	}
}


// **************************************************************
// Methode om naar de Queue te luisteren of er WorkerRegistration
// berichten binnen komen. Wanneer dit het geval is, worden deze
// ingelezen en verwerkt in de repository
// **************************************************************
func listener() {
    
    logging.InfoMSG("Listening on queue '" + queueName + "'")
	conn, err := amqp.Dial(esb.ConnectString)
	utils.FailOnError(err, "Failed to connect to RabbitMQ")
	defer conn.Close()

	ch, err := conn.Channel()
	utils.FailOnError(err, "Failed to open a channel")
	defer ch.Close()

	q, err := ch.QueueDeclare(queueName, false, false, false, false, nil)
	utils.FailOnError(err, "Failed to declare a queue")

	msgs, err := ch.Consume(q.Name, "", true, false, false, false, nil)
	utils.FailOnError(err, "Failed to register a consumer")

	forever := make(chan bool)

	go func() {
		for d := range msgs {
			
			// Er zijn 2 verschillende acties mogelijk
			action := "HEARTBEATWORKER"
			action = "MONITORWORKER"
			
			if strings.Contains(string(d.Body), "WorkerHeartBeat")  == true {
				action = "HEARTBEATWORKER"
			}
			
			// Afhandelen van een MonitorWorker bericht
			if action == "MONITORWORKER" {
				// Converteren van het bericht naar een Object
				var tmpXML berichten.MonitorWorker
				xml.Unmarshal(d.Body, &tmpXML)
			
				// Door de CentralInstance is er aangegeven dat er een nieuwe Worker
				// moet worden gemonitored. Dit wordt geadministreerd in de MemoryMap workers
				// Er moeten echter 2 gegevens worden bijgehouden, dus er wordt gebruik gemaakt
				// van een csv-lijst (ExpireTimeInSeconds;CurrentTimeStamp;DeadManSwitchTimeStamp)
				tmp := utils.DeadManSwitchTimeStamp(30)
				workers[tmpXML.WorkerId] = tmp
				logging.DevMSG("Worker with ID " + strconv.FormatInt(tmpXML.WorkerId,10)  + " has been added to monitored Workers " + tmp)
			}
			
			// Afhandelen van een HeartBeatWorker bericht
			if action == "HEARTBEATWORKER" {
				var tmpXML berichten.WorkerHeartBeat
				xml.Unmarshal(d.Body, &tmpXML)
				tmp := utils.DeadManSwitchTimeStamp(30)
				workers[tmpXML.WorkerId] = tmp
				
				// Status aanpassen naar Running. Dit moet alleen gebeuren wanneer
				// de oude status Connecting was om geen Workers die met opzet Down
				// zijn te starten
				wrk := worker.Worker{}
				wrk.GetWorkerById(tmpXML.WorkerId)
				
				// Wanneer de Worker de status Created heeft, moet dit worden gewijzigd
				// in de status Connecting
				if wrk.GetActualStatus() == workerstatus.Created {
					worker.ChangeActualStatus(tmpXML.WorkerId, workerstatus.Connecting)
				}
				
				if wrk.GetActualStatus() == workerstatus.Connecting {
				  worker.ChangeActualStatus(tmpXML.WorkerId, workerstatus.Running)
				  worker.ChangeRequestedStatus(tmpXML.WorkerId, workerstatus.Running)
				  
				  // De status van de Worker is naar Running gezet. In dat geval is het 
				  // nodig om de configuratie van de Worker opnieuw op te vragen. 
				  wrkcfg.RequestWorkerConfiguration(wrk)
				  
				}
				
				// Load-Metrics van de Worker bijwerken
				if settings.CollectWorkerMetrics == constanten.Yes {
					// Converteren van de Load Metrics van String naar int
					load1, err1 := strconv.Atoi(tmpXML.Load1)
					if err1 != nil {
						load1 = 0   
					}		
					load5, err5 := strconv.Atoi(tmpXML.Load5)
					if err5 != nil {
						load5 = 0   
					}
					load15, err15 := strconv.Atoi(tmpXML.Load15)
					if err15 != nil {
						load15 = 0   
					}
			 
					wrk.Load1 = load1
					wrk.Load5 = load5
					wrk.Load15 = load15
					logging.DebugMSG("Saving load statistics for Worker " + wrk.GetName() )
					wrk.SetLoadMetrics()
					
					// Bepalen of de Worker Overloaded is
					if wrk.IsOverloaded() == true && wrk.GetActualStatus() == workerstatus.Running {
						worker.ChangeActualStatus(tmpXML.WorkerId, workerstatus.Overload)
					}
					
					// Bepalen of een Overloaded Worker weer naar Running kan worden gezet
					if wrk.IsOverloaded() == false && wrk.GetActualStatus() == workerstatus.Overload {
						worker.ChangeActualStatus(tmpXML.WorkerId, workerstatus.Running)
					}
					
					
				}
				
				logging.DevMSG("Updating Heartbeat of Worker with ID " + strconv.FormatInt(tmpXML.WorkerId,10)  + " to " + tmp)
				
			}
			
			
		}
	}()

	<-forever
    
}



// **************************************************************
// Methode om met een bepaalde tijdsinterval de huidige timestamp
// naar de tabel agent weg te schrijven. Dit wordt gedaan om te 
// kunnen controleren of de agent nog actief is
// **************************************************************
func UpdateTimeStampAgentInRepository() {
    
    for {
    
        // Initialiseren van de tabel waarin informatie over de agents wordt opgeslagen
	    stmt, err := repository.DBConnection.Prepare(repository.SqlCmd_agt_002)
	    if err != nil {
		    logging.ErrorMSG("Cannot prepare SQL Command:")
		    logging.ErrorMSG(err.Error())
	    }
	    _, err = stmt.Exec(utils.CurrentTimeStamp("2006-01-02 15:04:05"), agentLastActivity, agentVersion, agentName)
	    if err != nil {
		    logging.ErrorMSG("Cannot execute SQL Command:")
		    logging.ErrorMSG(err.Error())
	    }
	    
	    // 20 Seconden wachten tot de volgende update
	    time.Sleep(settings.WrkhrtbeatIntervalUpdateTimestampInSeconds)
	    
	}
    
}


func agentActive() bool {
    
    outActive := false 
    sqlCmd := repository.SqlCmd_agt_004
    rows, err := repository.DBConnection.Query(sqlCmd, agentName)
	if err != nil {
	    logging.ErrorMSG("Failed to connect to repository: " + err.Error())
	}
	    
	active := "" 

    for rows.Next() {
	    err := rows.Scan(&active)
		if err != nil {
		    logging.ErrorMSG(err.Error())
	    } 
    }
    
    if active == constanten.Yes {
        outActive = true 
    }
    
    return outActive
}



func Start() {
    
    
    logging.InfoMSG("Starting agent for Worker Registration [" + agentName + "]")
    logging.InfoMSG(versieinfo.WrkhrtbeatVersion + " ("  + versieinfo.WrkhrtbeatDatum + ")")
    
    workers = make(map[int64]string) 
    
    // Initialiseren van de connectie met de Repository
    var err error
	repository.DBConnection, err = sql.Open(settings.RepositoryType, settings.RepositoryConnectString)
	if err != nil {
		logging.ErrorMSG("Cannot connect to the Repository:" + err.Error())
		 os.Exit(1)
	}
	
	
	// Controleren of de Agent wel op actief staat. Zo niet, dan hoeft de agent
	// namelijk niet gestart te worden
	if agentActive() == false {
	    logging.InfoMSG("Agent is set to inactive")
	    logging.InfoMSG("Agent will no be started")
	    os.Exit(0)
	}
	
	
    
    // Starten van het proces dat de Timestamp aanpast van de agent
    go UpdateTimeStampAgentInRepository() 
    
    // Starten van de listener
    go listener() 
    
    // Background proces starten voor het afhandelen van MonitorWorker berichten
	go monitorHeartBeatWorkers() 
    
    // In een endless-loop gaan en wachten totdat de stop-indicator voor deze agent
    // op YES wordt gezet
    
    for {
        sqlCmd := repository.SqlCmd_agt_003
        rows, err := repository.DBConnection.Query(sqlCmd, agentName)
	    if err != nil {
		    logging.ErrorMSG("Failed to connect to repository: " + err.Error())
	    }
	    
	    stopInd := "" 

	    for rows.Next() {
		    err := rows.Scan(&stopInd)
		    if err != nil {
			    logging.ErrorMSG(err.Error())
		    } 
        }
        
        if stopInd == constanten.Yes {
            // Stoppen van de Agent
            logging.InfoMSG("Stop signal received for agent.")
            os.Exit(0)
        }
        
        // 10 Seconden wachten tot de volgende controle
	    time.Sleep(10 * time.Second)
        
    }
}