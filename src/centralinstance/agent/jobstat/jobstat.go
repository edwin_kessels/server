package jobstat 

import "logging"
import "repository"
import "settings"
import "utils"
import _ "github.com/lib/pq"
import "time"
import "os"
import "constanten"
import "github.com/streadway/amqp"
import "esb"
import "encoding/xml"
import "berichten"
import "versieinfo"
import "strings"
import "strconv"
import "io/ioutil"
import "centralinstance/job"


var prefix = "JobStat"

// Instellen van de Agentname. Is ondermeer van belang voor het registeren
// van de agent
var agentName = "jobstat"
var agentVersion = versieinfo.JobStatVersion
var agentStop = false ; 
var agentLastActivity = "N/A"

var queueName = settings.ESB_QUEUE_JobStatus 



// **************************************************************
// Methode om naar de Queue te luisteren of er WorkerRegistration
// berichten binnen komen. Wanneer dit het geval is, worden deze
// ingelezen en verwerkt in de repository
// **************************************************************
func listener() {
    
    logging.InfoMSG("Listening on queue '" + queueName + "'")
	conn, err := amqp.Dial(esb.ConnectString)
	utils.FailOnError(err, "Failed to connect to RabbitMQ")
	defer conn.Close()

	ch, err := conn.Channel()
	utils.FailOnError(err, "Failed to open a channel")
	defer ch.Close()

	q, err := ch.QueueDeclare(queueName, false, false, false, false, nil)
	utils.FailOnError(err, "Failed to declare a queue")

	msgs, err := ch.Consume(q.Name, "", true, false, false, false, nil)
	utils.FailOnError(err, "Failed to register a consumer")

	forever := make(chan bool)

	go func() {
		for d := range msgs {
		    // Update van de agent_last_activity veld
		    agentLastActivity = utils.CurrentTimeStamp("2006-01-02 15:04:05")
			//logging.InfoMSG(string(d.Body))
								
			logging.DevMSG("Processing Job Status Change")
			
			// Bepalen van het type van het bericht
			// Hiervoor wordt gekeken in de eerst 
			action := ""
			
			if strings.Contains(string(d.Body), "<JobStatusChange>")  == true {
				action = "JOBSTATUSCHANGE"
			}
			
			if strings.Contains(string(d.Body), "<JobStatusCompletion>")  == true {
				action = "JOBSTATUSCOMPLETION"
			}
			
			if strings.Contains(string(d.Body), "<JobFileTransfer>")  == true {
				action = "JOBFILETRANSFER"
			}
			
			
			// *************************************************
			// * Verwerken van een inkomende JobFile
			// *************************************************
			if action == "JOBFILETRANSFER" {
				var tmpXML berichten.JobFileTransfer
				xml.Unmarshal(d.Body, &tmpXML)
				
				// Wegschrijven van de File
				fileName := settings.JobDirectoryCentralInstance + string(os.PathSeparator) + tmpXML.JobId  + string(os.PathSeparator) +tmpXML.JobFileName
				err = ioutil.WriteFile(fileName, []byte(string(tmpXML.JobFileContent)), 0640)
    			if err != nil {
        			logging.ErrorMSG("Cannot write Job File '" + fileName + "'")
        			
    			}
    			
    			// Registeren van de JobFile in de Repository
    			stmt, err := repository.DBConnection.Prepare(repository.SqlCmd_jobfile_001)
	    		if err != nil {
		    		logging.ErrorMSG("Cannot prepare SQL Command:")
		    		logging.ErrorMSG(err.Error())
	    		}
	    		_, err = stmt.Exec(tmpXML.JobId, tmpXML.JobFileType, len(tmpXML.JobFileContent), fileName )
	    		if err != nil {
		   			logging.ErrorMSG("Cannot execute SQL Command:")
		  			logging.ErrorMSG(err.Error())
	    		}
    			
    			logging.DebugMSG("Jobfile " + fileName + " has been created" )
				
			}
			
			
			// *************************************************
			// * Verwerken van een bericht met informatie van 
			// * een Job die een eindstatus heeft gekregen
			// *************************************************
			if action == "JOBSTATUSCOMPLETION" {
				var tmpXML berichten.JobStatusCompletion
				xml.Unmarshal(d.Body, &tmpXML)
				
			stmt, err := repository.DBConnection.Prepare(repository.SqlCmd_job_010)
	    	if err != nil {
		    	logging.ErrorMSG("Cannot prepare SQL Command:")
		    	logging.ErrorMSG(err.Error())
	    	}
	    	tmpJobid, _ := strconv.Atoi(tmpXML.JobId)
	    	_, err = stmt.Exec(tmpXML.Status, tmpXML.ExitCode, utils.CurrentTimeStamp("2006-01-02 15:04:05"), tmpXML.ElapsedTime, tmpJobid)
	    	if err != nil {
		   	 logging.ErrorMSG("Cannot execute SQL Command:")
		  	  logging.ErrorMSG(err.Error())
	    	}
	    	
	    	// Opnemen van de JobLog in de tabel joblog 
	    	stmt, err = repository.DBConnection.Prepare(repository.SqlCmd_joblog_001)
	    	if err != nil {
		    	logging.ErrorMSG("Cannot prepare SQL Command:")
		    	logging.ErrorMSG(err.Error())
	    	}
	    	tmpJobid, _ = strconv.Atoi(tmpXML.JobId)
	    	_, err = stmt.Exec(tmpJobid, tmpXML.ExecutionLog)
	    	if err != nil {
		   	 logging.ErrorMSG("Cannot execute SQL Command:")
		  	  logging.ErrorMSG(err.Error())
	    	}
	    	
				
			}	
			
			
			// *************************************************
			// * Verwerken van een Worker Change Status bericht
			// *************************************************
			if action == "JOBSTATUSCHANGE" {
				var tmpXML berichten.JobStatusChange
				xml.Unmarshal(d.Body, &tmpXML)
				tmpJobId, _ := strconv.ParseInt(utils.Trim(tmpXML.JobId), 10, 64)
	    		job.ChangeJobStatus(tmpJobId, tmpXML.Status)
			}
			
		}	
		
	}()

	<-forever
    
}



// **************************************************************
// Methode om met een bepaalde tijdsinterval de huidige timestamp
// naar de tabel agent weg te schrijven. Dit wordt gedaan om te 
// kunnen controleren of de agent nog actief is
// **************************************************************
func UpdateTimeStampAgentInRepository() {
    
    for {
    
        // Initialiseren van de tabel waarin informatie over de agents wordt opgeslagen
	    stmt, err := repository.DBConnection.Prepare(repository.SqlCmd_agt_002)
	    if err != nil {
		    logging.ErrorMSG("Cannot prepare SQL Command:")
		    logging.ErrorMSG(err.Error())
	    }
	    _, err = stmt.Exec(utils.CurrentTimeStamp("2006-01-02 15:04:05"), agentLastActivity, agentVersion, agentName)
	    if err != nil {
		    logging.ErrorMSG("Cannot execute SQL Command:")
		    logging.ErrorMSG(err.Error())
	    }
	    
	    // 20 Seconden wachten tot de volgende update
	    time.Sleep(settings.WrkcfgIntervalUpdateTimestampInSeconds)
	    
	}
    
}


func agentActive() bool {
    
    outActive := false 
    sqlCmd := repository.SqlCmd_agt_004
    rows, err := repository.DBConnection.Query(sqlCmd, agentName)
	if err != nil {
	    logging.ErrorMSG("Failed to connect to repository: " + err.Error())
	}
	    
	active := "" 

    for rows.Next() {
	    err := rows.Scan(&active)
		if err != nil {
		    logging.ErrorMSG(err.Error())
	    } 
    }
    
    if active == constanten.Yes {
        outActive = true 
    }
    
    return outActive
}



func Start() {
    
    
    logging.InfoMSG("Starting agent for Processing Job Statusses [" + agentName + "]")
    logging.InfoMSG(versieinfo.JobStatVersion + " ("  + versieinfo.JobStatDatum + ")")
    
    // Initialiseren van de connectie met de Repository
    //var err error
	//repository.DBConnection, err = sql.Open(settings.RepositoryType, settings.RepositoryConnectString)
	//if err != nil {
	//	logging.ErrorMSG("Cannot connect to the Repository:" + err.Error())
	//	 os.Exit(1)
	//}
	
	
	// Bepalen of er Commandline-argumenten zijn meegegeven bij het opstarten 
    // van de Agent. 
    //cmdArgActive := flag.Bool("active", false, "Set the state of the agent to active")
    //cmdArgInActive := flag.Bool("inactive", false, "Set the state of the agent to inactive")
    //flag.Parse()
    
    
    
    //if *cmdArgActive == true || *cmdArgInActive == true {
    //	// De status van de agent wordt aangepast
    //	agentActive := constanten.No
    //	newState := "Inactive"
    //	if *cmdArgActive == true {
    //		agentActive = constanten.Yes
    //		newState = "Active"
    //	}
    	
    	// Wijziging opslaan in de database
	  //  stmt, err := repository.DBConnection.Prepare(repository.SqlCmd_agt_005)
	  //  if err != nil {
		//    logging.ErrorMSG("Cannot prepare SQL Command:")
		  //  logging.ErrorMSG(err.Error())//
	  //  }
	    //_, err = stmt.Exec(agentActive, agentName)
	   // if err != nil {
		 //   logging.ErrorMSG("Cannot execute SQL Command:")
		   // logging.ErrorMSG(err.Error())
	    //}
    	
//    	logging.InfoMSG("State of the Agent has been changed to " + newState)
  //  	os.Exit(0)
    //}
	
	
	// Controleren of de Agent wel op actief staat. Zo niet, dan hoeft de agent
	// namelijk niet gestart te worden
	if agentActive() == false {
	    logging.InfoMSG("Agent is set to inactive")
	    logging.InfoMSG("Agent will no be started")
	    os.Exit(0)
	}
	
	
    
    // Starten van het proces dat de Timestamp aanpast van de agent
    go UpdateTimeStampAgentInRepository() 
    
    // Starten van de listener
    go listener() 
    
    // In een endless-loop gaan en wachten totdat de stop-indicator voor deze agent
    // op YES wordt gezet
    
    for {
        sqlCmd := repository.SqlCmd_agt_003
        rows, err := repository.DBConnection.Query(sqlCmd, agentName)
	    if err != nil {
		    logging.ErrorMSG("Failed to connect to repository: " + err.Error())
	    }
	    
	    stopInd := "" 

	    for rows.Next() {
		    err := rows.Scan(&stopInd)
		    if err != nil {
			    logging.ErrorMSG(err.Error())
		    } 
        }
        
        if stopInd == constanten.Yes {
            // Stoppen van de Agent
            logging.InfoMSG("Stop signal received for agent.")
            os.Exit(0)
        }
        
        // 10 Seconden wachten tot de volgende controle
	    time.Sleep(10 * time.Second)
        
    }
}