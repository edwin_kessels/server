package main

import "logging"
import "repository"
import "settings"
import "utils"
import "database/sql"
import _ "github.com/lib/pq"
import "time"
import "os"
import "constanten"
import "github.com/streadway/amqp"
import "esb"
import "encoding/xml"
import "berichten"
import "versieinfo"
import "flag"

// Instellen van de Agentname. Is ondermeer van belang voor het registeren
// van de agent
var agentName = "jobcomp"
var agentVersion = versieinfo.JobCompVersion
var agentStop = false;
var agentLastActivity = "N/A"

var queueName = settings.ESB_IN_Queue_JobCompletion


// **************************************************************
// Methode om naar de Queue te luisteren of er WorkerRegistration
// berichten binnen komen. Wanneer dit het geval is, worden deze
// ingelezen en verwerkt in de repository
// **************************************************************
func listener() {

    logging.InfoMSG("Listening on queue '" + queueName + "'")
    conn, err:= amqp.Dial(esb.ConnectString)
    utils.FailOnError(err, "Failed to connect to RabbitMQ")
    defer conn.Close()

    ch, err:= conn.Channel()
    utils.FailOnError(err, "Failed to open a channel")
    defer ch.Close()

    q, err:= ch.QueueDeclare(queueName, false, false, false, false, nil)
    utils.FailOnError(err, "Failed to declare a queue")

    msgs, err:= ch.Consume(q.Name, "", true, false, false, false, nil)
    utils.FailOnError(err, "Failed to register a consumer")

    forever:= make(chan bool)

    go func() {
        for d:= range msgs {
            // Update van de agent_last_activity veld
            agentLastActivity = utils.CurrentTimeStamp("2006-01-02 15:04:05")

            // Converteren van het bericht naar een Object
            var tmpXML berichten.JobCompletion
            xml.Unmarshal(d.Body, & tmpXML)

            logging.DebugMSG(utils.CurrentTimeStamp("2006-01-02 15:04:05") + " Receiving JobCompletion Message from " + tmpXML.WorkerAddress + ":" + tmpXML.PortNumber)

            // Er moet al een record bestaan in de job tabel omdat de Job uiteraard al is aangemaakt.
            // Er moet dus een update van het record plaatsvinden

            stmt, err:= repository.DBConnection.Prepare(repository.SqlCmd_job_008)
            if err != nil {
                logging.ErrorMSG("Cannot prepare SQL Command:")
                logging.ErrorMSG(err.Error())
            }
            _, err = stmt.Exec(tmpXML.ExitCode, tmpXML.Status, tmpXML.ElapsedTime, tmpXML.JobId)
            if err != nil {
                logging.ErrorMSG("Cannot execute SQL Command:")
                logging.ErrorMSG(err.Error())
            }

        }
    }()

    <-forever

}



// **************************************************************
// Methode om met een bepaalde tijdsinterval de huidige timestamp
// naar de tabel agent weg te schrijven. Dit wordt gedaan om te 
// kunnen controleren of de agent nog actief is
// **************************************************************
func UpdateTimeStampAgentInRepository() {

    for {

        // Initialiseren van de tabel waarin informatie over de agents wordt opgeslagen
        stmt,
        err:= repository.DBConnection.Prepare(repository.SqlCmd_agt_002)
        if err != nil {
            logging.ErrorMSG("Cannot prepare SQL Command:")
            logging.ErrorMSG(err.Error())
        }
        _,
        err = stmt.Exec(utils.CurrentTimeStamp("2006-01-02 15:04:05"), agentLastActivity, agentVersion, agentName)
        if err != nil {
            logging.ErrorMSG("Cannot execute SQL Command:")
            logging.ErrorMSG(err.Error())
        }

        // 20 Seconden wachten tot de volgende update
        time.Sleep(settings.WrkregIntervalUpdateTimestampInSeconds)

    }

}


func agentActive() bool {

    outActive:= false
    sqlCmd:= repository.SqlCmd_agt_004
    rows,
    err:= repository.DBConnection.Query(sqlCmd, agentName)
    if err != nil {
        logging.ErrorMSG("Failed to connect to repository: " + err.Error())
    }

    active:= ""

        for rows.Next() {
        err:= rows.Scan( & active)
        if err != nil {
            logging.ErrorMSG(err.Error())
        }
    }

        if active == constanten.Yes {
        outActive = true
    }

    return outActive
}



func main() {


    logging.InfoMSG("Starting agent for Worker Registration [" + agentName + "]")
    logging.InfoMSG(versieinfo.JobCompVersion + " (" + versieinfo.JobCompDatum + ")")

    // Initialiseren van de connectie met de Repository
    var err error
    repository.DBConnection, err = sql.Open(settings.RepositoryType, settings.RepositoryConnectString)
    if err != nil {
        logging.ErrorMSG("Cannot connect to the Repository:" + err.Error())
        os.Exit(1)
    }


    // Bepalen of er Commandline-argumenten zijn meegegeven bij het opstarten 
    // van de Agent. 
    cmdArgActive:= flag.Bool("active", false, "Set the state of the agent to active")
    cmdArgInActive:= flag.Bool("inactive", false, "Set the state of the agent to inactive")
    flag.Parse()



    if *cmdArgActive == true || * cmdArgInActive == true {
        // De status van de agent wordt aangepast
        agentActive:= constanten.No
        newState:= "Inactive"
        if *cmdArgActive == true {
            agentActive = constanten.Yes
            newState = "Active"
        }

        // Wijziging opslaan in de database
        stmt,
        err:= repository.DBConnection.Prepare(repository.SqlCmd_agt_005)
        if err != nil {
            logging.ErrorMSG("Cannot prepare SQL Command:")
            logging.ErrorMSG(err.Error())
        }
        _,
        err = stmt.Exec(agentActive, agentName)
        if err != nil {
            logging.ErrorMSG("Cannot execute SQL Command:")
            logging.ErrorMSG(err.Error())
        }

        logging.InfoMSG("State of the Agent has been changed to " + newState)
        os.Exit(0)
    }


    // Controleren of de Agent wel op actief staat. Zo niet, dan hoeft de agent
    // namelijk niet gestart te worden
    if agentActive() == false {
        logging.InfoMSG("Agent is set to inactive")
        logging.InfoMSG("Agent will no be started")
        os.Exit(0)
    }



    // Starten van het proces dat de Timestamp aanpast van de agent
    go UpdateTimeStampAgentInRepository()

    // Starten van de listener
    go listener()

    // In een endless-loop gaan en wachten totdat de stop-indicator voor deze agent
    // op YES wordt gezet

    for {
        sqlCmd:= repository.SqlCmd_agt_003
        rows,
        err:= repository.DBConnection.Query(sqlCmd, agentName)
        if err != nil {
            logging.ErrorMSG("Failed to connect to repository: " + err.Error())
        }

        stopInd:= ""

            for rows.Next() {
            err:= rows.Scan( & stopInd)
            if err != nil {
                logging.ErrorMSG(err.Error())
            }
        }

            if stopInd == constanten.Yes {
            // Stoppen van de Agent
            logging.InfoMSG("Stop signal received for agent.")
            os.Exit(0)
        }

        // 10 Seconden wachten tot de volgende controle
        time.Sleep(10 * time.Second)

    }
}