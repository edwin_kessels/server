package jobtype

import "strconv"

var Linux_sh         = 1
var PostgreSQL       = 2 
var numJobType = 2


func GetJobTypeName(jobType int) string {
    
    outName := ""
    
    if jobType == 1 {
        outName = "Linux Shell script (sh)"
    }
    if jobType == 2 {
        outName = "PostgreSQL Script"
    }
    
    return outName
}


func GetHTMLListOfJobTypes(defaultId int) string {
    
    var outHTML string = ""
    
    for i := 1; i <= numJobType; i++ {
        tagSelected := ""
        if defaultId == i {
            tagSelected = "selected"  
        }
        outHTML = outHTML + "<option " + tagSelected +  " value=" + strconv.Itoa(i) +  ">" + GetJobTypeName(i) + "</option>" 
    }
    
    outHTML = "<select name='jobtype'>" + outHTML + "</select>"
	
	return outHTML
}