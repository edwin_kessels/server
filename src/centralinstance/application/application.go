package application

import "repository"
import "logging"
import "constanten"
import "naming"
import "strconv"
import "strings"
import "melding"
import "validatie"
import "utils"
import "centralinstance/metaobject"
import "centralinstance/audit"

var prefix = naming.ApplicationObjNameEV + ": "

var auditAction = ""

type Application struct {
	Id          int64
	Name        string
	FQName      string
	Description string
	Default     string
	Internal    string
	Deleted     string
}

// **********************************************************************
// Methode om de naam van een Application in te stellen
// **********************************************************************
func (a *Application) SetName(appName string) {
	a.Name = appName
	logging.DevMSG(prefix + "Set " + naming.PartitionObjNameEV + " Name to '" + appName + "'")
}

// **********************************************************************
// Methode om de naam van een Application op te vragen
// **********************************************************************
func (a *Application) GetName() string {
	return a.Name
}

// **********************************************************************
// Methode om de omschrijving van een Application in te stellen
// **********************************************************************
func (a *Application) SetDescription(appDesc string) {
	a.Description = appDesc
	logging.DevMSG(prefix + "Set " + naming.PartitionObjNameEV + " description to '" + appDesc + "'")
}

// **********************************************************************
// Methode om de omschrijving van een Application op te vragen
// **********************************************************************
func (a *Application) GetDescription() string {
	return a.Description
}

// **********************************************************************
// * Verwijderen van een Application
// * Dit houdt in dat de Application niet wordt verwijderd, maar de
// * delete-indicator op Yes wordt gezet.
// **********************************************************************
func (a *Application) Delete() {
	a.Deleted = constanten.Yes
	logging.DevMSG(prefix + naming.ApplicationObjNameEV + " is marked for deletion")

}

func (a *Application) GetFQName() string {
	return  a.FQName
}

func (a *Application) SetFQName() {
	tmp := a.GetName()
	tmp = utils.Trim(tmp)
	tmp = strings.ToUpper(tmp)
	a.FQName = tmp
	logging.DevMSG("Set object Fully Qualified Name to '" + a.FQName + "'")
}

// **********************************************************************
// * Methode om de gegevens van een Application op te halen op basis
// * van de ApplicationId
// **********************************************************************

// **********************************************************************
// * Methode om de gegevens van een Application op te halen op basis van
// * de Application Name. In onderstaande methode wordt er op basis van
// * de name het ApplicationId bepaald. Vervolgens wordt GetApplicationByID
// * aangeroepen om alle eigenschappen op te halen
// **********************************************************************
func (a *Application) GetApplicationByFQName(applicationFQName string) {

	applicationName := strings.ToUpper(utils.Trim(applicationFQName))
	applicationFound := false

	var application_id int64 = 0

	sqlCmd := repository.SqlCmd_app_006
	logging.DevMSG(prefix + "getting " + naming.ApplicationObjNameEV + " information by name (" + applicationName + ")")

	rows, err := repository.DBConnection.Query(sqlCmd, applicationName, constanten.No)
	if err != nil {
		logging.ErrorMSG(prefix + "Failed to connect to repository: " + err.Error())
	}

	for rows.Next() {
		err := rows.Scan(&application_id)
		if err != nil {
			logging.DebugMSG(err.Error())
		} else {
			applicationFound = true
			a.GetApplicationByID(application_id)
			logging.DevMSG(naming.ApplicationObjNameEV + " '" + applicationFQName + "' found")
		}
	}

	if applicationFound == false {
		logging.DevMSG(naming.ApplicationObjNameEV + " '" + applicationFQName + "' not found")
	}
}

// **********************************************************************
// * Methode om de gegevens van een Application op te halen op basis
// * van de ApplicationId
// **********************************************************************
func (a *Application) GetApplicationByID(applicationId int64) {

	applicationFound := false
	sqlCmd := repository.SqlCmd_app_005
	logging.DevMSG(prefix + "getting " + naming.ApplicationObjNameEV + " information by id (" + strconv.FormatInt(applicationId,10) + ")")

	// Variabelen declareren die gelezen worden uit de tabel
	application_name := ""
	application_fqname := ""
	application_description := ""
	application_default := ""
	application_internal := ""
	application_deleted := ""

	rows, err := repository.DBConnection.Query(sqlCmd, applicationId, constanten.No)
	if err != nil {
		logging.ErrorMSG(prefix + "Failed to connect to repository: " + err.Error())
	}
	for rows.Next() {
		err := rows.Scan(&application_name, &application_fqname, &application_description, &application_default, &application_internal, &application_deleted)
		if err != nil {
			logging.DebugMSG(err.Error())
		} else {
			logging.DevMSG(naming.ApplicationObjNameEV + " found. Name=" + a.FQName)
			// Overzetten van de gegevens
			applicationFound = true
			a.Id = applicationId
			a.Name = application_name
			a.FQName = application_fqname
			a.Description = application_description
			a.Default = application_default
			a.Internal = application_internal
			a.Deleted = application_deleted
		}
	}

	// Controleren of de Partitie is gevonden
	if applicationFound == false {
		logging.DevMSG(naming.ApplicationObjNameEV + " '" + strconv.FormatInt(applicationId,10) + "' not found")
	}

}

// **********************************************************************
// * Procedure voor het dumpen van het Object. Dit is bedoeld om tijdens
// * development van de applicatie makkelijker problemen te kunnen
// * analyseren
// **********************************************************************
func (a *Application) DumpObject() {
	logging.DevMSG("Id							= " + strconv.FormatInt(a.Id,10))
	logging.DevMSG("FQName						= " + a.FQName)
	logging.DevMSG("Name							= " + a.Name)
	logging.DevMSG("Description					= " + a.Description)
	logging.DevMSG("Default						= " + a.Default)
	logging.DevMSG("Internal						= " + a.Internal)
	logging.DevMSG("Deleted						= " + a.Deleted)
}



// **********************************************************************
// * Opslaan van de Application
// * Hierbij wordt eerst gecontroleerd of alles wel correct is ingevuld
// **********************************************************************
func (a *Application) Commit() (int, string) {

	exitCode := 0
	exitMsg := strings.Replace(melding.Melding_app_001, "$1", a.Name, 1)

	// Wanneer een Application Object wordt aangemaakt, worden de String-velden
	// voorzien van een Null-value. Waneer een Application wordt opgeslagen, moet
	// dit gecontroleerd worden en zonodig worden aangepast
	if a.Deleted == "" {
		a.Deleted = constanten.No
	}
	if a.Internal == "" {
		a.Internal = constanten.No
	}
	if a.Default == "" {
		a.Default = constanten.No
	}

	// Uitvoeren van de Controles voor het Object Application. Wanneer een Application wordt verwijderd
	// hoeven deze checks niet te worden uitgevoerd
	if a.Deleted == constanten.No {
		exitCode, exitMsg := a.CheckObject()
		if exitCode > 0 {
			return exitCode, exitMsg
		}
	}

	// Wanneer de Applicatie niet wordt verwijderd, moet de FQName opnieuw worden gezet
	if a.Deleted == constanten.No {
		a.SetFQName()
	}

	// Aanmaken van een Audit Object voor deze Application
	auditInfo := audit.AuditInfo{}
//	auditInfo.SetObjectType(naming.ApplicationObjNameEV)
//	auditInfo.SetFQName(a.FQName)

	// Controleren of een Application die verwijderd wordt, geen Internal
	// Application is. Dit is namelijk niet toegestaan
	if a.Deleted == constanten.Yes && a.Internal == constanten.Yes {
		return 6, strings.Replace(melding.Melding_app_006, "$1", a.Name, 1)
	}

	// Controleren of de Application al bestaat
	if a.Id == 0 {
		auditInfo.SetActionCreate()
		logging.DevMSG("Saving a new " + naming.ApplicationObjNameEV + ". Check if the " + naming.ApplicationObjNameEV + " '" + a.Name + "' already exists")
		applicationExists := Application{}
		applicationExists.Id = 0
		applicationExists.GetApplicationByFQName(a.FQName)
		if applicationExists.Id > 0 {
			return 4, strings.Replace(melding.Melding_app_004, "$1", a.FQName, 1)
		}
	}

	// Starten van een transactie
	logging.DevMSG(prefix + "Starting SQL Transaction")
	tx, err := repository.DBConnection.Begin()
	if err != nil {
		logging.ErrorMSG(prefix + "Failed to start a SQL transaction: " + err.Error())
	}

	// Opslaan van het Application Object.
	// Voor een Application wordt een bijbehorend MetaObject aangemaakt
	if a.Id == 0 {
		logging.DevMSG(prefix + "Storing a new " + naming.ApplicationObjNameEV + " in the repository")

		// Instellen van de Fully Qualified Name voor het object
		a.SetFQName()

		// binden en uitvoeren SQL-commando om de partitie in het geval van een nieuwe Partition
		if a.Id == 0 {

			// Omdat er een nieuwe Application wordt aangemaakt, moet er eerst een MetaObject worden
			// aangemaakt om ondermeer de ID voor de Application te verkrijgen
			metaObject := metaobject.MetaObject{}
			metaObject.SetPartition(0)
			metaObject.SetApplication(0)
			//TODO metaObjectPartition.SetUser(p.UserId)
			metaObject.SetObjectType(metaobject.ObjectTypeApplication)

			metaObject.Commit()
			logging.DevMSG("Created new MetaObject with id " + strconv.FormatInt(metaObject.Id,10))
			a.Id = metaObject.Id

			// Na de commit is het MetaObject.Id beschikbaar
			sqlCmd := repository.SqlCmd_app_003
			stmt, err := tx.Prepare(sqlCmd)
			if err != nil {
				logging.ErrorMSG(prefix + "Failed to parse SQL command: " + err.Error())
			}
			_, err = stmt.Exec(metaObject.Id, a.Name, a.FQName, a.Description, a.Default, a.Internal, a.Deleted)
			if err != nil {
				logging.ErrorMSG(prefix + "Failed to execute SQL command: " + err.Error())
				logging.DevMSG(prefix + "Rollback SQL Transaction")
				tx.Rollback()
			} else {
				logging.DevMSG(prefix + naming.ApplicationObjNameEV + " was stored succesfully")
			}
		}
	}

	// Logisch verwijderen van een Application
	if a.Deleted == constanten.Yes {
		auditInfo.SetActionDelete()
		auditInfo.SetFQName(a.FQName)
		// Om ervoor te zorgen dat de Partition niet meer gevonden wordt,
		// wordt de FQName tussen sterretjes geplaatst
		a.FQName = constanten.DeleteTag

		sqlCmd := repository.SqlCmd_app_007
		stmt, err := tx.Prepare(sqlCmd)
		if err != nil {
			logging.ErrorMSG(prefix + "Failed to parse SQL command: " + err.Error())
		}
		_, err = stmt.Exec(constanten.Yes, a.Id)
		if err != nil {
			logging.ErrorMSG(prefix + "Failed to execute SQL command: " + err.Error())
		} else {
			// User Account is verwijderd
			exitCode = 0
			exitMsg = strings.Replace(melding.Melding_app_005, "$1", a.Name, 1)
		}

		// Verwijderen van het Meta-object
		logging.DevMSG("Deleting MetaObject")
		sqlCmd = repository.SqlCmd_met_004
		stmt, err = tx.Prepare(sqlCmd)
		if err != nil {
			logging.ErrorMSG(prefix + "Failed to parse SQL command: " + err.Error())
		}
		_, err = stmt.Exec(a.Id)
		if err != nil {
			logging.ErrorMSG(prefix + "Failed to execute SQL command: " + err.Error())
			logging.DevMSG(prefix + "Rollback SQL Transaction")
			tx.Rollback()
		} else {
			logging.DevMSG(prefix + "MetaObject was deleted")
		}

	}

	// Aanmaken van Audit-informatie voor de actie
//	sqlCmd := repository.SqlCmd_aud_003
//	stmt, err := tx.Prepare(sqlCmd)
//	if err != nil {
//		logging.ErrorMSG(prefix + "Failed to parse SQL command: " + err.Error())
//	}
//	_, err = stmt.Exec(0, metaobject.ObjectTypeApplication, a.Id, auditInfo.Action, utils.CurrentTimeStamp(), auditInfo.FQName, auditInfo.ObjectType)
//	if err != nil {
//		logging.ErrorMSG(prefix + "Failed to execute SQL command: " + err.Error())
//	}

//	// Afsluiten (commit) van de transactie
	logging.DevMSG(prefix + "Commit SQL Transaction")
	tx.Commit()
//
	return exitCode, exitMsg

}

// **********************************************************************
// * Controleren van het Application voordat het wordt opgeslagen. Voor een
// * Application geldt eigenlijk alleen dat een geldige naam moet worden
// * toegekend aan de Application. Daarnaast is het van belang te controleren
// * dat een Internal Application niet mag worden verwijderd
// **********************************************************************
func (a *Application) CheckObject() (int, string) {
	exitCode := 0
	exitMsg := "All Application checks were succesful"

	// Eerst de controles voor een Application die niet wordt verwijderd
	if a.Deleted == constanten.No {

		// Controleren van de Object Name van de Application
		exitCode, exitMsg = validatie.Check(a.Name, validatie.ObjectName)
		if exitCode > 0 {
			return exitCode, exitMsg
		}

	}

	return exitCode, exitMsg
}

// Procedure om de default Application op te halen
// Zowel de ApplicationID als Naam worden teruggegeven
func GetDefaultApplication() (int, string) {

	outAppId := 0
	outAppName := ""

	appid := 0
	appname := ""
	rows, _ := repository.DBConnection.Query(repository.SqlCmd_app_001, constanten.Yes)
	for rows.Next() {
		err := rows.Scan(&appid, &appname)
		if err != nil {
			logging.DebugMSG(err.Error())
		} else {
			outAppId = appid
			outAppName = appname
		}
	}

	return outAppId, outAppName
}


// **********************************************************************
// * Methode voor het genereren van HTML Code om een Applicatie in een
// * Combobox te selecteren. Er worden alleen Applicaties opgenomen die
// * niet verwijderd zijn en die niet als Internal gekenmerkt zijn
// **********************************************************************
func GetHTMLListOfApplications() string {
	
	outHTML := ""
	
	// De volgende velden worden opgenomen in de HTML-code
	//  - application_id
	//  - application_name
	//  - application_description
	
	var application_id int64 = 0
	application_name := "" 
	application_description := "" 
	
	applicationFound := false 
	
	rows, _ := repository.DBConnection.Query(repository.SqlCmd_app_008, constanten.No, constanten.No)
	for rows.Next() {
		err := rows.Scan(&application_id, &application_name, &application_description)
		if err != nil {
			logging.DebugMSG(err.Error())
		} else {
			applicationFound = true 
			//tmp := application_name + " | " + application_description
			tmp := application_name 
			outHTML = outHTML + "<option value=" + strconv.FormatInt(application_id, 10) + ">" + tmp + "</option>" 
		}
	}
	
	// Wanneer er Applicaties zijn gevonden, kan de begin en sluit-tag worden opgenomen
	if applicationFound {
		outHTML = "<select name='application'>" + outHTML + "</select>"
	}
	
	return outHTML
}
