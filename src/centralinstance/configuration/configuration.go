package configuration


// **********************************************************************
// * CONFIGURATION
// *
// * Package voor vaste instellingen in de applicatie




// **********************************************************************
// * SECURITY CONFIGURATION
// **********************************************************************

// Variabele die aangeeft wat de lengte is van de token die gegenereerd wordt indien
// er sprake is van Twofactor Authenticatie
var TwoFactorAuthenticationTokenLength = 6

// Variabele die aangeeft hoe lang een gegenereerde token (in minuten) geldig is. 
// De gebruiker moet binnen deze tijd worden ingevoerd, anders kan de Token niet 
// meer gebruikt worden
var TwoFactorAuthenticationTokenExpireTimeInMinutes = 15 

// Default wachtwoord voor de System gebruiker
var DefaultPasswordSystemUser = "Welkom01" 

// Zout om het versleutelen van het wachtwoord complexer te maken
var PasswordSalt = "&^^#^&#()_&*#$#$"


// **********************************************************************
// * LOGGIN CONFIGURATION
// **********************************************************************

// Variabele voor het tonen van informatie specificiek bedoeld voor het 
// development proces
var DevelopmentLogging = true

// Variabele voor het tonen van informatieve meldingen
var InfoLogging = true 

// Variabele voor het tonen van Fouten
var ErrorLogging = true 

// Variabele waarmee kan worden ingesteld dat wanneer de DumpObject-methode
// wordt aangeroepen, deze informatie ook wordt getoond
var DumpObjectLogging = true ; 