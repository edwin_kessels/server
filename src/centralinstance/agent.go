package main  

import "centralinstance/agent/wrkstat"
import "centralinstance/agent/wrkstrt"
import "centralinstance/agent/wrkcfg"
import "repository"
import "logging"
import "database/sql"
import "constanten"
import "os"
import "settings"
import "time"
import "webserver"



// Methode om een wachttijd in te stellen tussen het starten van twee agents
// Hiermee wordt bereikt dat de update van de timestamp voor alle agents
// niet op precies hetzelffde moment wordt uitgevoerd
func sleep() {
 if settings.DevelopmentMode == false {
   time.Sleep(2 * time.Second)
 }
}


func main() {
 
 // Initialiseren van de connectie met de Repository
 var err error
 repository.DBConnection, err = sql.Open(settings.RepositoryType, settings.RepositoryConnectString)
if err != nil {
	logging.ErrorMSG("Cannot connect to the Repository:" + err.Error())
		 os.Exit(1)
	}
 
 // Initieren van de tabel waarin de status van de verschillende agents worden bijgehouden
 stmt, err := repository.DBConnection.Prepare(repository.SqlCmd_agt_006)
    if err != nil {
		logging.ErrorMSG("Cannot prepare SQL Command:")
		logging.ErrorMSG(err.Error())
	}
	_, err = stmt.Exec(constanten.No, "N/A")
	if err != nil {
    	logging.ErrorMSG("Cannot execute SQL Command:")
		logging.ErrorMSG(err.Error())
	}
 
 
 // Starten Agent voor bijwerken van Status van Worker
 go wrkstat.Start()
 
 // 2 seconden wachten voordat de volgende agent wordt gestart. Dit alleen doen
 // wanneer de Agent neit in de DevelopmentMode draait
  sleep()
 
 // Starten van de Agent die de WorkerCOnfiguratie-berichten afhandelt
 go wrkcfg.Start() 

 // 2 seconden wachten voordat de volgende agent wordt gestart
 sleep()

    
   // Starten Agent voor verwerking van het starten van een Worker
   go wrkstrt.Start() 
   
   // 2 seconden wachten voordat de volgende agent wordt gestart
 sleep()
 
 // Starten van de WebServer
 webserver.Start()
 
    
}

