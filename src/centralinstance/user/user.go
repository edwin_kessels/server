package user

// **********************************************************************
// * USER
// *
// * De naamgeving van een User kan gemakkelijk worden aangepast. Hiervoor
// * moet in het Constanten-package de variabele UserObjNameEV en UserObjNameMV
// * worden aangepast hierdoor

import "repository"
import "centralinstance/configuration"
import "logging"
import "constanten"
import "time"
import "utils"
import "crypto/sha1"
import "encoding/hex"
import "strconv"
import "strings"
import "validatie"
import "melding"
import "naming"

type User struct {
	Id                      int
	AccountNameHash         string
	AccountName             string
	PasswordHash            string
	Firstname               string
	Lastname                string
	EmailAddress            string
	TwoFactorAuthentication string
	Token                   string
	TokenExpire             string
	Status                  int
	DefaultPartition        int
	DefaultApplication      int
	ProfileId               int
	LastLogin               string
	Deleted                 string
}

var prefix = naming.UserObjNameEV + ": "

// **********************************************************************
// * Meldingen
// **********************************************************************
//var melding001 = "USR-001: " + constanten.UserObjNameEV + " already exists"
//var melding002 = "USR-002: Email Address is a mandatory property"
//var melding003 = "USR-003: First Name is a mandatory property"
//var melding004 = "USR-004: First Name is a mandatory property"

// **********************************************************************
// Methode om een gebruikersaccount te verwijderen.
// * In de database wordt de gebruiker niet echt verwijderd, maar alleen
// * de Deleted-indicator wordt op YES gezet
// **********************************************************************
func (u *User) Delete() {

	u.Deleted = constanten.Yes
	logging.InfoMSG(prefix + "Set Deleted to '" + constanten.Yes + "'")
}

// **********************************************************************
// * Methode voor het instellen van de juiste default waarden voor een
// * nieuw User Object
// **********************************************************************
func (u *User) New() {
	logging.DevMSG(prefix + "Creating a new User Object")

	u.Id = 0
	u.TwoFactorAuthentication = constanten.No
	u.ProfileId = 0
	u.Deleted = constanten.No
}

// **********************************************************************
// * Methode om in te stellen dat er voor de gebruiker bij het inloggen
// * gebruik moet worden gemaakt van TwoFactor-authenticatie
// **********************************************************************
func (u *User) SetTwoFactorAuthentication(authMode bool) {
	u.TwoFactorAuthentication = constanten.Yes
	if authMode == false {
		u.TwoFactorAuthentication = constanten.No
	}
	logging.InfoMSG(prefix + "Set TwoFactorAuthentication to '" + u.TwoFactorAuthentication + "'")
}

// **********************************************************************
// * Methode om een token voor een gebruiker in te stellen. Dit gebeurt
// * direct nadat
// **********************************************************************
func (u *User) SetToken() {

	// Genereren en toekennen van de Token aan het UserObject
	u.Token = generateTwoFactorToken()

	// Instellen van de geldigheid van de token
	huidigTijdstip := time.Now()

	// Toevoegen van het aantal minuten zoals gespecificeerd in de variabele
	// TwoFactorAuthenticationTokenExpireTimeInMinutes
	huidigTijdstip.Add(time.Duration(configuration.TwoFactorAuthenticationTokenExpireTimeInMinutes) * time.Minute)
	u.TokenExpire = huidigTijdstip.Format("20060102150405")
}

// **********************************************************************
// * Methode voor het instellen van de AccountName
// **********************************************************************
func (u *User) SetAccountName(accountName string) {
	u.AccountName = strings.ToUpper(accountName)
	logging.InfoMSG(prefix + "Set AccountName to '" + u.AccountName + "'")
}

// **********************************************************************
// * Methode voor het ophalen van de AccountName
// **********************************************************************
func (u *User) GetAccountName() string {
	return u.AccountName
}

// **********************************************************************
// * Methode voor het instellen van de AccountHash. Dit is een versleutelde
// * versie van het UserAccount. Dit wordt gebruikt in het login-bericht
// * in plaats van de plain-text username
// **********************************************************************
func (u *User) SetAccountNameHash() {
	u.AccountNameHash = encryptString(u.AccountName)
	logging.InfoMSG(prefix + "Set AccountNameHash to '" + u.AccountNameHash + "'")
}

// **********************************************************************
// * Methode voor het ophalen van de AccountNameHash
// **********************************************************************
func (u *User) GetAccountNameHash() string {
	return u.AccountNameHash
}

// **********************************************************************
// * Methode voor het instellen van de voornaam de een gebruiker
// **********************************************************************
func (u *User) SetFirstName(firstName string) {
	u.Firstname = utils.Trim(firstName)
	logging.InfoMSG(prefix + "Set Firstname to '" + u.Firstname + "'")
}

// **********************************************************************
// * Methode voor het opvragen van de voornaam de een gebruiker
// **********************************************************************
func (u *User) GetFirstName() string {
	return u.Firstname
}

// **********************************************************************
// * Methode voor het instellen van de achternaam de een gebruiker
// **********************************************************************
func (u *User) SetLastName(lastName string) {
	u.Lastname = utils.Trim(lastName)
	logging.InfoMSG(prefix + "Set Lastname to '" + u.Lastname + "'")
}

// **********************************************************************
// * Methode voor het opvragen van de achternaam de een gebruiker
// **********************************************************************
func (u *User) GetLastName() string {
	return u.Lastname
}

// **********************************************************************
// * Methode voor het opvragen van de volledige naam van de gebruiker
// * om in de applicatie te tonen
// **********************************************************************
func (u *User) GetDisplayName() string {
	return u.Firstname + " " + u.Lastname
}

// **********************************************************************
// * Methode voor het instellen van emailadres van de gebruiker
// **********************************************************************
func (u *User) SetEmailAddress(emailAddress string) {
	u.EmailAddress = utils.Trim(emailAddress)
	logging.InfoMSG(prefix + "Set Email Address to '" + u.EmailAddress + "'")
}

// **********************************************************************
// * Methode voor het opvragen van emailadres van de gebruiker
// **********************************************************************
func (u *User) GetEmailAddress() string {
	return u.EmailAddress
}

// **********************************************************************
// * Methode voor het instellen van de PasswordHash
// **********************************************************************
func (u *User) SetPasswordHash(passwordHash string) {
	u.PasswordHash = passwordHash
	logging.InfoMSG(prefix + "Set PasswordHash to '" + u.PasswordHash + "'")
}

// **********************************************************************
// * Procedure voor het opslaan van een gebruiker
// * Afhankelijk van het Id wordt er een nieuw gebruikersaccount aangemaakt
// * of een bestaand gebruikersaccount gewijzigd.
// **********************************************************************
func (u *User) Commit() (int, string) {

	logging.InfoMSG(prefix + "Commit User Object (Id=" + strconv.Itoa(u.Id) + ", AccountName=" + u.AccountName + ")")

	resCode := 0
	resMsg := melding.Melding_usr_015

	// Controles uitvoeren en de Commit procedure verlaten indien niet alle controles succesvol zijn
	// Wanneer een User wordt verwijderd, hoeven de controles niet doorlopen te worden
	if u.Deleted == constanten.No {
		resCode, resMsg = u.CheckObject()
		if resCode > 0 {
			resMsg = strings.Replace(resMsg, "$OBJECTTYPE", naming.UserObjNameEV, 1)
			return resCode, resMsg
		}
	}

	// Controleren van de gegevens van de user
	// Eerst controleren of de gebruikersnaam niet al voorkomt
	if u.Id == 0 {
		testExistingUser := User{}
		testExistingUser.GetUserByAccountName(u.AccountName)
		//testExistingUser.DumpObject()
		if testExistingUser.Id > 0 {
			return 16, melding.Melding_usr_016
		}
	}

	// Starten van een transactie
	logging.InfoMSG(prefix + "Starting SQL Transaction")
	tx, err := repository.DBConnection.Begin()
	if err != nil {
		logging.ErrorMSG(prefix + "Failed to start a SQL transaction: " + err.Error())
	}

	// Opslaan van een nieuw gebruikersaccount
	if u.Id == 0 {

		// Het betreft een nieuwe gebruiker. De AccountHash moet nog bepaald worden
		// Daarnaast wordt het accountnaam altijd in hoofdletters opgeslagen
		u.AccountName = strings.ToUpper(u.AccountName)
		u.AccountNameHash = encryptString(u.AccountName)

		sqlCmd := repository.SqlCmd_usr_001
		stmt, err := tx.Prepare(sqlCmd)
		if err != nil {
			logging.ErrorMSG(prefix + "Failed to parse SQL command: " + err.Error())
		}
		_, err = stmt.Exec(u.AccountNameHash, u.AccountName, u.PasswordHash, u.Firstname, u.Lastname, u.EmailAddress, u.DefaultPartition, u.DefaultApplication, u.TwoFactorAuthentication)
		if err != nil {
			logging.ErrorMSG(prefix + "Failed to execute SQL command: " + err.Error())
		}
	}

	// Opslaan van een bestaand gebruikersaccount wat niet verwijderd is
	if u.Id > 0 && u.Deleted == constanten.No {

	}

	// Verwijderen van een gebruiker
	if u.Deleted == constanten.Yes {
		sqlCmd := repository.SqlCmd_usr_004
		stmt, err := tx.Prepare(sqlCmd)
		if err != nil {
			logging.ErrorMSG(prefix + "Failed to parse SQL command: " + err.Error())
		}
		_, err = stmt.Exec(constanten.Yes, u.Id)
		if err != nil {
			logging.ErrorMSG(prefix + "Failed to execute SQL command: " + err.Error())
		} else {
			// User Account is verwijderd
			resCode = 0
			resMsg = melding.Melding_usr_018
		}
	}

	// Afsluiten (commit) van de transactie
	logging.InfoMSG(prefix + "Commit SQL Transaction")
	tx.Commit()

	return resCode, resMsg
}

// **********************************************************************
// * Procedure om op basis van de AccountName de user gegevens op te halen
// * In principe wordt er alleen op basis van de AccountName het Id opgezocht
// * Op basis van dit Id wordt GetUserById aangeroepen voor het echt ophalen
// * van de gegevens
// **********************************************************************
func (u *User) GetUserByAccountName(accountName string) {
	user_id := 0
	sqlCmd := repository.SqlCmd_usr_003

	accountName = strings.ToUpper(utils.Trim(accountName))

	logging.InfoMSG(prefix + "getting " + naming.UserObjNameEV + " information by Account Name (" + accountName + ")")

	rows, err := repository.DBConnection.Query(sqlCmd, accountName, constanten.No)
	if err != nil {
		logging.ErrorMSG(prefix + "Failed to connect to repository: " + err.Error())
	}

	for rows.Next() {
		err := rows.Scan(&user_id)
		if err != nil {
			logging.DebugMSG(err.Error())
		} else {
			// Overzetten van de gegevens
			logging.ParameterMSG("GetUserById=>userID=" + strconv.Itoa(user_id))
			u.GetUserById(user_id)
		}
	}
}

// **********************************************************************
// * Procedure om op basis van het UserId de gegevens van een User
// * op te halen
// **********************************************************************
func (u *User) GetUserById(userID int) {

	userFound := false

	sqlCmd := repository.SqlCmd_usr_002
	logging.InfoMSG(prefix + "getting " + naming.UserObjNameEV + " information by id (" + strconv.Itoa(userID) + ")")

	// Variabelen declareren die gelezen worden uit de tabel
	user_hash := ""
	user_account := ""
	user_password := ""
	user_firstname := ""
	user_lastname := ""
	user_email := ""
	user_default_partition := 0
	user_default_application := 0
	TwoFactorAuthentication := ""
	user_last_login := ""
	user_deleted := ""

	rows, err := repository.DBConnection.Query(sqlCmd, userID)
	if err != nil {
		logging.ErrorMSG(prefix + "Failed to connect to repository: " + err.Error())
	}
	for rows.Next() {
		err := rows.Scan(&user_hash, &user_account, &user_password, &user_firstname, &user_lastname, &user_email, &user_default_partition, &user_default_application, &TwoFactorAuthentication, &user_last_login, &user_deleted)
		if err != nil {
			logging.DebugMSG(err.Error())
		} else {
			// Overzetten van de gegevens
			userFound = true
			u.Id = userID
			u.AccountNameHash = user_hash
			u.AccountName = user_account
			u.PasswordHash = user_password
			u.Firstname = user_firstname
			u.Lastname = user_lastname
			u.EmailAddress = user_email
			u.LastLogin = user_last_login
			u.Deleted = user_deleted

			//  if partition_internal == constanten.Yes {
			//      p.Internal = true
			//  }
		}
	}

	// Controleren of de User is gevonden
	if userFound == false {
		logging.ErrorMSG(naming.UserObjNameEV + "' not found")
	}
}

// **********************************************************************
// * Procedure om de ingevoerde gegevens van een User te controleren
// * voordat ze worden opgeslagen.
// **********************************************************************
func (u *User) CheckObject() (int, string) {

	exitCode := 0
	exitMsg := melding.Melding_usr_017

	// Wanneer er sprake is van een nieuwe object (u.Id = 0) dan moeten
	// een aantal velden die niet door de FrontEnd gezet worden, een default
	// waarde krijgen. Dit zijn de volgende velden:
	// -     Deleted	                    string
	// -     TwoFactorAuthentication        string
	if u.Deleted == "" {
		logging.DevMSG("Set user.Deleted to " + constanten.No)
		u.Deleted = constanten.No
	}
	if u.TwoFactorAuthentication == "" {
		logging.DevMSG("Set user.TwoFactorAuthentication to " + constanten.Yes)
		u.TwoFactorAuthentication = constanten.Yes
	}

	// Algemene controles die zowel voor een nieuwe als bestaande gebruiker
	// moeten worden uitgevoerd
	// Deze controles niet uitvoeren wanneer een gebruiker wordt verwijderd

	if u.Deleted == constanten.No {

		// Controleren of de Object name (useraccount) geldig is
		exitCode, exitMsg = validatie.Check(u.AccountName, validatie.ObjectName)
		if exitCode > 0 {
			return exitCode, exitMsg
		}

		// Controleren of de Firstname geldig is
		exitCode, exitMsg = validatie.Check(u.Firstname, validatie.FirstName)
		if exitCode > 0 {
			return exitCode, exitMsg
		}

		// Controleren of de Lastname geldig is
		exitCode, exitMsg = validatie.Check(u.Lastname, validatie.LastName)
		if exitCode > 0 {
			return exitCode, exitMsg
		}

		// Controleren van het Email-adres. Dit hoeft niet voor de SYSTEM gebruiker
		if u.AccountName != "SYSTEM" {
			exitCode, exitMsg = validatie.Check(u.EmailAddress, validatie.EmailAddress)
			if exitCode > 0 {
				return exitCode, exitMsg
			}
		}

	}

	return exitCode, exitMsg
}

// **********************************************************************
// * Procedure voor het dumpen van het Object. Dit is bedoeld om tijdens
// * development van de applicatie makkelijker problemen te kunnen
// * analyseren
// **********************************************************************
func (u *User) DumpObject() {
	logging.ObjectDumpMSG("Id                          = " + strconv.Itoa(u.Id))
	logging.ObjectDumpMSG("AccountNameHash             = " + u.AccountNameHash)
	logging.ObjectDumpMSG("AccountName                 = " + u.AccountName)
	logging.ObjectDumpMSG("PasswordHash                = " + u.PasswordHash)
	logging.ObjectDumpMSG("Firstname                   = " + u.Firstname)
	logging.ObjectDumpMSG("Lastname                    = " + u.Lastname)
	logging.ObjectDumpMSG("EmailAddress                = " + u.EmailAddress)
	logging.ObjectDumpMSG("TwoFactorAuthentication     = " + u.TwoFactorAuthentication)
	logging.ObjectDumpMSG("Token                       = " + u.Token)
	logging.ObjectDumpMSG("TokenExpire                 = " + u.TokenExpire)
	logging.ObjectDumpMSG("Status                      = " + strconv.Itoa(u.Status))
	logging.ObjectDumpMSG("DefaultPartition            = " + strconv.Itoa(u.DefaultPartition))
	logging.ObjectDumpMSG("DefaultApplication          = " + strconv.Itoa(u.DefaultApplication))
	logging.ObjectDumpMSG("ProfileId                   = " + strconv.Itoa(u.ProfileId))
	logging.ObjectDumpMSG("LastLogin                   = " + u.LastLogin)
	logging.ObjectDumpMSG("Deleted                     = " + u.Deleted)
}

// **********************************************************************
// * Methode om de System-gebruiker in te stellen. Tijdens het uitvoeren
// * van de catalog.sql is deze user al aangemaakt, maar er is nog geen
// * wachtwoord en dergelijke ingesteld zodat er niet mee kan worden
// * aangelogd. In deze procedure wordt de AccountHash bepaalt en een
// * standaard wachtwoord ingsteld
// * De System-gebruiker heeft altijd ID =1
// **********************************************************************
func CheckSystemUser() {

	sysUser := User{}
	sysUser.GetUserById(1)

	if sysUser.GetAccountNameHash() == "NULL" {
		logging.InfoMSG("Configuring System User Account")
		sysUser.SetAccountNameHash()
		sysUser.SetPasswordHash(encryptString(configuration.DefaultPasswordSystemUser))

		sysUser.Commit()
	}

}

// **********************************************************************
// * Methode voor het genereren van een Token die wordt gebruikt wanneer
// * er gebruik wordt gemaakt van TwoFactor authenticatie. De lengte van
// * de token die wordt gegenereerd wordt door bepaald door de variabele
// * configuration.TwoFactorAuthenticationTokenLength
// **********************************************************************
func generateTwoFactorToken() string {
	token := utils.GenerateHash(configuration.TwoFactorAuthenticationTokenLength, "number")
	logging.InfoMSG(prefix + "Generated Token for TwoFactorAuthentication: " + token)
	return token
}

// **********************************************************************
// * Methode voor het Encrypten (one-way) van een String. Het resultaat
// * is een string
// **********************************************************************
func encryptString(strToBeEncrypted string) string {
	encryptedStr := sha1.New()
	encryptedStr.Write([]byte(strToBeEncrypted))
	return hex.EncodeToString(encryptedStr.Sum(nil))
}

// **********************************************************************
// * Procedure voor het genereren van een PasswordHard
// **********************************************************************
func GeneratePasswordHash(password string) string {
	tmp := configuration.PasswordSalt + password
	return encryptString(tmp)
}
