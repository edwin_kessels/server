package job

import "centralinstance/jobsource"
import "centralinstance/metaobject"
import "centralinstance/worker"
import "logging"
import "naming"
import "repository"
import "utils"
import "strconv"
import "berichten"
import "encoding/xml"
import "statussen/jobstatus"
import "constanten"
import "esb"
import "os"
import "settings"
import "path/filepath"
import "io/ioutil"

var prefix = naming.JobObjNameEV + ": "

type Job struct {
	JobId           int64
	JobSourceId     int64
	JobType         int
	Name            string
	Description     string
	Status          string
	ExitCode        string
	WorkerId        int64
	WorkerName      string
	SourceCode      string
	PartitionId     int64
	PartitionName   string
	ApplicationId   int64
	ApplicationName string
	RunStart        string
	RunEnd          string
	ElapsedTime     string
}

// ************************************************************************
// * SetJobId(inJobId int64)
// * Instellen van JobId
// ************************************************************************
func (j *Job) SetJobId(inJobId int64) {
	j.JobId = inJobId
}

// ************************************************************************
// * GetJobByJobId(inJobId bigint)
// * Methode voor het ophalen van eigenschappen van een Job op basis van
// * de JobId
// ************************************************************************
func (j *Job) GetJobByJobId(inJobId int64) {

	jobFound := false

	logging.DevMSG(prefix + "getting " + naming.JobObjNameEV + " information by JobId (" + strconv.FormatInt(inJobId, 10) + ")")

	// Initialiseren van de variabelen
	var jobsrc_id int64 = 0
	var partitionId int64 = 0
	var applicationId int64 = 0
	jobtype := 0
	jobname := ""
	jobdescr := ""
	jobstatus := ""
	exitcode := ""
	var runstart string = ""
	runend := ""
	elapsedtime := ""
	var jobworker int64 = 0

	sqlCmd := repository.SqlCmd_job_011
	rows, err := repository.DBConnection.Query(sqlCmd, inJobId)
	if err != nil {
		logging.ErrorMSG(prefix + "Failed to connect to repository: " + err.Error())
	}

	for rows.Next() {
		err := rows.Scan(&jobsrc_id, &partitionId, &applicationId, &jobtype, &jobname, &jobdescr, &jobstatus, &exitcode, &runstart, &runend, &elapsedtime, &jobworker)
		if err != nil {
			logging.DebugMSG(err.Error())
		} else {
			jobFound = true
			j.JobId = inJobId
			j.JobSourceId = jobsrc_id
			j.PartitionId = partitionId
			j.ApplicationId = applicationId
			j.JobType = jobtype
			j.Name = jobname
			j.Description = jobdescr
			j.Status = jobstatus
			j.ExitCode = exitcode
			j.RunStart = runstart
			j.RunEnd = runend
			j.ElapsedTime = elapsedtime
			j.WorkerId = jobworker

			// Ophalen van de naam van de Worker
			wrk := worker.Worker{}
			wrk.GetWorkerById(jobworker)
			j.WorkerName = wrk.GetFQName()
		}
	}

	if jobFound == false {
		logging.ErrorMSG("Cannot find Job")
	}

}

// ************************************************************************
// * GetJobId
// ************************************************************************
func (j *Job) GetJobId() int64 {
	return j.JobId
}

// ************************************************************************
// Start()
// Starten van een Scheduled job uit de Job tabel
// ************************************************************************
func (j *Job) Start() {

	// Ophalen van de gegevens van de Job. Dit is nodig om te achterhalen
	// welke JobSource er moet worden gebruikt op basis waarvan de JobRequest
	// worden samengesteld

	//	-- bepalen op welke Worker de job moet worden uitgevoerd
	//	-- samenstellen JobRequestXML
	//	-- Wijzigen status in Dispatched
	//	-- Versturen van de JobRequestXML naar de ESB

	// Samenstellen van de JobRequestXML. Om dit te kunnen doen moeten we
	// bepalen welke JobSource_id er aan de Job is gekoppeld. Met deze ID
	// kan de JobSource worden opgehaald en kan de JobRequest worden samengesteld

	var queueESB string = "123"

	wrk := worker.Worker{}
	// Wanneer het veld WorkerId is gevuld, kan hiervoor de ESBQueue worden
	// gebruikt voor het uitvoeren van de job
	logging.DebugMSG("Retrieving information about Worker with ID=" + strconv.FormatInt(j.WorkerId, 10))
	if j.WorkerId != 0 {
		wrk.GetWorkerById(j.WorkerId)
		queueESB = wrk.QueueName
	}
	logging.DebugMSG("Queue=" + queueESB)

	// jobEnvironment
	// In deze variabele wordt het environment opgebouwd in welke de job draait
	// Deze wordt aan de SourceCode toegevoegd. De manier waarop de variabele
	// wordt opgenomen is afhankelijk van het JobType
	var jobEnvironment berichten.DataWithSpecialCharacters

	sqlCmd := repository.SqlCmd_jp_003
	logging.DebugMSG("Preparing SQLCommand repository.SqlCmd_jp_003")
	rows, err := repository.DBConnection.Query(sqlCmd, j.JobId)
	if err != nil {
		logging.ErrorMSG(prefix + "Failed to connect to repository: " + err.Error())
	}

	var parName string = ""
	var parValue string = ""
	var parVisible string = ""
	for rows.Next() {
		err := rows.Scan(&parName, &parValue, &parVisible)
		if err != nil {
			logging.DebugMSG(err.Error())
		} else {
			// Parameter toevoegen aan het Environment
			var tmp string = "export " + parName + "=" + parValue + "\n"
			jobEnvironment = jobEnvironment + berichten.DataWithSpecialCharacters(tmp)
		}
	}

	// De SourceCode wordt niet vastgelegd in de Job, maar moet bij de Start
	// worden gelezen uit de JobSource
	js := jobsource.JobSource{}
	js.GetJobSourceById(j.JobSourceId)
	j.SourceCode = js.GetSourceCode()

	// Samenstellen van de JobRequest
	var jobRequest berichten.JobRequest
	jobRequest.JobId = strconv.FormatInt(j.JobId, 10)
	jobRequest.JobSourceId = j.JobSourceId
	jobRequest.Worker = wrk.FQName
	jobRequest.Name = j.Name
	jobRequest.Description = j.Description
	jobRequest.JobType = j.JobType
	jobRequest.SourceCode = jobEnvironment + berichten.DataWithSpecialCharacters(j.SourceCode)
	jobRequest.TimeStamp = utils.CurrentTimeStamp(constanten.DateFormatLong)
	jobRequest.CheckSum = berichten.GetCheckSumJobRequest(jobRequest)
	xmlBericht, _ := xml.MarshalIndent(jobRequest, "", constanten.XMLIndent)
	esb.SendMessageESBWithoutWait(string(xmlBericht), queueESB)

	// Status van de Job moet worden aangepast naar Dispatched. Dit houdt in dat de
	// JobRequestXML is verstuurd aan de Enterprise Server Bus
	tx, err := repository.DBConnection.Begin()
	if err != nil {
		logging.ErrorMSG(prefix + "Failed to start a SQL transaction: " + err.Error())
	}
	sqlCmd = repository.SqlCmd_job_007
	stmt, err := tx.Prepare(sqlCmd)
	if err != nil {
		logging.ErrorMSG(prefix + "Failed to parse SQL command: " + err.Error())
	}
	_, err = stmt.Exec(jobstatus.Dispatched, j.JobId)
	if err != nil {
		logging.ErrorMSG(prefix + "Failed to execute SQL command: " + err.Error())
	} else {
		logging.DevMSG("Status of job " + strconv.FormatInt(j.JobId, 10) + " has been set to Dispatched")
	}
	tx.Commit()

}

// ************************************************************************
// Prepare()
// Voorbereiden van een Job op basis van een JobSource
// De volgende acties worden uitgevoerd tijdens de prepare van een Job
//   - In de tabel Job wordt een record aangemaakt
//   - Op basis van het TimeWindow wordt de starttijd bepaald (optioneel)
//   - Samenstellen van de JobRequest XML en opnemen in de Job tabel
// ************************************************************************
func (j *Job) Prepare(jobSource jobsource.JobSource) {

	logging.DebugMSG("Preparing Job for " + naming.JobObjNameEV + " " + jobSource.FQName + " with ID=" + strconv.FormatInt(jobSource.Id, 10))

	// MetaObject er bijzoeken omdat ook de Application en Partition moet worden opgenomen
	metaObject := metaobject.MetaObject{}
	metaObject.GetMetaObjectById(jobSource.Id)

	j.JobSourceId = jobSource.Id
	j.Name = jobSource.GetName()
	j.Description = jobSource.GetDescription()
	j.SourceCode = jobSource.SourceCode
	j.JobType = jobSource.JobSourceType

	// Record toevoegen
	logging.DevMSG("RunStart of the job: " + j.RunStart)

	var job_id int64 = 0
	// Aanmaken van een nieuwe job in de job-tabel. Hierbij wordt het JobID geretourneerd
	err := repository.DBConnection.QueryRow(repository.SqlCmd_job_004, metaObject.PartitionId, metaObject.ApplicationId, 1, j.Name, j.Description, 1, 1, 50, 0, j.RunStart, jobSource.Id, jobstatus.Scheduled, j.WorkerId).Scan(&job_id)
	if err != nil {
		logging.ErrorMSG(prefix + "Failed to execute SQL command: " + err.Error())
	} else {
		j.JobId = job_id
		logging.ParameterMSG(prefix + "Create job '" + j.Name + "' with JobId" + strconv.FormatInt(j.JobId, 10))

	}

	// Aanmaken van de JobDirectory op de CentralInstance server
	jobDirectory := settings.JobDirectoryCentralInstance + string(os.PathSeparator) + strconv.FormatInt(j.JobId, 10)
	err = os.MkdirAll(jobDirectory, 0750)
	if err != nil {
		logging.DebugMSG("An error occurred during the creation of directory '" + jobDirectory + "'")
		logging.DebugMSG("Cannot create the JobDirectory")
	}

	// Aanmaken van de JobParameters. Wanneer deze voor de JobSource zijn gespecificeerd,
	// wordt in deze stap de records aangemaakt. Hierbij wordt gebruikt gemaakt van de
	// default waarde van de parameter. Als tijdens de submit van de Job afwijkende
	// waarden worden ingevoerd, wordt deze in de Start() methode overschreven
	tx, err := repository.DBConnection.Begin()
	for i := 0; i < len(jobSource.JobSourceParameter); i++ {

		var sqlCmd string = repository.SqlCmd_jp_001
		logging.DebugMSG("Preparing SQLCommand repository.SqlCmd_jp_001")
		stmt, err := tx.Prepare(sqlCmd)
		if err != nil {
			logging.ErrorMSG(prefix + "Failed to parse SQL command: " + err.Error())
		}
		_, err = stmt.Exec(j.JobId, jobSource.JobSourceParameter[i].Name, jobSource.JobSourceParameter[i].DefaultValue, jobSource.JobSourceParameter[i].Visible)
		if err != nil {
			logging.ErrorMSG(prefix + "Failed to execute SQL command: " + err.Error())
		}

	}
	tx.Commit()

	// Opnieuw opbouwen van de cache voor Scheduled Jobs
	settings.RebuildScheduledJobs = true

}

// ************************************************************************
// GetExecutionLog
// Methode om de ExecutionLog van de Job op te halen
// ************************************************************************
func (j *Job) GetExecutionLog() string {

	var outExecutionLog string = ""

	logging.DevMSG("Retrieving Execution Log for Job " + strconv.FormatInt(j.JobId, 10))

	// Ophalen van de ExecutionLog
	var sqlCmd string = repository.SqlCmd_joblog_002
	rows, err := repository.DBConnection.Query(sqlCmd, j.JobId)
	if err != nil {
		logging.ErrorMSG(prefix + "Failed to connect to repository: " + err.Error())
	}

	var jobLog string = "'"

	for rows.Next() {
		err := rows.Scan(&jobLog)
		if err != nil {
			logging.DebugMSG(err.Error())
		} else {
			outExecutionLog = jobLog
		}
	}

	return outExecutionLog
}

// ************************************************************************
// ChangeJobStatus
// Methode om de status van een Job aan te passen.
// Hierbij wordt ook gecontroleerd of de statusovergang wel valide is
// Als eerste wordt de actuele status opgehaald
// ************************************************************************
func ChangeJobStatus(inJobId int64, inNewStatus string) {

	var statusChangeOk bool = true
	var jobFound bool = false

	var inOldStatus string = jobstatus.Created

	// Bepalen van de actuele status van de Job
	// Ophalen van de ExecutionLog
	var sqlCmd string = repository.SqlCmd_job_012
	rows, err := repository.DBConnection.Query(sqlCmd, inJobId)
	if err != nil {
		logging.ErrorMSG(prefix + "Failed to connect to repository: " + err.Error())
	}

	for rows.Next() {
		err := rows.Scan(&inOldStatus)
		if err != nil {
			logging.DebugMSG(err.Error())
		} else {
			jobFound = true
		}
	}

	// Alleen doorgaan wanneer de Job is gevonden
	if jobFound == true {

		// Controleren van de overgangen

		// Completed -> Running  : Niet toegestaan
		if inOldStatus == jobstatus.Completed && inNewStatus == jobstatus.Running {
			statusChangeOk = false
		}

	}

	// Wanneer de checks goed zijn doorlopen, kan de status worden aangepast
	if statusChangeOk == true {
		stmt, err := repository.DBConnection.Prepare(repository.SqlCmd_job_009)
		if err != nil {
			logging.ErrorMSG("Cannot prepare SQL Command:")
			logging.ErrorMSG(err.Error())
		}
		_, err = stmt.Exec(inNewStatus, inJobId)
		if err != nil {
			logging.ErrorMSG("Cannot execute SQL Command:")
			logging.ErrorMSG(err.Error())
		}
	}

}

// ************************************************************************
// * GetJobFilesHTML
// * Aanmaken van een HTML-overzicht met daarin de JobFiles van de uitgevoerde
// * job. De JobFiles worden als een link opgenomen naar het echte bestand
// ************************************************************************
func (j *Job) GetJobFilesHTML() string {

	var htmlOvz string = ""

	var jobfile_id int64 = 0
	var jobfile_filename string = ""
	var jobfile_filesize string = ""

	var jobFileFound bool = false

	sqlCmd := repository.SqlCmd_jobfile_002

	rows, err := repository.DBConnection.Query(sqlCmd, j.JobId)
	if err != nil {
		logging.ErrorMSG(prefix + "Failed to connect to repository: " + err.Error())
	}

	for rows.Next() {
		err := rows.Scan(&jobfile_id, &jobfile_filename, &jobfile_filesize)
		if err != nil {
			logging.DebugMSG(err.Error())
		} else {
			jobFileFound = true

			// Isoleren van de bestandsnaam
			_, fileLink := filepath.Split(jobfile_filename)

			var link string = "<a href=/jobfile/" + strconv.FormatInt(jobfile_id, 10) + ">" + fileLink + "</a>"

			htmlOvz = htmlOvz + "<tr>"
			htmlOvz = htmlOvz + "<td>" + link + "</td>"
			htmlOvz = htmlOvz + "<td>" + jobfile_filesize + "</td>"
			htmlOvz = htmlOvz + "</tr>"
		}
	}

	if jobFileFound == true {
		var tableHeader string = "<table><tr>"
		tableHeader = tableHeader + "<th>JobFile</th>"
		tableHeader = tableHeader + "<th>Size</th>"
		tableHeader = tableHeader + "</tr>"
		htmlOvz = tableHeader + htmlOvz + "</table>"
	}

	return htmlOvz
}

// ************************************************************************
// * GetJobParametersHTML
// * Methode om de JobParameters in HTML uit te lijsten die zijn gebruikt
// * voor het draaien van de job
// ************************************************************************
func (j *Job) GetJobParametersHTML() string {

	var htmlOvz string = ""
	var jobParameterFound bool = false

	// Query uitvoeren om de Parameters op te halen
	sqlCmd := repository.SqlCmd_jp_003
	logging.DebugMSG("Preparing SQLCommand repository.SqlCmd_jp_003")
	rows, err := repository.DBConnection.Query(sqlCmd, j.JobId)
	if err != nil {
		logging.ErrorMSG(prefix + "Failed to connect to repository: " + err.Error())
	}

	var parName string = ""
	var parValue string = ""
	var parVisible string = ""
	for rows.Next() {
		err := rows.Scan(&parName, &parValue, &parVisible)
		if err != nil {
			logging.DebugMSG(err.Error())
		} else {
			// Parameter opnemen in de output wanneer de parameter visible is
			if parVisible == constanten.Yes {
				jobParameterFound = true
				htmlOvz = htmlOvz + "<tr><td>" + parName + "</td><td>" + parValue + "</td></tr>"
			}

		}
	}

	if jobParameterFound == true {
		// Tabel definitie toevoegen
		var tableHeader string = "<table><tr><th>Parameter</th><th>Value</th></tr>"
		htmlOvz = tableHeader + htmlOvz + "</table>"
	}

	return htmlOvz
}

// ************************************************************************
// * GetJobFileContentById
// * Ophalen van Content van de JobFile op basis van het Id
// ************************************************************************
func GetJobFileContentById(inJobFileId int64) string {

	var jobfile_filename string = ""
	var jobFileContent string = "JobFile Not Found"
	var filename string = ""

	sqlCmd := repository.SqlCmd_jobfile_003

	logging.DebugMSG("Get filename for JobFile " + strconv.FormatInt(inJobFileId, 10))

	rows, err := repository.DBConnection.Query(sqlCmd, inJobFileId)
	if err != nil {
		logging.ErrorMSG(prefix + "Failed to connect to repository: " + err.Error())
	}

	for rows.Next() {
		err := rows.Scan(&jobfile_filename)
		if err != nil {
			logging.DebugMSG(err.Error())
		} else {
			logging.DebugMSG("Full Path JobFile=" + jobfile_filename)
			filename = jobfile_filename
		}
	}

	// Inlezen van externe JobFile
	logging.DebugMSG("Try to read JobFile " + filename)
	if filename != "" {
		tmp, _ := ioutil.ReadFile(filename)
		jobFileContent = string(tmp)
	}

	return jobFileContent
}

// ************************************************************************
// * UpdateJobParameter
// * Methode om de default waarde van een JobParameter te actualiseren met
// * de waarde die is gespecificeerd tijdens de Submit van de job
// ************************************************************************
func UpdateJobParameter(inJobId int64, inJobParName string, inJobValue string) {

	var sqlCmd string = repository.SqlCmd_jp_002
	logging.DebugMSG("Preparing SQLCommand repository.SqlCmd_jp_002")
	stmt, err := repository.DBConnection.Prepare(sqlCmd)
	if err != nil {
		logging.ErrorMSG("Cannot prepare SQL Command:")
		logging.ErrorMSG(err.Error())
	}
	_, err = stmt.Exec(inJobValue, inJobId, inJobParName)
	if err != nil {
		logging.ErrorMSG("Cannot execute SQL Command:")
		logging.ErrorMSG(err.Error())
	}
}

func main() {

}
