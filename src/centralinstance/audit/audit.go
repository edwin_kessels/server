package audit

import "repository"
import "utils"
import "logging"
import "naming"

var prefix = naming.AuditObjNameEV + ": "

type AuditInfo struct {
	Id          int64
	Action      string
	FQName      string
	ObjectId    int64
	ObjectType  int
	ObjectName  string
	UserId      int64
	BeforeImage string
	AfterImage  string
}

var auditCreate = "CREATE"
var auditDelete = "DELETE"
var auditModify = "MODIFY"

func (a *AuditInfo) SetObjectName(objectName string) {
	a.ObjectName = objectName
}

func (a *AuditInfo) SetObjectId(objectId int64) {
	a.ObjectId = objectId
}

func (a *AuditInfo) SetActionCreate() {
	a.Action = auditCreate
}

func (a *AuditInfo) ActionCreate() bool {
	if a.Action == auditCreate {
		return true
	}
	return false
}

func (a *AuditInfo) SetActionDelete() {
	a.Action = auditDelete
}

func (a *AuditInfo) ActionDelete() bool {
	if a.Action == auditDelete {
		return true
	}
	return false
}

func (a *AuditInfo) SetActionModify() {
	a.Action = auditModify
}

func (a *AuditInfo) ActionModify() bool {
	if a.Action == auditModify {
		return true
	}
	return false
}

func (a *AuditInfo) SetFQName(fqname string) {
	a.FQName = fqname
}

func (a *AuditInfo) SetObjectType(objectType int) {
	a.ObjectType = objectType
}

func (a *AuditInfo) SetBeforeImageObject(beforeImage string) {
	a.BeforeImage = beforeImage
}

func (a *AuditInfo) SetAfterImageObject(afterImage string) {
	a.AfterImage = afterImage
}

func (a *AuditInfo) WriteAuditRecord() {

	audit_id := 0

	sqlCmd := repository.SqlCmd_aud_001
	//audit_user_id, audit_object_type, audit_object_id, audit_object_action, audit_timestamp, audit_object_fqname, audit_object_name
	err := repository.DBConnection.QueryRow(sqlCmd, a.UserId, a.ObjectType, a.ObjectId, a.Action, utils.CurrentTimeStamp("2016-10-11 14:40:03"), a.FQName, a.ObjectName).Scan(&audit_id)
	if err != nil {
		logging.ErrorMSG(prefix + "Failed to execute SQL command: " + err.Error())
	}

	// Audit-Record aanmaken voor het Before and After Image
	stmt, err := repository.DBConnection.Prepare(repository.SqlCmd_aud_002)
	if err != nil {
		logging.ErrorMSG("Probleem met samenestellen commando:" + err.Error())
	}
	_, err = stmt.Exec(audit_id, a.BeforeImage, a.AfterImage)
	if err != nil {
		logging.ErrorMSG(prefix + "Failed to execute SQL command: " + err.Error())
	}

}
