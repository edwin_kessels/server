package partition

// **********************************************************************
// * PARTITION
// *
// * Partitions worden gebruikt om Scheduling Objecten te ordenen (logisch)
// * en een niveau waarop rechten kunnen worden uitgedeeld.
// *
// * De naamgeving van een Partition kan gemakkelijk worden aangepast. Hiervoor
// * moet in het Constanten-package de variabele PartitionObjNameEV en PartitionObjNameMV
// * worden aangepast hierdoor

import "strings"
import "logging"
import "repository"
import "constanten"
import "utils"
import "strconv"
import "centralinstance/metaobject"
import "berichten"
import "encoding/xml"
import "validatie"
import "melding"
import "naming"
import "centralinstance/audit"
import "centralinstance/privilege"

var prefix = naming.PartitionObjNameEV + ": "

// UserId wordt gebruikt om de UserId in te stellen (deze wordt uit het SessionObject gelezen
// Het is niet mogelijk om via SetUserID een ID op te geven omdat dit dan via de API ook mogelijk is
// Via partition.UserId = x kan dit wel worden bereikt.
var UserId int64 = 0

type Partition struct {
	Id          int64
	FQName      string
	Name        string
	Description string
	Internal    string
	Default     string
	Public      string
	Deleted     string
	//	UserId      int
	XMLCode string
}

// **********************************************************************
// * Meldingen
// **********************************************************************

var melding002 = "PRT-002: " + naming.PartitionObjNameEV + " Name contains characters which are not allowed"
var melding003 = "PRT-003: " + naming.PartitionObjNameEV + " Name is too long (max 32 characters)"
var melding004 = "PRT-004: Description of the " + naming.PartitionObjNameEV + " is too long (max 128 characters)"
var melding006 = "PRT-005: An internal " + naming.PartitionObjNameEV + " cannot be deleted"
var melding007 = "PRT-006: " + naming.PartitionObjNameEV + " Name cannot be empty"
var melding008 = "PRT-006: " + naming.PartitionObjNameEV + " contains objects and cannot be delete"

// **********************************************************************
// * Procedure om de definitie van een Partition te exporteren naar
// * een XML-file. Het export-format maakt het mogelijk om het object
// * in dezelfde of andere omgeving te importeren. De resultaat-XML wordt
// * in de variabele XMLCode geplaatst.
// **********************************************************************
func (p *Partition) Export() {

	logging.InfoMSG("Creating XML export file for " + naming.PartitionObjNameEV + " '" + p.Name + "'")

	xmlExport := ""
	var tmpXML berichten.PartitionXML
	tmpXML.FQName = p.FQName
	tmpXML.Name = p.GetName()
	tmpXML.Description = p.GetDescription()
	tmpXML.Internal = p.Internal
	tmpXML.Default = p.Default
	//tmpXML.ExportDate = utils.CurrentTimeStamp()

	bericht, _ := xml.MarshalIndent(tmpXML, "", "\t")
	xmlExport = string(bericht)

	// Plaatsen van de XML-code in de property XMLCode
	p.XMLCode = xmlExport
}

// **********************************************************************
// * Methode om een partition als Public te kenmerken. Wanneer een
// * Partition public is, kunnen objecten in de partition ook in andere
// * partitiions worden gebruikt. Objecten in een private Partition zijn alleen
// * maar bruikbaar voor objecten uit dezelfde partition
// **********************************************************************
func (p *Partition) SetPublic() {
	p.Public = constanten.Yes
	logging.InfoMSG("Partition set to Public")
}

// **********************************************************************
// * Methode om in te stellen onder welke gebruiker een commando wordt
// * uitgevoerd. Dit is van belang voor ondermeer de autorisatie: mag
// * de gebruiker dit commando uitvoeren
// **********************************************************************
//func (p *Part/ition) SetUserId() {
//	p.UserId = UserId
//	logging.InfoMSG("Set User Id to '" + strconv.Itoa(p.UserId) + "'")
//}

// **********************************************************************
// * Methode om op te vragen of een Partition Public is
// **********************************************************************
func (p *Partition) GetPublic() bool {
	if p.Public == constanten.Yes {
		return true
	}
	return false
}

// **********************************************************************
// * Methode om op te vragen of een Partition Private is
// **********************************************************************
func (p *Partition) GetPrivate() bool {
	if p.Public == constanten.No {
		return true
	}
	return false
}

// **********************************************************************
// * Methode om een Partition Private te maken. Dit houdt in dat wanneer
// * het een nieuwe Partition is, dit zondermeer kan. Wanneer het een
// * bestaande Partition is, moet er gecontroleerd worden of de Partition
// * geen objecten bevat. Wanneer dit het geval is, kan de Partition
// * Private worden gemaakt.
// **********************************************************************
func (p *Partition) SetPrivate() {
	p.Public = constanten.No
	logging.InfoMSG("Partition set to Private")
}

// **********************************************************************
// * Procedure om de Fully Qualified Object naam in te stellen. Dit is
// * met name bedoeld om het object unique te kunnen identificeren (
// * bijvoorbeeld bij een export). Bij deze FQName worden alle onnodige
// * spaties verwijderen en wordt de objectname geconverteerd naar
// * uppercase. De eigenschap Name wordt dan veel meer cosmetisch gebruikt
// * als een display naam waarbij verschillende cases gebruikt kunnen worden.
// * De FQName wordt op basis van de Name gezet (worden geen parameters
// * bij de aanroep meegegeven)
// **********************************************************************
func (p *Partition) SetFQName() {

	tmp := p.GetName()
	tmp = utils.Trim(tmp)
	tmp = strings.ToUpper(tmp)
	p.FQName = tmp
	logging.InfoMSG("Set object Fully Qualified Name to '" + p.FQName + "'")
}

func (p *Partition) GetFQName() string {
	return p.FQName
}



// **********************************************************************
// * Procedure om de naam van de Partitie in te stellen
// **********************************************************************
func (p *Partition) SetName(name string) {
	p.Name = name
	logging.InfoMSG(prefix + "Set " + naming.PartitionObjNameEV + " Name to '" + name + "'")
}

// **********************************************************************
// * Procedure om de naam van de Partitie op te vragen
// **********************************************************************
func (p *Partition) GetName() string {
	return p.Name
}

// **********************************************************************
// * Procedure om de omschrijving van de Partitie in te stellen
// **********************************************************************
func (p *Partition) SetDescription(description string) {
	p.Description = description
	logging.InfoMSG(prefix + "Set " + naming.PartitionObjNameEV + " Description to '" + description + "'")
}

// **********************************************************************
// * Procedure om de omschrijving van de Partitie op te vragen
// **********************************************************************
func (p *Partition) GetDescription() string {
	return p.Description
}

// **********************************************************************
// * Procedure om Partition als internal in te stellen. Dit houdt in dat
// * er door gebruikers geen objecten in de partitie aangemaakt kunnen
// * worden
// **********************************************************************
func (p *Partition) SetAsInternal(internal bool) {
	p.Internal = constanten.No
	tmp := "Public"
	if internal == true {
		tmp = "Internal"
		p.Internal = constanten.Yes
		tmp = "Internal"
	}
	logging.InfoMSG(prefix + "Set " + naming.PartitionObjNameEV + " mode to '" + tmp + "'")
}

// **********************************************************************
// * Procedure om uit te vragen of de Partition als Internal is gekenmerkt
// **********************************************************************
func (p *Partition) GetInternal() string {
	return p.Internal
}

// **********************************************************************
// * Procedure om de Partition als default in te stellen
// **********************************************************************
func (p *Partition) SetAsDefault(isDefault bool) {
	p.Default = constanten.No
	if isDefault == true {
		p.Default = constanten.Yes
	}

	logging.InfoMSG(prefix + "Set Default " + naming.PartitionObjNameEV + " to '" + p.Default + "'")
}

// **********************************************************************
// * Procedure om uit te vragen of de Partition als Default is gekenmerkt
// **********************************************************************
func (p *Partition) GetAsDefault() string {
	return p.Default
}

// **********************************************************************
// * Verwijderen van een Partitie
// * Dit houdt in dat de Partitie niet wordt verwijderd, maar de
// * delete-indicator op Yes wordt gezet. Een partitie gemakeerd
// * als interna kan niet verwijderd worden
// **********************************************************************
func (p *Partition) Delete() {
	p.Deleted = constanten.Yes
	logging.InfoMSG(prefix + naming.PartitionObjNameEV + " is marked for deletion")
}

// **********************************************************************
// Procedure om op basis van de Partition ID de gegevens van de Partitie
// ophaalt. Deze procedure is in principe hetzelfde als GetPartitionByName en
// vandaar wordt hier niet de code opgenomen.
// Er wordt hier op basis van de ID de naam van de Partition opgezocht en
// vervolgens wordt hiermee GetPartitionByName aangeroepen
// **********************************************************************
func (p *Partition) GetPartitionById(partitionID int64) {
	partName := ""
	sqlCmd := repository.SqlCmd_par_009

	rows, err := repository.DBConnection.Query(sqlCmd, partitionID, constanten.No)
	if err != nil {
		logging.ErrorMSG(prefix + "Failed to connect to repository: " + err.Error())
	}

	for rows.Next() {
		err := rows.Scan(&partName)
		if err != nil {
			logging.DebugMSG(err.Error())
		} else {
			// Overzetten van de gegevens
			logging.ParameterMSG("GetPartitionByName=>Name=" + partName)
			p.GetPartitionByName(partName)
		}
	}

}

// **********************************************************************
// Procedure om op basis van de Partition naam de gegevens van de
// betreffende Partitie op te halen. De ingelezen gegevens worden
// aangeboden via het object
// **********************************************************************
func (p *Partition) GetPartitionByName(partitionFQName string) {

	partitionName := strings.ToUpper(utils.Trim(partitionFQName))

	partitionFound := false

	sqlCmd := repository.SqlCmd_par_003
	logging.InfoMSG(prefix + "getting " + naming.PartitionObjNameEV + " information by name (" + partitionName + ")")
	p.Name = partitionName

	// Variabelen declareren die gelezen worden uit de tabel
	var partition_id int64 = 0
	partition_description := ""
	partition_default := ""
	partition_internal := ""
	partition_fqname := ""
	partition_name := ""
	partition_public := ""

	rows, err := repository.DBConnection.Query(sqlCmd, partitionName, constanten.No)
	if err != nil {
		logging.ErrorMSG(prefix + "Failed to connect to repository: " + err.Error())
	}
	for rows.Next() {
		err := rows.Scan(&partition_id, &partition_description, &partition_default, &partition_internal, &partition_fqname, &partition_name, &partition_public)
		if err != nil {
			logging.DebugMSG(err.Error())
		} else {
			// Overzetten van de gegevens
			partitionFound = true
			p.Id = partition_id
			p.FQName = partition_fqname
			p.Name = partition_name
			p.Description = partition_description
			p.Default = partition_default
			p.Public = partition_public
			p.Internal = partition_internal
		}
	}

	// Controleren of de Partitie is gevonden
	if partitionFound == false {
		logging.ErrorMSG(naming.PartitionObjNameEV + " '" + partitionName + "' not found")
	}

}

// **********************************************************************
// * Procedure om het Partition object te controleren voordat het wordt
// * opgeslagen. Deze procedure aan worden aangeroepen voor zowel een
// * nieuwe Partition als een reeds bestaande Partition
// **********************************************************************
func (p *Partition) CheckObject() (int, string) {

	exitCode := 0
	exitMsg := strings.Replace(melding.Melding_prt_002, "$1", p.Name, 1)

	// Controleren of er een UserID is gezet
	// Dit is noodzakelijk in verband met het controleren van de juiste privileges
	//if UserId == 0 {
	//	return 9, melding.Melding_prt_009
	//}

	// Voor de zekerheid de Application Name trimmen
	p.Name = utils.Trim(p.Name)

	logging.InfoMSG(prefix + "Checking the " + naming.PartitionObjNameEV + " Object")

	// Controleren in het geval van een nieuwe Partition, of de Partition
	// niet al bestaat
	if p.Id == 0 {
		testExistingPartition := Partition{}
		testExistingPartition.GetPartitionById(p.Id)
		if testExistingPartition.Id > 0 {
			return 1, melding.Melding_prt_001
		}
	}

	// Controleren van de Object Name
	exitCode, exitMsg = validatie.Check(p.Name, validatie.ObjectName)
	if exitCode > 0 {
		return exitCode, exitMsg
	}

	// Controleren van de omschrijving
	exitCode, exitMsg = validatie.Check(p.Description, validatie.Description)
	if exitCode > 0 {
		return exitCode, exitMsg
	}

	// Controles voor nieuwe objecten
	if p.Id == 0 {

		// C5 : Komt de Partition Name niet al voor
		if partitionExist(p.Name) == true {
			exitMsg = strings.Replace(melding.Melding_prt_001, "$1", p.Name, 1)
			exitCode = 1
			return exitCode, exitMsg
		}
	}

	// Controles voor bestaande objecten
	if p.Id > 0 {
		// C6 : Indien verwijderd wordt => controle of het geen interne Partition is
		if p.Internal == constanten.Yes && p.Deleted == constanten.Yes {
			logging.ErrorMSG(prefix + melding006)
			exitMsg = melding006
			exitCode = 6
			return exitCode, exitMsg
		}

		// C7 : Indien verwijderd wordt => controle of er geen objecten meer aanwezig zijn in de Partition
		if p.Deleted == constanten.Yes {
			if p.GetNumberOfObjectsInPartition() > 0 {
				logging.ErrorMSG(prefix + melding008)
				exitMsg = melding008
				exitCode = 8
				return exitCode, exitMsg
			}
		}

	}

	// Controleren indien de Partition is gewijzigd van Public naar Private of de
	// Partition geen objecten meer bevat. Deze controle alleen uitvoeren voor een
	// bestaande Partition
	if p.Id > 0 {
		backupPartition := Partition{}
		backupPartition.GetPartitionById(p.Id)
		if backupPartition.GetPublic() == true && p.GetPublic() == false {
			if p.GetNumberOfObjectsInPartition() > 0 {
				return 11, melding.Melding_prt_011
			}

		}
	}

	return exitCode, exitMsg
}

// **********************************************************************
// * Procedure om de wijzigingen in de Partitie weg te schrijven naar
// * de database. Voordat dat alles wordt weggeschreven worden er nog wel
// * een aantal controles uitgevoerd:
// *   - is de naam van de Paritie wel gevuld
// *   - Komt de naam van de Partitie nog niet voor
// *   - Is de lengte van de name van de Partitie niet te lang
// *   - Bestaat de naam uit louter toegestaande characters (letters, cijfers, underscores)
// *   - Is de omschrijving niet te lang
// *   - een Internal Partition niet verwijderd kan worden
// **********************************************************************
func (p *Partition) Commit() (int, string) {

	logging.InfoMSG(prefix + "Commit Partition Object (Id=" + strconv.FormatInt(p.Id,10) + ", Name=" + p.Name + ")")

	resCode := 0
	resMsg := strings.Replace(melding.Melding_prt_002, "$1", p.Name, 1)

	// Controleren of p.Deleted een waarde heeft. Als deze leeg is, wordt deze op No gezet
	if p.Deleted == "" {
		p.Deleted = constanten.No
	}

	// Wanneer Public niet is ingevuld, wordt de partition standaard als private opgeslagen. Hierdoor kunnen de objecten
	// uit de Partition niet vanuit andere Partitions worden aangeroepen
	if p.Public == "" {
		p.Public = constanten.No
	}

	if p.Internal == "" {
		p.Internal = constanten.No
	}

	// Controles uitvoeren en de Commit procedure verlaten indien niet alle controles succesvol zijn
	// Wanneer een Partition wordt verwijderd, hoeven de controles niet doorlopen te worden
	if p.Deleted == constanten.No {
		resCode, resMsg = p.CheckObject()
		if resCode > 0 {
			resMsg = strings.Replace(resMsg, "$OBJECTTYPE", naming.PartitionObjNameEV, 1)
			return resCode, resMsg
		}
	}

	// Controleren of de Partition die wordt verwijderd, geen internal Partition is
	if p.Deleted == constanten.Yes && p.Internal == constanten.Yes {
		return 5, strings.Replace(melding.Melding_prt_005, "$1", p.Name, 1)
	}

	// Instellen van de Fully Qualified Name voor het object
	p.SetFQName()

	// Aanmaken van een Audit Object voor deze Application
	auditInfo := audit.AuditInfo{}
	auditInfo.SetObjectType(metaobject.ObjectTypePartition)
	auditInfo.SetFQName(p.FQName)
	auditInfo.SetObjectName(naming.PartitionObjNameEV)

	// Bepalen welke Audit actie er wordt uitgevoerd
	if p.Id > 0 && p.Deleted == constanten.No {
		auditInfo.SetActionModify()
	}
	if p.Id == 0 {
		auditInfo.SetActionCreate()
	}
	if p.Deleted == constanten.Yes {
		auditInfo.SetActionDelete()
	}

	sqlCmd := ""

	// Alle Controles zijn succesvol doorlopen. Nu kan de Partition worden opgeslagen
	// Wanneer p.Id gelijk is aan 0, dan betreft het een nieuwe Partitie
	// Wanneer p.Id groter is dan 0, dan betreft het een bestaande Partitie
	logging.ParameterMSG(prefix + naming.PartitionObjNameEV + "ID=" + strconv.FormatInt(p.Id,10))
	logging.ParameterMSG(prefix + naming.PartitionObjNameEV + "Name=" + p.Name)

	// Starten van een transactie
	logging.InfoMSG(prefix + "Starting SQL Transaction")
	tx, err := repository.DBConnection.Begin()
	if err != nil {
		logging.ErrorMSG(prefix + "Failed to start a SQL transaction: " + err.Error())
	}

	// Wanneer er wordt ingesteld dat deze partitie de default partitie wordt,
	// moeten voor alle andere actieve partities, de default-indicator op NO gezet
	if p.Default == constanten.Yes {
		sqlCmd = repository.SqlCmd_par_002
		stmt, err := tx.Prepare(sqlCmd)
		if err != nil {
			logging.ErrorMSG(prefix + "Failed to parse SQL command: " + err.Error())
		}
		logging.InfoMSG(prefix + "Set all Default-option of active " + naming.PartitionObjNameMV + " to No")
		_, err = stmt.Exec(constanten.No)
		if err != nil {
			logging.ErrorMSG(prefix + "Failed to execute SQL command: " + err.Error())
		}
	}

	// Opslaan van een gewijzigde partitie
	if auditInfo.ActionModify() {
		// Vastleggen van de AfterImage
		logging.InfoMSG(prefix + "Creating After Image of Partition " + p.FQName)
		p.Export()
		auditInfo.SetAfterImageObject(p.XMLCode)
		// Vastleggen van de BeforeImage
		logging.InfoMSG(prefix + "Creating Before Image of Partition " + p.FQName)
		beforePartition := Partition{}
		beforePartition.GetPartitionByName(p.FQName)
		beforePartition.Export()
		auditInfo.SetBeforeImageObject(beforePartition.XMLCode)

		sqlCmd = repository.SqlCmd_par_004
		stmt, err := tx.Prepare(sqlCmd)
		if err != nil {
			logging.ErrorMSG(prefix + "Failed to parse SQL command: " + err.Error())
		}

		partition_default := p.Default
		partition_internal := p.Internal

		partition_deleted := p.Deleted

		_, err = stmt.Exec(p.Description, partition_default, partition_internal, partition_deleted, p.Id, p.FQName, p.Public)
		if err != nil {
			logging.ErrorMSG(prefix + "Failed to execute SQL command: " + err.Error())
			logging.InfoMSG(prefix + "Rollback SQL Transaction")
			tx.Rollback()
		} else {
			logging.InfoMSG(prefix + naming.PartitionObjNameEV + " was stored succesfully")
		}

		// Wanneer het Object wordt verwijderd, moet het MetaObject verwijderd worden
		// in dezelfde transactie
		if p.Deleted == constanten.Yes {

		}

		// Als het Object is gewijzigd, moet de LastModificationDate en LastModificationUser worden aangepast
		// Dit kan worden overgeslagen bij het verwijderen van een object
		if p.Deleted == constanten.No {
			logging.InfoMSG(prefix + "Updating LastModifiedDate and LastModifiedUser")
			metObj := metaobject.MetaObject{}
			metObj.GetMetaObjectById(p.Id)
			metObj.SetUserLastModification(UserId)
			metObj.Commit()
		}
	}

	// Aanmaken van een nieuwe Partition
	if auditInfo.ActionCreate() {
		logging.InfoMSG(prefix + "Storing a new " + naming.PartitionObjNameEV + " in the repository")

		auditInfo.SetActionCreate()

		// Er wordt een insert uitgevoerd. De XML-code van het te aangemaakte object
		// wordt in de AfterImage geplaatst
		p.Export()
		auditInfo.SetAfterImageObject(p.XMLCode)

		tmpDefault := constanten.No
		if p.Default == constanten.Yes {
			tmpDefault = constanten.Yes
		}

		// Omdat er een nieuwe Partition wordt aangemaakt, moet er eerst een MetaObject worden
		// aangemaakt om ondermeer de ID voor de Partition te verkrijgen
		metaObjectPartition := metaobject.MetaObject{}
		metaObjectPartition.SetPartition(0)
		metaObjectPartition.SetApplication(0)
		metaObjectPartition.SetUser(UserId)
		metaObjectPartition.SetObjectType(metaobject.ObjectTypePartition)

		// Wanneer het om de SYSTEM Partition gaat, moet het MetaObject als ReadOnly worden ingesteld
		if p.Name == naming.SystemPartitionName {
			metaObjectPartition.SetReadOnly()
		}

		metaObjectPartition.Commit()

		// Na de commit is het MetaObject.Id beschikbaar
		p.Id = metaObjectPartition.Id

		sqlCmd = repository.SqlCmd_par_001
		stmt, err := tx.Prepare(sqlCmd)
		if err != nil {
			logging.ErrorMSG(prefix + "Failed to parse SQL command: " + err.Error())
		}
		_, err = stmt.Exec(p.Id, p.FQName, p.Name, p.Description, tmpDefault, p.Public, p.Internal, constanten.No)
		if err != nil {
			logging.ErrorMSG(prefix + "Failed to execute SQL command: " + err.Error())
			logging.InfoMSG(prefix + "Rollback SQL Transaction")
			tx.Rollback()
		} else {
			logging.InfoMSG(prefix + naming.PartitionObjNameEV + " was stored succesfully")
		}
	}

	// Verwijderen van de Partition
	// Er moet nog wel eerst gecontroleerd worden dat er geen objecten maar zijn
	// toegewezen aan de partition
	if auditInfo.ActionDelete() {

		auditInfo.SetActionDelete()
		auditInfo.SetFQName(p.FQName)

		// Er wordt een delete uitgevoerd. De XML-code van het te verwijderen objecten
		// wordt in de BeforeImage geplaatst
		p.Export()
		auditInfo.SetBeforeImageObject(p.XMLCode)

		// Om ervoor te zorgen dat de Partition niet meer gevonden wordt,
		// wordt de FQName tussen sterretjes geplaatst
		p.FQName = constanten.DeleteTag

		// Controleren op actieve objecten
		if p.GetNumberOfObjectsInPartition() > 0 {
			return 3, strings.Replace(melding.Melding_prt_003, "$1", p.Name, 1)
		}

		// Partition heeft geen actieve objecten meer kan kan (logisch) verwijderd worden
		sqlCmd := repository.SqlCmd_par_007
		stmt, err := tx.Prepare(sqlCmd)
		if err != nil {
			logging.ErrorMSG(prefix + "Failed to parse SQL command: " + err.Error())
		}
		_, err = stmt.Exec(constanten.Yes, p.Id, constanten.DeleteTag)
		if err != nil {
			logging.ErrorMSG(prefix + "Failed to execute SQL command: " + err.Error())
		} else {
			// Partition is verwijderd
			resMsg = strings.Replace(melding.Melding_prt_004, "$1", p.Name, 1)
		}

		// Bijbehorende MetaObject verwijderen
		sqlCmd = repository.SqlCmd_met_004
		stmt, err = tx.Prepare(sqlCmd)
		if err != nil {
			logging.ErrorMSG(prefix + "Failed to parse SQL command: " + err.Error())
		}
		_, err = stmt.Exec(p.Id)
		if err != nil {
			logging.ErrorMSG(prefix + "Failed to execute SQL command: " + err.Error())
			logging.InfoMSG(prefix + "Rollback SQL Transaction")
			tx.Rollback()
		} else {
			logging.InfoMSG(prefix + "MetaObject was deleted")
		}

	}

	// Aanmaken van Audit-informatie voor de actie. Wanneer het een Internal-object betreft
	// wordt er geen Audit aangemaakt
	auditInfo.SetObjectId(p.Id)
	auditInfo.WriteAuditRecord()

	// Afsluiten (commit) van de transactie
	logging.InfoMSG(prefix + "Commit SQL Transaction")
	tx.Commit()

	return 0, resMsg
}

// **********************************************************************
// Instellen van de rechten op een Partition. Omdat dit een 1-op-n
// relatie is, wordt dit niet via het objecten-model gedaan, maar
// direct door middel van een SQL-commando
// De verschillende soorten privileges voor Partitions worden
// gedefinieerd in het privilege-packages.
// **********************************************************************
func (p Partition) SetPartitionModeForUser(userId int, PartitionMode int) (int, string) {

	// Controleren of de Partition al eens is opgeslagen, want er moet een
	// PartitionId bekend zijn om het privielege te kunnen opslaan
	if p.Id == 0 {
		return 10, melding.Melding_prt_010
	}

	sqlCmd := ""

	exitMsg := ""

	// Starten van een transactie
	logging.InfoMSG(prefix + "Starting SQL Transaction")
	tx, err := repository.DBConnection.Begin()

	// Voordat er privileges worden toegekend aan Gebruiker op een Partition, worden eerst
	// alle bestaande rechten van die gebruiker op de Partition verwijderd om te voorkomen
	// dat er dubbele rijen gaan ontstaan
	sqlCmd = repository.SqlCmd_par_010
	stmt, err := tx.Prepare(sqlCmd)
	if err != nil {
		logging.ErrorMSG(prefix + "Failed to parse SQL command: " + err.Error())
	}
	_, err = stmt.Exec(p.Id, userId)
	if err != nil {
		logging.ErrorMSG(prefix + "Failed to execute SQL command: " + err.Error())
		logging.InfoMSG(prefix + "Rollback SQL Transaction")
		tx.Rollback()
	} else {
		logging.InfoMSG("Existing privileges for the user on the " + naming.PartitionObjNameEV + " have been flushed")
		exitMsg = "Revoke was succesful"
	}

	// Er hoeft alleen een Insert te worden uitgevoerd wanneer het Read of ReadWrite privilege wordt toegekend
	if PartitionMode == privilege.PartitionReadMode || PartitionMode == privilege.PartitionReadWriteMode {
		sqlCmd = repository.SqlCmd_par_011
		stmt, err = tx.Prepare(sqlCmd)
		if err != nil {
			logging.ErrorMSG(prefix + "Failed to parse SQL command: " + err.Error())
		}
		_, err = stmt.Exec(p.Id, userId, PartitionMode)
		if err != nil {
			logging.ErrorMSG(prefix + "Failed to execute SQL command: " + err.Error())
			logging.InfoMSG(prefix + "Rollback SQL Transaction")
			tx.Rollback()
		} else {
			logging.InfoMSG("Privileges for the user on the " + naming.PartitionObjNameEV + " has been granted")
			exitMsg = "Grant was succesful"
		}

	}

	// Afsluiten (commit) van de transactie
	logging.InfoMSG(prefix + "Commit SQL Transaction")
	tx.Commit()

	return 0, exitMsg

}

// **********************************************************************
// * Procedure om te controleren of er nog actieve objecten aanwezig
// * zijn in de Partition. Als resultaat wordt het aantal objecten
// * geretourneerd. Deze controle moet worden uitgevoerd voordat een
// * Partition verwijderd kan wordens
// **********************************************************************
func (p *Partition) GetNumberOfObjectsInPartition() int {

	logging.InfoMSG(prefix + "Getting the number of Objects stored in " + naming.PartitionObjNameEV + " '" + p.Name + "'")

	sqlCmd := repository.SqlCmd_met_005
	queryStmt, err := repository.DBConnection.Prepare(sqlCmd)
	if err != nil {
		logging.ErrorMSG(prefix + "Failed to parse SQL command: " + err.Error())
	}

	cnt := 0
	queryStmt.QueryRow(p.Id).Scan(&cnt)
	logging.InfoMSG("Number of objects=" + strconv.Itoa(cnt))

	return cnt
}

// Definition van de SQLCommando's die ten behoeve van de Partition worden uitgevoerd

// SQLCommando om te bepalen of de SystemPartion bestaat
var sqlCmd001 = "select count(*)  as cnt from " + repository.RepositorySchema + "partition where partition_fqname=$1 and partition_deleted=$2"

// **********************************************************************
// * Procedure waarin een aantal controles worden uitgevoerd bij het
// * starten van de Central Instance. De volgende zaken worden gecontroleerd:
//
//  - Bestaat de System-partition. Zo niet dan wordt deze aangemaakt
// **********************************************************************
func StartupCheck() {
	logging.InfoMSG(prefix + "Checking " + naming.PartitionObjNameMV)

	// Controleren of de System-partitie bestaat
	if partitionExist(naming.SystemPartitionName) == false {
		logging.InfoMSG(prefix + "Creating the " + strings.ToUpper(naming.SystemPartitionName) + " " + naming.PartitionObjNameEV)
		sysPart := Partition{}
		sysPart.SetName(naming.SystemPartitionName)
		sysPart.SetDescription(naming.SystemPartitionDescription)
		sysPart.SetAsInternal(true)
		sysPart.SetPublic()
		//sysPart.SetAsDefault(true)
		sysPart.Commit()
	}

	// Controleren of de Global Partition bestaat
	if partitionExist(naming.GlobalPartitionName) == false {
		logging.InfoMSG(prefix + "Creating the " + strings.ToUpper(naming.GlobalPartitionName) + " " + naming.PartitionObjNameEV)
		globalPart := Partition{}
		globalPart.SetName(naming.GlobalPartitionName)
		globalPart.SetDescription(naming.GlobalPartitionDescription)
		globalPart.SetAsInternal(false)
		globalPart.SetAsDefault(true)
		globalPart.Commit()
	}

}

// **********************************************************************
// * Procedure voor het dumpen van het Object. Dit is bedoeld om tijdens
// * development van de applicatie makkelijker problemen te kunnen
// * analyseren
// **********************************************************************
func (p *Partition) DumpObject() {
	logging.DevMSG("Id                          = " + strconv.FormatInt(p.Id,10))
	logging.DevMSG("FQName                      = " + p.FQName)
	logging.DevMSG("Name                        = " + p.Name)
	logging.DevMSG("Description                 = " + p.Description)
	logging.DevMSG("Internal                    = " + p.Internal)
	logging.DevMSG("Default                     = " + p.Default)
	logging.DevMSG("Public                      = " + p.Public)
	logging.DevMSG("Deleted                     = " + p.Deleted)
}

// **********************************************************************
// * Procedure waarin op basis van de naam van de Partitie bepaald wordt
// * of deze al voorkomt. Er worden alleen niet-verwijderde Partities
// * in de controle meegenomen
// **********************************************************************
func partitionExist(name string) bool {

	outPartitionExist := false

	queryStmt, err := repository.DBConnection.Prepare(sqlCmd001)
	if err != nil {
		logging.ErrorMSG(prefix + err.Error())
	}

	cnt := 0
	queryStmt.QueryRow(strings.ToUpper(name), constanten.No).Scan(&cnt)

	logging.InfoMSG(prefix + "Check if " + naming.PartitionObjNameEV + " '" + strings.ToUpper(name) + "' exists")

	if cnt == 0 {
		logging.InfoMSG(prefix + naming.PartitionObjNameEV + " '" + strings.ToUpper(name) + "' does not exist")
	} else {
		logging.InfoMSG(prefix + naming.PartitionObjNameEV + " '" + strings.ToUpper(name) + "' exists")
		outPartitionExist = true
	}

	return outPartitionExist
}


// **********************************************************************
// * Methode voor het genereren van HTML Code om een Partition in een
// * Combobox te selecteren. Er worden alleen Partition opgenomen die
// * niet verwijderd zijn en die niet als Internal gekenmerkt zijn
// **********************************************************************
func GetHTMLListOfPartitions() string {
	
	outHTML := ""
	
	// De volgende velden worden opgenomen in de HTML-code
	//  - partition_id
	//  - partition_name
	//  - partition_description
	
	var partition_id int64 = 0
	partition_name := "" 
	partition_description := "" 
	
	partitionFound := false 
	
	rows, _ := repository.DBConnection.Query(repository.SqlCmd_par_012, constanten.No, constanten.No)
	logging.InfoMSG(prefix + "Executing query SqlCmd_par_012")
	for rows.Next() {
		err := rows.Scan(&partition_id, &partition_name, &partition_description)
		if err != nil {
			logging.DebugMSG(err.Error())
		} else {
			partitionFound = true 
			//tmp := application_name + " | " + partition_description
			tmp := partition_name 
			outHTML = outHTML + "<option value=" + strconv.FormatInt(partition_id, 10) + ">" + tmp + "</option>" 
		}
	}
	
	// Wanneer er Partitions zijn gevonden, kan de begin en sluit-tag worden opgenomen
	if partitionFound {
		outHTML = "<select name='partition'>" + outHTML + "</select>"
	}
	
	return outHTML
}