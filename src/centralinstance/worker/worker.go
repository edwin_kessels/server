package worker 

import "logging"
import "strconv"
import "centralinstance/partition"
import "centralinstance/metaobject"
import "repository"
import "strings"
import "naming"
import "constanten"
import "esb"
import "berichten"
import "encoding/xml"
import "settings"
import "statussen/workerstatus"
import "utils"
import "melding"
import "validatie"

var prefix = naming.WorkerObjNameEV + ": "

// Bij het aanmaken van een Worker zijn de volgende gegevens verplicht:
//  - Name
//  - Hostname
//  - PortNumber
//  - Partition 
//  - Application

type Worker struct {
    Id					int64
    Protocol			string
    Name				string 
    Description			string
	HostName			string  
	PortNumber			string 
	PartitionId			int64
	ApplicationId		int64	
	FQName				string 
	QueueName			string
	StatusRequested 	string  
	StatusActual 		string  
	AutoStart			string  
	HeartBeatTimeOut	int 		// Hoeveel seconden mag de laatste heartbeat in het verleden liggen
	Maxjobs				int
	audit				string  
	deleted				string  
	Load1				int
	Load5				int
	Load15				int 
	Platform			string
	StartTime			string	
	// WorkerConfiguration
	PlatformFamily		string
	PlatformVersion		string
	KernelVersion		string
	HostId				string 
	VirtSystem			string 
	VirtRole			string 
	SoftwareVersion		string 
	SoftwareDate		string
	JobDirectory		string 
	
}



// **********************************************************************
// * SetId / GetId methode
// * Instellen / Opvragen van de Id van de Worker
// **********************************************************************
func (w *Worker) SetId(inId int64)  {
	w.Id = inId
	logging.DevMSG("Worker Id set to " + strconv.FormatInt(w.Id,10))
}

func (w *Worker) GetId() int64 {
    return w.Id 
}


// **********************************************************************
// * SetProtocol / GetProtocol methode
// * Instellen / opvragen van het protocol van de Worker
// **********************************************************************
func (w *Worker) SetProtocol(inProtocol string)  {
    w.Protocol = inProtocol
    logging.DevMSG("Worker Protocol set to " + w.Protocol)
}

func (w *Worker) GetProtocol() string {
    return w.Protocol 
}


// **********************************************************************
// * SetName / GetName methode
// * Instellen / opvragen van de Actual Status van de Worker
// **********************************************************************
func (w *Worker) SetName(inName string)  {
    w.Name = inName
    logging.DevMSG("Worker Name set to " + w.Name)
}

func (w *Worker) GetName() string {
    return w.Name 
}


// **********************************************************************
// * GetHeartBeatTimeOut methode
// * Opvragen van de tijd (in seconden) dat de laatste Heartbeat oud mag zijn
// **********************************************************************
func (w *Worker) GetHeartBeatTimeOut() int {
	return 30 
}

// **********************************************************************
// * SetActualStatus / GetActualStatus methode
// * Instellen / opvragen van de Requested Status van de Worker
// **********************************************************************
func (w *Worker) SetActualStatus(inStatus string)  {
    w.StatusActual = inStatus
    logging.DevMSG("Worker Actual Status set to " + w.StatusActual)
}

func (w *Worker) GetActualStatus() string {
    return w.StatusActual 
}


// **********************************************************************
// * SetRequestedStatus / GetRequestedStatus methode
// * Instellen / opvragen van de Requested van de Worker
// **********************************************************************
func (w *Worker) SetRequestedStatus(inStatus string)  {
    w.StatusRequested = inStatus
    logging.DevMSG("Worker Requested Status set to " + w.StatusRequested)
}

func (w *Worker) GetRequestedStatus() string {
    return w.StatusRequested 
}



// **********************************************************************
// * SetAutoStart / GetAutoStart methode
// * Instellen / opvragen van de AutoStart van de Worker
// **********************************************************************
func (w *Worker) SetAutoStart(autostart string)  {
    w.AutoStart = autostart 
    logging.DevMSG("Worker AutoStart set to " + w.AutoStart )
}

func (w *Worker) GetAutoStart() string {
    return w.AutoStart 
}


// **********************************************************************
// * SetDescription / GetDescription methode
// * Instellen / opvragen van de omschrijving van de Worker
// **********************************************************************
func (w *Worker) SetDescription(inDescription string)  {
    w.Description = inDescription
    logging.DevMSG("Worker Name set to " + w.Description)
}

func (w *Worker) GetDescription() string {
    return w.Description 
}


// **********************************************************************
// * SetPlatform / GetPlatform methode
// * Instellen / opvragen van het Platform van de Worker
// **********************************************************************
func (w *Worker) SetPlatform(inPlatform string)  {
    w.Platform = inPlatform
    logging.DevMSG("Worker Platform set to " + w.Platform)
}

func (w *Worker) GetPlatform() string {
    return w.Platform 
}


// **********************************************************************
// * GetHostName
// * Opvragen van de Hostname waarop de server draait
// * Er is geen SetHostName-methode omdat de Hostname automatisch wordt
// * geactualiseerd door de agent wrkstrt
// **********************************************************************
func (w *Worker) GetHostName() string {
    return w.HostName 
}


// **********************************************************************
// * SetPortNumber / GetPortNumber
// * Instellen / opvragen van het PortNumber
// **********************************************************************
func (w *Worker) SetPortNumber(inPortNumber string)  {
    w.PortNumber = inPortNumber
    logging.DevMSG("Worker PortNumber set to " + w.PortNumber)
}

func (w *Worker) GetPortNumber() string {
    return w.PortNumber 
}


// **********************************************************************
// * SetPartitionId / GetPartitionId methode
// * Instellen / opvragen van de Partition van de Worker
// **********************************************************************
func (w *Worker) SetPartitionId(inPartition int64)  {
    w.PartitionId = inPartition
    logging.DevMSG("Worker Partition Id set to " + strconv.FormatInt(w.PartitionId,10))
}

func (w *Worker) GetPartitionId() int64 {
    return w.PartitionId 
}


// **********************************************************************
// * SetApplicationId / GetApplicationId methode
// * Instellen / opvragen van de Application van de Worker
// **********************************************************************
func (w *Worker) SetApplicationId(inApplication int64)  {
    w.ApplicationId = inApplication
    logging.DevMSG("Worker Application Id set to " + strconv.FormatInt(w.ApplicationId,10))
}

func (w *Worker) GetApplicationId() int64 {
    return w.ApplicationId 
}


// **********************************************************************
// * SetFQName / GetFQName methode
// * Instellen / opvragen van de FQName van de Worker
// * Wanneer er een nieuwe Worker wordt aangemaakt, is alleen het ID van
// * de Partition bekend. Op basis van dit ID wordt de Partition opgehaald
// **********************************************************************
func (w *Worker) SetFQName()  {
	
	// Ophalen van de Partition Name
	part := partition.Partition{}
	part.GetPartitionById(w.PartitionId)
	w.FQName = strings.ToUpper(part.Name) + "." + strings.ToUpper(w.Name) 
	logging.DevMSG("Worker FQName set to " + w.FQName)
}

func (w *Worker) GetFQName() string {
	return 	w.FQName
}


// **********************************************************************
// * SetQueueName / GetQueueName methode
// * Methode voor het instellen en uitlezen van de private queue die door
// * de Worker wordt gebruikt
// **********************************************************************
func (w *Worker) SetQueueName()  {
	
	// Ophalen van de Partition Name
	part := partition.Partition{}
	part.GetPartitionById(w.PartitionId)
	w.QueueName = "Q_WRK_" + strings.ToUpper(part.Name) + "_" + strings.ToUpper(w.Name) 
	logging.DevMSG("Worker Queue Name set to " + w.QueueName)
}

func (w *Worker) GetQueueName() string {
	return 	w.QueueName
}



// **********************************************************************
// CheckObject
// Uitvoeren van de verschillende Controles om te kunnen bepalen of de
// Worker kan worden opgeslagen
// **********************************************************************
func (w *Worker) checkObject() (int, string) {

	exitCode := 0 
	exitMsg := "Worker was saved succesfully"
	
	// Controleren of er wel een WorkerName is ingevoerd
	if utils.Trim(w.Name) == "" {
		return 6, melding.Melding_wrk_006
	}
	
	// Controleren of de Worker Name wel een valide Object name is
	exitCode, exitMsg = validatie.Check(w.Name, validatie.ObjectName)
	if exitCode > 0 {
		return exitCode, strings.Replace(exitMsg, "$OBJECTTYPE", naming.WorkerObjNameEV ,1)
	}
	
	// Controleren of de WorkerName niet al voorkomt als actieve Worker
	// Deze controle alleen uitvoeren wanneer er een nieuwe Worker wordt opgevoerd
	if w.Id == 0 {
		sqlCmd := repository.SqlCmd_wrk_020
    	rows, err := repository.DBConnection.Query(sqlCmd, w.FQName, constanten.No)
		if err != nil {
			logging.ErrorMSG("Failed to connect to repository: " + err.Error())
		}	
			
		workerId := 0 
	    
		for rows.Next() {
			err := rows.Scan(&workerId)
			if err != nil {
				logging.ErrorMSG(err.Error())
			} else {
			  // Worker komt al voor
			  exitCode = 3
			  exitMsg = melding.Melding_wrk_003
		  }
		}
		
    }		


	return exitCode, exitMsg
}


// **********************************************************************
// * Opslaan van de Worker 
// **********************************************************************
func (w *Worker) Commit() (int, string) {
	
	exitCode := 0 
	exitMsg := "Worker was saved succesfully"
	
	// Wanneer er een nieuwe Worker wordt opgeslagen, moet de FQnaam en de 
	// Private Queue worden bepaald. 
	// Dit moet worden uitgevoerd voordat de controles op de Worker worden uitgevoerd
	if w.Id == 0 {
		w.SetFQName() 
		w.SetQueueName()
	}
	
	
	// Uitvoeren van Controles voor de Worker
	exitCode, exitMsg = w.checkObject()
	
	// Wanneer niet alle controles succesvol waren, moet de Commit() methode
	// worden verlaten
	if exitCode > 0 {
		return exitCode, exitMsg
	}
	
	// Worker kan worden opgeslagen
	
	action := "NEW"
	
	
	
	
	// Alle Controles zijn succesvol doorlopen. 
	// De Worker (nieuw of bestaand) kan worden opgeslagen
	
	// Starten van een transactie
	logging.InfoMSG(prefix + "Starting SQL Transaction")
	tx, err := repository.DBConnection.Begin()
	if err != nil {
		logging.DevMSG(prefix + "Failed to start a SQL transaction: " + err.Error())
	}
	
	// Opslaan van een nieuwe Worker
	if w.Id == 0 {
		
		// Aanmaken van een nieuw MetaObject voor de Worker
		metaObjectWorker := metaobject.MetaObject{}
		metaObjectWorker.SetPartition(w.GetPartitionId())
		metaObjectWorker.SetApplication(w.GetApplicationId())
		metaObjectWorker.SetUser(0)
		metaObjectWorker.SetObjectType(metaobject.ObjectTypeWorker)
		metaObjectWorker.Commit()
		// Na de commit is het MetaObject.Id beschikbaar
		w.Id = metaObjectWorker.Id
		
		sqlCmd := repository.SqlCmd_wrk_011
		stmt, err := tx.Prepare(sqlCmd)
		if err != nil {
			logging.ErrorMSG(prefix + "Failed to parse SQL command: " + err.Error())
		}

		_, err = stmt.Exec(w.Id, w.Name, w.Description, w.HostName, w.PartitionId, w.ApplicationId, w.AutoStart, w.FQName, w.QueueName, w.Platform)
		if err != nil {
			logging.ErrorMSG(prefix + "Failed to execute SQL command: " + err.Error())
			logging.InfoMSG(prefix + "Rollback SQL Transaction")
			tx.Rollback()
		} else {
			logging.InfoMSG(prefix + naming.WorkerObjNameEV + " was stored succesfully")
		}
		
	}
	
	
	// Afsluiten (commit) van de transactie
	logging.InfoMSG(prefix + "Commit SQL Transaction")
	tx.Commit()
	
	if action == "NEW" {
		// Zetten van de Created status
		ChangeActualStatus(w.Id, workerstatus.Created)
		ChangeRequestedStatus(w.Id, workerstatus.Created)
		
		// Queue aanmaken voor de Worker door er een fake bericht
		// naar toe te sturen
		

		//esb.CreateQueue(w.QueueName)
		esb.SendMessageESBWithoutWait("Init", w.QueueName ) 
		
	}
	
	return exitCode, exitMsg
}


// **********************************************************************
// * GetWorkerByFQName
// **********************************************************************
func (w *Worker) GetWorkerByFQName(inWorkerFQName string) {
	
	var workerId int64 = 0 
	
	// Ophalen van de WorkerId om daarna de gegevens op te halen
	// via de methode GetWorkerById
	sqlCmd := repository.SqlCmd_wrk_025
	rows, err := repository.DBConnection.Query(sqlCmd, inWorkerFQName)
	if err != nil {
		logging.ErrorMSG(prefix + "Failed to connect to repository: " + err.Error())
	}
	for rows.Next() {
		err := rows.Scan(&workerId)
		if err != nil {
			logging.DebugMSG(err.Error())
		} else {
			
			w.Id = workerId
		}
	}
	
	w.GetWorkerById(w.Id)
	
}


// **********************************************************************
// * GetWorkerById
// * Opvragen van de gegevens van een Worker op basis van de ID van de Worker
// **********************************************************************
func (w *Worker) GetWorkerById(inId int64) {
	
	sqlCmd := repository.SqlCmd_wrk_013
	logging.InfoMSG(prefix + "getting " + naming.WorkerObjNameEV + " information by Id (" + strconv.FormatInt(inId,10) + ")")
	
	// Variabelen declareren die gelezen worden uit de tabel
	worker_name := ""
	worker_description := ""
	worker_hostname := ""
	worker_fqname := "" 
	worker_status_requested := ""
	worker_status_actual := ""
    worker_autostart := ""
    worker_maxjobs := 0 
    var worker_partition int64 = 0 
    var worker_application int64 = 0 
    worker_deleted := "" 
    worker_esb_queue := ""
    var worker_starttime string = ""

	workerFound := false

	rows, err := repository.DBConnection.Query(sqlCmd, inId)
	if err != nil {
		logging.ErrorMSG(prefix + "Failed to connect to repository: " + err.Error())
	}
	for rows.Next() {
		err := rows.Scan(&worker_name, &worker_description, &worker_hostname,  &worker_fqname, &worker_status_requested, &worker_status_actual, &worker_autostart, &worker_maxjobs, &worker_partition, &worker_application, &worker_deleted, &worker_esb_queue, &worker_starttime)
		if err != nil {
			logging.DebugMSG(err.Error())
		} else {
			workerFound = true 
			// Overzetten van de gegevens
			w.Id				= inId
			w.Name				= worker_name
			w.Description		= worker_description
			w.HostName			= worker_hostname
			w.FQName			= worker_fqname
			w.StatusRequested	= worker_status_requested
			w.StatusActual		= worker_status_actual
			w.AutoStart			= worker_autostart
			w.Maxjobs			= worker_maxjobs
			w.PartitionId		= worker_partition
			w.ApplicationId		= worker_application
			w.deleted			= worker_deleted
			w.QueueName			= worker_esb_queue
			w.StartTime			= worker_starttime
		}
	}
	
	// Ophalen van de Configuratie van de Worker
	var platform string = ""
	var platform_family string = ""  
	var platform_version  string = ""
	var kernel_version string = ""
	var host_id  string = ""
	var virtualization_system string = ""
	var virtualization_role string = ""
	var softwareversion string = ""
	var softwaredate string = ""
	var jobdir string = ""
	rows, err = repository.DBConnection.Query(repository.SqlCmd_wrkcfg_003, w.Id)
	if err != nil {
		logging.ErrorMSG(prefix + "Failed to connect to repository: " + err.Error())
	}
	for rows.Next() {
		err := rows.Scan(&platform, &platform_family, &platform_version, &kernel_version,  &host_id, &virtualization_system, &virtualization_role, &softwareversion, &softwaredate, &jobdir)
		if err != nil {
			logging.DebugMSG(err.Error())
		} else {
			// Overzetten van de gegevens
			w.Platform = platform
			w.PlatformFamily = platform_family
			w.PlatformVersion =platform_version
			w.KernelVersion	= kernel_version
			w.HostId = host_id 
			w.VirtSystem = virtualization_system 
			w.VirtRole = virtualization_role 
			w.SoftwareVersion = softwareversion
			w.SoftwareDate = softwaredate
			w.JobDirectory = jobdir			
		}
	}
	
	

	// Controleren of de Partitie is gevonden
	if workerFound == false {
		logging.ErrorMSG(naming.WorkerObjNameEV + " not found")
		w.Id = 0 
	}
	
}


// **********************************************************************
// * GetWorkerById
// * Opvragen van de gegevens van een Worker op basis van de ID van de Worker
// **********************************************************************
func (w *Worker) DumpObject() {
	logging.DevMSG("Id					= " + strconv.FormatInt(w.Id,10))
    logging.DevMSG("Protocol			= " + w.Protocol)
    logging.DevMSG("Name				= " + w.Name)
    logging.DevMSG("Description			= " + w.Description)
	logging.DevMSG("HostName			= " + w.HostName) 
	logging.DevMSG("PortNumber			= " + w.PortNumber)
	logging.DevMSG("PartitionId			= " + strconv.FormatInt(w.PartitionId,10))
	logging.DevMSG("ApplicationId		= " + strconv.FormatInt(w.ApplicationId,10))
	logging.DevMSG("FQName				= " + w.FQName)
	logging.DevMSG("StatusRequested		= " + w.StatusRequested)
	logging.DevMSG("StatusActual		= " + w.StatusActual) 
	logging.DevMSG("Autostart			= " + w.AutoStart)
	logging.DevMSG("Maxjobs				= " + strconv.Itoa(w.Maxjobs))
	logging.DevMSG("Deleted				= " + w.deleted)
}


// **********************************************************************
// * Stop
// * Methode om een Worker te stoppen. 
// * Voorlopig houdt dit in dat alleen de Status in de Repository
// * wordt aangepast
// **********************************************************************
func (w *Worker) Stop() {
	ChangeRequestedStatus(w.Id, workerstatus.Down)
	ChangeActualStatus(w.Id, workerstatus.Down)
	
	// Toevoegen van een Comment dat de Worker gestart is
	 AddWorkerComment(w.Id, "Worker shutdown")
}

// **********************************************************************
// * Start
// * Het starten van een Worker. Dit houdt in dat er een bericht naar
// * de HUB wordt gestuurd, om de Worker te controleren. De HUB die het
// * bericht van de start oppakt, moet dit bericht na uitvoering delen
// * met de overige HUBs
// **********************************************************************
func (w *Worker) Start() {
	
	// De volgende acties moeten worden uitgevoerd:
	//  - De requested_status van de Worker moet worden aangepast
	//  - Er moet een RequestHeartBeat -message worden verstuurd
	
	//var tmpXML berichten.WorkerStartUp
	//tmpXML.Id = w.Id
	//tmpXML.WorkerAddress = w.Protocol + w.HostName + ":" + w.PortNumber
	//xmlBericht, _ := xml.MarshalIndent(tmpXML, "", "\t")
	//esb.SendMessageESBWithoutWait(string(xmlBericht), settings.ESB_OUT_Queue_WorkerStartup)
	
	// aanpassen van de RequestedStatus naar Running
	// Wanneer de RequestedStatus naar Running wordt gezet, wordt er automatisch
	// een MonitorWorker-bericht verstuurd
	ChangeRequestedStatus(w.Id, workerstatus.Running)
	ChangeActualStatus(w.Id, workerstatus.Connecting)
	
	
	// Toevoegen van een Comment dat de Worker gestart is
	 AddWorkerComment(w.Id, "Worker started")
}

// **********************************************************************
// * SetLoadMetrics
// * Methode om de 3 load-metrics (load1, load5 en load15) weg te schrijven
// * naar de workermetrics tabel
// **********************************************************************
func (w *Worker) SetLoadMetrics() {
	
	// Bepalen van het huidige timestamp
	currentTimestamp := utils.CurrentTimeStamp(constanten.DateFormatLong)
	
	// Stap 1: verwijderen van de reeds aanwezige metrics voor de Worker
	logging.DebugMSG("Deleting actual metrics of the Worker " + w.GetName() )
	stmt, err := repository.DBConnection.Prepare(repository.SqlCmd_wrkmet_001)
	if err != nil {
		logging.ErrorMSG("Cannot prepare SQL Command:")
		logging.ErrorMSG(err.Error())
	}
	_, err = stmt.Exec(w.Id)
	if err != nil {
	    logging.ErrorMSG("Cannot execute SQL Command:")
	    logging.ErrorMSG(err.Error())
	}
	
	
	// Stap 2: opslaan van de metrics voor de Worker
	logging.DebugMSG("Fresh metrics have been added for Worker " + w.GetName() )
	stmt, err = repository.DBConnection.Prepare(repository.SqlCmd_wrkmet_002)
	if err != nil {
		logging.ErrorMSG("Cannot prepare SQL Command:")
		logging.ErrorMSG(err.Error())
	}
	_, err = stmt.Exec(w.Id, w.Load1, w.Load5, w.Load15, currentTimestamp)
	if err != nil {
	    logging.ErrorMSG("Cannot execute SQL Command:")
	    logging.ErrorMSG(err.Error())
	}
	
	
}


// **********************************************************************
// * getLoadMetrics
// * Methode om de actuele metrics van een Worker uit te lezen
// **********************************************************************
func (w *Worker) GetLoadMetrics() {
	
	rows, err := repository.DBConnection.Query(repository.SqlCmd_wrkmet_003, w.Id )
	if err != nil {
		logging.ErrorMSG("Error=" + err.Error() )
	}
	
	load1 := 0 
	load5 := 0 
	load15 := 0 
	
	for rows.Next() {
		err := rows.Scan(&load1, &load5, &load15)
		if err != nil {
			logging.DebugMSG(err.Error())
		} else {
			// Metrics zijn gevonden en worden overgezet naar de Worker
			w.Load1 = load1
			w.Load5 = load5
			w.Load15 = load15
		}
	}
	 
}

// **********************************************************************
// * IsOverloaded
// * Methode om te bepalen of een Worker Overloaded is
// **********************************************************************
func (w *Worker) IsOverloaded() bool {
	
	isOverloaded := false 
	if w.Load1 > settings.OverloadPercLoad1 {
		isOverloaded = true 
	}
	if w.Load5 > settings.OverloadPercLoad5 {
		isOverloaded = true 
	}
	if w.Load15 > settings.OverloadPercLoad15 {
		isOverloaded = true 
	}
	
	if isOverloaded == true {
		logging.DebugMSG("Worker " + w.GetFQName() + " is overloaded")
	}
	
	return isOverloaded
}


// **********************************************************************
// * getCSSClassForWorkerStatus
// * Methode om de juiste CCS-class te selecteren op basis van de 
// * status die bij de aanroep wordt meegegeven
// **********************************************************************
func getCSSClassForWorkerStatus(status string) string {
	
	outHTML := "<td font color='black'>"
	
	if status == workerstatus.Connecting {
		outHTML = "<td style='color: orange;'>"
	}
	if status == workerstatus.Overload {
		outHTML = "<td style='color: purple;'>"
	}
	if status == workerstatus.Standby {
		outHTML = "<td style='color: navy;'>"
	}
	if status == workerstatus.Down {
		outHTML = "<td style='color: red;'>"
	}
	if status == workerstatus.Running {
		outHTML = "<td style='color: green;'>"
	}
	
	return outHTML
}


// **********************************************************************
// * Methode voor het genereren van HTML Code om de status van de 
// * verschillende Workers te tonen. 
// **********************************************************************
func GetHTMLStatusWorkers() string {
	
	outHTML := ""
	
	// De volgende velden worden opgenomen in de HTML-code
	//  - worker_id
	//  - worker_fqname
	//  - worker_description
	
	var worker_id int64 = 0 
	worker_name := "" 
	worker_fqname := "" 
	//worker_description := "" 
	worker_actual_status := ""
	worker_requested_status := ""
	worker_hostname := ""
	var worker_platform string = ""
	
	workerFound := false 
	
	rows, err := repository.DBConnection.Query(repository.SqlCmd_wrk_022, constanten.No)
	if err != nil {
		logging.ErrorMSG("Error=" + err.Error() )
	}
	
	for rows.Next() {
		err := rows.Scan(&worker_id, &worker_name, &worker_fqname, &worker_actual_status, &worker_requested_status, &worker_hostname, &worker_platform)
		if err != nil {
			logging.DebugMSG(err.Error())
		} else {
			workerFound = true 
			var link string = "<a href=/worker/details/" + worker_fqname +  ">" + worker_fqname + "</a>"
			metrics := ""
			outHTML = outHTML + "<tr>" 
			outHTML = outHTML + "<td>" +link + "</td>"
			outHTML = outHTML + "<td>" + worker_hostname + "</td>"
			outHTML = outHTML + "<td>" + worker_platform + "</td>"
			outHTML = outHTML + getCSSClassForWorkerStatus(worker_actual_status) + worker_actual_status + "</td>"
			
			// Requested Status alleen laten zien wanneer deze afwijkt van de actual status
			requestedStatus := ""
			if worker_actual_status != worker_requested_status {
				requestedStatus = worker_requested_status
			}
			
			outHTML = outHTML + "<td style='color: lightgray;'>" + requestedStatus + "</td>"
			
			// Wanneer een Worker Running of Overloaded is, kunnen ook de Metrics worden getoond
			if worker_actual_status == workerstatus.Running || worker_actual_status == workerstatus.Overload {
				// Ophalen van de actuele Metrics
				metricsWrk := Worker{} 
				metricsWrk.GetWorkerById(worker_id)
				metricsWrk.GetLoadMetrics() 
				metrics = strconv.Itoa(metricsWrk.Load1) + "% - " + strconv.Itoa(metricsWrk.Load5) + "% - " + strconv.Itoa(metricsWrk.Load15) + "%"
				
			}
			outHTML = outHTML + "<td>" + metrics + "</td>"
			outHTML = outHTML + "</tr>"
		}
	}
	
	// Wanneer er Workers zijn gevonden, kan de begin en sluit-tag worden opgenomen
	if workerFound {
		outHTML = "<table><tr><th>Worker</th><th>Host</th><th>Platform</th><th>Actual&nbsp;status</th><th>Requested&nbsp;status</th><th>Load</th></tr>" + outHTML
		outHTML = outHTML + "</table>"
	}
	
	return outHTML
}


// **********************************************************************
// * Methode voor het genereren van HTML Code om een Worker in een
// * Combobox te selecteren. 
// **********************************************************************
func GetHTMLListOfWorkers() string {
	
	outHTML := ""
	
	// De volgende velden worden opgenomen in de HTML-code
	//  - worker_id
	//  - worker_fqname
	//  - worker_description
	
	worker_id := 0 
	worker_name := "" 
	worker_fqname := "" 
	worker_description := "" 
	
	workerFound := false 
	
	rows, _ := repository.DBConnection.Query(repository.SqlCmd_wrk_012, constanten.No)
	for rows.Next() {
		err := rows.Scan(&worker_id, &worker_name, &worker_fqname, &worker_description)
		if err != nil {
			logging.DebugMSG(err.Error())
		} else {
			workerFound = true 
			//tmp := application_name + " | " + application_description
			tmp := worker_fqname 
			outHTML = outHTML + "<option value=" + strconv.Itoa(worker_id) +  ">" + tmp + "</option>" 
		}
	}
	
	// Wanneer er Workers zijn gevonden, kan de begin en sluit-tag worden opgenomen
	if workerFound {
		outHTML = "<select name='worker'>" + outHTML + "</select>"
	}
	
	return outHTML
}


// *************************************************************************
// * Methode om de waarde van Hostname in de tabel Worker te actualiseren
// * met de waarde zoals deze op de Worker is vastgesteld
// *************************************************************************
func SetHostName(inWorkerId int, inHostname string) {
	
	logging.DevMSG("Updating the Hostname of Worker")
	
	stmt, err := repository.DBConnection.Prepare(repository.SqlCmd_wrk_021)
	if err != nil {
		logging.ErrorMSG("Cannot prepare SQL Command:")
		logging.ErrorMSG(err.Error())
	}
	_, err = stmt.Exec(inHostname, inWorkerId)
	if err != nil {
    	logging.ErrorMSG("Cannot execute SQL Command:")
		logging.ErrorMSG(err.Error())
	}
}


// *************************************************************************
// * Methode voor het actualiseren van de status van de Worker. Dit wordt
// * niet binnen het object zelf uitgevoerd. De reden is dat er voor een 
// * statuswijziging direct een Commit wordt gegeven.
// * Wanneer de status van de Worker wordt gewijzigd naar Runnig, dan moet 
// * de StartTijd van de Worker worden geregisteerd. In alle andere
// * gevallen moet de StartTijd op "N/A" wordt gezet
// *************************************************************************
func ChangeActualStatus(workerId int64, newStatus string) {
	
	logging.DevMSG("Changing Actual status of Worker " + strconv.FormatInt(workerId,10) + " to " + newStatus )
	
	var startTime string = "N/A"
	if newStatus == workerstatus.Running {
		startTime = utils.CurrentTimeStamp(constanten.DateFormatLong)	
	}
	
	
	stmt, err := repository.DBConnection.Prepare(repository.SqlCmd_wrk_014)
	if err != nil {
		logging.ErrorMSG("Cannot prepare SQL Command:")
		logging.ErrorMSG(err.Error())
	}
	_, err = stmt.Exec(newStatus, workerId)
	if err != nil {
    	logging.ErrorMSG("Cannot execute SQL Command:")
		logging.ErrorMSG(err.Error())
	}
	
	// Updaten van de starttijd van de Worker
	stmt, err = repository.DBConnection.Prepare(repository.SqlCmd_wrk_026)
	if err != nil {
		logging.ErrorMSG("Cannot prepare SQL Command:")
		logging.ErrorMSG(err.Error())
	}
	_, err = stmt.Exec(startTime, workerId)
	if err != nil {
    	logging.ErrorMSG("Cannot execute SQL Command:")
		logging.ErrorMSG(err.Error())
	}
	
}

func ChangeRequestedStatus(workerId int64, newStatus string) {
	
	logging.DevMSG("Changing Requested status of Worker " + strconv.FormatInt(workerId,10) + " to " + newStatus )
	
	stmt, err := repository.DBConnection.Prepare(repository.SqlCmd_wrk_015)
	if err != nil {
		logging.ErrorMSG("Cannot prepare SQL Command:")
		logging.ErrorMSG(err.Error())
	}
	_, err = stmt.Exec(newStatus, workerId)
	if err != nil {
    	logging.ErrorMSG("Cannot execute SQL Command:")
		logging.ErrorMSG(err.Error())
	}
	
	// Wanneer de RequestedStatus naar Running wordt gezet, is het nodig
	// dat de HUB op de hoogte wordt gebracht om de Worker te monitoren
	// Om de HUB op de hoogte te stellen, wordt het bericht MonitorWorker
	// verstuurd
	if newStatus == workerstatus.Running {
		var tmpXML berichten.MonitorWorker
		tmpXML.WorkerId = workerId
		tmpXML.HeartBeatTimeOut = 30 //TODO Hardcoded opgenomen
		tmpXML.TimeStamp = utils.CurrentTimeStamp(constanten.DateFormatShort)
		tmpXML.CheckSum = berichten.GetCheckSumMonitorWorker(tmpXML)	
		xmlBericht, _ := xml.MarshalIndent(tmpXML, "", constanten.XMLIndent)
		esb.SendMessageESBWithoutWait(string(xmlBericht), settings.ESB_INNER_QUEUE_HeartBeat) 
	}
	
}

// *************************************************************************
// * Methode om de tabel workercomment aan te passen
// * Er kunnen meerdere comments per Worker worden opgenomen. De laatst
// * toegevoegde comment is terug te vinden door middel van 
// * worker_comment_actual=YES. 
// *************************************************************************
func AddWorkerComment(workerId int64, comment string) {
	
	// Alle andere comments van de Worker op actual=NO zetten
	stmt, err := repository.DBConnection.Prepare(repository.SqlCmd_wrk_017)
	if err != nil {
		logging.ErrorMSG("Cannot prepare SQL Command:")
		logging.ErrorMSG(err.Error())
	}
	_, err = stmt.Exec(constanten.No, workerId)
	if err != nil {
    	logging.ErrorMSG("Cannot execute SQL Command:")
		logging.ErrorMSG(err.Error())
	}
	
	// Toevoegen van de nieuwe Comment
	stmt, err = repository.DBConnection.Prepare(repository.SqlCmd_wrk_018)
	if err != nil {
		logging.ErrorMSG("Cannot prepare SQL Command:")
		logging.ErrorMSG(err.Error())
	}
	_, err = stmt.Exec(workerId, utils.CurrentTimeStamp("2006-01-02 15:04:05"), comment)
	if err != nil {
    	logging.ErrorMSG("Cannot execute SQL Command:")
		logging.ErrorMSG(err.Error())
	}
	
}


// *************************************************************************
// Methode om alle Worker waarvoor AutoStart is ingesteld, op te starten
// Deze Methode wordt aangeroepen vanuit de Startup van de CentralInstance
// wanneer deze gestart wordt
// *************************************************************************
func StartWorkersWithAutoStartSetting() {
	
	sqlCmd := repository.SqlCmd_wrk_019
    rows, err := repository.DBConnection.Query(sqlCmd, constanten.Yes, constanten.No)
	if err != nil {
		logging.ErrorMSG("Failed to connect to repository: " + err.Error())
	}
	
	logging.InfoMSG("Starting Workers AutoStart setting")
	
	workerFound := false 
	
	var workerId int64 = 0 
	    
	for rows.Next() {
		err := rows.Scan(&workerId)
		if err != nil {
			logging.ErrorMSG(err.Error())
		} else {
			// Er is een Worker gevonden waarvoor AutoStart is ingesteld
			workerFound = true
			wrk := Worker{}
			wrk.GetWorkerById(workerId)
			if wrk.GetName() != "" {
				wrk.Start()
				// Commentaar toevoegen dat Worker is gestart door CentralInstance
				logging.DevMSG("Start Worker " + wrk.GetFQName() + " on " + wrk.GetHostName() )
				AddWorkerComment(workerId, "Worker automatically started by CentralInstance")
			}
		}
    }
    
    if workerFound == false {
    	logging.InfoMSG("No Workers found with AutoStart setting")
    }
    
}


// *********************************************************************
// * GetHTMLListOfWorkers
// * Methode om een listbox te genereren van de Workers met de 
// * opgegeven status
// *********************************************************************
func GetHTMLListOfWorkersWithStatus(inStatus string) string {
	
	var outHTML string = ""
	
	var wrk_id int64 = 0 
	var wrk_name string = ""
	var wrk_fqname string = ""
	
	var workerFound bool = false 
	
	sqlCmd := repository.SqlCmd_wrk_024

	rows, err := repository.DBConnection.Query(sqlCmd, inStatus, constanten.No)
	if err != nil {
		logging.ErrorMSG(prefix + "Failed to connect to repository: " + err.Error())
	}

	for rows.Next() {
		err := rows.Scan(&wrk_id, &wrk_name, &wrk_fqname)
		if err != nil {
			logging.DebugMSG(err.Error())
		} else {
			workerFound = true 
			outHTML = outHTML + "<option value=" + strconv.FormatInt(wrk_id,10) +  ">" + wrk_fqname + "</option>" 
		}
	}
	
	if workerFound == true {
		outHTML = "<select name='workerid'>" + outHTML + "</select>"		
	}
	
	return outHTML
}

