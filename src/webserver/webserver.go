package webserver

import "net/http"
import "io"
import "centralinstance/application"
import "centralinstance/partition"
import "centralinstance/worker"
import "centralinstance/jobsource"
import "centralinstance/job"
import "centralinstance/jobtype"
import "centralinstance/agent/scheduler"
import _ "github.com/lib/pq"
import "strings"
import "strconv"
import "github.com/gorilla/mux"
import "log"
import "utils"
import "statussen/jobstatus"
import "settings"
import "importobject"
import "logging"
import "statussen/workerstatus"
import "constanten"

// http://thenewstack.io/make-a-restful-json-api-go/

var css = `table a:link {
	color: #666;
	font-weight: bold;
	text-decoration:none;
}
table a:visited {
	color: #999999;
	font-weight:bold;
	text-decoration:none;
}
table a:active,
table a:hover {
	color: #bd5a35;
	text-decoration:underline;
}
table {
	font-family:Arial, Helvetica, sans-serif;
	color:#666;
	font-size:12px;
	text-shadow: 1px 1px 0px #fff;
	background:#eaebec;
	margin:20px;
	border:#ccc 1px solid;
	-moz-border-radius:3px;
	-webkit-border-radius:3px;
	border-radius:3px;
	-moz-box-shadow: 0 1px 2px #d1d1d1;
	-webkit-box-shadow: 0 1px 2px #d1d1d1;
	box-shadow: 0 1px 2px #d1d1d1;
}
table th {
	//padding:21px 25px 22px 25px;) ;
	padding:2px 2px 2px 2px;
	border-top:1px solid #fafafa;
	border-bottom:1px solid #e0e0e0;
	background: #ededed;
	background: -webkit-gradient(linear, left top, left bottom, from(#ededed), to(#ebebeb));
	background: -moz-linear-gradient(top,  #ededed,  #ebebeb);
}
table th:first-child {
	text-align: left;
	padding-left:20px;
}
table tr:first-child th:first-child {
	-moz-border-radius-topleft:3px;
	-webkit-border-top-left-radius:3px;
	border-top-left-radius:3px;
}
table tr:first-child th:last-child {
	-moz-border-radius-topright:3px;
	-webkit-border-top-right-radius:3px;
	border-top-right-radius:3px;
}
table tr {
	text-align: left;
	padding-left:20px;
}
table td:first-child {
	text-align: left;
	padding-left:20px;
	border-left: 0;
}
table td {
	padding:2px;
	border-top: 1px solid #ffffff;
	border-bottom:1px solid #e0e0e0;
	border-left: 1px solid #e0e0e0;
	background: #fafafa;
	background: -webkit-gradient(linear, left top, left bottom, from(#fbfbfb), to(#fafafa));
	background: -moz-linear-gradient(top,  #fbfbfb,  #fafafa);
}
table tr.even td {
	background: #f6f6f6;
	background: -webkit-gradient(linear, left top, left bottom, from(#f8f8f8), to(#f6f6f6));
	background: -moz-linear-gradient(top,  #f8f8f8,  #f6f6f6);
}
table tr:last-child td {
	border-bottom:0;
}
table tr:last-child td:first-child {
	-moz-border-radius-bottomleft:3px;
	-webkit-border-bottom-left-radius:3px;
	border-bottom-left-radius:3px;
}
table tr:last-child td:last-child {
	-moz-border-radius-bottomright:3px;
	-webkit-border-bottom-right-radius:3px;
	border-bottom-right-radius:3px;
}
table tr:hover td {
	background: #f2f2f2;
	background: -webkit-gradient(linear, left top, left bottom, from(#f2f2f2), to(#f0f0f0));
	background: -moz-linear-gradient(top,  #f2f2f2,  #f0f0f0);	
}
`

var importObject = `<html>
<head>
<title></title>
</head>
<body>
<form action="/import/object" method="post">
    <h2>Import ObjectDefinition</h2>
    <table border=0>
    <tr><td>XML</td><td>&nbsp;:&nbsp;</td><td><textarea name='definition' rows='19' cols='70'></textarea></td></tr>
    </table>	
    <input type='submit' value='Save'>
</form>
</body>
</html>`

var newJobSource = `<html>
<head>
<title></title>
[PLACEHOLDER_STYLE]
</head>
<body>
<form action="/jobsource/save" method="post">
    <h2>[PLACEHOLDER_PAGETITLE]</h2>
    <input type='hidden' name='jobsourceid' value='0'>
    <table border=0>
    <tr><td>Partition</td><td>&nbsp;:&nbsp;</td><td>[PLACEHOLDER_PARITITION]</td></tr>
    <tr><td>JobSource&nbsp;Name</td><td>&nbsp;:&nbsp;</td><td><input type='text' name='jsname'></td></tr>
    <tr><td>Application</td><td>&nbsp;:&nbsp;</td><td>[PLACEHOLDER_APPLICATION]</td></tr>
    <tr><td>Type</td><td>&nbsp;:&nbsp;</td><td>[PLACEHOLDER_JOBTYPE]</td></tr>
    <tr><td>Description</td><td>&nbsp;:&nbsp;</td><td><input type='text' name='jsdesc'></td></tr>
    <tr><td></td><td></td></tr>
    </table>
    <br>
    <table>
    <tr><td>Name&nbsp;Parameter&nbsp;<input type='text' name='parameter1'></td>
    <td>Description&nbsp;<input type='text' name='description1'></td>
    <td>Default&nbsp;Value&nbsp;<input type='text' name='value1'></td></tr>
    <tr><td>Name&nbsp;Parameter&nbsp;<input type='text' name='parameter2'></td>
    <td>Description&nbsp;<input type='text' name='description2'></td>
    <td>Default&nbsp;Value&nbsp;<input type='text' name='value2'></td></tr>
    <tr><td>Name&nbsp;Parameter&nbsp;<input type='text' name='parameter3'></td>
    <td>Description&nbsp;<input type='text' name='description3'></td>
    <td>Default&nbsp;Value&nbsp;<input type='text' name='value3'></td></tr>
    </table>
    <br>
    <table>
    </tr>
    <tr><td>SourceCode</td><td>&nbsp;:&nbsp;</td><td><textarea name='srccode' rows='10' cols='70'></textarea></td></tr>
    </table>	
    <input type='submit' value='Save'>
</form>
</body>
</html>`

var editJobSource = `<html>
<head>
<title></title>
</head>
<body>
<form action="/jobsource/save" method="post">
    <h2>[PLACEHOLDER_PAGETITLE]</h2>
    <input type='hidden' name='jobsourceid' value='[JOBSOURCEID]'>
    <table border=0>
    <tr><td>Partition</td><td>&nbsp;:&nbsp;</td><td>[PLACEHOLDER_PARITITION]</td></tr>
    <tr><td>JobSource&nbsp;Name</td><td>&nbsp;:&nbsp;</td><td><input type='text' name='jsname' value='[PLACEHOLDER_NAME]' size='128' disabled></td></tr>
    <tr><td>Application</td><td>&nbsp;:&nbsp;</td><td>[PLACEHOLDER_APPLICATION]</td></tr>
    <tr><td>Type</td><td>&nbsp;:&nbsp;</td><td>[PLACEHOLDER_JOBTYPE]</td></tr>
    <tr><td>Description</td><td>&nbsp;:&nbsp;</td><td><input type='text' value='[PLACEHOLDER_DESCRIPTION]' name='jsdesc'></td></tr>
    <tr><td></td><td></td></tr>
    </table>
    
    <!--br>
    <table>
    <tr><td>Name&nbsp;Parameter<input type='text' name='parameter1'></td>
    <td>Description<input type='text' name='description1'></td>
    <td>Default&nbsp;Value<input type='text' name='value1'></td>
    </table-->
    
    <br>
    <table>
    </tr>
    <tr><td>SourceCode</td><td>&nbsp;:&nbsp;</td><td><textarea name='srccode' rows='10' cols='70'>[PLACEHOLDER_SOURCECODE]</textarea></td></tr>
    </table>	
    <input type='submit' value='Save'>
</form>
</body>
</html>`

var newApplication = `<html>
<head>
<title></title>
</head>
<body>
<form action="/application/save" method="post">
    <table border=0>
    <tr><td>Application Name</td><td>&nbsp;:&nbsp;</td><td><input type='text' name='appname'></td></tr>
    <tr><td>Application Description</td><td>&nbsp;:&nbsp;</td><td><input type='text' name='appdesc'></td></tr>
    </table>	
    <input type='submit' value='Save'>
</form>
</body>
</html>`

var newWorkerHTML = `<html>
<head>
<title></title>
</head>
<body>
<form action="/worker/save" method="post">
    <table border=0>
    <tr><td>Partition</td><td>&nbsp;:&nbsp;</td><td>[PLACEHOLDER_PARITITION]</td></tr>
    <tr><td>Protocol</td><td>&nbsp;:&nbsp;</td><td><input type='text' name='wrkprot'></td></tr>
    <tr><td>Worker Name</td><td>&nbsp;:&nbsp;</td><td><input type='text' name='wrkname'></td></tr>
    <tr><td>Description</td><td>&nbsp;:&nbsp;</td><td><input type='text' name='wrkdesc'></td></tr>
    <tr><td>Hostname</td><td>&nbsp;:&nbsp;</td><td><input type='text' name='wrkhostname'></td></tr>
    <tr><td>PortNumber</td><td>&nbsp;:&nbsp;</td><td><input type='text' name='wrkportnumber'></td></tr>
    <tr><td>Application</td><td>&nbsp;:&nbsp;</td><td>[PLACEHOLDER_APPLICATION]</td></tr>
    </table>	
    <input type='submit' value='Save'>
</form>
</body>
</html>`

var listWorkerHTML = `<html>
<head>
<title></title>
</head>
<body>
<form action="/worker/do" method="post">
    <table border=0>
    <tr><td>Select Worker</td><td>&nbsp;:&nbsp;</td><td>[PLACEHOLDER_WORKER]</td></tr>
    </table>	
    <input type='submit' name='action' value='start'>
</form>
</body>
</html>`

var statusWorkerHTML = `<html>
<head>
<title></title>
</head>
<body>
<h2>Status Workers</h2>
[PLACEHOLDER_STATUSWORKERS]
</body>
</html>`

func importDialog(w http.ResponseWriter, r *http.Request) {
	io.WriteString(w, importObject)
}

func importObjectDefinition(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()

	xmlObjectDefinition := r.PostFormValue("definition")

	var htmlCode string = ""

	var url string = importobject.Import(xmlObjectDefinition)

	// Wanneer de url niet leeg is, is het object succesvol geimporteerd
	if utils.Trim(url) != "" {
		// Er kan een redirect naar de detailpagina worden gedaan
		htmlCode = htmlCode + "<html><head>"
		htmlCode = htmlCode + "<script type='text/javascript'>"
		htmlCode = htmlCode + "window.location.href = '" + url + "'"
		htmlCode = htmlCode + "</script><body></body></html>"
	}

	io.WriteString(w, htmlCode)
}

// ******************************************************************************
// * Application
// ******************************************************************************
func applicationSave(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	app := application.Application{}
	defer r.Body.Close()

	app.SetName(r.PostFormValue("appname"))
	app.SetDescription(r.PostFormValue("appdesc"))
	_, exitMsg := app.Commit()
	io.WriteString(w, exitMsg)
}

func applicationNew(w http.ResponseWriter, r *http.Request) {
	//tmp := application.GetHTMLListOfApplications()
	io.WriteString(w, newApplication)
}

// ******************************************************************************
// * JobSource
// ******************************************************************************
func jobsourceNew(w http.ResponseWriter, r *http.Request) {
	tmpApp := application.GetHTMLListOfApplications()
	newJobSource = strings.Replace(newJobSource, "[PLACEHOLDER_APPLICATION]", tmpApp, 1)
	tmpPart := partition.GetHTMLListOfPartitions()
	newJobSource = strings.Replace(newJobSource, "[PLACEHOLDER_PARITITION]", tmpPart, 1)
	tmpType := jobtype.GetHTMLListOfJobTypes(1)
	newJobSource = strings.Replace(newJobSource, "[PLACEHOLDER_JOBTYPE]", tmpType, 1)
	newJobSource = strings.Replace(newJobSource, "[PLACEHOLDER_PAGETITLE]", "New JobSource", 1)

	newJobSource = strings.Replace(newJobSource, "[PLACEHOLDER_STYLE]", "<style>"+css+"</style>", 1)

	io.WriteString(w, newJobSource)
}

func jobsourceEdit(w http.ResponseWriter, r *http.Request) {

	// Ophalen van de gegevens van de JobSource
	vars := mux.Vars(r)
	jobSourceFQName := utils.Trim(vars["JobSourceFQName"])
	js := jobsource.JobSource{}
	js.GetJobSourceByFQName(jobSourceFQName)

	tmpApp := application.GetHTMLListOfApplications()
	var htmlCode string = editJobSource
	htmlCode = strings.Replace(htmlCode, "[PLACEHOLDER_PAGETITLE]", "Edit JobSource", 1)
	htmlCode = strings.Replace(htmlCode, "[PLACEHOLDER_APPLICATION]", tmpApp, 1)
	htmlCode = strings.Replace(htmlCode, "[PLACEHOLDER_PARITITION]", js.Partition, 1)
	tmpType := jobtype.GetHTMLListOfJobTypes(js.JobSourceType)
	htmlCode = strings.Replace(htmlCode, "[PLACEHOLDER_JOBTYPE]", tmpType, 1)

	// Invoegen van de JobSourceId om op die manier kenbaar te maken dat een bestaande
	// JobSource wordt bewerkt
	htmlCode = strings.Replace(htmlCode, "[JOBSOURCEID]", strconv.FormatInt(js.Id, 10), 1)

	// Invoegen van de naam van de JobSource
	htmlCode = strings.Replace(htmlCode, "[PLACEHOLDER_NAME]", js.GetFQName(), 1)

	// Invoegen van de SourceCode
	htmlCode = strings.Replace(htmlCode, "[PLACEHOLDER_SOURCECODE]", js.GetSourceCode(), 1)

	// Invoegen van de Description
	htmlCode = strings.Replace(htmlCode, "[PLACEHOLDER_DESCRIPTION]", js.GetDescription(), 1)

	io.WriteString(w, htmlCode)
}

func cacheOverview(w http.ResponseWriter, r *http.Request) {
	var htmlCode string = "<html><head><style>" + css + "</style></head><body>"
	htmlCode = htmlCode + scheduler.GetScheduledJobsCache()
	htmlCode = htmlCode + "</body></html>"
	io.WriteString(w, htmlCode)
}

func jobsourceSave(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()

	tmpPartitionID, _ := strconv.ParseInt(r.PostFormValue("partition"), 10, 64)
	tmpApplicationID, _ := strconv.ParseInt(r.PostFormValue("application"), 10, 64)
	tmpJobType, _ := strconv.Atoi(r.PostFormValue("jobtype"))
	tmpJobSourceId, _ := strconv.ParseInt(r.PostFormValue("jobsourceid"), 10, 64)

	// Initialiseren van een Worker object
	jobsrc := jobsource.JobSource{}

	// Opslaan van een bestaande JobSource
	if tmpJobSourceId > 0 {
		jobsrc.GetJobSourceById(tmpJobSourceId)
		jobsrc.ApplicationId = tmpApplicationID
		jobsrc.JobSourceType = tmpJobType
		jobsrc.SetDescription(r.PostFormValue("jsdesc"))
		jobsrc.SetSourceCode(r.PostFormValue("srccode"))
	}

	// Opslaan van een nieuwe JobSource
	if tmpJobSourceId == 0 {
		jobsrc.Id = 0
		jobsrc.SetJobSourceType(1)
		jobsrc.PartitionId = tmpPartitionID
		jobsrc.ApplicationId = tmpApplicationID
		jobsrc.JobSourceType = tmpJobType
		jobsrc.SetName(r.PostFormValue("jsname"))
		jobsrc.SetDescription(r.PostFormValue("jsdesc"))
		jobsrc.SetSourceCode(r.PostFormValue("srccode"))
	}

	// Overzetten van de parameters. Een parameter wordt alleen overgezet wanneer
	// tenminste de ParameterNaam is ingevuld
	var parameterName string = ""
	for i := 1; i < 4; i++ {
		parameterName = r.PostFormValue("parameter" + strconv.Itoa(i))
		if utils.Trim(parameterName) != "" {
			// Parameter kan worden toegevoegd aan de JobSource
			par := jobsource.JobSourceParameter{}
			par.Name = parameterName
			par.Description = r.PostFormValue("description" + strconv.Itoa(i))
			par.DefaultValue = r.PostFormValue("value" + strconv.Itoa(i))
			jobsrc.AddJobSourceParameter(par)
			logging.DebugMSG("Parameter " + par.Name + " has been added to the JobSource")
		}
	}

	exitCode, exitMsg := jobsrc.Commit()
	tmpJobSourceId = jobsrc.Id

	// Wanneer er een fout is opgetreden, moet de foutmelding worden getoond. Indien
	// de wijzigingen succesvol zijn doorgevoerd, kunnen de JobSource details worden
	// getoond
	if exitCode == 0 {
		var htmlCode string = ""
		htmlCode = htmlCode + "<html><head>"
		htmlCode = htmlCode + "<script type='text/javascript'>"
		htmlCode = htmlCode + "window.location.href = '/jobsource/details/" + strconv.FormatInt(tmpJobSourceId, 10) + "'"
		htmlCode = htmlCode + "</script><body></body></html>"
		io.WriteString(w, htmlCode)
	} else {
		// Tonen van de foutmelding
		io.WriteString(w, exitMsg)
	}

}

func jobsourceDetails(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	jobSourceId, _ := strconv.ParseInt(utils.Trim(vars["JobSourceId"]), 10, 64)

	// Ophalen van de gegevens van de JobSource
	jobSource := jobsource.JobSource{}
	jobSource.GetJobSourceById(jobSourceId)

	var htmlCode string = "<html><head><style>" + css + "</style></head><body>"
	htmlCode = htmlCode + "<h3>JOBSOURCE</h3>"

	htmlCode = htmlCode + "<table>"
	htmlCode = htmlCode + "<tr><td>Name</td><td>" + jobSource.GetFQName() + "</td></tr>"
	htmlCode = htmlCode + "<tr><td>Description</td><td>" + jobSource.GetDescription() + "</td></tr>"
	htmlCode = htmlCode + "<tr><td>Partition</td><td>" + jobSource.Partition + "</td></tr>"
	htmlCode = htmlCode + "<tr><td>Application</td><td>" + jobSource.Application + "</td></tr>"
	htmlCode = htmlCode + "<tr><td>Job&nbsp;Type</td><td>" + jobSource.JobSourceTypeName + "</td></tr>"
	htmlCode = htmlCode + "<tr><td>Created</td><td>" + utils.FormatDate(jobSource.DateCreated) + "</td></tr>"
	htmlCode = htmlCode + "<tr><td>Modified</td><td>" + utils.FormatDate(jobSource.DateModified) + "</td></tr>"
	htmlCode = htmlCode + "</table>"

	// JobSourceParameters
	htmlCode = htmlCode + jobSource.GetJobsourceParametersDetailsHTML()

	htmlCode = htmlCode + "<table><tr><td>Source</td></tr>"
	htmlCode = htmlCode + "<tr><td>" + "<textarea readonly name='definition' rows='19' cols='70'>" + jobSource.GetSourceCode() + "</textarea>" + "</td></tr>"
	htmlCode = htmlCode + "</table>"

	htmlCode = htmlCode + "<table><tr><td>"
	// Toevoegen van een Submit-buttom om de JobSource uit te voeren
	htmlCode = htmlCode + "<form action='/jobsource/submit/" + jobSource.GetFQName() + "'>"
	htmlCode = htmlCode + "<input type='submit' value='Submit'/>"
	htmlCode = htmlCode + "</form>"

	htmlCode = htmlCode + "</td><td>"

	// Toevoegen van een Export-buttom om de JobSource uit te voeren
	htmlCode = htmlCode + "<form action='/jobsource/export/" + jobSource.GetFQName() + "'>"
	htmlCode = htmlCode + "<input type='submit' value='Export'/>"
	htmlCode = htmlCode + "</form>"

	htmlCode = htmlCode + "</td><td>"

	// Toevoegen van een buttom om Jobs te kunnen bekijken
	htmlCode = htmlCode + "<form action='/jobsource/relatedjobs/" + jobSource.GetFQName() + "'>"
	htmlCode = htmlCode + "<input type='submit' value='Jobs'/>"
	htmlCode = htmlCode + "</form>"

	htmlCode = htmlCode + "</td><td>"

	// Toevoegen van een buttom om de JobSource te kunnen editen
	htmlCode = htmlCode + "<form action='/jobsource/edit/" + jobSource.GetFQName() + "'>"
	htmlCode = htmlCode + "<input type='submit' value='Edit'/>"
	htmlCode = htmlCode + "</form>"

	htmlCode = htmlCode + "</td></tr></table>"

	htmlCode = htmlCode + "</body></html>"
	io.WriteString(w, htmlCode)
}

func jobsourceExport(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	jobSourceFQName := utils.Trim(vars["JobSourceFQName"])

	jobSource := jobsource.JobSource{}
	jobSource.GetJobSourceByFQName(jobSourceFQName)
	var exportFileName string = jobSource.Export(settings.ExportDirectory)

	var htmlCode string = "<html><body>"
	htmlCode = htmlCode + "Download JobSource <a href='" + exportFileName + "'>" + jobSourceFQName + "</a>"
	htmlCode = htmlCode + "</body></html>"
	io.WriteString(w, htmlCode)
}

func jobsourceSubmit(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	jobSourceFQName := utils.Trim(vars["JobSourceFQName"])

	// Ophalen van de gegevens van de JobSource
	jobSource := jobsource.JobSource{}
	jobSource.GetJobSourceByFQName(jobSourceFQName)

	var htmlCode string = "<html><head><style>" + css + "</style></head><body>"
	htmlCode = htmlCode + "<h3>SUBMIT " + jobSource.GetFQName() + "</h3>"

	htmlCode = htmlCode + "<form action='/jobsource/start' method='post'>"

	// Ophalen van de lijst met actieve Workers
	var activeWorkers string = worker.GetHTMLListOfWorkersWithStatus(jobstatus.Running)

	// Wanneer er geen actieve workers zijn, dan moet de Start-button gedisabled worden
	var startButtonStatus string = "disabled"
	if utils.Trim(activeWorkers) != "" {
		startButtonStatus = ""
	}

	// Opnemen van de JobSourceFQName in een (hidden field)
	htmlCode = htmlCode + "<input type='" + settings.HtmlHiddenField + "' name='jobsourcefqname' value='" + jobSourceFQName + "'>"

	htmlCode = htmlCode + "<table>"
	htmlCode = htmlCode + "<tr><td>Select Worker</td><td>"
	htmlCode = htmlCode + activeWorkers
	htmlCode = htmlCode + "</td></tr>"
	htmlCode = htmlCode + "<tr><td>Start time</td><td>"
	htmlCode = htmlCode + "<input type='text' name='startdate' value='" + utils.CurrentTimeStamp(constanten.DateFormatLong) + "'>"
	htmlCode = htmlCode + "</td></tr>"
	htmlCode = htmlCode + "</table>"

	// Bepalen of er ook nog parameters zijn, die getoond moeten worden
	if len(jobSource.JobSourceParameter) > 0 {
		htmlCode = htmlCode + "<table>"
		htmlCode = htmlCode + "<tr><th>Parameter</th><th>Value</th></tr>"
		for i := 0; i < len(jobSource.JobSourceParameter); i++ {
			var parameterName string = jobSource.JobSourceParameter[i].Name
			htmlCode = htmlCode + "<tr><td>" + parameterName + "</td><td>"
			htmlCode = htmlCode + "<input type='text' name='" + parameterName + "' value='" + jobSource.JobSourceParameter[i].DefaultValue + "'>"
			htmlCode = htmlCode + "</td></tr>"
		}
		htmlCode = htmlCode + "</table>"
	}

	htmlCode = htmlCode + "<input type='submit' " + startButtonStatus + " value='Start'/>"
	htmlCode = htmlCode + "</form>"

	htmlCode = htmlCode + "</body></html>"
	io.WriteString(w, htmlCode)
}

func jobsourceStartJob(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	workerId := r.PostFormValue("workerid")
	startTimeJob := r.PostFormValue("startdate")
	jobSourceFQName := r.PostFormValue("jobsourcefqname")

	//vars := mux.Vars(r)
	//jobSourceId,_ := strconv.ParseInt(utils.Trim(vars["JobSourceId"]), 10, 64)

	// Uitlezen van de Worker die is geselecteerd
	//workerId, _ := strconv.ParseInt(r.PostFormValue("workerid"),10, 64)
	//jobSourceId, _ := strconv.ParseInt(r.PostFormValue("jobsourceid"),10, 64)
	//jobSourceId, _ := strconv.ParseInt(utils.Trim(vars["jobsourceid"]), 10, 64)
	//workerId, _ := strconv.ParseInt(utils.Trim(vars["workerid"]), 10, 64)

	//io.WriteString(w, "Dit gaan we doen: JobSourceId=" + strconv.FormatInt(workerId,10) + "  WorkerId="  + strconv.FormatInt(workerId,10) )
	//io.WriteString(w, "Dit gaan we doen: JobSource=" + jobSourceFQName + "  WorkerId="  + workerId)

	js := jobsource.JobSource{}
	js.GetJobSourceByFQName(jobSourceFQName)

	j := job.Job{}

	// Instellen van de Worker waarop de Job moet worden uitgevoerd
	j.WorkerId, _ = strconv.ParseInt(workerId, 10, 64)

	// Instellen van de Start datum/tijd van de job
	j.RunStart = startTimeJob

	j.Prepare(js)
	logging.DebugMSG("Job prepared")

	// Wanneer de StartDatum van de job in de toekomst ligt, moet deze niet direct gestart worden
	//var runJobImmediate bool = false
	//if startTimeJob <= utils.CurrentTimeStamp(constanten.DateFormatLong) {
	//	runJobImmediate = true
	//}

	// Opnemen van de waarden van de ingevoerde parameters
	// Uitlezen van eventuele JobSourceParameters
	for i := 0; i < len(js.JobSourceParameter); i++ {
		var parameterValue string = r.PostFormValue(js.JobSourceParameter[i].Name)
		logging.DebugMSG("Updating runtime value of JobParameter " + js.JobSourceParameter[i].Name + " with value '" + parameterValue + "'")
		job.UpdateJobParameter(j.JobId, js.JobSourceParameter[i].Name, parameterValue)
	}

	// if runJobImmediate == true {
	// 	  j.Start()
	//}

	var htmlCode string = ""
	htmlCode = htmlCode + "<html><head>"
	htmlCode = htmlCode + "<script type='text/javascript'>"
	htmlCode = htmlCode + "window.location.href = '/job/details/" + strconv.FormatInt(j.GetJobId(), 10) + "'"
	htmlCode = htmlCode + "</script><body></body></html>"
	io.WriteString(w, htmlCode)
}

func jobsourceRelatedJobs(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	jobSourceFQName := utils.Trim(vars["JobSourceFQName"])

	// Ophalen van de gegevens van de JobSource
	jobSource := jobsource.JobSource{}
	jobSource.GetJobSourceByFQName(jobSourceFQName)

	var htmlCode string = ""
	htmlCode = htmlCode + "<html><head><style>" + css + "</style></head><body>"
	htmlCode = htmlCode + "<H3>RELATED JOBS " + jobSourceFQName

	htmlCode = htmlCode + jobSource.GetRelatedJobs()

	htmlCode = htmlCode + "</body></html>"
	io.WriteString(w, htmlCode)
}

func jobsourceOverview(w http.ResponseWriter, r *http.Request) {

	logging.DebugMSG("HTML Overview JobSource")

	var htmlCode string = ""
	htmlCode = htmlCode + "<html><head><style>" + css + "</style></head><body>"
	htmlCode = htmlCode + "<H3>OVERVIEW JOBSOURCE "

	htmlCode = htmlCode + jobsource.GetJobSourcesByFilter("")

	htmlCode = htmlCode + "</body></html>"
	io.WriteString(w, htmlCode)
}

// ******************************************************************************
// * Worker
// ******************************************************************************
func workerNew(w http.ResponseWriter, r *http.Request) {
	tmpApp := application.GetHTMLListOfApplications()
	newWorkerHTML = strings.Replace(newWorkerHTML, "[PLACEHOLDER_APPLICATION]", tmpApp, 1)
	tmpPart := partition.GetHTMLListOfPartitions()
	newWorkerHTML = strings.Replace(newWorkerHTML, "[PLACEHOLDER_PARITITION]", tmpPart, 1)
	io.WriteString(w, newWorkerHTML)
}

func workerList(w http.ResponseWriter, r *http.Request) {
	tmpWrk := worker.GetHTMLListOfWorkers()
	listWorkerHTML = strings.Replace(listWorkerHTML, "[PLACEHOLDER_WORKER]", tmpWrk, 1)

	io.WriteString(w, listWorkerHTML)
}

func workerDo(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()

	// Bepalen welke actie er moet worden uitgevoerd
	action := r.PostFormValue("action")

	// Er kunnen verschillende acties vanuit het scherm worden uitgevoerd. De
	// aangeroepen actie is beschikbaar in de variabele action

	// Starten van een Worker
	if action == "start" {
		tmpWorkerID, _ := strconv.ParseInt(r.PostFormValue("worker"), 10, 64)
		wrk := worker.Worker{}
		wrk.GetWorkerById(tmpWorkerID)
		wrk.DumpObject()
		wrk.Start()
		io.WriteString(w, "Starting Worker")
	}

}

func workerSave(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()

	// Initialiseren van een Worker object
	wrk := worker.Worker{}
	wrk.SetProtocol(r.PostFormValue("wrkprot"))
	wrk.SetName(r.PostFormValue("wrkname"))
	wrk.SetDescription(r.PostFormValue("wrkdesc"))

	tmpPartitionID, _ := strconv.ParseInt(r.PostFormValue("partition"), 10, 64)
	wrk.SetPartitionId(tmpPartitionID)

	tmpApplicationID, _ := strconv.ParseInt(r.PostFormValue("application"), 10, 64)
	wrk.SetApplicationId(tmpApplicationID)

	_, exitMsg := wrk.Commit()
	io.WriteString(w, exitMsg)

}

func workerStatus(w http.ResponseWriter, r *http.Request) {
	tmpWrk := worker.GetHTMLStatusWorkers()

	listWorkerHTML = strings.Replace(statusWorkerHTML, "[PLACEHOLDER_STATUSWORKERS]", tmpWrk, 1)

	var htmlCode string = ""
	htmlCode = htmlCode + "<html><head><style>" + css + "</style></head><body>"
	htmlCode = htmlCode + listWorkerHTML
	htmlCode = htmlCode + "</body></html>"
	io.WriteString(w, htmlCode)
}

func workerDetails(w http.ResponseWriter, r *http.Request) {

	var htmlCode string = "<html><head><style>" + css + "</style></head><body>"

	vars := mux.Vars(r)
	WorkerFQName := utils.Trim(vars["WorkerFQName"])

	wrk := worker.Worker{}
	wrk.GetWorkerByFQName(WorkerFQName)

	htmlCode = htmlCode + "<h3>DETAIL WORKER " + wrk.GetFQName()
	htmlCode = htmlCode + "<table>"
	htmlCode = htmlCode + "<tr><td>Name</td><td>" + wrk.GetFQName() + "</td></tr>"
	htmlCode = htmlCode + "<tr><td>Description</td><td>" + wrk.GetDescription() + "</td></tr>"
	htmlCode = htmlCode + "<tr><td>Status</td><td>" + wrk.GetActualStatus() + "</td></tr>"
	htmlCode = htmlCode + "<tr><td>Hostname</td><td>" + wrk.GetHostName() + "</td></tr>"
	htmlCode = htmlCode + "<tr><td>ESB&nbsp;Queue</td><td>" + wrk.QueueName + "</td></tr>"
	htmlCode = htmlCode + "<tr><td>Started</td><td>" + utils.FormatDate(wrk.StartTime) + "</td></tr>"
	htmlCode = htmlCode + "<tr><td>AutoStart</td><td>" + wrk.GetAutoStart() + "</td></tr>"
	htmlCode = htmlCode + "<tr><td>&nbsp;</td><td>" + "&nbsp;" + "</td></tr>"
	htmlCode = htmlCode + "<tr><td><b>Software</b></td><td>" + "&nbsp;" + "</td></tr>"
	htmlCode = htmlCode + "<tr><td>Software&nbsp;Version</td><td>" + wrk.SoftwareVersion + "</td></tr>"
	htmlCode = htmlCode + "<tr><td>Software&nbsp;Date</td><td>" + wrk.SoftwareDate + "</td></tr>"
	htmlCode = htmlCode + "<tr><td>&nbsp;</td><td>" + "&nbsp;" + "</td></tr>"
	htmlCode = htmlCode + "<tr><td><b>Configuration</b></td><td>" + "&nbsp;" + "</td></tr>"
	htmlCode = htmlCode + "<tr><td>Platform</td><td>" + wrk.GetPlatform() + "</td></tr>"
	htmlCode = htmlCode + "<tr><td>Platform&nbsp;Family</td><td>" + wrk.PlatformFamily + "</td></tr>"
	htmlCode = htmlCode + "<tr><td>Platform&nbsp;Version</td><td>" + wrk.PlatformVersion + "</td></tr>"
	htmlCode = htmlCode + "<tr><td>Kernel&nbsp;Version</td><td>" + wrk.KernelVersion + "</td></tr>"
	htmlCode = htmlCode + "<tr><td>Virtualization&nbsp;System</td><td>" + wrk.VirtSystem + "</td></tr>"
	htmlCode = htmlCode + "<tr><td>Virtualization&nbsp;Role</td><td>" + wrk.VirtRole + "</td></tr>"
	htmlCode = htmlCode + "</table>"

	// Buttons opnemen voor acties uit te voeren op de Worker
	// Shutdown => wanneer status Running, Hold of Connecting
	var wrkStatus = wrk.GetActualStatus()
	if wrkStatus == workerstatus.Running || wrkStatus == workerstatus.Connecting || wrkStatus == workerstatus.Hold || wrkStatus == workerstatus.Overload {
		htmlCode = htmlCode + "<form action='/worker/shutdown/" + wrk.GetFQName() + "'>"
		htmlCode = htmlCode + "<input type='submit' value='Shutdown'/>"
		htmlCode = htmlCode + "</form>"
	}

	if wrkStatus == workerstatus.Down {
		htmlCode = htmlCode + "<form action='/worker/startup/" + wrk.GetFQName() + "'>"
		htmlCode = htmlCode + "<input type='submit' value='Start'/>"
		htmlCode = htmlCode + "</form>"
	}

	htmlCode = htmlCode + "</body></html>"
	io.WriteString(w, htmlCode)

}

func workerShutdown(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	WorkerFQName := utils.Trim(vars["WorkerFQName"])
	wrk := worker.Worker{}
	wrk.GetWorkerByFQName(WorkerFQName)
	wrk.Stop()

	// Redirect naar de Worker/details
	var htmlCode string = ""
	htmlCode = htmlCode + "<html><head>"
	htmlCode = htmlCode + "<script type='text/javascript'>"
	htmlCode = htmlCode + "window.location.href = '/worker/details/" + WorkerFQName + "'"
	htmlCode = htmlCode + "</script><body></body></html>"
	io.WriteString(w, htmlCode)
}

func workerStartup(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	WorkerFQName := utils.Trim(vars["WorkerFQName"])
	wrk := worker.Worker{}
	wrk.GetWorkerByFQName(WorkerFQName)
	wrk.Start()

	// Redirect naar de Worker/details
	var htmlCode string = ""
	htmlCode = htmlCode + "<html><head>"
	htmlCode = htmlCode + "<script type='text/javascript'>"
	htmlCode = htmlCode + "window.location.href = '/worker/details/" + WorkerFQName + "'"
	htmlCode = htmlCode + "</script><body></body></html>"
	io.WriteString(w, htmlCode)
}

// ***************************************************************************
// * jobDetails {JobId}
// * Methode die de eigenschappen van een Job ophaalt op basis van de
// * meegegeven JobId
// ***************************************************************************
func jobDetails(w http.ResponseWriter, r *http.Request) {

	var htmlCode string = "<html><head><style>" + css + "</style></head><body>"

	vars := mux.Vars(r)
	jobId, _ := strconv.ParseInt(utils.Trim(vars["JobId"]), 10, 64)

	job := job.Job{}
	job.GetJobByJobId(jobId)

	// Ophalen van de FQname van de JobSource
	jobSource := jobsource.JobSource{}
	jobSource.GetJobSourceById(job.JobSourceId)
	htmlCode = htmlCode + "<h3>JOBDETAILS " + jobSource.GetFQName() + "</h3>"
	htmlCode = htmlCode + "<table>"
	htmlCode = htmlCode + "<tr><td>JobId</td><td>" + strconv.FormatInt(job.JobId, 10) + "</td></tr>"
	htmlCode = htmlCode + "<tr><td>Definition</td><td><a href='/jobsource/details/" + strconv.FormatInt(job.JobSourceId, 10) + "'>" + jobSource.FQName + "</a></td></tr>"
	htmlCode = htmlCode + "<tr><td>Worker</td><td>" + job.WorkerName + "</td></tr>"
	htmlCode = htmlCode + "<tr><td>Start</td><td>" + utils.FormatDate(job.RunStart) + "</td></tr>"
	htmlCode = htmlCode + "<tr><td>End</td><td>" + utils.FormatDate(job.RunEnd) + "</td></tr>"

	// Wanneer het tweede teken (index=1) een punt is, dan heeft de Job minder dan 1 seconden gelopen
	// In dat geval wordt de tijd in 1000ste van secondes getoond
	//tmp := []byte(job.ElapsedTime)
	// if string(tmp[1]) == "." {
	//        job.ElapsedTime = string(tmp[0]) + string(tmp[1]) + string(tmp[2]) + string(tmp[3]) + string(tmp[4])
	//   }

	// De Exitcode alleen weergeven wanneer de status Completed of Error of Killed is
	var statusSuffix string = " "
	if job.Status == jobstatus.Completed || job.Status == jobstatus.Error || job.Status == jobstatus.Killed {
		statusSuffix = " (" + job.ExitCode + ")"
	}

	htmlCode = htmlCode + "<tr><td>Remote&nbsp;Execution&nbsp;Time</td><td>" + job.ElapsedTime + " Secs</td></tr>"
	htmlCode = htmlCode + "<tr><td>Status</td><td>" + jobstatus.GetStatusWithCSS(job.Status) + statusSuffix + "</td></tr>"
	htmlCode = htmlCode + "</table>"

	// Opnemen van de parameters waarmee gedraaid is
	htmlCode = htmlCode + job.GetJobParametersHTML()

	// Opnemen van de JobFiles
	htmlCode = htmlCode + job.GetJobFilesHTML()

	// Ophalen van de ExecutionLog

	htmlCode = htmlCode + "<br>Execution Log:<br>"
	htmlCode = htmlCode + "<textarea readonly name='definition' rows='7' cols='70'>" + job.GetExecutionLog() + "</textarea>"

	htmlCode = htmlCode + "</body></html>"
	io.WriteString(w, htmlCode)

}

// ***************************************************************************
// * Test procedure voor het opstarten van een Job
// ***************************************************************************
func jobStart(w http.ResponseWriter, r *http.Request) {

	js := jobsource.JobSource{}
	js.GetJobSourceByFQName("PUBLIC.TEST")
	job := job.Job{}
	job.Prepare(js)
	io.WriteString(w, "Job "+strconv.FormatInt(job.GetJobId(), 10)+" prepared")
	job.Start()

}

func jobFileShow(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	jobfileId, _ := strconv.ParseInt(utils.Trim(vars["JobFileId"]), 10, 64)

	fileContent := job.GetJobFileContentById(jobfileId)

	io.WriteString(w, "<html><body><pre>"+fileContent+"</pre></body></html>")

}

func test(w http.ResponseWriter, r *http.Request) {

	var htmlCode string = "<html><head><style>" + css + "</style></head><body>"
	htmlCode = htmlCode + "<h3>TestProcedure</h3>"

	//	js := jobsource.JobSource{}
	//	js.GetJobSourceByFQName("PUBLIC.WAIT")
	//	htmlCode = htmlCode + "Jobsource=" + js.FQName

	//	par := jobsource.JobSourceParameter{}
	//	par.Name = "P_WAITTIME_IN_SEC"
	//	par.Description = "Waiting time in seconds"
	//	par.DefaultValue = "10"
	//	js.AddJobSourceParameter(par)

	//	par1 := jobsource.JobSourceParameter{}
	//	par1.Name = "P_TEST"
	//	par1.Description = "Test parameter"
	//	par1.DefaultValue = "time.Now()"
	//js.AddJobSourceParameter(par1)

	// js.JobSourceParameter = append(js.JobSourceParameter, js.JobSourceParameter.Name, "P1")
	//js.JobSourceParameter.Description = "D1"
	//js.JobSourceParameter.DefaultValue = "V1"
	//js.Export("/tmp")

	//	scheduler.RebuildScheduledJobs = true
	io.WriteString(w, htmlCode)
}

// *************************************************************************
// * appStart
// * Tonen van het beginscherm van de applicatie
// *************************************************************************
func appStart(w http.ResponseWriter, r *http.Request) {

	var htmlCode string = "<html><head></head><body>"
	htmlCode = htmlCode + "<center><h1>Sched 2.0</h1></center>"

	htmlCode = htmlCode + "<table>"
	htmlCode = htmlCode + "<tr><td><a href='/jobsource'>Overzicht JobSources</a></td><td><a href='/import'>Importeren Object</a></td></tr>"
	htmlCode = htmlCode + "<tr><td><a href='/jobsource/new'>Nieuwe JobSources</a></td><td></td></tr>"
	htmlCode = htmlCode + "<tr><td>&nbsp;</td><td>&nbsp;</td></tr>"
	htmlCode = htmlCode + "<tr><td><a href='/worker/status'>Status Workers</a></td><td><a href='/scheduler'>Scheduler Cache</a></td></tr>"
	htmlCode = htmlCode + "</table>"

	io.WriteString(w, htmlCode)
}

func Start() {

	//var err error
	//repository.DBConnection, err = sql.Open(constanten.RepositoryType, constanten.RepositoryConnectString)
	//if err != nil {
	//    fmt.Println("Gaat fout")
	//}

	//ttp.HandleFunc("/jobsource/new", jobsourceNew)
	//http.HandleFunc("/jobsource/save", jobsourceSave)
	//http.HandleFunc("/application/new", applicationNew)
	//http.HandleFunc("/application/save", applicationSave)

	//http.HandleFunc("/job/start/{todoId}", jobStart)
	//http.HandleFunc("/job/start", jobStart)

	//http.HandleFunc("/worker/new", workerNew)
	//http.HandleFunc("/worker/save", workerSave)
	//http.HandleFunc("/worker/status", workerStatus)

	//http.HandleFunc("/worker/list", workerList)
	//http.HandleFunc("/worker/do", workerDo)

	//http.ListenAndServe(":8080", nil)

	router := mux.NewRouter().StrictSlash(true)

	router.HandleFunc("/", appStart)

	router.HandleFunc("/job/start", jobStart)
	router.HandleFunc("/jobfile/{JobFileId}", jobFileShow)
	router.HandleFunc("/job/details/{JobId}", jobDetails)

	router.HandleFunc("/jobsource", jobsourceOverview)
	router.HandleFunc("/jobsource/new", jobsourceNew)
	router.HandleFunc("/jobsource/save", jobsourceSave)
	router.HandleFunc("/jobsource/submit/{JobSourceFQName}", jobsourceSubmit)
	router.HandleFunc("/jobsource/export/{JobSourceFQName}", jobsourceExport)
	router.HandleFunc("/jobsource/edit/{JobSourceFQName}", jobsourceEdit)
	router.HandleFunc("/jobsource/start", jobsourceStartJob)
	router.HandleFunc("/jobsource/relatedjobs/{JobSourceFQName}", jobsourceRelatedJobs)
	router.HandleFunc("/jobsource/details/{JobSourceId}", jobsourceDetails)

	router.HandleFunc("/import/object", importObjectDefinition)
	router.HandleFunc("/import", importDialog)

	router.HandleFunc("/worker/status", workerStatus)
	router.HandleFunc("/worker/details/{WorkerFQName}", workerDetails)
	router.HandleFunc("/worker/shutdown/{WorkerFQName}", workerShutdown)
	router.HandleFunc("/worker/startup/{WorkerFQName}", workerStartup)

	router.HandleFunc("/scheduler", cacheOverview)

	// Route om zaken te kunnen testen
	router.HandleFunc("/test", test)

	log.Fatal(http.ListenAndServe(":8080", router))
}
