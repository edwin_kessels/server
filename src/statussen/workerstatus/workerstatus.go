package workerstatus

var Created = "Created"
var Running = "Running"
var Connecting = "Connecting"
var Down = "Shutdown"
var Standby = "Standby"
var Hold = "Hold"
var Overload = "Overloaded"
var NotAvailable = "N/A"

