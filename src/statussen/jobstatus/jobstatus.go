package jobstatus

var Created = "Created"
var Prepared = "Prepared"
var Scheduled = "Scheduled"
var Dispatched = "Dispatched"
var Running = "Running"
var Standby = "Standby" //Situatie wanneer de Local Worker gestart is, maar er voor de Worker geen AutoStart is ingesteld

// Eindstatussen
var Completed = "Completed"
var Error = "Error"
var Killed = "Killed"

// *************************************************************
// * GetStatusWithCSS
// * Methode om de status inclusief CSS op te vragen
// *************************************************************
func GetStatusWithCSS(inStatus string) string {

	var cssCode string = ""
	var statusColor string = "black"

	if inStatus == Completed {
		statusColor = "green"
	}
	if inStatus == Error {
		statusColor = "red"
	}

	if inStatus == Killed {
		statusColor = "purple"
	}

	if inStatus == Scheduled {
		statusColor = "orange"
	}

	cssCode = "<font color='" + statusColor + "'>" + inStatus + "</font>"
	return cssCode
}
