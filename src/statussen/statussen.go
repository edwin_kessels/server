package statussen

// Definitie van Status-constanten
var Worker_Connecting = "Connecting"
var Worker_Running = "Running"
var Worker_Stopped = "Stopped"


// Statussen van een Job
var Job_Scheduled = "Scheduled"
var Job_Running = "Running"
var Job_Completed = "Completed"
var Job_Error = "Error"
var Job_Killed = "Killed"