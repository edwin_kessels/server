package main 

import "fmt"
import "flag"
import "strings"
import "berichten"
import "esb"
import "constanten"
import "utils"
import "encoding/xml"


var guiRequestQueue = "GUI_Request"

// Procedure voor het genereren van het bericht met de eigenschappen van de Worker
// die moet worden aangemaakt. Wanneer er geen naam is opgegeven, wordt de naam
// samengesteld voor de hostname en portnumber te concattineren, gescheiden door een 
// dubbele punt
func createWorker(hostname string, portnumber string, name string, autoStart bool) int {
    
    exitCode := 0 
    
    if name == "" {
        name = hostname + ":" + portnumber 
    }
    
    tmpAutoStart := constanten.No 
    if autoStart == true {
       tmpAutoStart = constanten.Yes
    }
    
    var tmpXML berichten.CreateWorker
	tmpXML.Name        = name 
	tmpXML.Hostname    = hostname
	tmpXML.PortNumber  = portnumber
	tmpXML.AutoStart   = tmpAutoStart
	tmpXML.Partition   = 1 
	tmpXML.Application = 1 
	tmpXML.CheckSum    = constanten.XMLPlaceHolderChecksum
    tmpXML.TimeStamp   = utils.Now()
    tmpXML.Version     = berichten.CreateWorkerVersion
    tmpXML.UserID      = 0 
	output, _ := xml.MarshalIndent(tmpXML, "", "\t")
	msg := string(output)
    
    // versturen van het bericht naar de CentralInstance
    // Het bericht wordt synchroon verstuurd; er wordt gewacht op een response
    esb.SendMessageESBWithoutWait(msg, guiRequestQueue, "")
    fmt.Println("Message send to CentralInstance")
    
    return exitCode 
}


func deleteWorker(hostname string, portnumber string) int {
    
    exitCode := 0 
    
    var tmpXML berichten.DeleteWorker
	tmpXML.Hostname    = hostname
	tmpXML.PortNumber  = portnumber
	tmpXML.CheckSum    = constanten.XMLPlaceHolderChecksum
    tmpXML.TimeStamp   = utils.Now()
    tmpXML.Version     = berichten.DeleteWorkerVersion
	output, _ := xml.MarshalIndent(tmpXML, "", "\t")
	msg := string(output)
    
    // versturen van het bericht naar de CentralInstance
    // Het bericht wordt synchroon verstuurd; er wordt gewacht op een response
    esb.SendMessageESBWithoutWait(msg, guiRequestQueue, "")
    fmt.Println("Message send to CentralInstance")
    
    return exitCode 
}


func stopWorker(hostname string, portnumber string) int {
    
    exitCode := 0 
    
    var tmpXML berichten.StopWorker
	tmpXML.Hostname    = hostname
	tmpXML.PortNumber  = portnumber
	tmpXML.CheckSum    = constanten.XMLPlaceHolderChecksum
    tmpXML.TimeStamp   = utils.Now()
    tmpXML.Version     = berichten.StopWorkerVersion
	output, _ := xml.MarshalIndent(tmpXML, "", "\t")
	msg := string(output)
    
    // versturen van het bericht naar de CentralInstance
    // Het bericht wordt synchroon verstuurd; er wordt gewacht op een response
    esb.SendMessageESBWithoutWait(msg, guiRequestQueue, "")
    fmt.Println("Message send to CentralInstance")
    
    return exitCode 
}

func startWorker(hostname string, portnumber string) int {
    
    exitCode := 0 
    
    var tmpXML berichten.StartWorker
	tmpXML.Hostname    = hostname
	tmpXML.PortNumber  = portnumber
	tmpXML.CheckSum    = constanten.XMLPlaceHolderChecksum
    tmpXML.TimeStamp   = utils.Now()
    tmpXML.Version     = berichten.StopWorkerVersion
	output, _ := xml.MarshalIndent(tmpXML, "", "\t")
	msg := string(output)
    
    // versturen van het bericht naar de CentralInstance
    // Het bericht wordt synchroon verstuurd; er wordt gewacht op een response
    esb.SendMessageESBWithoutWait(msg, guiRequestQueue, "")
    fmt.Println("Message send to CentralInstance")
    
    return exitCode 
}



func main() {
    
    parName := flag.String("name", "", "Name of the Worker")
    parHostname := flag.String("hostname", "localhost", "IP-Address of Hostname")
    parPortnumber := flag.String("portnumber", "8765", "PortNumber of listener")
    parAction := flag.String("action", "", "Action")
    parAutostart := flag.Bool("autostart", false, "Automatic start Worker")
    
    flag.Parse()
    fmt.Println("action:", *parAction)
    fmt.Println("name:", *parName)
    fmt.Println("hostname:", *parHostname)
    fmt.Println("portnumber:", *parPortnumber)
    fmt.Println("autostart:", *parAutostart)
    
    if *parAction  == strings.ToLower("create") {
        fmt.Println("Creating Worker") 
        exitCode := createWorker(*parHostname, *parPortnumber, *parName, *parAutostart)
        fmt.Println(exitCode)
    }
    
    if *parAction  == strings.ToLower("delete") {
        fmt.Println("Deleting Worker") 
        exitCode := deleteWorker(*parHostname, *parPortnumber)
        fmt.Println(exitCode)
    }
    
    if *parAction  == strings.ToLower("stop") {
        fmt.Println("Stopping Worker") 
        exitCode := stopWorker(*parHostname, *parPortnumber)
        fmt.Println(exitCode)
    }
    
    
     if *parAction  == strings.ToLower("start") {
        fmt.Println("Starting Worker") 
        exitCode := startWorker(*parHostname, *parPortnumber)
        fmt.Println(exitCode)
    }
    
}
