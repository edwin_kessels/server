package main 

import "fmt"
import "flag"
import "strings"
import "berichten"
import "esb"
import "constanten"
import "utils"
import "encoding/xml"


var guiRequestQueue = "GUI_Request"

var userId = 10 


func createPartition(name string, description string, isDefault bool, isInternal bool) int {
    
    exitCode := 0 
    
    tmpDefault := constanten.No 
    if isDefault == true {
       tmpDefault = constanten.Yes
    }
    
    tmpInternal := constanten.No 
    if isInternal == true {
       tmpInternal = constanten.Yes
    }
    
    if utils.Trim(description) == "" {
        description = constanten.PartitionObjNameEV + " " + name 
    }
    
    var tmpXML berichten.CreatePartition
	tmpXML.Name                 = name 
	tmpXML.Description          = description
	tmpXML.DefaultPartition     = tmpDefault
	tmpXML.InternalPartition    = tmpInternal
	tmpXML.CheckSum             = constanten.XMLPlaceHolderChecksum
    tmpXML.TimeStamp            = utils.Now()
    tmpXML.Version              = berichten.CreatePartitionVersion
    tmpXML.UserID               = userId
	output, _ := xml.MarshalIndent(tmpXML, "", "\t")
	msg := string(output)
    
    // versturen van het bericht naar de CentralInstance
    // Het bericht wordt synchroon verstuurd; er wordt gewacht op een response
    esb.SendMessageESBWithoutWait(msg, guiRequestQueue, "")
    fmt.Println("Message send to CentralInstance")
    
    return exitCode 
}


func deletePartition(name string) int {
    
    exitCode := 0 
    
    var tmpXML berichten.DeletePartition
	tmpXML.Name                 = name 
	tmpXML.CheckSum             = constanten.XMLPlaceHolderChecksum
    tmpXML.TimeStamp            = utils.Now()
    tmpXML.Version              = berichten.CreatePartitionVersion
    tmpXML.UserID               = userId
	output, _ := xml.MarshalIndent(tmpXML, "", "\t")
	msg := string(output)
    
    // versturen van het bericht naar de CentralInstance
    // Het bericht wordt synchroon verstuurd; er wordt gewacht op een response
    esb.SendMessageESBWithoutWait(msg, guiRequestQueue, "")
    fmt.Println("Message send to CentralInstance")
    
    return exitCode 
}



func modifyPartition(name string, description string, isDefault bool, isInternal bool) int {
    
    exitCode := 0 
    
    tmpDefault := constanten.No 
    if isDefault == true {
       tmpDefault = constanten.Yes
    }
    
    tmpInternal := constanten.No 
    if isInternal == true {
       tmpInternal = constanten.Yes
    }
    
    if utils.Trim(description) == "" {
        description = constanten.PartitionObjNameEV + " " + name 
    }
    
    var tmpXML berichten.ModifyPartition
	tmpXML.Name                 = name 
	tmpXML.Description          = description
	tmpXML.DefaultPartition     = tmpDefault
	tmpXML.InternalPartition    = tmpInternal
	tmpXML.CheckSum             = constanten.XMLPlaceHolderChecksum
    tmpXML.TimeStamp            = utils.Now()
    tmpXML.Version              = berichten.CreatePartitionVersion
    tmpXML.UserID               = userId
	output, _ := xml.MarshalIndent(tmpXML, "", "\t")
	msg := string(output)
    
    // versturen van het bericht naar de CentralInstance
    // Het bericht wordt synchroon verstuurd; er wordt gewacht op een response
    esb.SendMessageESBWithoutWait(msg, guiRequestQueue, "")
    fmt.Println("Message send to CentralInstance")
    
    return exitCode 
    
    
}




func main() {
    
    parAction := flag.String("action", "", "Action")
    parName := flag.String("name", "", "Name of the " + constanten.PartitionObjNameEV)
    parDescription := flag.String("description", "", "Description of the " + constanten.PartitionObjNameEV)
    parDefault := flag.Bool("default", false, "Mark " + constanten.PartitionObjNameEV + " as default for creating objects")
    parInternal := flag.Bool("internal", false, "Mark " + constanten.PartitionObjNameEV + " as internal")
    
    flag.Parse()
    fmt.Println("action:", *parAction)
    fmt.Println("name:", *parName)
    fmt.Println("description:", *parDescription)
    fmt.Println("default:", *parDefault)
    fmt.Println("internal:", *parInternal)
    
    if *parAction  == strings.ToLower("create") {
        fmt.Println("Creating " + constanten.PartitionObjNameEV) 
        exitCode := createPartition(*parName, *parDescription, *parDefault,*parInternal)
        fmt.Println(exitCode)
    }
    
    if *parAction  == strings.ToLower("delete") {
        fmt.Println("Deleting " + constanten.PartitionObjNameEV) 
        exitCode := deletePartition(*parName)
        fmt.Println(exitCode)
    }
    
    if *parAction  == strings.ToLower("modify") {
        fmt.Println("Modifying " + constanten.PartitionObjNameEV) 
        exitCode := modifyPartition(*parName, *parDescription, *parDefault,*parInternal)
        fmt.Println(exitCode)
    }    
    

    
}
