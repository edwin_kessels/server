package platform

import "runtime"
import "strings"

var Unknown = "Unknown"
var Windows = "Windows"
var Linux   = "Linux"

func GetPlatform() string {
    
    detectedPlatform := Unknown
    
    
	if strings.ToLower(runtime.GOOS) == "windows" {
		detectedPlatform = Windows
	}
	if strings.ToLower(runtime.GOOS) == "linux" {
		detectedPlatform = Linux
	}
    
    return detectedPlatform
}
