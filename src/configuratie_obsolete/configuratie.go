package configuratie

import "io/ioutil"
import "os"
import "logging"
import "strings"
import "encoding/xml"
import "utils"

// DefaultPortNumber voor de Worker
// Deze poort wordt gebruikt om te luisteren naar binnenkomende http-requests
var WorkerDefaultPortNumber = "8082"

// Type dat gebruikt wordt om de configuratie van een HUB lokaal op te slaan
type localHubConfiguration struct {
	HubID       string
	PortNumber  string
	ESBHost     string
	ESBUser     string
	ESBPassword string
	Environment string
}

// Type dat gebruikt wordt om de configuratie van een Worker lokaal op te slaan
type localWorkerConfiguration struct {
	WorkerID   string
	PortNumber string
}



// Procedure om een configuratie veilig te stellen en vervolgens het originele
// bestand te verwijderen
func resetConfigFile(cfgFileName string) {

	logging.DebugMSG("Reset Configuration file '" + cfgFileName + "'")
	// Controleren of het bestand bestaat om dit veilig te stellen
	if utils.FileExists(cfgFileName) {
		backupConfiguration(cfgFileName)
	}

	// Verwijderen van het originele bestand
	os.Remove(cfgFileName)
	logging.DebugMSG("Configuration file deleted")
}

// Procedure die bepaald of een bepaalde Configuratiebestand bestaat
// Dit is een wrapper voor de aanroep van een 'interne' procedure
func configFileExists(cfgFileName string) bool {
	fileExists := utils.FileExists(cfgFileName)
	return (fileExists)
}

// Procedure om de configuratie van een Worker naar de configuratie file weg te schrijven
func WriteWorkerConfiguration(cfgFilename string, inWorkerID string, inPortNumber string) {

	logging.DebugMSG("Writing new WORKER configuration to file")
	logging.DebugMSG("Values:")
	logging.DebugMSG("WorkerID      =" + inWorkerID)
	logging.DebugMSG("PortNumber    =" + inPortNumber)

	wrkCfg := &localWorkerConfiguration{WorkerID: inWorkerID, PortNumber: inPortNumber}
	output, err := xml.MarshalIndent(wrkCfg, "  ", "    ")

	if err != nil {
		logging.DebugMSG("An error occurred during generating XML for Worker Configuration")
		logging.DebugMSG(err.Error())
	} else {
		// Wegschrijven van de XML naar een bestand inclusief backup van het huidige configuratiebestand
		writeConfiguration(cfgFilename, output, true)
	}
}

// Procedure voor het opslaan van een HUB configuratie
// Hierbij worden de waarden die moeten worden opgeslagen, samen met het
// volledige pad als parameters meegegeven
func writeHubConfiguration(cfgFilename string, inHubID string, inPortNumber string, inESBHost string, inESBUser string, inESBPassword string) {

	logging.DebugMSG("Writing new HUB configuration to file")
	logging.DebugMSG("Values:")
	logging.DebugMSG("HubID         =" + inHubID)
	logging.DebugMSG("PortNumber    =" + inPortNumber)
	logging.DebugMSG("ESBHost       =" + inESBHost)
	logging.DebugMSG("ESBUser       =" + inESBUser)
	logging.DebugMSG("ESBPassword   =" + inESBPassword)

	hubCfg := &localHubConfiguration{HubID: inHubID, PortNumber: inPortNumber, ESBHost: inESBHost, ESBUser: inESBUser, ESBPassword: inESBPassword}
	output, err := xml.MarshalIndent(hubCfg, "  ", "    ")
	if err != nil {
		logging.DebugMSG("An error occurred during generating XML for HUB Configuration")
		logging.DebugMSG(err.Error())
	} else {
		// Wegschrijven van de XML naar een bestand inclusief backup van het huidige configuratiebestand
		writeConfiguration(cfgFilename, output, true)
	}

}

// Procedure voor het inlezen van de configuratie van de HUB
// De specificatie van de HUB configuratie is vastgelegd in de
// xml-structuur localWorkerConfiguration
func ReadWorkerConfiguration(cfgFileWrk string) (string, string) {

	// Declareren en initieren van de configuratieparameters
	outWorkerID := ""
	outPortNumber := ""

	confXML := readConfiguration(cfgFileWrk)
	if confXML != nil {
		var wrkConf localWorkerConfiguration
		xml.Unmarshal(confXML, &wrkConf)

		outWorkerID = wrkConf.WorkerID
		outPortNumber = wrkConf.PortNumber
	}

	return outWorkerID, outPortNumber

}

// Procedure voor het inlezen van de configuratie van de HUB
// De specificatie van de HUB configuratie is vastgelegd in de
// xml-structuur localHubConfiguration
func readHubConfiguration(cfgFileHub string) (string, string, string, string, string) {

	// Declareren en initieren van de configuratieparameters
	outHubID := ""
	outPortNumber := ""
	outESBHost := ""
	outESBUser := ""
	outESBPassword := ""

	confXML := readConfiguration(cfgFileHub)
	if confXML != nil {
		var hubConf localHubConfiguration
		xml.Unmarshal(confXML, &hubConf)

		outHubID = hubConf.HubID
		outPortNumber = hubConf.PortNumber
		outESBHost = hubConf.ESBHost
		outESBUser = hubConf.ESBUser
		outESBPassword = hubConf.ESBPassword
	}

	return outHubID, outPortNumber, outESBHost, outESBUser, outESBPassword
}

// Procedure om een kopie te maken van de huidige Configuratie-file
// Door middel van dit mechanisme kan in het geval dat een nieuwe
// configuratie niet werkt, de oude configuratiefile hersteld wordt
// Aan het originele configuratiebestand wordt .bak toegevoegd.
func backupConfiguration(cfgFile string) {

	tmp := []string{cfgFile, "bak"}
	backupFilename := strings.Join(tmp, ".")

	// Inlezen van het huidige configuratie bestand
	contents := readConfiguration(cfgFile)

	// Controleren of het veilig te stellen bestand wel bestaat.
	// Anders kan er geen backup worden gemaakt
	if utils.FileExists(cfgFile) {
		logging.DebugMSG("Backup current configuration file to '" + backupFilename + "'")
		file, err := os.Create(backupFilename)
		defer file.Close()
		if err != nil {
			logging.DebugMSG("An error occured during the creation of a backup of the current configuration file")
		} else {
			file.WriteString(string(contents))
			logging.DebugMSG("Backup was succesful")
		}

	} else {
		logging.DebugMSG("Configuration file does not exist. No backup is made")
	}
}

// Procedure om de Configuratie  weg te schrijven naar de opgegeven
// Configuratie bestand. Het maakt niet uit welke XML hier aan ten
// grondslag ligt.
func writeConfiguration(cfgFile string, contents []byte, backupFile bool) {

	if backupFile == true {
		backupConfiguration(cfgFile)
	}

	logging.DebugMSG("Writing configuration data to '" + cfgFile + "'")
	//file, err := os.Create(cfgFile)
	//defer file.Close()
	//  if err != nil {
	//      logging.DebugMSG("An error occured during writing the the configuration file '" + cfgFile + "'")
	//      logging.DebugMSG(err.Error())
	//  }
	ioutil.WriteFile(cfgFile, contents, 0644)
}

// Procedure to check the existance of the Configuration file
// and read the contents of the file
// Bij de aanroep van deze procedure wordt het volledige pad naar het configuratiebestand
// meegegeven met behulp van de variabele cfgFile.
// Wanneer er geen bestand gevonden is of wanneer het bestand leeg is, wordt er een
// lege array of bytes geretourneerd
func readConfiguration(cfgFile string) []byte {

	var retValue []byte

	// Controleren of het configuratiebestand bestaat
	logging.DebugMSG("Check if file '" + cfgFile + "' exists")
	cfgFileExists := utils.FileExists(cfgFile)

	if cfgFileExists == true {
		logging.DebugMSG("Configuration file '" + cfgFile + "' exists")
		// Inlezen van de inhoud van het bestand
		contents, err := ioutil.ReadFile(cfgFile)
		if err != nil {
			// Een fout opgetreden bij het inlezen van het Configuratiebestand
			// dit wordt opgelost door een lege array te retourneren
			logging.DebugMSG("An error occured during reading the configuration file")
			logging.DebugMSG(err.Error())
		} else {
			retValue = contents
		}
	} else {
		logging.DebugMSG("Configuration file '" + cfgFile + "' does not exists")
	}

	return (retValue)
}
