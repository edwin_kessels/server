package settings

import "time"
import "constanten"

// Boolean die aangeeft of de Cache van de Scheduled Jobs opnieuw moet worden opgebouwd
// In verband met import cycle not allowed is deze parameter in deze package opgenomen
var RebuildScheduledJobs = true

// Hidden tag voor HTML-forms
// Tijdens de ontwikkeling van de software is het handig om de hidden fields te
// zien. In de productie-versie is dit niet meer de bedoeling
var HtmlHiddenField = "hidden"

// Magic Date
// Dit houdt in dat voor datums van vandaag, het datum-deel wordt weggelaten en voor datums
// van gisteren wordt 'Yesterday' neergezet
var MagicDates bool = true

//var MagicDates bool = false

// Lokatie waar de JobFiles van Jobs worden opgeslagen op de CentralInstance  //TODO
var JobDirectoryCentralInstance = "/home/ubuntu/workspace/jobs"

// Locatie waar de exportbestand naar toe worden geschreven
var ExportDirectory = "/home/ubuntu/workspace/server/export"

// Variabelen waarmee kan worden ingesteld of de communicatie tussen de HUB en Worker moet worden
// beveiligd met interne credentials.
var ProtectURLWithCredentials = true

// ShowContentMessage
// Variabele waarmee kan worden aangegeven dat wanneer een bericht wordt verstuurd wordt,
// de inhoud van het bericht wordt getoond
var ShowContentMessageSend = true

// Definities van Queues voor de Enterprise Service Bus
// De queues worden bekeken vanuit de CentralInstance.
// Queue met de prefix in_ zijn queues die de CentralInstance uitleest
// Queue met de prefix out_ zijn queues die de CentralInstance vult
// ===============================================================================

// Queue waarin de HUB de JobStart-berichen plaatst om de CentralInstance te
// informeren dat de aangevraagde Job is gestart
var ESB_IN_Queue_JobStart = "in_JobStart"
var ESB_OUT_Queue_JobStart = "out_JobStart"

// Queue waarin de HUB de JobCompletion-berichen plaatst om de CentralInstance te
// informeren dat de aangevraagde Job een eindstatus heeft
var ESB_IN_Queue_JobCompletion = "in_JobCompletion"
var ESB_OUT_Queue_JobCompletion = "out_JobCompletion"

// Queue waarin de CentralInstance een JobRequest-bericht plaatst
var ESB_OUT_Queue_JobRequest = "out_JobRequest"

// Queue waarin de Hub de WorkerConfiguration berichten plaatst
var ESB_IN_Queue_WorkerRegistration = "in_WorkerRegistration"

// Queue waarin verzoeken worden geplaatst om een nieuwe Worker aan te maken
// Dit zijn Workers die vanaf de commandline worden aangemaakt
var ESB_IN_Queue_NewWorkerRegistration = "NewWorkerRegistration"

// Queue voor het starten van een Worker
var ESB_OUT_Queue_WorkerStartup = "out_WorkerStartUp"

// Queue voor het doorgeven van de Configuration van een Worker
var ESB_OUT_Queue_WorkerConfiguration = "WorkerConfiguration"

// Queue voor het doorgeven van een (nieuwe) status van een Worker
var ESB_IN_Queue_WorkerStatus = "in_WorkerStatus"

// Queue waarin Workers de HeartBeat versturen naar de HUB
// om aan te geven dat ze nog actief zijn
var ESB_INNER_QUEUE_HeartBeat = "WorkerHeartBeat"

// Queue waarin berichten worden verstuurd met betrekking tot wijzigingen
// in de status van een Job. Deze berichten worden vanuit de Worker
// naar de CentralInstance gestuurd
var ESB_QUEUE_JobStatus = "JobStatus"

// Instellingen voor de communicatie tussen de verschillende componenten van de
// infrastructuur
//var Pass1  = "dhg38ehj393jdk3DHF49ds$%#^@"  // Wordt gebruikt als gebruikersnaam in de https-communicatie
//var Pass2  = "273fjf9fjk4JJ9j((dddjdjd#32"  // Wordt gebruikt als wachtwoord in de https-communicatie
var Pass1 = "user1"    // Wordt gebruikt als gebruikersnaam in de https-communicatie
var Pass2 = "welkom01" // Wordt gebruikt als wachtwoord in de https-communicatie

var DevelopmentLogging = true
var DevelopmentMode = true

// **************************************************
// * Instellingen voor de connectie met de Repository
// **************************************************
var RepositoryType = "postgres"
var RepositoryConnectString = "user=sched20 password=Welkom01 dbname=sched20 sslmode=disable"

// **************************************************
// * Instellingen voor verplichte en standaard
// * componenten
// **************************************************
var App001 = "Sched20" // Application voor Systeem Objecten
var App002 = "Users"   // Application voor niet-Systeem Objecten
var Part001 = "System"
var Part002 = "Public" // Partition voor gebruikers

// ******************************************************************************
// * Instellingen voor de agent wrkreg (WorkerRegistration)                     *
// ******************************************************************************
var WrkcfgIntervalUpdateTimestampInSeconds time.Duration = 10 * time.Second
var WrkstatIntervalUpdateTimestampInSeconds time.Duration = 10 * time.Second
var WrkstrtIntervalUpdateTimestampInSeconds time.Duration = 10 * time.Second
var WrkregIntervalUpdateTimestampInSeconds time.Duration = 10 * time.Second
var WrkmetricIntervalUpdateTimestampInSeconds time.Duration = 10 * time.Second
var WrkhrtbeatIntervalUpdateTimestampInSeconds time.Duration = 10 * time.Second

// ******************************************************************************
// * Instellingen voor de ESB Connection                                        *
// ******************************************************************************
//var ESB_00245323 = "jbukjugz"                         // Username
//var ESB_00232323 = "9_FCD3K_7J0mCa-6mOGdInrTQTVzfnAD" // Password
//var ESB_00323443 = "owl.rmq.cloudamqp.com"            // Hostname
//var ESB_00554534 = "jbukjugz"                         // Virtual Host
var ESB_00245323 = "testuser"                         // Username
var ESB_00232323 = "Welkom01" // Password
var ESB_00323443 = "localhost"            // Hostname
var ESB_00554534 = "testenv"                         // Virtual Host

var CollectWorkerMetrics = constanten.Yes

// ******************************************************************************
// * Instellingen om te bepalen of een Worker evt Overloaded is
// ******************************************************************************
var OverloadPercLoad1 = 101 // Ligt boven de 100 procent dus komt niet voor
var OverloadPercLoad5 = 80
var OverloadPercLoad15 = 80
