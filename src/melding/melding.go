package melding

import "naming"

var prefixPRT = "PRT-"
var prefixUSR = "USR-"
var prefixOBJ = "OBJ-"
var prefixCRE = "CRE-"
var prefixAPP = "APP-"
var prefixJSC = "JSC-"

var Melding_wrk_001 = "Worker has been created"
var Melding_wrk_002 = "Worker (hostname/portnumber) already exists in this environment"
var Melding_wrk_003 = "Name already assigned to another worker."
var Melding_wrk_004 = "Status of Worker %1 has been set to %2"
var Melding_wrk_005 = "Worker has been deleted"
var Melding_wrk_006 = "Worker Name is mandatory and cannot be empty"


var Melding_sql_001 = "A problem occured during the execution of SQL: %1"
var Melding_sql_002 = "A problem occured during the parse of SQL: %1"
var Melding_sql_003 = "SQL Command was executed succesfully"

var Melding_esb_001 = "Cannot declare ESB queue"
var Melding_esb_002 = "Failed to set ESB QoS"
var Melding_esb_003 = "Failed to register an ESB consumer"

// Meldingen die betrekking hebben op Users
var Melding_usr_001 = prefixUSR + "001: Object Name is mandatory and cannot be empty"
var Melding_usr_002 = prefixUSR + "002: The value for Last Name is too long. Maximum size is $1"
var Melding_usr_003 = prefixUSR + "003: Last Name contains invalid characters"
var Melding_usr_004 = prefixUSR + "004: Email Address is invalid"
var Melding_usr_011 = prefixUSR + "011: First name is mandatory and cannot be empty"
var Melding_usr_012 = prefixUSR + "012: The value for First Name is too long. Maximum size is $1"
var Melding_usr_013 = prefixUSR + "013: First Name contains invalid characters"
var Melding_usr_014 = prefixUSR + "014: Last name is mandatory and cannot be empty"
var Melding_usr_015 = prefixUSR + "015: User has been created succesfully"
var Melding_usr_016 = prefixUSR + "016: User already exists"
var Melding_usr_017 = prefixUSR + "017: All checks succesfull on User Object"
var Melding_usr_018 = prefixUSR + "018: User Account has been deleted"
var Melding_usr_019 = prefixUSR + "019: Email Address contains invalid characters"

var Melding_obj_001 = prefixOBJ + "001: Name is mandatory"
var Melding_obj_002 = prefixOBJ + "002: $OBJECTTYPE Name constains invalid characters"
var Melding_obj_003 = prefixOBJ + "003: Reserved Word used as Object Name"
var Melding_obj_004 = prefixOBJ + "004: Object name cannot start with a number"
var Melding_obj_005 = prefixOBJ + "005: Object Name is too long. Maximum size is $1"
var Melding_obj_006 = prefixOBJ + "006: Description is too long. Maximum size is $1"
var Melding_obj_007 = prefixOBJ + "007: $1 is mandatory and cannot be empty"

// Meldingen die betrekking hebben op Partitions
var Melding_prt_001 = prefixPRT + "001: " + naming.PartitionObjNameEV + " $1 already exists"
var Melding_prt_002 = prefixPRT + "002: " + naming.PartitionObjNameEV + " $1 was created succesfully"
var Melding_prt_003 = prefixPRT + "003: " + naming.PartitionObjNameEV + " $1 contains active objects and cannot be deleted"
var Melding_prt_004 = prefixPRT + "004: " + naming.PartitionObjNameEV + " $1 has been deleted"
var Melding_prt_005 = prefixPRT + "005: " + naming.PartitionObjNameEV + " $1 cannot be deleted as it is an internal " + naming.PartitionObjNameEV
var Melding_prt_006 = prefixPRT + "006: " + naming.PartitionObjNameEV + " not found"
var Melding_prt_007 = prefixPRT + "007: " + "Cannot create objects in " + naming.PartitionObjNameEV + " $1"
var Melding_prt_008 = prefixPRT + "007: " + naming.PartitionObjNameEV + " $1 is set to be Internal. Cannot create user objects in this " + naming.PartitionObjNameEV
var Melding_prt_009 = prefixPRT + "009: " + naming.PartitionObjNameEV + ": UserID was not set"
var Melding_prt_010 = prefixPRT + "010: Privileges can only grant to a " + naming.PartitionObjNameEV + " which was created and saved"
var Melding_prt_011 = prefixPRT + "011: " + naming.PartitionObjNameEV + " cannot be set to Private as the " + naming.PartitionObjNameEV + " still contains objects"

// Meldingen die betrekking hebben op Credentials
var Melding_cre_001 = prefixCRE + "001: An " + naming.ApplicationObjNameEV + " must be specified for the " + naming.CredentialObjNameEV
var Melding_cre_002 = prefixCRE + "002: An " + naming.PartitionObjNameEV + " must be specified for the " + naming.CredentialObjNameEV
var Melding_cre_003 = prefixCRE + "003: The name for the " + naming.CredentialObjNameEV + " ($2) is too long. Maximum size is $1"

// Meldingen die betrekking hebben op Applications
var Melding_app_001 = prefixAPP + "001: " + naming.ApplicationObjNameEV + " $1 was saved succesfully"
var Melding_app_002 = prefixAPP + "002: " + naming.ApplicationObjNameEV + " not found"
var Melding_app_003 = prefixAPP + "003: " + naming.ApplicationObjNameEV + " $1 is set to be Internal. Cannot create user objects in this " + naming.ApplicationObjNameEV
var Melding_app_004 = prefixAPP + "004: " + naming.ApplicationObjNameEV + " $1 already exists"
var Melding_app_005 = prefixAPP + "005: " + naming.ApplicationObjNameEV + " $1 has been deleted"
var Melding_app_006 = prefixAPP + "006: " + naming.ApplicationObjNameEV + " $1 cannot be deleted as it is an internal " + naming.ApplicationObjNameEV


// Meldingen die betrekking hebben op JobSources
var Melding_jsc_001 = prefixJSC + "001: " + "No JobSourceType specified for " + naming.JobSourceObjNameEV