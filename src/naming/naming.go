// naming.go
package naming

// Namen voor Objecten
var PartitionObjNameEV = "Partition"
var PartitionObjNameMV = "Partitions"
var UserObjNameEV = "User"
var UserObjNameMV = "Users"
var CredentialObjNameEV = "Credential"
var CredentialObjNameMV = "Credentials"
var ApplicationObjNameEV = "Application"
var ApplicationObjNameMV = "Applications"
var JobSourceObjNameEV = "JobSource"
var JobSourceObjNameMV = "JobSources"
var JobObjNameEV = "Job"
var JobObjNameMV = "Jobs"
var WorkerObjNameEV = "Worker"
var WorkerObjNameMV = "Workers"
var AuditObjNameEV = "Audit"

var SystemPartitionName = "System"
var SystemPartitionDescription = PartitionObjNameEV + " for System Objects"
var GlobalPartitionName = "Global"
var GlobalPartitionDescription = PartitionObjNameEV + " for user objects"

var SystemApplicationName = "BuildIn"
var SystemApplicationDescription = ApplicationObjNameEV + " for System Objects"
