package startup

import "logging"
import "settings"
import "centralinstance/application"
import "centralinstance/partition"
import "centralinstance/worker"
import "repository"
import "constanten"
import "database/sql"
import _ "github.com/lib/pq"
import "statussen/workerstatus" 




// *********************************************************************************************
// * Controleren of de verplichte Applicaties aanwezig zijn
// * Wanneer dit niet het geval is, worden deze aangemaakt
func checkMandatoryApplications() {
    
    app1 := application.Application{}
    app1.GetApplicationByFQName(settings.App001)
    if app1.Id == 0 {
    	logging.InfoMSG("Application " + settings.App001 + " does not exist")
    	app1.SetName(settings.App001)
    	app1.SetDescription("Application for build-in functionality")
    	app1.Internal = constanten.Yes
    	exitCode, exitMSG := app1.Commit() 
    	if exitCode != 0 {
    	    logging.ErrorMSG("Error during the creation of Application '" + settings.App001 + "'")
    	    logging.ErrorMSG(exitMSG)
    	}
    }
    
    app2 := application.Application{}
    app2.GetApplicationByFQName(settings.App002)
    if app2.Id == 0 {
    	logging.InfoMSG("Application " + settings.App002 + " does not exist")
    	app2.SetName(settings.App002)
    	app2.SetDescription("Application for User objects")
    	app2.Internal = constanten.No
    	exitCode, exitMSG := app2.Commit() 
    	if exitCode != 0 {
    	    logging.ErrorMSG("Error during the creation of Application '" + settings.App002 + "'")
    	    logging.ErrorMSG(exitMSG)
    	}
    }
    
}


// *********************************************************************************************
// * Controleren of de verplichte Partitions aanwezig zijn
// * Wanneer dit niet het geval is, worden deze aangemaakt
func checkMandatoryPartitions() {
    
    part1 := partition.Partition{}
    part1.GetPartitionByName(settings.Part001)
    if part1.Id == 0 {
        logging.InfoMSG("Partition " + settings.Part001 + " does not exist")
    	part1.SetName(settings.Part001)
    	part1.SetDescription("Partition for System Objects")
    	part1.SetAsInternal(true)
    	exitCode, exitMSG := part1.Commit() 
    	if exitCode != 0 {
    	    logging.ErrorMSG("Error during the creation of Application '" + settings.Part001 + "'")
    	    logging.ErrorMSG(exitMSG)
    	}
    }
    
    part2 := partition.Partition{}
    part2.GetPartitionByName(settings.Part002)
    if part2.Id == 0 {
        logging.InfoMSG("Partition " + settings.Part002 + " does not exist")
    	part2.SetName(settings.Part002)
    	part2.SetDescription("Partition for User Objects")
    	part2.SetAsInternal(false)
    	exitCode, exitMSG := part2.Commit() 
    	if exitCode != 0 {
    	    logging.ErrorMSG("Error during the creation of Application '" + settings.Part002 + "'")
    	    logging.ErrorMSG(exitMSG)
    	}
    }
    
}



func initCentralInstanceAgents() {

	// Initialiseren van de tabel waarin informatie over de agents wordt opgeslagen
	stmt, err := repository.DBConnection.Prepare(repository.SqlCmd_agt_001)
	if err != nil {
		logging.ErrorMSG("Cannot prepare SQL Command:")
		logging.ErrorMSG(err.Error())
	}
	_, err = stmt.Exec(constanten.No)
	if err != nil {
		logging.ErrorMSG("Cannot execute SQL Command:")
		logging.ErrorMSG(err.Error())
	}

}


func shutdownAllWorkers() {
    stmt, err := repository.DBConnection.Prepare(repository.SqlCmd_wrk_016)
	if err != nil {
		logging.ErrorMSG("Cannot prepare SQL Command:")
		logging.ErrorMSG(err.Error())
	}
	_, err = stmt.Exec(workerstatus.Down, workerstatus.Down)
	if err != nil {
		logging.ErrorMSG("Cannot execute SQL Command:")
		logging.ErrorMSG(err.Error())
	}
}





func ExecuteChecks() {
 
 		// Initieren van de Database connectie	
		var err error
		repository.DBConnection, err = sql.Open(settings.RepositoryType, settings.RepositoryConnectString)
		if err != nil {
			logging.ErrorMSG("Cannot connect to the Repository:" + err.Error())
		}
    
    logging.DevMSG("Executing Startup procedures and checks")
    
    
    logging.DevMSG("Initializing CentralInstance Agents")
    initCentralInstanceAgents()
    
    logging.DevMSG("Checking Application objects")
    checkMandatoryApplications()
    
    logging.DevMSG("Checking Partition objects")
    checkMandatoryPartitions()
    
    logging.DevMSG("Shutdown all Workers")
    shutdownAllWorkers() 
    
    logging.DevMSG("Start Workers with AutoStart option") 
    worker.StartWorkersWithAutoStartSetting()
     
}