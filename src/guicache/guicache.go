package guicache

// Package voor het communiceren met de GUI. In de package worden ondermeer XML files
// voorbereid (met bijvoorbeeld alle JobSources) zodat deze snel door de GUI
// verwerkt kunnen worden

import "repository"
import "berichten"
import "logging"
import "constanten"
import "encoding/xml"
import "github.com/bradhe/stopwatch"
import "fmt"

var JobSourceCache string = "" ; 


// Functie om de cache op te bouwen voor alle JobSources (current version)
func CreateCacheJobSources() {
    
    start := stopwatch.Start()
    
    sqlCmd := "select jobsrc_id, application_name, partition_name, jobsrc_name, jobsrc_description, jobsrc_type, jobsrc_version from centralinstance.v_jobsources" 
    
    // Initialiseren van de variabelen
    jobsourceid := ""  
    appname := "" 
    partname := ""
    jobname := "" 
    jobdesc := "" 
    jobtype := 0 
    jobversion := ""
    
    var xmlSource berichten.GuiRequestJobSource 
    
    logging.DebugMSG("Generating cache for JobSources")
    
    rows, _ := repository.DBConnection.Query(sqlCmd)
   	for rows.Next() {
  	  err := rows.Scan(&jobsourceid, &appname, &partname, &jobname, &jobdesc, &jobtype, &jobversion)
	  if err != nil {
	 	logging.DebugMSG("Uitvoeren SQL: " + err.Error())
	  } else {
	    xmlSource.JobSource = append(xmlSource.JobSource, berichten.GuiRequestJobSourceDetail{jobsourceid, partname,appname, jobname,jobdesc, constanten.GetJobTypeDescription(jobtype)})  
	  }	
    }
    
    output, _ := xml.MarshalIndent(xmlSource, "", " ")
    JobSourceCache = string(output) 
    logging.DebugMSG("Cache for JobSources has been generated")
    //logging.DebugMSG("XML=" + JobSourceCache)
    
    watch := stopwatch.Stop(start)
    tmp := fmt.Sprint(watch.Milliseconds())
    logging.DebugMSG("Time elapsed: " + tmp + " Milliseconds" )
    
    
    
} 