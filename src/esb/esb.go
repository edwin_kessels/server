package esb

import "fmt"
import "logging"
import "berichten"
import "github.com/streadway/amqp"
import "settings"
import "utils"


type ESBMessage struct {
	User					string
	Password      			string
	HostName				string
	VirtualHost				string
	Queue					string
	Body					string 
}

//TODO: Central Instance Connectstring is hardcoded opgenomen
// amqp://[username]:[password]@[host]/[vhost]
//var ConnectString = "amqp://jbukjugz:9_FCD3K_7J0mCa-6mOGdInrTQTVzfnAD@owl.rmq.cloudamqp.com/jbukjugz"
var ConnectString = "amqp://" + settings.ESB_00245323 +  ":" + settings.ESB_00232323 + "@" + settings.ESB_00323443 + "/" + settings.ESB_00554534

// Queue voor het plaatsen van een connectie-test
var testQueue = "TestConnection"


func reverse(s string) string {
    r := []rune(s)
//    for i, j := 0, len(r)-1; i < len(r)/2; i, j = i+1, j-1 {
//        r[i], r[j] = r[j], r[i]
//    }
    return string(r)
}


func encryptMessage(msg string) []byte {
	
	tmp := reverse(msg)
	return []byte(tmp)
}



func (m *ESBMessage) getConnectString() string {
	// Controleren of er een afwijkende Useraccount is gespecifeerd
	tmpUser := m.User
	tmpPassword := m.Password
	tmpHostName := m.HostName
	tmpVirtualHost := m.VirtualHost
	
	if utils.Trim(m.User) == "" {
		tmpUser  = settings.ESB_00245323
	}
	// Controleren of er een afwijkende Password is gespecifeerd
	if utils.Trim(m.Password) == "" {
		tmpPassword  = settings.ESB_00232323
	}
	// Controleren of er een afwijkende Hostname is gespecifeerd
	if utils.Trim(m.HostName) == "" {
		tmpHostName  = settings.ESB_00323443
	}
	// Controleren of er een afwijkende Virtuele Host is gespecifeerd
	if utils.Trim(m.VirtualHost) == "" {
		tmpVirtualHost  = settings.ESB_00554534
	}
	
	// Samenstellen van de Connectstring
	cs := "amqp://" + tmpUser + ":" + tmpPassword + "@" + tmpHostName + "/" + tmpVirtualHost
	return cs
}


func (m *ESBMessage) SendMessageWithoutWait() {
	
	conn, err := amqp.Dial(m.getConnectString())
	defer conn.Close()
	if err != nil {
		fmt.Println("Cannot connect to ESB:" + err.Error())
	}

	// Afhankelijk van de parameter showContentMessage moet de inhoud van de message getoond worden
	ShowContentESBMessage(m.Body, m.Queue)

	ch, err := conn.Channel()
	if err != nil {
		fmt.Println("Cannot allocate Channel")
	}
	defer ch.Close()


		q, err := ch.QueueDeclare(m.Queue, false, false, false, false, nil)
	//err = ch.Publish("", q.Name, false, false, amqp.Publishing{ContentType: "text/plain", Body: []byte(msg)})
	err = ch.Publish("", q.Name, false, false, amqp.Publishing{ContentType: "text/plain", Body: encryptMessage(m.Body)})
	
}



// Versturen van een bericht via de Enterprise Service Bus waarbij er niet wordt gewacht op een 
// antwoord (asynchroon).  
func SendMessageESBWithoutWait(msg string, queue string) {
	conn, err := amqp.Dial(ConnectString)
	defer conn.Close()
	if err != nil {
		fmt.Println("Cannot connect to ESB:" + err.Error())
	}

	// Afhankelijk van de parameter showContentMessage moet de inhoud van de message getoond worden
	ShowContentESBMessage(msg, queue)

	ch, err := conn.Channel()
	if err != nil {
		fmt.Println("Cannot allocate Channel")
	}
	defer ch.Close()

	deleteQueueWhenNotUsed := false  

	q, err := ch.QueueDeclare(queue, false, deleteQueueWhenNotUsed, false, false, nil)
	//err = ch.Publish("", q.Name, false, false, amqp.Publishing{ContentType: "text/plain", Body: []byte(msg)})
	err = ch.Publish("", q.Name, false, false, amqp.Publishing{ContentType: "text/plain", Body: encryptMessage(msg)})
}


// ***********************************************************************
// * ShowContentESBMessage
// * Toon de inhoud van een Message
// ***********************************************************************
func ShowContentESBMessage(inMessage string, inQueue string) {
	if settings.ShowContentMessageSend == true {
		logging.DebugMSG("Sending ESB Message using queue " + inQueue)
		logging.DebugMSG("-- Start outgoing ESB message --")
		logging.DebugMSG(inMessage)
		logging.DebugMSG("-- End outgoing ESB message --")
	}
}



// ***********************************************************************
// * DeleteQueue
// * Methode om een queue in RabbitMQ te verwijderen
// ***********************************************************************
func DeleteQueue(queueName string) {
	
	conn, err := amqp.Dial(ConnectString)
	defer conn.Close()
	if err != nil {
		fmt.Println("Cannot connect to ESB:" + err.Error())
	}

	ch, err := conn.Channel()
	if err != nil {
		fmt.Println("Cannot allocate Channel")
	}
	defer ch.Close()
	
	_, err = ch. QueueDelete(queueName, false, false, true)
	if err != nil {
		logging.ErrorMSG("Error during deletion of queue " + queueName)
	}
	
}


// ***********************************************************************
// * CreateQueue
// * Methode om een queue in RabbitMQ aan te maken
// ***********************************************************************
func CreateQueueObsolete(queueName string) {
	
	conn, err := amqp.Dial(ConnectString)
	defer conn.Close()
	if err != nil {
		fmt.Println("Cannot connect to ESB:" + err.Error())
	}

	ch, err := conn.Channel()
	if err != nil {
		fmt.Println("Cannot allocate Channel")
	}
	defer ch.Close()
	
	_, err = ch.  QueueDeclare(queueName, true, false, false, false, nil)
	if err != nil {
		logging.ErrorMSG("Error during creation of queue " + queueName)
	}
	
}


// Procedure waarmee een regel kan worden weggeschreven naar de logging van de Central Instance
// Deze procedure wordt nooit rechtstreeks aangeroepen maar altijd vanuit sendMessageESB wanneer
// de parameter logging gevuld is. Wanneer de logging arriveert in de Central Instance wordt daar
// een timestamp geplakt aan de message zodat het eenduidig is wanneer de logging is aangemaakt en
// deze dus niet afhankelijk is van de tijd zoals ingesteld op de Worker of HUB.
func sendLoggingToCentralInstance(msg string) {
	conn, err := amqp.Dial(ConnectString)
	defer conn.Close()
	if err != nil {
		fmt.Println("Cannot connect to ESB:" + err.Error())
	}

	ch, err := conn.Channel()
	if err != nil {
		fmt.Println("Cannot allocate Channel")
	}
	defer ch.Close()

	q, err := ch.QueueDeclare("logging", false, false, false, false, nil)
	err = ch.Publish("", q.Name, false, false, amqp.Publishing{ContentType: "text/plain", Body: []byte(msg)})
}

// Procedure om de connectie met de Enterprise Service Bus te testen. Wanneer er geen connectie
// kan worden gemaakt, wordt de HUB niet verder opgestart omdat zinloos is. Voor de Worker is er
// geen Enterprise Service Bus vereist. De controle bestaat het versturen van een (willekeurige)
// string in de Queue TestConnect. Er wordt niet gecontroleerd of de Central Instance daadwerkelijk
// reageert op dit bericht. Het kunnen versturen is in principe voldoende
func CheckConnectionESB() {

	// Testen van connectie met de Central Instance
	logging.DebugMSG("Checking the connection to the Central Instance")
	conn, err := amqp.Dial(ConnectString)
	defer conn.Close()
	if err != nil {
		fmt.Println("Cannot connect to ESB:" + err.Error())
	}

	ch, err := conn.Channel()
	if err != nil {
		fmt.Println("Cannot allocate Channel")
	}
	defer ch.Close()

	q, err := ch.QueueDeclare(testQueue, false, false, false, false, nil)
	err = ch.Publish("", q.Name, false, false, amqp.Publishing{ContentType: "text/plain", Body: []byte(berichten.PingNaarCentralInstance)})

}



// ************************************************************
// * CreateFanOutExchange
// * Methode voor het aanmaken van een nieuwe Exchange met 
// * FanOut functionaliteit
// ************************************************************
func CreateFanOutExchange(exchangeName string) {
	
	conn, err := amqp.Dial(ConnectString)
	defer conn.Close()
	if err != nil {
		fmt.Println("Cannot connect to ESB:" + err.Error())
	}

	ch, err := conn.Channel()
	if err != nil {
		fmt.Println("Cannot allocate Channel")
	}
	defer ch.Close()
	
	err = ch.ExchangeDeclare(
	exchangeName,   // name
	"fanout", // type
	true,     // durable
	false,    // auto-deleted
	false,    // internal
	false,    // no-wait
	nil,      // arguments
	)
	
	if err != nil {
	  logging.ErrorMSG(err.Error() + ": Failed to declare an exchange")
	}
	
}


// ************************************************************
// * AddQueueToExchange
// * Toevoegen aan een Queue aan een Exchange
// ************************************************************
func AddQueueToExchange(queueName string, exchangeName string) {	
	conn, err := amqp.Dial(ConnectString)
	defer conn.Close()
	if err != nil {
		fmt.Println("Cannot connect to ESB:" + err.Error())
	}

	ch, err := conn.Channel()
	if err != nil {
		fmt.Println("Cannot allocate Channel")
	}
	defer ch.Close()
	
	err = ch.QueueBind(
	queueName, // queue name
	"",     // routing key
	exchangeName, // exchange
	false,
	nil	)
	
}